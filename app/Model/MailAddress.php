<?php
class MailAddress extends AppModel {
 
    public $name = 'MailAddress';
    public $belongsTo = array(
        'User' => array(
            'className'    => 'User',
            'foreignKey'   => 'user_id'
        )
    );
    public $validate = Array(
        'email' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => Array(
                'rule' => array('email', true),
                'message' => 'メールアドレスを正しく入力してください。'
            ),
            array(
                'rule' => 'isUnique',
                'message' => 'このメールアドレスは既に登録されています。'
            )
        )
    );

    /***********************************
     * profileSave
     ************************************/
    // 追加
    function addEmailSave( $user ){
        /* プロフィールの編集
         *******************/
        $wdata["MailAddress"]["user_id"]     = $user["User"]["id"];
        $wdata["MailAddress"]["email"]       = $user["MailAddress"]["add"]["email"];
        /* DB書きこみ
         *******************/
        return $this->save( $wdata );
    }
    // 削除
    function deleteEmailSave( $mail_id ){
        /* DB Record 削除
         *******************/
        return $this->delete( $mail_id );
    }
}
?>