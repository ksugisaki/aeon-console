<?php
App::uses('AppModel', 'Model');
/**
 * ChristmasFeature Model
 *
 * @property User $User
 */
class ChristmasFeature extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
		'UploadPack.Upload' => array(
			'syouhin1_image1' => array(
				'path' => ':webroot/upload/:model/:id/syouhin1_image1.:extension'
				),
			'syouhin2_image1' => array(
				'path' => ':webroot/upload/:model/:id/syouhin2_image1.:extension'
			),
			'syouhin3_image1' => array(
				'path' => ':webroot/upload/:model/:id/syouhin3_image1.:extension'
			)
		)
	);

//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function afterFind($results, $primary = false) {
		
		$results = parent::afterFind( $results ); // 親
		foreach ($results as $key1 => $val1) {
			foreach ($val1['ChristmasFeature'] as $key2 => $val2) {
				if( $key2 =='id'){
					$id = $val2;
				}

				// 画像の最終更新日を抽出
				$images = array('syouhin1_image1','syouhin2_image1','syouhin3_image1');
				foreach( $images as $img ){
					if ( $key2 == $img .'_file_name'){
						$ext = @pathinfo( $val2, PATHINFO_EXTENSION );
						$fname = APP .'webroot/upload/christmas_features/'. $id .'/'. $img .'.'. $ext;
						if( file_exists( $fname )){
							$results[$key1]['ChristmasFeature'][$img .'_modified'] = @filemtime( $fname );
						}else{
							$results[$key1]['ChristmasFeature'][$img .'_modified'] = null;
						}
					}
				}
			}
		}

		return $results;
	}


}
