<?php
App::uses('AppModel', 'Model');
/**
 * OptionHearing Model
 *
 * @property User $User
 */
class OptionHearing extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'gyotai';

	public $actsAs = array(
		'UploadPack.Upload' => array(
			'image1' => array(
				//'default_url' => ':webroot/img/default.jpg',
				'path' => ':webroot/upload/:model/:id/image1.:extension'
				),
			'image2' => array(
				//'default_url' => ':webroot/img/default.jpg',
				'path' => ':webroot/upload/:model/:id/image2.:extension'
			),
			'image3' => array(
				//'default_url' => ':webroot/img/default.jpg',
				'path' => ':webroot/upload/:model/:id/image3.:extension'
			),
			'logo' => array(
				//'default_url' => ':webroot/img/default.jpg',
				'path' => ':webroot/upload/:model/:id/logo.:extension'
			),
			'logo2' => array(
				//'default_url' => ':webroot/img/default.jpg',
				'path' => ':webroot/upload/:model/:id/logo2.:extension'
			),
		)
	);


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	// htmlタグを取り除く
	public function afterFind($results, $primary = false) {
		
		$results = parent::afterFind( $results ); // 親
		foreach ($results as $key1 => $val1) {
			foreach ($val1['OptionHearing'] as $key2 => $val2) {
				if( $key2 =='id'){
					$id = $val2;
				}

				// シリアライズされてないのは、ここでタグ除去できる
				$unserialized = @unserialize( $val2 );
				if( $unserialized == false ){
					if( $val2 != '' && ( ! is_array( $val2 ))){
						$results[$key1]['OptionHearing'][$key2] = strip_tags($val2, '');
					}
				}

				// 画像の最終更新日を抽出
				$images = array('image1','image2','image3','logo','logo2');
				foreach( $images as $img ){
					if ( $key2 == $img .'_file_name'){
						$ext = @pathinfo( $val2, PATHINFO_EXTENSION );
						$fname = APP .'webroot/upload/option_hearings/'. $id .'/'. $img .'.'. $ext;
						if( file_exists( $fname )){
							$results[$key1]['OptionHearing'][$img .'_modified'] = @filemtime( $fname );
						}else{
							$results[$key1]['OptionHearing'][$img .'_modified'] = null;
						}
					}
				}
			}
		}

		return $results;
	}
}
