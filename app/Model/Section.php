<?php

App::uses('AppModel', 'Model');
/**
 * Section Model
 *
 * @property User $User
 */
class Section extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'user_id is required.',
				'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        'name' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')
        ),
        'name_kana' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'メールアドレスを入力して下さい。',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
        ),
        'zipcode' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', '/^[0-9]{7}$/' ),
				'allowEmpty' => true,
                'message' => '半角数字７桁で入力してください。'
            )
        ),
        'phone' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '１４文字以内の半角数字で入力してください。'
            )
        ),
        'extension' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '１４文字以内の半角数字で入力してください。'
            )
        ),
        'fax' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '１４文字以内の半角数字で入力してください。'
            )
        ),
        'cellphone' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '１４文字以内の半角数字で入力してください。'
            )
        ),
	);
    function phoneConfirm( $check ){
        list( $key, $value ) = each( $check );
        $phone = mb_convert_kana( $value, "as", "UTF-8");
        $phone = str_replace('-','', $phone );
        if( ereg("^[0-9]{0,14}$", $phone )){
            $this->data['Section'][ $key ] = $phone;
            return true;
        } else {
            return false;
        }
    }

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $actsAs = array(
		'CsvExport' => array(
			'fields' =>array('department','position','name','zipcode','address','phone','fax','cellphone','email'),
		)
	); 

}
