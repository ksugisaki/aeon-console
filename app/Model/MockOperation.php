<?php
App::uses('AppModel', 'Model');
/**
 * MockOperation Model
 *
 * @property User $User
 */
class MockOperation extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/*
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $validate = Array(
		'attend_mode' => Array(
			'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')
		)
	);

}
