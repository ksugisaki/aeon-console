<?php
class Hearing extends AppModel {
    public $name = 'Hearing';
    public $belongsTo = array(
        'User' => array(
            'className'    => 'User',
            'foreignKey'   => 'user_id'
        )
    );

	var $serializeKey = array('jyoui_tenpo','itiduke','target_danjyo','target_nenrei',
		'syouhin_kousei','kokyaku_kanri_houhou','open_hansoku','smart_phone_hansoku',
		'jinin_keikaku','jinji1','jinji2','jyuugyouin_kyouiku_before_open','jyuugyouin_kyouiku_after_open'
	);

	// htmlタグを取り除く
	public function afterFind($results, $primary = false) {
		
		$results = parent::afterFind( $results ); // 親
		
		foreach ($results as $key1 => $val1) {
			foreach ($val1['Hearing'] as $key2 => $val2) {
				// シリアライズされてないのは、ここでタグ除去できる
				$unserialized = @unserialize( $val2 );
				if( $unserialized == false ){
					if( $val2 != '' && ( ! is_array( $val2 ))){
						$results[$key1]['Hearing'][$key2] = strip_tags($val2, '');
					}
				}
			}
		}
		return $results;
	}
}
?>
