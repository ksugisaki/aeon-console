<?php
class History extends AppModel {
    public $name = 'History';
    public $belongsTo = array(
        'User' => array(
            'className'    => 'User',
            'foreignKey'   => 'user_id'
        )
    );
}
?>