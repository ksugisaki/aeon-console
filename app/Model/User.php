<?php
class User extends AppModel {
	var $serializeKey = array('read_permission','write_permission');
	public $actsAs = array(
		'CsvExport' => array(
			'fields' =>array('loginid','block',
				'company','zipcode','address','company_phone','fax',
				'email','department','name_kana','name','phone','extension'),
		)
	);
    public $name = 'User';
    public $hasMany = array(
        'MailAddress' => array(
            'className'  => 'MailAddress',
            'conditions' => array('MailAddress.deleted' => '0'),
            'order'      => 'MailAddress.created',
            'dependent'  => true,
        ),
        'History' => array(
            'className'  => 'History',
            'conditions' => array('History.deleted' => '0'),
            'order'      => 'History.created',
            'dependent'  => true,
        ),
        'Section' => array(
            'className'  => 'Section',
            'order'      => 'Section.id',
            'dependent'  => true,
        ),
        'Hearing' => array(
            'className'  => 'Hearing',
            'dependent'  => true,
        ),
        'OptionHearing' => array(
            'className'  => 'OptionHearing',
            'dependent'  => true,
        ),
        'Signboard' => array(
            'className'  => 'Signboard',
            'dependent'  => true,
        ),
		'SyokujyuItem' => array(
			'className'	 => 'SyokujyuItem',
			'dependent'	 => true,
		),
		'PreCarPort' => array(
			'className'	 => 'PreCarPort',
			'dependent'	 => true,
		),
		'CarPort' => array(
			'className'	 => 'CarPort',
			'dependent'	 => true,
		),
		'Bicycle' => array(
			'className'	 => 'Bicycle',
			'dependent'	 => true,
		),
		'PermitCard' => array(
			'className'	 => 'PermitCard',
			'dependent'	 => true,
		),
		'Cashbox' => array(
			'className'	 => 'Cashbox',
			'dependent'	 => true,
		),
//		'Bill' => array(
//			'className'	 => 'Bill',
//			'dependent'	 => true,
//		),
		'MockOperation' => array(
			'className'	 => 'MockOperation',
			'dependent'	 => true,
		),
		'Locker' => array(
			'className'	 => 'Locker',
			'dependent'	 => true,
		),
//		'EmergencyContact' => array(
//			'className'	 => 'EmergencyContact',
//			'dependent'	 => true,
//		),
//		'Cashier' => array(
//			'className'	 => 'Cashier',
//			'dependent'	 => true,
//		),
		'FireDrill' => array(
			'className'	 => 'FireDrill',
			'dependent'	 => true,
		),
		'OperationBriefing' => array(
			'className'	 => 'OperationBriefing',
			'dependent'	 => true,
		),
		'Fukubukuro' => array(
			'className'	 => 'Fukubukuro',
			'dependent'	 => true,
		),
		'MediaStation' => array(
			'className'	 => 'MediaStation',
			'dependent'	 => true,
		),
		'LastHansoku' => array(
			'className'	 => 'LastHansoku',
			'dependent'	 => true,
		),
		'ChristmasFeature' => array(
			'className'	 => 'ChristmasFeature',
			'dependent'	 => true,
		)

	);
    public $validate = Array(
        'email' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => Array(
                'rule' => array('email', true),
                'message' => 'メールアドレスを正しく入力してください。'
            )
        ),
        'address' => Array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
        'company' => Array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
        'name' => Array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
		'block' => array(
					'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
					'required2' => array('rule'=>array('isUnique'),'message'=>'重複しています。')),
        'phone' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
        'fax' => Array(
            'required2' => array(
                'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
        'tenant_fax' => Array(
            'required2' => array(
                'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
        'extension' => Array(
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
        'tenant_phone' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
		'receipt_phone' => Array(
			'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm2'),
				'message' => '電話番号は１４文字以内の数字とハイフン(-)で入力してください。'
			)
		),
		'receipt_fax' => Array(
			'required2' => array(
				'rule' => array('phoneConfirm2'),
				'allowEmpty' => true,
				'message' => '電話番号は１４文字以内の数字とハイフン(-)で入力してください。'
			)
		),
        'zipcode' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', '/^[0-9]{7}$/' ),
				'allowEmpty' => true,
                'message' => '半角数字７桁で入力してください。'
            )
        ),
        'name_kana' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
        'tenant_name' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => Array(
                'rule'    => array('maxLength16'),
				'allowEmpty' => true,
                'message' => '半角１６文字以内、全角を含む場合は８文字以内で入力してください。'
            )
        ),
        'receipt_tenant_name' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('maxLength16'),
                'message' => '半角なら16文字以内、全角なら8文字以内で入力してください。'
            )
        ),
        'regi_num' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', '/^[0-9]*$/' ),
                'message' => '半角数字で入力してください。'
            )
        ),
        'terminal_num' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', '/^[0-9]*$/' ),
                'message' => '半角数字で入力してください。'
            )
        ),
        'password' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => Array(
                'rule'    => array('custom', '/^[a-z0-9]{8,}$/i'),
                'message' => 'パスワードは８文字以上の半角英数字で入力してください。'
            )
        ),
        'password_confirm' => Array(
            'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('passwordConfirm'),
                'message' => '同じパスワードをここにも入力してください。'
            )
        ),





		'contact_name_kana' => Array(
			'required2' => array(
				'rule'	  => array('custom', VALID_KATAKANA ),
				'allowEmpty' => true,
				'message' => '全角カタカナで入力してください。'
			)
		),
		'contact_email' => array(
			'email' => array(
				'rule' => array('email'),
				'allowEmpty' => true,
				'message' => 'メールアドレスを入力して下さい。',
			),
		),
		'contact_zipcode' => Array(
			//'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule'	  => array('custom', '/^[0-9]{7}$/' ),
				'allowEmpty' => true,
				'message' => '半角数字７桁で入力してください。'
			)
		),
		'contact_phone' => Array(
			// 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
				'message' => '１４文字以内の半角数字で入力してください。'
			)
		),
		'contact_extension' => Array(
			// 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
				'message' => '１４文字以内の半角数字で入力してください。'
			)
		),
		'contact_fax' => Array(
			// 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
				'message' => '１４文字以内の半角数字で入力してください。'
			)
		),
		'contact_cellphone' => Array(
			// 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm'),
				'allowEmpty' => true,
				'message' => '１４文字以内の半角数字で入力してください。'
			)
		),

		'company_phone' => Array(
			'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm'),
				'message' => '１４文字以内の半角数字で入力してください。'
			)
		),

	);

//    function phoneConfirm( $check ){
//        list( $key, $value ) = each( $check );
//        $phone = mb_convert_kana( $value, "as", "UTF-8");
//        $phone_num = str_replace('-','', $phone );
//        if( ereg("^[0-9-]+$", $phone )
//         && ereg("^[0-9]{0,14}$", $phone_num )){
//            $this->data['User'][ $key ] = $phone;
//            return true;
//        } else {
//            return false;
//        }
//    }
    function phoneConfirm( $check ){
        list( $key, $value ) = each( $check );
        $phone = mb_convert_kana( $value, "as", "UTF-8");
        $phone = str_replace('-','', $phone );
        if( ereg("^[0-9]{0,14}$", $phone )){
            $this->data['User'][ $key ] = $phone;
            return true;
        } else {
            return false;
        }
    }
	function phoneConfirm2( $check ){
		list( $key, $value ) = each( $check );
		$phone = mb_convert_kana( $value, "as", "UTF-8");
		$number = str_replace('-','', $phone );
		if( strlen($number) <= 14 // 数字は１４文字以内
		 && ereg("^[0-9\-]+$", $phone )){  // 数字とハイフンのみ
			$this->data['User'][ $key ] = $phone; // 書き換えて保存
			return true;
		} else {
			return false;
		}
	}
    function passwordConfirm(){
        if ($this->data['User']['password']
         == $this->data['User']['password_confirm']) {
            return true;
        } else {
            return false;
        }
    }

	// 半角１６文字以内、全角を含む場合は８文字以内
	function maxLength16( $check ){
		$check_str = array_shift( $check );
		$str_array = preg_split("//u", $check_str, -1, PREG_SPLIT_NO_EMPTY); // 配列に分割

		$char_num = 0;
		foreach( $str_array as $char ){
			// 半角カタカナやASCII文字以外は全角
			if (!preg_match("/(?:\xEF\xBD[\xA1-\xBF]|\xEF\xBE[\x80-\x9F])|[\x20-\x7E]/", $char)) {
				$char_num += 2; // 全角
			} else {
				$char_num += 1; // 半角
			}
		}
		return ( $char_num <= 16 ); // 16文字以内なら良し
	}

    // 全角対応文字数チェック
    function maxLengthJp( $check, $max){
        $check_str = array_shift( $check );
        $length = mb_strlen($check_str, mb_detect_encoding( $check_str,"UTF-8"));
        return ( $length <= $max );
    }
    public function beforeSave() {
		parent::beforeSave();

        if ( isset( $this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password']
              = AuthComponent::password($this->data[$this->alias]['password']);
            $this->data[$this->alias]['password_confirm']
              = AuthComponent::password($this->data[$this->alias]['password_confirm']);
        }

		// 和歌山は、blockに連動してloginidを保存
		if( SUB_DOMAIN =='wakayama' ){
			if( isset( $this->data[$this->alias]['block']))
				$this->data[$this->alias]['loginid']
				= MALL_CODE . $this->data[$this->alias]['block'];
		}

        if( isset( $_SESSION['Auth']['User']['id'] )){
            $beforeData = $this->find('first', array('conditions' => array(
                             'User.id' => $_SESSION['Auth']['User']['id'],
                             'User.deleted' => '0',
            )));

            /***********************************
             * Userのプロパティの変更履歴を保存
             ************************************/
            $user_id = isset( $this->data['User']['id'])? $this->data['User']['id'] : '';

            App::import('Model','History');
            $History = new History;

            if( $user_id == $_SESSION['Auth']['User']['id'] ){
                foreach( $this->data['User'] as $item => $value ){
                    if( $item == 'password' || $item == 'password_confirm'
					|| $item == 'write_permission'
					|| $item == 'read_permission'
					)
                        break;
                    $history = array();
                    $history['History']= array();
                    if(( ! isset( $beforeData['User'][$item]))
                     ||( $item != 'id' && $item != 'updated' && $value != $beforeData['User'][$item])){

                        $history = $this->History->find('first', array(
                            'fields'=>array('History.id'),
                            'conditions' => array(
                                             'History.user_id' => $user_id,
                                             'History.item' => $item,
                                             'History.deleted' => '0')
                        ));
                        if( ! isset( $history['History']['id'] )){
                            $history['id']['History'] = null;
                            $History->create();
                        }
                        $history['History']['user_id'] = $user_id;
                        $history['History']['item'] = $item;
                        $history['History']['value'] = $value;
                        $History->save( $history );
                    }
                }
            }
        }
    }

    /***********************************
     * authBackSave
     * メール認証コールバック完了
     ************************************/
    function authBackSave( $id ){
        /* DB読み出し
         **************/
        $user  = $this->find('first', array(
            'fields'=>array('id','authkey','email','authkey'),
            'conditions' => array(
                             'User.id' => $id,
                             'User.deleted' => '0')
        ));
        /* 書きこみデータを生成
         *******************/
        $user["User"]["user_status"] = 'authbacked';
        /* DB書きこみ
         *******************/
        return $this->save( $user );
    }
    /***********************************
     * 登録
     ************************************/
    function registerSave( $user ){
        /* ユーザー・ステータスを変更
         *************************/
        $user["User"]["user_status"] = 'registered';
        /* DB書きこみ
         *************************/
        return $this->save( $user );
    }
	/***********************************
	 * GoogleMap設定
	 ************************************/
	function googleMapSave( $user ){
		/* 書きこみデータ
		 *************************/
		$wdata["User"]["id"]				= $user["User"]["id"];
		$wdata["User"]["allow_google_map"]	= $user["User"]["allow_google_map"];
		$wdata["User"]["google_map_modified"]		= date('c');
		/* DB書きこみ
		 *************************/
		return $this->save( $wdata );
	}

	/***********************************
	 * 最終店名確認
	 ************************************/
	function lastShopSignSave( $user ){
		/* 書きこみデータ
		 *************************/
		$wdata["User"]["id"]							= $user["User"]["id"];
		
		$wdata["User"]["shop_sign_last"]				= $user["User"]["shop_sign_last"];
		$wdata["User"]["shop_sign_remark"]				= $user["User"]["shop_sign_remark"];
		$wdata["User"]["shop_sign_kakutei"]				= $user["User"]["shop_sign_kakutei"];
		$wdata["User"]["shop_sign_yotei"]				= $user["User"]["shop_sign_yotei"];
		$wdata["User"]["shop_sign_syuusei"]				= $user["User"]["shop_sign_syuusei"];

		$wdata["User"]["shop_category_last"]			= $user["User"]["shop_category_last"];
		$wdata["User"]["shop_category_remark"]			= $user["User"]["shop_category_remark"];
		$wdata["User"]["shop_category_kakutei"]			= $user["User"]["shop_category_kakutei"];
		$wdata["User"]["shop_category_yotei"]			= $user["User"]["shop_category_yotei"];
		$wdata["User"]["shop_category_syuusei"]			= $user["User"]["shop_category_syuusei"];

		$wdata["User"]["syutten_jyoukyou_last"]			= $user["User"]["syutten_jyoukyou_last"];
		$wdata["User"]["syutten_jyoukyou_remark"]		= $user["User"]["syutten_jyoukyou_remark"];
		$wdata["User"]["syutten_jyoukyou_kakutei"]		= $user["User"]["syutten_jyoukyou_kakutei"];
		$wdata["User"]["syutten_jyoukyou_syuusei"]		= $user["User"]["syutten_jyoukyou_syuusei"];
		
		$wdata["User"]["last_shop_sign_modified"]		= date('c');
		
		/* DB書きこみ
		 *************************/
		return $this->save( $wdata );
	}

	/***********************************
	 * シートロック設定
	 ************************************/
	function lockDataSave( $data ){
		$res = array();
		foreach( $data['User'] as $id => $lock_datas ){
			/* 書きこみデータ
			 *************************/
			$wdata["User"]["id"] = "$id";
			if( empty( $lock_datas)) continue;
			foreach( $lock_datas as $field => $value ){
				$wdata["User"][ $field ] = $value;
				/* DB書きこみ
				*************************/
				//$res[] = $this->save( $wdata );
				$wdata = array( $value, $id );
				$res[] = $this->query('update users set '. $field .'=? where id=?',$wdata);
			}
		}
		return $res;
	}
    /***********************************
     * authSave
     * 本登録完了
     ************************************/
    function authSave( $id, $status ){
        /* DB読み出し
         **************/
        $user  = $this->find('first', array(
            'fields'=>array('id','email','name'),
            'conditions' => array(
                             'User.id' => $id,
                             'User.deleted' => '0')
        ));

		/* 書きこみデータを生成
		 *******************/
		for( $num=1; $num<9900; $num++ ){
			if( SUB_DOMAIN =='makuharishintoshin'){
				$loginid = MALL_CODE . str_pad( $num, 4, '0', STR_PAD_LEFT );
			}else if( SUB_DOMAIN =='testsite'){
				$loginid = MALL_CODE . str_pad( $num, 4, '0', STR_PAD_LEFT );
			}else{
				$loginid = MALL_CODE . rand( 1000, 9999 );
			}
			if( ! $this->find('first', array('conditions' => array(
							 'User.loginid' => $loginid
					)))) break;
		};

        /* 書きこみデータを生成
         *******************/
		$user["User"]["loginid"] = $loginid;
        $user["User"]["user_status"] = $status;
        /* DB書きこみ
         *******************/
        return $this->save( $user );
    }
    /***********************************
     * profileSave
     ************************************/
    function profileSave( $user ){
        /* プロフィールの編集
         *******************/
        $wdata["User"]["id"]            = $user["User"]["id"];
        $wdata["User"]["company"]       = $user["User"]["company"];
        $wdata["User"]["zipcode"]       = $user["User"]["zipcode"];
        $wdata["User"]["address"]       = $user["User"]["address"];
        $wdata["User"]["company_phone"] = $user["User"]["company_phone"];
        $wdata["User"]["fax"]           = $user["User"]["fax"];

        $wdata["User"]["email"]         = $user["User"]["email"];
        $wdata["User"]["department"]    = $user["User"]["department"];
        $wdata["User"]["name_kana"]     = $user["User"]["name_kana"];
        $wdata["User"]["name"]          = $user["User"]["name"];
        $wdata["User"]["phone"]         = $user["User"]["phone"];
        $wdata["User"]["extension"]     = $user["User"]["extension"];

        $wdata["User"]["profile_modified"] = date('c');

		/* Lock確認
		 *******************/
		$user = $this->find('first', array('conditions' => array(
							'User.id' => $_SESSION['Auth']['User']['id'],
							'User.deleted' => '0',
		)));
		$lock = ($user['User']['role']=='admin'|| $user['User']['role']=='partner')? 0
				: $user['User']['lock_user_profile'];
		/* DB書きこみ
		 *******************/
		if( $lock == 0 ){
			return $this->save( $wdata );
		}else{
			return 'locked';
		}
	}

    /***********************************
     * 端末情報
     ************************************/
	function terminalSave( $user, $fields ){
		foreach($fields as $field){
			$wdata["User"][$field] = $user["User"][$field];
		}
		$wdata["User"]["terminal_modified"] = date('c');
		/* Lock確認
		 *******************/
		$user = $this->find('first', array('conditions' => array(
							'User.id' => $_SESSION['Auth']['User']['id'],
							'User.deleted' => '0',
		)));
		$lock = ($user['User']['role']=='admin'||$user['User']['role']=='partner')? 0
				: $user['User']['lock_user_terminal'];
		/* DB書きこみ
		 *******************/
		if( $lock == 0 ){
			return $this->save( $wdata );
		}else{
			return 'locked';
		}
	}
    /***********************************
     * レシート情報
     ************************************/
	function receiptSave( $user, $fields ){
		foreach($fields as $field){
			$wdata["User"][$field] = $user["User"][$field];
		}
		$wdata["User"]["receipt_modified"] = date('c');
		/* Lock確認
		 *******************/
		$user = $this->find('first', array('conditions' => array(
							'User.id' => $_SESSION['Auth']['User']['id'],
							'User.deleted' => '0',
		)));
		$lock = ($user['User']['role']=='admin'||$user['User']['role']=='partner')? 0
				: $user['User']['lock_receipt'];
		/* DB書きこみ
		 *******************/
		if( $lock == 0 ){
			return $this->save( $wdata );
		}else{
			return 'locked';
		}
	}
    /***********************************
     * profileSaveAdmin
     ************************************/
    function profileSaveAdmin( $user ){
        /* DB書きこみ
         *******************/
        $user["User"]["profile_modified"] = date('c');
        return $this->save( $user );
    }

    /***********************************
     * fieldsSave
     ************************************/
    function fieldsSave( $user, $fields ){
        $wdata['User']['id'] = $user['User']['id'];
        foreach ( $fields as $field ){
            $wdata['User'][$field] = $user['User'][$field];
        }
        /* DB書きこみ
         *******************/
        return $this->save( $wdata );
    }
    /***********************************
     * agreedSave
     ************************************/
    function agreedSave( $id ){
        $wdata['User']['id'] = $id;
        $wdata['User']['agreed'] = '1';
        /* DB書きこみ
         *******************/
        return $this->save( $wdata );
    }
    /*******************************
     * emailSave
     *
     *******************************/
    function emailSave($data = null) {
        $res = array('res' => false);
        if ($data === null) return $res;

        $user  = $this->find('first', array('conditions' => array(
                             'User.id' => $_SESSION['Auth']['User']['id'],
                             'User.deleted' => '0',
        )));
        if (empty($data["User"]["email"])){
                // Emailが入力されていない場合はエラー
                $res['msg'] = __('メールアドレスを入力してください', true);
                return $res;
        } elseif (!$this->mailValidator($data["User"]["email"])) {
                // バリデーション
                $res['msg'] = __('正しいメールアドレスを入力してください', true);
                return $res;
        }

        // すでにレコードがあるかチェック
        $user_x  = $this->find('first', array('conditions' => array(
                'User.email' => $data["User"]["email"],
                'User.deleted' => '0'
        )));
        if ($user_x) {
                if ($user_x["User"]["status"] == 1) {
                        // Emailが登録済みの場合はエラー
                        $res['msg'] = __('このメールアドレスはすでに登録されています', true);
                        return $res;
                } else {
                        // 仮登録中の場合は上書き
                        $data["User"]["id"] = $user_x["User"]["id"];
                        $user_id = $user_x["User"]["id"];
                }
        }else{
            $user_id = $user["User"]["id"];
        }

        /* ユーザー情報の保存 */
        $data["User"]["id"] = $user_id;
        $data["User"]["email"] = $data["User"]["email"];

        $this->save($data);
        $res['res'] = true;
        $res['params'] = $this->findById( $user_id );
        return $res;
    }

	/***********************************
	 * 登録状況をチェック
	 ************************************/
	function registered_user( $id ){
		$user = $this->find('first', array('conditions' => array(
			'User.id' => $id
		)));
		if( ! $user ) return false;
		if( empty($user['User']['company'])) return false; // 必須
		if( empty($user['User']['zipcode'])) return false; // 必須
		if( empty($user['User']['address'])) return false; // 必須
		if( empty($user['User']['company_phone'])) return false; // 必須
		if( empty($user['User']['email'])) return false; // 必須
		if( empty($user['User']['name_kana'])) return false; // 必須
		if( empty($user['User']['name'])) return false; // 必須
		return $user;
	}

	public function afterFind($results, $primary = false) {
		// parent::afterFind( $results ); // なぜかうまくいかない。
		foreach ($results as $key => $val) {
			// アンシリアライズ
			if( isset( $val['User']['write_permission']) && !empty( $val['User']['write_permission']))
				$results[$key]['User']['write_permission'] = @unserialize( $val['User']['write_permission'] );
			if( isset( $val['User']['read_permission']) && !empty( $val['User']['read_permission']))
				$results[$key]['User']['read_permission'] = @unserialize( $val['User']['read_permission'] );
		}

		if( SUB_DOMAIN == 'wakayama'){ // 和歌山の場合
			foreach ($results as $key => $val) {
				// 'loginid' フィールドを書き換え
				if( isset($val['User']['block']) && ! empty($val['User']['block']))
					$loginid = MALL_CODE . $val['User']['block'];
				else
					$loginid = "";
				$results[$key]['User']['loginid'] = $loginid;
			}
		}

		return $results;
	}
}
?>