<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    var $serializeKey = array();
 
 
    function afterFind( $results ) {
 
        // シリアライズを行う
        if ( empty( $this->serializeKey ) ) {
            // シリアライズしない場合
            return $results;
        }
 
        foreach( $results as &$result ) {
            foreach( $this->serializeKey as $skey ) {
                if ( isset( $result[$this->alias][$skey] ) && !empty( $result[$this->alias][$skey] ) ) {
                    $raw_data = @unserialize( $result[$this->alias][$skey] );
					if( $raw_data )
						$result[$this->alias][$skey] = $raw_data;
					else
						$result[$this->alias][$skey] = '';
                }
            }
        }
 
        return $results;
 
    }
 
 
    function beforeSave() {
 
        // 保存前にシリアライズを行う
        if ( empty( $this->serializeKey ) ) {
            return true;
        }
        foreach( $this->serializeKey as $key => $value ) {
            if ( isset( $this->data[$this->alias][$value] ) ) {
                $this->data[$this->alias][$value] = serialize($this->data[$this->alias][$value]);
            }
        }
        return true;
    }
 
    function afterSave( $created ) {
 
        // 保存後にシリアライズを戻す
        if ( empty( $this->serializeKey ) ) {
            return;
        }
 
        foreach( $this->serializeKey as $key => $value ) {
            if ( isset( $this->data[$this->alias][$value] ) ) {
                $this->data[$this->alias][$value] = unserialize($this->data[$this->alias][$value]);
            }
        }
    }
 
}
