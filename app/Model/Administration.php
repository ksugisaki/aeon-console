<?php
class Administration extends AppModel {
    public $name = 'Administration';
    public $validate = array(
        'entry_expiration' => array(
            'required' => array('rule'=>'date','message'=>'年月日 日時分を入力（書式: 20YY-MM-DD hh:mm:ss）'),
        ),
        'edit_expiration' => array(
            'required' => array('rule'=>'date','message'=>'年月日 日時分を入力（書式: 20YY-MM-DD hh:mm:ss）'),
        )
    );
}
?>
