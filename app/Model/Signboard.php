<?php
class Signboard extends AppModel {
    public $name = 'Signboard';
    public $belongsTo = array(
        'User' => array(
            'className'    => 'User',
            'foreignKey'   => 'user_id'
        )
    );
    public $validate = Array(
        'name_kana'    => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
        'company_kana'    => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
        'department_kana' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
        'shop_name_kana'  => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
        'shop_sign_kana'  => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', VALID_KATAKANA ),
                'message' => '全角カタカナで入力してください。'
            )
        ),
        'shop_sign_jpn' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            //'required2' => array(
            //    'rule' => array('maxLengthJp', '26'),
            //    'message' => '２６文字以内で入力してください。'
            //)
        ),
        'shop_sign_eng' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            //'required2' => array(
            //    'rule' => array('maxLengthJp', '26'),
            //    'message' => '２６文字以内で入力してください。'
            //),
            //'required3' => array(
            //    'rule' => array('singleStrings'),
            //    'message' => '半角文字で入力してください。'
            //)
        ),
        'email' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => Array(
                'rule' => array('email', true),
				'allowEmpty' => true,
                'message' => 'メールアドレスを正しく入力してください。'
            )
        ),
        'phone' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
        'fax' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => 'FAXは１４文字以内の数字で入力してください。'
            )
        ),
        'extension' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
		'cellphone' => Array(
			// 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('phoneConfirm'),
				'message' => '１４文字以内の半角数字で入力してください。'
			)
		),
		'zipcode' => Array(
			//'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule' => array('custom', '/^[0-9]{7}$/' ),
				'allowEmpty' => true,
				'message' => '半角数字７桁で入力してください。'
			)
		),
        'shop_phone' => Array(
            // 'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('phoneConfirm'),
                'message' => '電話番号は１４文字以内の数字で入力してください。'
            )
        ),
        'shop_open_time' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', '/^[0-2]{1,1}[0-9]{1,1}[0-5]{1,1}[0-9]{1,1}$/' ),
                'message' => '半角数字４桁で２４時制で入力してください。'
            )
        ),
        'shop_close_time' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule'    => array('custom', '/^[0-2]{1,1}[0-9]{1,1}[0-5]{1,1}[0-9]{1,1}$/' ),
                'message' => '半角数字４桁で２４時制で入力してください。'
            )
        ),
        'shop_copy' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('maxLengthJp', '20'),
                'message' => '２０文字以内で入力してください。'
            )
        ),
        'shop_concept' => Array(
            //'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
            'required2' => array(
                'rule' => array('maxLengthJp', '300'),
                'message' => '３００文字以内で入力してください。'
            )
        )
    );
    function phoneConfirm( $check ){
        list( $key, $value ) = each( $check );
        $phone = mb_convert_kana( $value, "as", "UTF-8");
        $phone = str_replace('-','', $phone );
        if( ereg("^[0-9]{0,14}$", $phone )){
            $this->data['Signboard'][ $key ] = $phone;
            return true;
        } else {
            return false;
        }
    }
    // 全角対応文字数チェック
    function maxLengthJp( $check, $max){
        $check_str = array_shift( $check );
        $length = mb_strlen($check_str, mb_detect_encoding( $check_str,"UTF-8"));
        return ( $length <= $max );
    }
    // 全角チェック
    function singleStrings( $data ) {
        list($k, $v) = each($data);
        if( $v === '' ) return TRUE;
        if( strlen($v) != mb_strlen( $v )){
            return false;
        }
        return true;
    }

	public function afterFind($results, $primary = false) {
		foreach ($results as $key => $val) {
			// シリアライズされてないのは、ここでタグ除去
			foreach ($val['Signboard'] as $key2 => $val2) {
				if( ! $unserialized = @unserialize( $val2 )){
					if( $val2 != ''){
						$results[$key]['Signboard'][$key2] = strip_tags($val2, '');
					}
				}
			}
			// 'shop_sign' フィールドを追加
			$shop_sign = isset($val['Signboard']['shop_sign_jpn'])? $val['Signboard']['shop_sign_jpn'] : '';
			if (isset($val['Signboard']['shop_sign_main_type'])
			&&       ($val['Signboard']['shop_sign_main_type']=='1')) {
				$shop_sign = $val['Signboard']['shop_sign_eng'];
			}
			$results[$key]['Signboard']['shop_sign'] = $shop_sign;
		}
		return $results;
	}

}
?>