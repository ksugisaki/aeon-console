<?php
App::uses('AppModel', 'Model');
/**
 * Bill Model
 *
 * @property User $User
 */
class Bill extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/*
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $validate = array(
		'zipcode'  => array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
		'address'  => array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
		'department' => array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
		'name' => array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。')),
		'phone' => array('required' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'))
	);
}
?>
