<?php
App::uses('AppModel', 'Model');
/**
 * LastHansoku Model
 *
 * @property User $User
 */
class LastHansoku extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/*
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $useTable = 'last_hansokus';

}
