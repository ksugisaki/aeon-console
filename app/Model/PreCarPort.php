<?php
App::uses('AppModel', 'Model');
/**
 * PreCarPort Model
 *
 * @property User $User
 */
class PreCarPort extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/*
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public $validate = Array(
		'port_num' => Array(
			'required1' => array('rule'=>array('notEmpty'),'message'=>'入力してください。'),
			'required2' => array(
				'rule'	  => array('custom', '/^[0-9]*$/' ),
				'message' => '半角数字で入力してください。'
			)
		)
	);

}
