<?php
App::uses('AppModel', 'Model');
/**
 * OperationBriefing Model
 *
 * @property User $User
 */
class OperationBriefing extends AppModel {

/**
 * Display field
 *
 * @var string
 */

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/*
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	var $useTable = 'operation_briefing';

}
