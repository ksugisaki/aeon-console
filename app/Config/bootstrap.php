<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright	  Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link		  http://cakephp.org CakePHP(tm) Project
 * @package		  app.Config
 * @since		  CakePHP(tm) v 0.10.8.2117
 * @license		  MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 *	 Cache::config('default', array(
 *		'engine' => 'File', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 *		'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 *		'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 *		'lock' => false, //[optional]  use file locking
 *		'serialize' => true, // [optional]
 *		'mask' => 0666, // [optional] permission mask to use when creating cache files
 *	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 *	 Cache::config('default', array(
 *		'engine' => 'Apc', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 *	 Cache::config('default', array(
 *		'engine' => 'Xcache', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 *		'user' => 'user', //user from xcache.admin.user settings
 *		'password' => 'password', //plaintext password (xcache.admin.pass)
 *	));
 *
 * Memcache (http://memcached.org/)
 *
 *	 Cache::config('default', array(
 *		'engine' => 'Memcache', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *		'servers' => array(
 *			'127.0.0.1:11211' // localhost, default port 11211
 *		), //[optional]
 *		'persistent' => true, // [optional] set this to false for non-persistent connections
 *		'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
 *	));
 *
 *	Wincache (http://php.net/wincache)
 *
 *	 Cache::config('default', array(
 *		'engine' => 'Wincache', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *	));
 *
 * Redis (http://http://redis.io/)
 *
 *	 Cache::config('default', array(
 *		'engine' => 'Redis', //[required]
 *		'duration'=> 3600, //[optional]
 *		'probability'=> 100, //[optional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *		'server' => '127.0.0.1' // localhost
 *		'port' => 6379 // default port 6379
 *		'timeout' => 0 // timeout in seconds, 0 = unlimited
 *		'persistent' => true, // [optional] set this to false for non-persistent connections
 *	));
 */
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *	   'Model'					   => array('/path/to/models', '/next/path/to/models'),
 *	   'Model/Behavior'			   => array('/path/to/behaviors', '/next/path/to/behaviors'),
 *	   'Model/Datasource'		   => array('/path/to/datasources', '/next/path/to/datasources'),
 *	   'Model/Datasource/Database' => array('/path/to/databases', '/next/path/to/database'),
 *	   'Model/Datasource/Session'  => array('/path/to/sessions', '/next/path/to/sessions'),
 *	   'Controller'				   => array('/path/to/controllers', '/next/path/to/controllers'),
 *	   'Controller/Component'	   => array('/path/to/components', '/next/path/to/components'),
 *	   'Controller/Component/Auth' => array('/path/to/auths', '/next/path/to/auths'),
 *	   'Controller/Component/Acl'  => array('/path/to/acls', '/next/path/to/acls'),
 *	   'View'					   => array('/path/to/views', '/next/path/to/views'),
 *	   'View/Helper'			   => array('/path/to/helpers', '/next/path/to/helpers'),
 *	   'Console'				   => array('/path/to/consoles', '/next/path/to/consoles'),
 *	   'Console/Command'		   => array('/path/to/commands', '/next/path/to/commands'),
 *	   'Console/Command/Task'	   => array('/path/to/tasks', '/next/path/to/tasks'),
 *	   'Lib'					   => array('/path/to/libs', '/next/path/to/libs'),
 *	   'Locale'					   => array('/path/to/locales', '/next/path/to/locales'),
 *	   'Vendor'					   => array('/path/to/vendors', '/next/path/to/vendors'),
 *	   'Plugin'					   => array('/path/to/plugins', '/next/path/to/plugins'),
 * ));
 *
 */

/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */


/**
 * You can attach event listeners to the request lifecyle as Dispatcher Filter . By Default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //	 will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'FileLog',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'FileLog',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));
CakePlugin::loadAll();
//CakePlugin::load('upload_pack');
//CakePlugin::load('TwitterBootstrap');
//CakePlugin::load('Migrations');

/**
 * Configures for AuthComponent
 */
Configure::write('Security', array(
	'level' => 'high',
	'salt' => 'uotierjkdjhfglslqwertuyals',
	'cipherSeed' => '96384758792140985723'
));

Configure::write('Session', array(
	'defaults' => 'database',
	'cookie' => 'SID',
	'timeout' => 259200,
	'ini' => Array(
		'session.cookie_lifetime' => 2580000,
		'session.gc_maxlifetime' => 2580000,
		'session.gc_probability' => 1,
		'session.gc_divisor' => 100
	)
));

//for Validation
//define('VALID_KATAKANA', '/^(?:\xE3\x82[\xA1-\xBF]|\xE3\x83[\x80-\xB6]|\xE3\x83\xBC)*$/');
define('VALID_KATAKANA', '/^.*$/');

// メッセージ定義
define('MESSAGE_AT_LOCKED','<p class="alert">現在、ご登録情報の変更はできません。<br />
        変更されたい場合は、業務メニューに記載しております各問合わせ先にご連絡ください。</p>');

// 一般（テナント）ユーザの条件
$tenant_user_cond =array(
	'OR'=>array(
		array('User.role'=> null),
		array('User.role'=> '')),
	'User.user_status'=> 'authorized',
	'User.deleted'=> 0
);
define('TENANT_USER_AUTHORIZED',serialize($tenant_user_cond));

// 共通設定
define('ADMIN_EDIT_PAGE', true ); // ユーザー管理ページ

// 幕張モード設定
function set_makuhari_mode(){
		define('MALL_NAME', "イオンモール幕張新都心");
		define('MALL_CODE', '1146');

		define('SUB_DOMAIN',		"makuharishintoshin");
		//define('TERMINAL_COMPANY',  "オムロンソフトウェア株式会社");
		define('SITE_MAIL_ADDRESS', "makuharishintoshin@com.aeonmall.com");
		define('SCHEDULE_LINK', '/files/makuharishintoshin/SC_schedule_20130705.pdf');
		define('THANKSDAY_LINK', '/files/makuharishintoshin/makuharishintoshin_thanksday_v1.1.pdf');
		define('SYOKUJYU_LINK', '/files/makuharishintoshin/makuharishintoshin_syokujyu_info.pdf');
		define('BAITAI_LINK', '/files/makuharishintoshin/makuharishintoshin_baitai_jimukyoku.pdf');

		define('PERMIT_CARDS','OP提出②【オープン時仮入館証発行願】');
		define('PRE_CAR_PORTS','OP提出④【オープン前：臨時駐車場利用台数アンケート】');
		define('CAR_PORTS','OP提出⑤【オープン後：従業員駐車場台数アンケート】');
		define('BICYCLES','OP提出⑥【従業員駐輪場利用台数アンケート】');
		define('LOCKERS','OP提出⑦【従業員ロッカー利用台数申込書】');
		define('CASHBOXES','OP提出⑧【釣銭金庫利用申込書】');
		//define('BILLS', true );
		//define('MOCK_OPERATIONS', true );
		//define('EMERGENCY_CONTACTS', true );
		//define('CASHIERS', true );
		define('FIRE_DRILLS','OP提出⑪【消防訓練説明会の参加申込】');
		define('OPERATION_BRIEFINGS','OP提出⑮【第２回運営管理説明会申込】');

		define('LAST_SHOP_SIGN', true );
}

// 東員モード設定
function set_toin_mode(){
		define('MALL_NAME', "イオンモール東員");
		define('MALL_CODE', '1141');

		define('SUB_DOMAIN',		"toin");
		define('TERMINAL_COMPANY',	"オムロンソフトウェア株式会社");
		define('SITE_MAIL_ADDRESS', "toin@com.aeonmall.com");

		define('HANSOKU_LINK', '/files/toin/toin_hansoku_kikaku_annai.pdf');
		define('GOOGLEMAP_LINK', '/files/toin/toin_indoor_google_map.pdf');
		define('SYOKUJYU_LINK', '/files/toin/toin_syokujyu_info_2013_1011.pdf');

		define('MEDIA_STATION_LINK', '/files/toin/media_station.pdf');
		define('MOCK_OPERATIONS_LINK', '/files/toin/mock_operation.pdf');

		define('FUKUBUKURO','【福袋について】');
		define('LAST_HANSOKU','【販促企画最終確認】');
		define('MEDIA_STATION','【メディアステーション・イオンモールメンバーズ説明会】');
		define('MOCK_OPERATIONS','【模擬営業、店頭準備についてのアンケート】');
		define('CHRISTMAS_FEATURES','【クリスマスギフト特集・クリスマスグルメ特集】');

		// 提出書類
		define('PERMIT_CARDS','【オープン時臨時入館証発行願】');
		define('CAR_PORTS','【従業員駐車場利用希望アンケート】');
		define('LOCKERS','【従業員ロッカー利用台数申込書】');
		define('CASHBOXES','【釣銭金庫利用申込書】');

		define('ADD_AUTHORIZED_USER', true ); // ユーザー追加機能

}

// 和歌山モード設定
function set_wakayama_mode(){
		define('MALL_NAME', "イオンモール和歌山");
		define('MALL_CODE', '1143');

		define('SUB_DOMAIN',		"wakayama");
		define('TERMINAL_COMPANY',	"オムロンソフトウェア株式会社");
		define('SITE_MAIL_ADDRESS', "wakayama@com.aeonmall.com");
		define('ADD_AUTHORIZED_USER', true ); // ユーザー追加機能
}

if( strpos( dirname(__FILE__), 'com.aeonmall.com/makuharishintoshin') !== false) {
// イオンモール幕張新都心
		define('SITE_NAME',			"イオンモール幕張新都心 コミュニケーション");
		define('SITE_URL',			"https://com.aeonmall.com/makuharishintoshin/");
		define('DATABASE_NAME',		'aeon_console_makuharishintoshin');

		set_makuhari_mode();

}else if( strpos( dirname(__FILE__), 'com.aeonmall.com/toin') !== false) {
// イオンモール東員
		define('SITE_NAME',			"イオンモール東員 コミュニケーション");
		define('SITE_URL',			"https://com.aeonmall.com/toin/");
		define('DATABASE_NAME',		'aeon_console_toin');

		set_toin_mode();

}else if( strpos( dirname(__FILE__), 'com.aeonmall.com/wakayama') !== false) {
// イオンモール和歌山
		define('SITE_NAME',			"イオンモール和歌山 コミュニケーション");
		define('SITE_URL',			"https://com.aeonmall.com/wakayama/");
		define('DATABASE_NAME',		'aeon_console_wakayama');

		set_wakayama_mode();

}else if( strpos( dirname(__FILE__), 'com.aeonmall.com/tendo') !== false) {
// イオンモール天童
		define('MALL_NAME', "イオンモール天童");
		define('MALL_CODE', '1144');
		define('SITE_NAME',			"イオンモール天童 コミュニケーション");
		define('SITE_URL',			"https://com.aeonmall.com/tendo/");
		define('DATABASE_NAME',		'aeon_console_tendo');

		define('SUB_DOMAIN',		"tendo");
		define('TERMINAL_COMPANY',	"オムロンソフトウェア株式会社");
		define('SITE_MAIL_ADDRESS', "tendo@com.aeonmall.com");

}else if( strpos( dirname(__FILE__), 'com.aeonmall.com/testsite') !== false) {
// テストサイト
		define('SITE_NAME',			"RCTモール	（テストサイト）");
		define('SITE_URL',			"https://com.aeonmall.com/testsite/");
		define('DATABASE_NAME',		'aeon_console_testsite');
		define('TESTING', true );

	switch( $_SERVER['QUERY_STRING'] ){
	case 'mode=makuhari':
		set_makuhari_mode();
		define('ADD_AUTHORIZED_USER', true ); // ユーザー追加機能
		break;
		
	case 'mode=wakayama':
		set_wakayama_mode();
		break;

	default:
		set_toin_mode();
		break;
	}

}else if( strpos( dirname(__FILE__), 'html/aeon-console') !== false) {
// 使ってない
		define('SITE_NAME',			"RCTモール （テストサイト）");
		define('SITE_URL',			"http://49.212.174.155/aeon-console/");
		define('DATABASE_NAME',		'aeon_console');
		define('TESTING', true );

	switch( $_SERVER['QUERY_STRING'] ){
	case 'mode=makuhari':
		set_makuhari_mode();
		break;
		
	case 'mode=wakayama':
		set_wakayama_mode();
		break;

	default:
		set_toin_mode();
		break;
	}

}else if( strpos( dirname(__FILE__), 'html/aeon_console_makuhari_test') !== false) {
// イオンモール幕張新都心（コピーサイト）
		define('SITE_NAME',			"イオンモール幕張新都心 （コピーサイト）");
		define('SITE_URL',			"http://49.212.174.155/aeon_console_makuhari_test/");
		define('DATABASE_NAME',		'aeon_console_makuhari_test');

		set_makuhari_mode();
		define('TESTING', true );
		define('ADD_AUTHORIZED_USER', true ); // ユーザー追加機能

}else if( strpos( dirname(__FILE__), 'html/aeon_console_toin_test') !== false) {
// イオンモール東員（コピーサイト）
		define('SITE_NAME',			"イオンモール東員 （コピーサイト）");
		define('SITE_URL',			"http://49.212.174.155/aeon_console_toin_test/");
		define('DATABASE_NAME',		'aeon_console_toin_test');

		set_toin_mode();
		define('TESTING', true );

}else if( strpos( dirname(__FILE__), 'html/aeon_console_wakayama_test') !== false) {
// イオンモール和歌山（コピーサイト）
		define('SITE_NAME',			"イオンモール和歌山 （コピーサイト）");
		define('SITE_URL',			"http://49.212.174.155/aeon_console_wakayama_test/");
		define('DATABASE_NAME',		'aeon_console_wakayama_test');

		set_wakayama_mode();
		define('TESTING', true );

}else{
	define('SITE_NAME',			"イオンモール ローカルテスト");
	define('DATABASE_NAME',		'aeon_console_testsite');
	define('TESTING', true );

// ***********************************
// Set hosts to use virtual host.
//     c:¥windows¥system32¥drivers¥etc
// ***********************************
	switch( $_SERVER['SERVER_NAME'] ){
	case 'makuhari.host':
		define('SITE_URL', 'http://makuhari.host/');
		set_makuhari_mode();
		define('ADD_AUTHORIZED_USER', true ); // ユーザー追加機能
		break;

	case 'wakayama.host':
		define('SITE_URL', 'http://wakayama.host/');
		set_wakayama_mode();
		break;

	case 'toin.host':
		define('SITE_URL', 'http://toin.host/');
		set_toin_mode();
		break;

	default:
		define('SITE_URL', 'http://toin.host/');
		set_toin_mode();
	}
}

