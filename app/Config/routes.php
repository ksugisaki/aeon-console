<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * Load all plugin routes.  See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * home
 */
Router::connect('/', array('controller' => 'users', 'action' => 'index', 'home'));

if( defined('TESTING')){
/**
 * /users/AdminEdit  ->  /users/AdminPages ( Testing )
 */
	Router::connect('/users/AdminList', array('controller' => 'users', 'action' => 'AdminPages'));
	Router::connect('/users/AdminList/*', array('controller' => 'users', 'action' => 'AdminPages'));
	
	Router::connect('/users/AdminEdit', array('controller' => 'users', 'action' => 'AdminPages'));
	Router::connect('/users/AdminEdit/*', array('controller' => 'users', 'action' => 'AdminPages'));
}else{
/**
 * /users/AdminEdit  ->  /users/AdminList
 */
	Router::connect('/users/AdminList', array('controller' => 'users', 'action' => 'AdminList'));
	Router::connect('/users/AdminList/*', array('controller' => 'users', 'action' => 'AdminList'));
	
	Router::connect('/users/AdminEdit', array('controller' => 'users', 'action' => 'AdminList'));
	Router::connect('/users/AdminEdit/*', array('controller' => 'users', 'action' => 'AdminList'));
/**
 * /users/AdminEdit2  ->  /users/AdminPages ( Testing )
 */
	Router::connect('/users/AdminList2', array('controller' => 'users', 'action' => 'AdminPages'));
	Router::connect('/users/AdminList2/*', array('controller' => 'users', 'action' => 'AdminPages'));
	
	Router::connect('/users/AdminEdit2', array('controller' => 'users', 'action' => 'AdminPages'));
	Router::connect('/users/AdminEdit2/*', array('controller' => 'users', 'action' => 'AdminPages'));
}

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

