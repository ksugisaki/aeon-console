window.onload = function() {

	//console.log('Start! scrollback.js');
	//console.log("sp= " + $.cookie('sp'));
	//console.log("modal= " + $.cookie('modal'));

	// スクロール位置に戻す
	var scrollY = $.cookie('sp');
	window.scrollBy( 0, scrollY );

	// モーダルを表示
	if( $.cookie('modal') == 'on'){
		if( $("div.alert")[0]){
			var message = $("div.alert:first p").text();
			$("#modal_message").text( message );
			$("#mymodal").removeClass('hide');
		}
	}

	// スクロール位置をrequest->dataに保存
	// $(window).scroll(function(){
	// 	var sp = String( $(window).scrollTop());
	// 	$("#yscroll").val( sp );
	// })

	// ボタン・クリック
	$(".btn").click(function(){
		var sp = String( $(window).scrollTop());
		$.cookie('sp', sp );
		$.cookie('modal', 'on');
	})
	// エンター
	$("input").keypress( function( event ) {
		if( event.which === 13 ){
			var sp = String( $(window).scrollTop());
			$.cookie('sp', sp );
			$.cookie('modal', 'on');
		}
	});

	// モーダル閉じるクリック
	$("#close_mymodal").click(function(){
		$("#mymodal").addClass('hide');
		$.cookie('modal', 'off');
		$.cookie('sp', '0');
	})

	tinyMCE.init({
	  mode: "textareas",
	  theme: "advanced",
	  theme_advanced_buttons1: "",
	  theme_advanced_buttons2: "",
	  theme_advanced_toolbar_location: "none",
	  theme_advanced_toolbar_align: "left",
	  theme_advanced_statusbar_location : "bottom",
	  theme_advanced_resizing: true,
	  relative_urls: false
	});

//	$("textarea").tinymce({
//	  script_url: '/js/tiny_mce/tiny_mce.js',
//	  language: 'ja',
//	  theme: "advanced",
//	  theme_advanced_buttons1: "",
//	  theme_advanced_buttons2: "",
//	  theme_advanced_toolbar_location: "none",
//	  theme_advanced_toolbar_align: "left",
//	  theme_advanced_statusbar_location : "none",
//	  theme_advanced_resizing: true,
//	  relative_urls: false
//	});

	$("td.mceStatusbar.mceFirst.mceLast").text("");
}
