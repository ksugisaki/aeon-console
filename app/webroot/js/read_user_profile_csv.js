/*
 * from http://ryu-tan.net/blog/?p=72
 */
function loadTextFile(){
  $(function(){
	var csv_path = $("#csv_path").text();
	$.get( csv_path, function(data){
		var csv = $.csv()(data);
		// 表に出す
		$(csv).each(function(index){
			if(index == 0){ // 一行目はヘッダー
			  $("#result table").append("<tr><th>"
				+this[0]+"</th><th>"
				+this[1]+"</th><th>"
				+this[2]+"</th></tr>"
			  );
			} else {
			  $("#result table").append("<tr><td>"
				+this[0]+"</td><td>"
				+this[1]+"</td><td>"
				+this[2]+"</td></tr>"
			  );
			}
		})

		// Formを作成
		$(csv).each(function(index){
		  if(index != 0){
			$("#import_form").append(
				'<p>'
				+ '<input type="text" name="data[User]['+ index +'][block]"'
				+' value=' + '"' + this[0] + '"' + 'method="post" accept-charset="utf-8">'
				+ '<input type="text" name="data[User]['+ index +'][terminal_num]"'
				+' value=' + '"' + this[1] + '"' + 'method="post" accept-charset="utf-8">'
				+ '<input type="text" name="data[User]['+ index +'][regi_num]"'
				+' value=' + '"' + this[2] + '"' + 'method="post" accept-charset="utf-8">'
				+ '</p><hr />'
			);
		  }
		})
	})
  })
}
