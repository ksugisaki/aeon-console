<script type="text/javascript" src="jquery.js">
</script>
<script type="text/javascript">
	//グローバル変数
	var prefix_order_list = 'order_list_'; //品目入力欄のname属性の接頭辞
	
	//ページ読み込み後に、イベントハンドラを設定
	jQuery(document).ready(function($){
		//--------------------------------------------------------
		//『品目の追加』ボタンを押した場合の処理
		//--------------------------------------------------------
		$('#btn_add').click(function(){
			//品目入力欄を追加
			var len_list = $('#order_list li').length;
			var new_list = '<li><input type="text" size="40" name="' + prefix_order_list + len_list + '"></li>';
			$('#order_list').append(new_list);

			//『削除』ボタンの一旦全消去し、配置し直す
			$('#order_list input[type="button"]').remove();
			len_list++;
			for(var i=0; i<len_list; i++){
				var new_btn = '<input type="button" value="削除">';
				$('#order_list li:eq(' + i + ')').append(new_btn);
			}
				
		});
		//--------------------------------------------------------
		//『削除』ボタンを押した場合の処理
		//--------------------------------------------------------
		$('#order_list input[type="button"]').live('click', function(ev){
			//品目入力欄を削除
			var idx_input = $(ev.target).parent().index();
			$('#order_list li:eq(' + idx_input + ')').remove();

			var len_list = $('#order_list li').length;

			//入力欄がひとつになるなら、削除ボタンは不要なので消去
			if(len_list == 1) $('#order_list input[type="button"]').remove();

			//入力欄の番号を振り直す
			for(var i=0; i<len_list; i++){
				$('#order_list li:eq(' + i + ') input[type="text"]').attr('name', prefix_order_list + i);
			}					
		});
	});
</script>
