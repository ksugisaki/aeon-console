$(document).ready(function(){

	//console.log('Start! scrollback.js');
	//console.log("sp= " + $.cookie('sp'));
	//console.log("modal= " + $.cookie('modal'));

	// スクロール位置に戻す
	var scrollY = $.cookie('sp');
	window.scrollBy( 0, scrollY );

	// モーダルを表示
	if( $.cookie('modal') == 'on'){
		if( $("div.alert")[0]){
			var message = $("div.alert:first p").text();
			$("#modal_message").text( message );
			$("#mymodal").removeClass('hide');
		}
	}

	// スクロール位置をrequest->dataに保存
	// $(window).scroll(function(){
	// 	var sp = String( $(window).scrollTop());
	// 	$("#yscroll").val( sp );
	// })

	// ボタン・クリック
	$(".btn").click(function(){
		var sp = String( $(window).scrollTop());
		$.cookie('sp', sp );
		$.cookie('modal', 'on');
	})
	// エンター
	$("input").keypress( function( event ) {
		if( event.which === 13 ){
			var sp = String( $(window).scrollTop());
			$.cookie('sp', sp );
			$.cookie('modal', 'on');
		}
	});

	// モーダル閉じるクリック
	$("#close_mymodal").click(function(){
		$("#mymodal").addClass('hide');
		$.cookie('modal', 'off');
		$.cookie('sp', '0');
	})

})
