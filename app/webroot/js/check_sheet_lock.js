$(document).ready(function(){

	// ロックのリスト
	item_list = new Array(
		'lock_last_shop_sign',
		'lock_user_profile',
		'lock_section',
		'lock_hearing1',
		'lock_hearing2',
		'lock_signboard',
		'lock_syokujyu',
		'lock_google_map',
		
		'lock_last_hansoku',
		'lock_fukubukuro',
		'lock_media_station',
		'lock_christmas_feature',
		
		'lock_permit_card',
		'lock_pre_car_port',
		'lock_car_port',
		'lock_bicycle',
		'lock_locker',
		'lock_cashbox',
		'lock_fire_drill',
		'lock_bill',
		'lock_mock_operation',
		'lock_emergency_contact',
		'lock_cashier',
		'lock_operation_briefing',
		
		'lock_user_terminal',
		'lock_receipt'
	);

	item_list.forEach( function( item ){
		// 下の全部チェッククラスが付いていたら、一番上のチェックIDを付ける
		var checked_all = true;
		$('input.'+ item).each(function(){
			if( ! $(this).prop('checked')){
				checked_all = false;
				return false;
			}
		});
		if( checked_all ){
			$('#'+ item).attr('checked', 'checked');
		}
	});

	item_list.forEach( function( item ){
		// 一番上のチェックIDが付いたら、下の全部のチェッククラスを付ける
		$(         "#"+ item ).click( function() {
			$('input.'+ item ).attr('checked', this.checked);
		})
	});


})
