// プリントモードでは、エンターによる登録を出来なくする。
// 変数、print_mode にはPHPから設定しておくこと。

$(function() {
	if( print_mode != ''){
		$('form').keypress(function(event) {
			if (event.keyCode == 13) {
				return false;
			}
		});
	}
});
