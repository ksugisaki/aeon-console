<?php
App::uses('AppController', 'Controller');
/**
 * MediaStations Controller
 *
 * @property MediaStation $MediaStation
 */
class MediaStationsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
	
/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_media_station'])?
		               $user_data['User']['lock_media_station'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->MediaStation->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['MediaStation']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if( $lock != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->MediaStation->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->MediaStation->read(null, $id);
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->MediaStation->recursive = 0;
		$this->set('MediaStations', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->MediaStation->id = $id;
		if (!$this->MediaStation->exists()) {
			throw new NotFoundException(__('Invalid %s', __('MediaStation')));
		}
		$this->set('MediaStation', $this->MediaStation->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->MediaStation->id = $id;
		if (!$this->MediaStation->exists()) {
			throw new NotFoundException(__('Invalid %s', __('MediaStation')));
		}
		if ($this->MediaStation->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('MediaStation')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('MediaStation')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

	public function download() {
		$this->layout = false;
		$this->set('filename', SUB_DOMAIN .'_media_station.csv');
		$th = array(
				'ログインID',
				'店名',
				'区画番号',
				'テナントコード',
				'会社名',
				'郵便番号',
				'住所',
				'電話番号',
				'FAX',
				'メールアドレス',
				'所属部署',
				'お名前（フリガナ）',
				'お名前',
				'ご担当者電話番号',
				'内線番号',

				'11時参加者１氏名',
				'11時参加者１フリガナ',
				'11時参加者１役職',
				'11時参加者２氏名',
				'11時参加者２フリガナ',
				'11時参加者２役職',
				'11時参加者３氏名',
				'11時参加者３フリガナ',
				'11時参加者３役職',
				'11時参加者４氏名',
				'11時参加者４フリガナ',
				'11時参加者４役職',
				'11時参加者５氏名',
				'11時参加者５フリガナ',
				'11時参加者５役職',

				'13時半参加者１氏名',
				'13時半参加者１フリガナ',
				'13時半参加者１役職',
				'13時半参加者２氏名',
				'13時半参加者２フリガナ',
				'13時半参加者２役職',
				'13時半参加者３氏名',
				'13時半参加者３フリガナ',
				'13時半参加者３役職',
				'13時半参加者４氏名',
				'13時半参加者４フリガナ',
				'13時半参加者４役職',
				'13時半参加者５氏名',
				'13時半参加者５フリガナ',
				'13時半参加者５役職',

				'16時参加者１氏名',
				'16時参加者１フリガナ',
				'16時参加者１役職',
				'16時参加者２氏名',
				'16時参加者２フリガナ',
				'16時参加者２役職',
				'16時参加者３氏名',
				'16時参加者３フリガナ',
				'16時参加者３役職',
				'16時参加者４氏名',
				'16時参加者４フリガナ',
				'16時参加者４役職',
				'16時参加者５氏名',
				'16時参加者５フリガナ',
				'16時参加者５役職',
		);

		$this->loadModel('User');
		$data_all = $this->User->find('all', array(
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		$td = array();
		foreach($data_all as $key => $data) {
			if( count( $data['MediaStation']) == 0 ) continue;
			
			// 店名、重複しているデータを除く
			if( isset( $data['Signboard'])){
				$all_signboards = $data['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			
			$result = array(
				$data['User']['loginid'],
				$shop_sign,
				$data['User']['block'],
				$data['User']['tenant_code'],
				$data['User']['company'],
				$data['User']['zipcode'],
				$data['User']['address'],
				$data['User']['company_phone'],
				$data['User']['fax'],
				$data['User']['email'],
				$data['User']['department'],
				$data['User']['name_kana'],
				$data['User']['name'],
				$data['User']['phone'],
				$data['User']['extension'],

				$data['MediaStation'][0]['time1_name1'],
				$data['MediaStation'][0]['time1_name_kana1'],
				$data['MediaStation'][0]['time1_position1'],
				$data['MediaStation'][0]['time1_name2'],
				$data['MediaStation'][0]['time1_name_kana2'],
				$data['MediaStation'][0]['time1_position2'],
				$data['MediaStation'][0]['time1_name3'],
				$data['MediaStation'][0]['time1_name_kana3'],
				$data['MediaStation'][0]['time1_position3'],
				$data['MediaStation'][0]['time1_name4'],
				$data['MediaStation'][0]['time1_name_kana4'],
				$data['MediaStation'][0]['time1_position4'],
				$data['MediaStation'][0]['time1_name5'],
				$data['MediaStation'][0]['time1_name_kana5'],
				$data['MediaStation'][0]['time1_position5'],

				$data['MediaStation'][0]['time2_name1'],
				$data['MediaStation'][0]['time2_name_kana1'],
				$data['MediaStation'][0]['time2_position1'],
				$data['MediaStation'][0]['time2_name2'],
				$data['MediaStation'][0]['time2_name_kana2'],
				$data['MediaStation'][0]['time2_position2'],
				$data['MediaStation'][0]['time2_name3'],
				$data['MediaStation'][0]['time2_name_kana3'],
				$data['MediaStation'][0]['time2_position3'],
				$data['MediaStation'][0]['time2_name4'],
				$data['MediaStation'][0]['time2_name_kana4'],
				$data['MediaStation'][0]['time2_position4'],
				$data['MediaStation'][0]['time2_name5'],
				$data['MediaStation'][0]['time2_name_kana5'],
				$data['MediaStation'][0]['time2_position5'],

				$data['MediaStation'][0]['time3_name1'],
				$data['MediaStation'][0]['time3_name_kana1'],
				$data['MediaStation'][0]['time3_position1'],
				$data['MediaStation'][0]['time3_name2'],
				$data['MediaStation'][0]['time3_name_kana2'],
				$data['MediaStation'][0]['time3_position2'],
				$data['MediaStation'][0]['time3_name3'],
				$data['MediaStation'][0]['time3_name_kana3'],
				$data['MediaStation'][0]['time3_position3'],
				$data['MediaStation'][0]['time3_name4'],
				$data['MediaStation'][0]['time3_name_kana4'],
				$data['MediaStation'][0]['time3_position4'],
				$data['MediaStation'][0]['time3_name5'],
				$data['MediaStation'][0]['time3_name_kana5'],
				$data['MediaStation'][0]['time3_position5'],
			);
			$td[] = $result;
		}

		$this->set(compact('th', 'td'));
	}

}
