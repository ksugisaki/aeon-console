<?php
App::uses('AppController', 'Controller');
/**
 * Bills Controller
 *
 * @property Bill $Bill
 */
class BillsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
	
/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('<p>編集権限がありません</p>');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_bill'])?
		               $user_data['User']['lock_bill'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->Bill->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		$this->loadModel('Section');
		$section_data = $this->Section->find('all', array('conditions' => array(
			'user_id' => $user_id
		)));
		$this->set('section_data', $section_data );
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['Bill']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			// 選択情報は保存しない
			$this->request->data['Bill']['use_data_from'] = null;
			if( $lock != 0 ){
				$this->Session->setFlash(
					__('<p>現在、ご登録情報の変更はできません。</p>'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->Bill->save($this->request->data)) {
				$this->Session->setFlash(
					__('<p>保存しました</p>'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('<p>入力内容を確認して下さい</p>'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->Bill->read(null, $id);
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->Bill->recursive = 0;
		$this->set('bills', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->Bill->id = $id;
		if (!$this->Bill->exists()) {
			throw new NotFoundException(__('Invalid %s', __('bill')));
		}
		$this->set('bill', $this->Bill->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Bill->id = $id;
		if (!$this->Bill->exists()) {
			throw new NotFoundException(__('Invalid %s', __('bill')));
		}
		if ($this->Bill->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('bill')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('bill')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */
}
