<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

class AppController extends Controller {

	var $components = array('Auth', 'Session', 'Cookie', 'Permit');
    public $helpers = array(
      'Session',
      'Html' => array('className' => 'TwitterBootstrap.BootstrapHtml'),
      'Form' => array('className' => 'TwitterBootstrap.BootstrapForm'),
      'Paginator' => array('className' => 'TwitterBootstrap.BootstrapPaginator'),
	  'Csv'
    );
	public $view = 'Theme';

	function beforeFilter() {
		parent::beforeFilter();
		
		// for default.po
		Configure::write('Config.language', 'ja');
		// meta information
		$this->set("meta_title", SITE_NAME);
		// ログイン状態なら $user を set
		if( $this->Session->check('Auth.User')) {
			$this->set('user', $this->Session->read('Auth'));
		}
	}

	public function afterFilter() {
	}
}
