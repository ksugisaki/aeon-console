<?php
App::uses('AppController', 'Controller');
/**
 * MockOperations Controller
 *
 * @property MockOperation $MockOperation
 */
class MockOperationsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
	
/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_mock_operation'])?
		               $user_data['User']['lock_mock_operation'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->MockOperation->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['MockOperation']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if( $lock != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->MockOperation->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->MockOperation->read(null, $id);
		}
		if( SUB_DOMAIN == 'toin' || SUB_DOMAIN == 'testsite'){
			$this->render('edit_toin');
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->MockOperation->recursive = 0;
		$this->set('MockOperations', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->MockOperation->id = $id;
		if (!$this->MockOperation->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mock operation')));
		}
		$this->set('MockOperation', $this->MockOperation->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->MockOperation->id = $id;
		if (!$this->MockOperation->exists()) {
			throw new NotFoundException(__('Invalid %s', __('mock operation')));
		}
		if ($this->MockOperation->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('mock operation')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('mock operation')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

	public function download_toin() {
		$this->layout = false;
		$this->set('filename', SUB_DOMAIN .'_mock_operation.csv');
		$th = array(
				'ログインID',
				'店名',
				'区画番号',
				'テナントコード',
				'会社名',
				'郵便番号',
				'住所',
				'電話番号',
				'FAX',
				'メールアドレス',
				'所属部署',
				'お名前（フリガナ）',
				'お名前',
				'ご担当者電話番号',
				'内線番号',

				'連絡先氏名',
				'連絡先フリガナ',
				'連絡先電話',
				'連絡先Email',

				'参加日程',
				'商品の限定',
				'提供メニュー',
				'価格設定',
				'サービス価格詳細',
				'お持ち帰り',
				'その他のサービス',
				'懸念点',
				'従業員さんへのメッセージ',

				'第１希望日程',
				'第１希望開始時刻',
				'第１希望終了時刻',
				'第２希望日程',
				'第２希望開始時刻',
				'第２希望終了時刻',
				'第３希望日程',
				'第３希望開始時刻',
				'第３希望終了時刻',
		);

		$this->loadModel('User');
		$data_all = $this->User->find('all', array(
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		$td = array();
		foreach($data_all as $key => $data) {
			if( count( $data['MockOperation']) == 0 ) continue;
			
			// 店名、重複しているデータを除く
			if( isset( $data['Signboard'])){
				$all_signboards = $data['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			
			$result = array(
				$data['User']['loginid'],
				$shop_sign,
				$data['User']['block'],
				$data['User']['tenant_code'],
				$data['User']['company'],
				$data['User']['zipcode'],
				$data['User']['address'],
				$data['User']['company_phone'],
				$data['User']['fax'],
				$data['User']['email'],
				$data['User']['department'],
				$data['User']['name_kana'],
				$data['User']['name'],
				$data['User']['phone'],
				$data['User']['extension'],

				$data['MockOperation'][0]['name'],
				$data['MockOperation'][0]['name_kana'],
				$data['MockOperation'][0]['phone'],
				$data['MockOperation'][0]['email'],

				$data['MockOperation'][0]['attend_mode'],
				$data['MockOperation'][0]['gentei_mode'],
				$data['MockOperation'][0]['gentei_menu'],
				$data['MockOperation'][0]['kakaku_mode'],
				$data['MockOperation'][0]['kakaku_detail'],
				$data['MockOperation'][0]['takeout_mode'],
				$data['MockOperation'][0]['other_service'],
				$data['MockOperation'][0]['kenen_detail'],
				$data['MockOperation'][0]['message'],

				$data['MockOperation'][0]['prepare_date1'],
				$data['MockOperation'][0]['prepare_start1'],
				$data['MockOperation'][0]['prepare_end1'],
				$data['MockOperation'][0]['prepare_date2'],
				$data['MockOperation'][0]['prepare_start2'],
				$data['MockOperation'][0]['prepare_end2'],
				$data['MockOperation'][0]['prepare_date3'],
				$data['MockOperation'][0]['prepare_start3'],
				$data['MockOperation'][0]['prepare_end3'],
			);
			$td[] = $result;
		}

		$this->set(compact('th', 'td'));
		$this->render('download');
	}

}
