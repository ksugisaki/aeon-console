<?php
class SignboardsController extends AppController
{
    public $helpers = array('Html');
    public
        $uses = Array('Signboard'),
        $components = Array(
            'Session'
        );
    public function beforeFilter()
    {
        parent::beforeFilter();
    }
    public function index( $user_id = null, $print = null )
    {
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
        $this->set('title_for_layout', '店名確認シート');

        $user = $this->Auth->user();
        $this->set('userinfo', $user );
        if( empty( $user_id )){
            $this->redirect(array('action' => 'index/'.$user['id']));
        }
        $this->set('user_id', $user_id);

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_signboard'];
		$this->set('locked', $locked);

		if( $user_id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, $this->params['controller'])){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, $this->params['controller'])&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

        $labels = array();    $fields = array();

        $labels[] = 'id';    $fields[] = 'id';
        $labels[] = 'user_id';    $fields[] = 'user_id';

        $labels[] = 'ご担当者氏名';    $fields[] = 'name';
        $labels[] = 'フリガナ';    $fields[] = 'name_kana';
        $labels[] = '部署';    $fields[] = 'department';
        $labels[] = '役職';    $fields[] = 'position';
		$labels[] = '郵便番号';    $fields[] = 'zipcode';
		$labels[] = '住所';    $fields[] = 'address';
        $labels[] = '電話番号';    $fields[] = 'phone';
        $labels[] = 'FAX';        $fields[] = 'fax';
        $labels[] = '内線番号';    $fields[] = 'extension';
		$labels[] = '携帯番号';        $fields[] = 'cellphone';
        $labels[] = 'メールアドレス';    $fields[] = 'email';

        $labels[] = '店名［正式］<br />（カナor日本語標記）'; $fields[] = 'shop_sign_jpn';
        $labels[] = '店名［正式］<br />（フリガナ）';        $fields[] = 'shop_sign_kana';
        $labels[] = '店名［正式］<br />（英語標記）';        $fields[] = 'shop_sign_eng';
        $labels[] = 'メインの標記方法';    $fields[] = 'shop_sign_main_type';
        $labels[] = 'カテゴリー1';    $fields[] = 'shop_category1';
        $labels[] = 'カテゴリー2';    $fields[] = 'shop_category2';
        $labels[] = 'カテゴリー3';    $fields[] = 'shop_category3';
        $labels[] = 'カテゴリー4';    $fields[] = 'shop_category4';
        $labels[] = 'カテゴリー5';    $fields[] = 'shop_category5';
        $labels[] = '詳細業種';    $fields[] = 'shop_detail';
        $labels[] = '備考';    $fields[] = 'shop_remarks';
        $labels[] = 'テナント電話番号';    $fields[] = 'shop_phone';
        $labels[] = '営業開始時刻';    $fields[] = 'shop_open_time';
        $labels[] = '営業終了時刻';    $fields[] = 'shop_close_time';

        $labels[] = 'オフィシャルサイトURL';    $fields[] = 'shop_url';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = 'ブログURL';    $fields[] = 'blog_url';
		}
        $labels[] = 'キャッチコピー<br />（20文字以内）';    $fields[] = 'shop_copy';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = 'SHOPコンセプト<br />（100文字以内）';    $fields[] = 'shop_concept';
		}else{
			$labels[] = 'SHOPコンセプト<br />（100〜200文字程度）';    $fields[] = 'shop_concept';
		}
		$fields[] = 'modified';
        $this->set('labels', $labels);
        $this->set('fields', $fields);

        $category_labels = array('ファッション','フード','エンターテイメント','グッズ','ライフ');
        $category_options = array();
        $category_options[0] = array(
                                   'レディス',
                                   'メンズ',
                                   'キッズ',
                                   'メンズ・レディス',
                                   'レディス・キッズ',
                                   'ファミリー',
                                   '靴・鞄',
                                   'アクセサリー'
        );
        $category_options[1] = array(
            'カフェ',
            'フードコート',
            '食料品'
        );
        $category_options[2] = array(
            '書籍',
            '音楽・楽器',
            'アミューズメント'
        );
        $category_options[3] = array(
            'インテリア',
            'キッチン',
            'スポーツ',
            'おもちゃ',
            '文具',
            '眼鏡・コンタクト',
            'ヘルス＆ビューティー'
        );
        $category_options[4] = array(
            'サービス',
            '銀行',
            '保険',
            '趣味',
            '病院',
            'ヘルス＆ビューティー',
            'スクール'
        );
        $this->set('category_labels', $category_labels );
        $this->set('category_options', $category_options );

        if ($this->request->is('get')) {
			$signboards = $this->Signboard->find('all',array(
				'fields'=>$fields,
				'order'=>array("Signboard.created","Signboard.modified"),
				'conditions'=>array('user_id'=>$user_id,"Signboard.deleted"=> 0 )
			));
			if( $signboards )
				$this->request->data = $signboards[ count( $signboards ) -1 ];
			else
				$this->request->data = null;

			$this->loadModel('User');
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$user_id )
			));
			$this->set('user_info', $user['User'] );
            // アンシリアライズ
            if( isset( $this->request->data['Signboard']['shop_category1']))
                       $this->request->data['Signboard']['shop_category1'] = unserialize( 
                       $this->request->data['Signboard']['shop_category1']);
            if( isset( $this->request->data['Signboard']['shop_category2']))
                       $this->request->data['Signboard']['shop_category2'] = unserialize( 
                       $this->request->data['Signboard']['shop_category2']);
            if( isset( $this->request->data['Signboard']['shop_category3']))
                       $this->request->data['Signboard']['shop_category3'] = unserialize( 
                       $this->request->data['Signboard']['shop_category3']);
            if( isset( $this->request->data['Signboard']['shop_category4']))
                       $this->request->data['Signboard']['shop_category4'] = unserialize( 
                       $this->request->data['Signboard']['shop_category4']);
            if( isset( $this->request->data['Signboard']['shop_category5']))
                       $this->request->data['Signboard']['shop_category5'] = unserialize( 
                       $this->request->data['Signboard']['shop_category5']);

			$this->set('modal_message', '' );

		}else{ // post
			if( $user_id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, $this->params['controller'])){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}
			// Lock確認
			if( $locked ){
				$message = __('<p>現在、ご登録情報の変更はできません。</p>');
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect('/');
			}

            $data = $this->request->data;
            // シリアライズ
            $data['Signboard']['shop_category1'] = serialize( $data['Signboard']['shop_category1']);
            $data['Signboard']['shop_category2'] = serialize( $data['Signboard']['shop_category2']);
            $data['Signboard']['shop_category3'] = serialize( $data['Signboard']['shop_category3']);
            $data['Signboard']['shop_category4'] = serialize( $data['Signboard']['shop_category4']);
            $data['Signboard']['shop_category5'] = serialize( $data['Signboard']['shop_category5']);

			$res = $this->Signboard->save( $data );
			if ( $res ) {
				$this->request->data['Signboard']['id'] = $res['Signboard']['id'];
				$message = __('<p>登録成功しました。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect(array('action' => 'index/'.$user_id));
			}else{
				$message = __('<p>登録失敗しました。入力内容をお確かめください。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
        }
    }

	public function DownloadAllCsv()
	{
		$this->set('title_for_layout', '店名情報エクスポート');

		$labels = array();	  $fields = array();

		$labels[] = 'id';	 $fields[] = 'id';
		$labels[] = 'user_id';	  $fields[] = 'user_id';

		$labels[] = '会社名';		   $fields[] = 'company';
		$labels[] = 'ご担当者氏名';	   $fields[] = 'name';
		$labels[] = 'フリガナ';	   $fields[] = 'name_kana';
		$labels[] = '部署';	   $fields[] = 'department';
		$labels[] = '役職';	   $fields[] = 'position';
		$labels[] = '郵便番号';	   $fields[] = 'zipcode';
		$labels[] = '住所';	   $fields[] = 'address';
		$labels[] = '電話番号';	   $fields[] = 'phone';
		$labels[] = 'FAX';		  $fields[] = 'fax';
		$labels[] = '内線番号';	   $fields[] = 'extension';
		$labels[] = '携帯番号';		   $fields[] = 'cellphone';
		$labels[] = 'メールアドレス';	 $fields[] = 'email';

		$labels[] = '店名［正式］（カナor日本語標記）'; $fields[] = 'shop_sign_jpn';
		$labels[] = '店名［正式］（フリガナ）';		 $fields[] = 'shop_sign_kana';
		$labels[] = '店名［正式］（英語標記）';		 $fields[] = 'shop_sign_eng';
		$labels[] = 'メインの標記方法';	   $fields[] = 'shop_sign_main_type';
		$labels[] = 'カテゴリー1';	  $fields[] = 'shop_category1';
		$labels[] = 'カテゴリー2';	  $fields[] = 'shop_category2';
		$labels[] = 'カテゴリー3';	  $fields[] = 'shop_category3';
		$labels[] = 'カテゴリー4';	  $fields[] = 'shop_category4';
		$labels[] = 'カテゴリー5';	  $fields[] = 'shop_category5';
		$labels[] = '詳細業種';	   $fields[] = 'shop_detail';
		$labels[] = '備考';	   $fields[] = 'shop_remarks';
		$labels[] = 'テナント電話番号';	   $fields[] = 'shop_phone';
		$labels[] = '営業開始時刻';	   $fields[] = 'shop_open_time';
		$labels[] = '営業終了時刻';	   $fields[] = 'shop_close_time';

		$labels[] = 'オフィシャルサイトURL';	$fields[] = 'shop_url';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = 'ブログURL';    $fields[] = 'blog_url';
		}
		$labels[] = 'キャッチコピー';	 $fields[] = 'shop_copy';
		$labels[] = 'SHOPコンセプト';	   $fields[] = 'shop_concept';

		// User.loginid
		$fields[] = 'User.loginid';
		$fields[] = 'User.company';

		$this->set('labels', $labels);
		$this->set('fields', $fields);

		$category_labels = array('ファッション','フード','エンターテイメント','グッズ','ライフ');
		$category_options = array();
		$category_options[0] = array(
								   'レディス',
								   'メンズ',
								   'キッズ',
								   'メンズ・レディス',
								   'レディス・キッズ',
								   'ファミリー',
								   '靴・鞄',
								   'アクセサリー'
		);
		$category_options[1] = array(
			'カフェ',
			'フードコート',
			'食料品'
		);
		$category_options[2] = array(
			'書籍',
			'音楽・楽器',
			'アミューズメント'
		);
		$category_options[3] = array(
			'インテリア',
			'キッチン',
			'スポーツ',
			'おもちゃ',
			'文具',
			'眼鏡・コンタクト',
			'ヘルス＆ビューティー'
		);
		$category_options[4] = array(
			'サービス',
			'銀行',
			'保険',
			'趣味',
			'病院',
			'ヘルス＆ビューティー',
			'スクール'
		);
		$this->set('category_labels', $category_labels );
		$this->set('category_options', $category_options );

		if ($this->request->is('get')) {
			$all_signboards = $this->Signboard->find('all',array(
				'fields'=>$fields,
				'order'=>array('User.role'=>'',"Signboard.user_id","Signboard.created","Signboard.modified"),
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
			));

			// user_id重複しているデータを除く
			$signboards = array();
			$prev_user_id = 0;
			foreach( $all_signboards as $signboard){
				if( $signboard['Signboard']['user_id'] == $prev_user_id ) // 同じなら
					$signboards[ count($signboards) -1 ] = $signboard; // 上書き
				else
					$signboards[] = $signboard;
				$prev_user_id = $signboard['Signboard']['user_id'];
			}

			// アンシリアライズ
			foreach( $signboards as $key=>$signboard ){
				if( isset( $signboards[$key]['Signboard']['shop_category1']))
						   $signboards[$key]['Signboard']['shop_category1'] = unserialize( 
						   $signboards[$key]['Signboard']['shop_category1']);
				if( isset( $signboards[$key]['Signboard']['shop_category2']))
						   $signboards[$key]['Signboard']['shop_category2'] = unserialize( 
						   $signboards[$key]['Signboard']['shop_category2']);
				if( isset( $signboards[$key]['Signboard']['shop_category3']))
						   $signboards[$key]['Signboard']['shop_category3'] = unserialize( 
						   $signboards[$key]['Signboard']['shop_category3']);
				if( isset( $signboards[$key]['Signboard']['shop_category4']))
						   $signboards[$key]['Signboard']['shop_category4'] = unserialize( 
						   $signboards[$key]['Signboard']['shop_category4']);
				if( isset( $signboards[$key]['Signboard']['shop_category5']))
						   $signboards[$key]['Signboard']['shop_category5'] = unserialize( 
						   $signboards[$key]['Signboard']['shop_category5']);
			}
		}
		
		//debug($signboards);exit();
		$this->set('signboards', $signboards);
		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;

		$filename = SUB_DOMAIN . '_signboard_list';
		$th = $labels;
		$this->set(compact('filename', 'th'));

	}

}
?>
