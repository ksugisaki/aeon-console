<?php
App::uses('AppController', 'Controller');
/**
 * Fukubukuros Controller
 *
 * @property Fukubukuro $Fukubukuro
 */
class FukubukurosController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
	
/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_fukubukuro'])?
		               $user_data['User']['lock_fukubukuro'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->Fukubukuro->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['Fukubukuro']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if( $lock != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->Fukubukuro->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->Fukubukuro->read(null, $id);
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->Fukubukuro->recursive = 0;
		$this->set('fukubukuros', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->Fukubukuro->id = $id;
		if (!$this->Fukubukuro->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fukubukuro')));
		}
		$this->set('fukubukuro', $this->Fukubukuro->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Fukubukuro->id = $id;
		if (!$this->Fukubukuro->exists()) {
			throw new NotFoundException(__('Invalid %s', __('fukubukuro')));
		}
		if ($this->Fukubukuro->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('fukubukuro')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('fukubukuro')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

	public function download() {
		$this->layout = false;
		$this->set('filename', SUB_DOMAIN .'_fukubukuro.csv');
		$th = array(
				'ログインID',
				'店名',
				'区画番号',
				'テナントコード',
				'会社名',
				'郵便番号',
				'住所',
				'電話番号',
				'FAX',
				'メールアドレス',
				'所属部署',
				'お名前（フリガナ）',
				'お名前',
				'ご担当者電話番号',
				'内線番号',

				'部署',
				'氏名',
				'フリガナ',
				'役職',
				'ご連絡先',

				'福袋実施',
				'しない理由',
				'決定時期',

				'販売総数',
				'販売総額',

				'福袋1名前',
				'福袋1単価(税込)',
				'福袋1個数',
				'福袋1内容',

				'福袋2名前',
				'福袋2単価(税込)',
				'福袋2個数',
				'福袋2内容',

				'福袋3名前',
				'福袋3単価(税込)',
				'福袋3個数',
				'福袋3内容',

				'福袋4名前',
				'福袋4単価(税込)',
				'福袋4個数',
				'福袋4内容',

				'福袋5名前',
				'福袋5単価(税込)',
				'福袋5個数',
				'福袋5内容',

				'福袋6名前',
				'福袋6単価(税込)',
				'福袋6個数',
				'福袋6内容',

				'福袋7名前',
				'福袋7単価(税込)',
				'福袋7個数',
				'福袋7内容',

				'福袋8名前',
				'福袋8単価(税込)',
				'福袋8個数',
				'福袋8内容',

				'予約の有無',
				'予約開始日',
				'個数制限',
				'制限個数',
				'予約方法',

				'倉庫利用',
				'希望坪数',
				'利用開始日',
				'利用最終日',
		);

		$this->loadModel('User');
		$data_all = $this->User->find('all', array(
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		$td = array();
		foreach($data_all as $key => $data) {
			if( count( $data['Fukubukuro']) == 0 ) continue;
			
			// 店名、重複しているデータを除く
			if( isset( $data['Signboard'])){
				$all_signboards = $data['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			
			$result = array(
				$data['User']['loginid'],
				$shop_sign,
				$data['User']['block'],
				$data['User']['tenant_code'],
				$data['User']['company'],
				$data['User']['zipcode'],
				$data['User']['address'],
				$data['User']['company_phone'],
				$data['User']['fax'],
				$data['User']['email'],
				$data['User']['department'],
				$data['User']['name_kana'],
				$data['User']['name'],
				$data['User']['phone'],
				$data['User']['extension'],

				$data['Fukubukuro'][0]['department'],
				$data['Fukubukuro'][0]['name'],
				$data['Fukubukuro'][0]['name_kana'],
				$data['Fukubukuro'][0]['position'],
				$data['Fukubukuro'][0]['phone'],

				$data['Fukubukuro'][0]['fukubukuro_hanbai'],
				$data['Fukubukuro'][0]['fukubukuro_riyuu'],
				$data['Fukubukuro'][0]['fukubukuro_jiki'],

				$data['Fukubukuro'][0]['hanbai_suu'],
				$data['Fukubukuro'][0]['hanbai_sou_gaku'],

				$data['Fukubukuro'][0]['fukubukuro1_name'],
				$data['Fukubukuro'][0]['fukubukuro1_tanka'],
				$data['Fukubukuro'][0]['fukubukuro1_suu'],
				$data['Fukubukuro'][0]['fukubukuro1_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro2_name'],
				$data['Fukubukuro'][0]['fukubukuro2_tanka'],
				$data['Fukubukuro'][0]['fukubukuro2_suu'],
				$data['Fukubukuro'][0]['fukubukuro2_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro3_name'],
				$data['Fukubukuro'][0]['fukubukuro3_tanka'],
				$data['Fukubukuro'][0]['fukubukuro3_suu'],
				$data['Fukubukuro'][0]['fukubukuro3_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro4_name'],
				$data['Fukubukuro'][0]['fukubukuro4_tanka'],
				$data['Fukubukuro'][0]['fukubukuro4_suu'],
				$data['Fukubukuro'][0]['fukubukuro4_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro5_name'],
				$data['Fukubukuro'][0]['fukubukuro5_tanka'],
				$data['Fukubukuro'][0]['fukubukuro5_suu'],
				$data['Fukubukuro'][0]['fukubukuro5_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro6_name'],
				$data['Fukubukuro'][0]['fukubukuro6_tanka'],
				$data['Fukubukuro'][0]['fukubukuro6_suu'],
				$data['Fukubukuro'][0]['fukubukuro6_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro7_name'],
				$data['Fukubukuro'][0]['fukubukuro7_tanka'],
				$data['Fukubukuro'][0]['fukubukuro7_suu'],
				$data['Fukubukuro'][0]['fukubukuro7_naiyou'],

				$data['Fukubukuro'][0]['fukubukuro8_name'],
				$data['Fukubukuro'][0]['fukubukuro8_tanka'],
				$data['Fukubukuro'][0]['fukubukuro8_suu'],
				$data['Fukubukuro'][0]['fukubukuro8_naiyou'],

				$data['Fukubukuro'][0]['yoyaku'],
				$data['Fukubukuro'][0]['yoyaku_kaisi'],
				$data['Fukubukuro'][0]['yoyaku_seigen'],
				$data['Fukubukuro'][0]['yoyaku_seigen_suu'],
				$data['Fukubukuro'][0]['yoyaku_houhou'],

				$data['Fukubukuro'][0]['souko_riyou'],
				$data['Fukubukuro'][0]['souko_tubo_suu'],
				$data['Fukubukuro'][0]['souko_riyou_kaisi'],
				$data['Fukubukuro'][0]['souko_riyou_saigo'],

			);
			$td[] = $result;
		}

		$this->set(compact('th', 'td'));
	}

}
