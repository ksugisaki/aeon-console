<?php 
App::uses('AppController', 'Controller');

class PrintController extends AppController {

	public $uses = array('Section');

	public function sections(){
		// 店舗カルテ
		$section_names = array(
			'eigyou',
			'tenpo_kaihatu',
			'keiri',
			'kouhou',
			'hannyuu',
			'remarks');
		//$section_name['eigyou'] ='【営業部門】';
		//$section_name['tenpo_kaihatu'] ='【店舗開発部門】';
		//$section_name['keiri'] ='【経理部門】';
		//$section_name['kouhou'] ='【広報部門】';
		//$section_name['hannyuu'] ='【搬入部門】';
		//$section_name['remarks'] ='【備考】';
		$this->layout = false;
		// ファイル名
		$filename = SUB_DOMAIN . '_section_all_list';
		// 表のフィールド
		$user_fields = array('loginid','company','zipcode','address','company_phone','fax',
			'email','department','name_kana','name','phone','extension');
		$section_fields = array('section_name','department','position','name','name_kana',
			'zipcode','address','phone','fax','cellphone','email','remarks');
		$this->Section->recursive = 0;
		$this->loadModel('User');
		// アソシエーションの条件
		$this->User->hasMany['Section']['fields'] = $section_fields;
		// データの読み出し
		$users = $this->User->find('all', array(
				'fields' => $user_fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		//debug( $users );exit();

		// 表データの整形
		foreach( $users as $user ){
			unset( $user['User']['id'] ); // idは除く

			$sections = array();
			foreach( $user['Section'] as $section ){
				$section_name = $section['section_name'];
				unset( $section['id'] ); // 除く
				unset( $section['user_id'] ); // 除く
				unset( $section['section_name'] ); // 除く
				if( $section_name != 'remarks'){
					unset( $section['remarks'] ); // 除く
					$sections[ $section_name ] = array_values( $section );
				}else{
					$sections[ $section_name ] = array( $section['remarks']);
				}
			}
			//debug($sections);
			// 重複しているデータを除く
			if( isset( $user['Signboard'])){
				$all_signboards = $user['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			
			$sections_row = array();
			foreach( $section_names as $section_name ){
				if( ! isset( $sections[ $section_name ])){
					$sections_row = array_merge( $sections_row, array('','','','','','','','','',''));
				}else{
					$sections_row = array_merge( $sections_row, array_values( $sections[ $section_name ]));
				}
			}
			//debug($sections_row);exit();

			$td[] = array_merge(
						array_values( $user['User']),
						array( $shop_sign ),
						$sections_row
			);
		}
		// データをセット
		$this->set(compact('td'));
	}


	public function section( $user_id ){
		$section_names = array(
			'eigyou',
			'tenpo_kaihatu',
			'keiri',
			'kouhou',
			'hannyuu',
			'remarks');
		//$section_name['eigyou'] ='【営業部門】';
		//$section_name['tenpo_kaihatu'] ='【店舗開発部門】';
		//$section_name['keiri'] ='【経理部門】';
		//$section_name['kouhou'] ='【広報部門】';
		//$section_name['hannyuu'] ='【搬入部門】';
		//$section_name['remarks'] ='【備考】';
		$this->layout = false;
		// ファイル名
		$filename = SUB_DOMAIN .'_'. $user_id .'_section_list';
		// 表のフィールド
		$user_fields = array('loginid','company','zipcode','address','company_phone','fax',
			'email','department','name_kana','name','phone','extension');
		$section_fields = array('section_name','department','position','name_kana','name',
			'zipcode','address','phone','fax','cellphone','email','remarks');
		$this->Section->recursive = 0;
		$this->loadModel('User');
		// アソシエーションの条件
		$this->User->hasMany['Section']['fields'] = $section_fields;
		// データの読み出し
		$users = $this->User->find('all', array(
				'fields' => $user_fields,
				'conditions'=> array('id'=>$user_id,
									'user_status'=>'authorized')
		));
		//debug( $users );exit();

		if( $users ){
			// 表データの整形
			foreach( $users as $user ){
				unset( $user['User']['id'] ); // idは除く
	
				$sections = array();
				foreach( $user['Section'] as $section ){
					$section_name = $section['section_name'];
					unset( $section['id'] ); // 除く
					unset( $section['user_id'] ); // 除く
					unset( $section['section_name'] ); // 除く
					if( $section_name != 'remarks'){
						unset( $section['remarks'] ); // 除く
						$sections[ $section_name ] = array_values( $section );
					}else{
						$sections[ $section_name ] = array( $section['remarks']);
					}
				}
				//debug($sections);
				// 重複しているデータを除く
				if( isset( $user['Signboard'])){
					$all_signboards = $user['Signboard'];
					$signboard_last = array();
					$prev_modified = 0;
					foreach( $all_signboards as $signboard){
						if( $signboard['modified'] > $prev_modified ) // 新しいなら
							$signboard_last = $signboard; // 上書き
						$prev_modified = $signboard['modified'];
					}
				}
				if( isset( $signboard_last['shop_sign']))
					$shop_sign = $signboard_last['shop_sign'];
				else
					$shop_sign = '';

				
				$sections_row = array();
				foreach( $section_names as $section_name ){
					if( ! isset( $sections[ $section_name ])){
						$sections_row = array_merge( $sections_row, array('','','','','','','','','',''));
					}else{
						$sections_row = array_merge( $sections_row, array_values( $sections[ $section_name ]));
					}
				}
				//debug($sections_row);exit();
	
				$td[] = array_merge(
							array_values( $user['User']),
							array( $shop_sign ),
							$sections_row
				);
			}
		}else{
			// 空データ
			$td = array(array('','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''));
		}
		// データをセット
		$this->set(compact('td'));
	}

}
