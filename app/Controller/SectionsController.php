<?php
App::uses('AppController', 'Controller');
/**
 * Sections Controller
 *
 * @property Section $Section
 */
class SectionsController extends AppController {

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

/**
 * 
 * Components
 *
 * @var array
 */
	public $components = array('Session');

	public function addSection( $user_id = null, $tab = null ) {

        /*
         * 'user_status'=>'authorized' じゃないと 
         *    add 出来ないようにしなきゃ。
         */

        $this->set('title_for_layout', '担当者確認シート');
        $user = $this->Auth->user();
        if( empty( $user_id )){
            $this->redirect(array('action' => 'addSection/'.$user['id']));
        }
        $this->set('user_id', $user_id );

		$this->set('tab_index', ( $tab == null )); // タブ指定が無いなら、インデックスを表示
		$this->set('tab', $tab );

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_section'];
		$this->set('locked', $locked);

		if( $user_id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, $this->params['controller'])){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, $this->params['controller'])&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

        if ($this->request->is('get')) {
            $section_fields = array(
                           'eigyou',
                           'tenpo_kaihatu',
                           'keiri',
                           'kouhou',
                           'hannyuu',
                           'remarks');
            $data = array();
            foreach( $section_fields as $sec_key => $section ){
                $data[ $sec_key ] = $this->Section->find('first',array(
                    'conditions'=>array('user_id' => $user_id,
                                        'section_name' => $section)
                ));
            }
            $this->set('data', $data);
        }else{ // post
			if( $user_id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, $this->params['controller'])){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}

            $data = array();
            $data[ $tab ] = $this->request->data;
            $this->set('data', $data);

            $wdata = $this->request->data['Section'];

			// Lock確認
			if( !empty( $locked )){
				$this->Session->setFlash(__('現在、ご登録情報の変更はできません。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				if( $tab ){
					$mark = '/'. $tab;
				}else{
					$mark = '';
				}
				$this->redirect(array('action' => 'addSection/'. $user_id . $mark ));
				$res = false;
			}else{
				$res = $this->Section->save( $wdata );
			}
			if( $res ){
                $this->Session->setFlash(__('登録成功しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
                if( $tab ){
                    $mark = '/'. $tab;
                }else{
                    $mark = '';
                }
                //$this->redirect(array('action' => 'addSection/'. $user_id . $mark ));
            }
		}
	}

	public function eigyou_section( $user_id = null ) {
        $this->set('title_for_layout', '担当者確認シート');
        $user = $this->Auth->user();
        if( empty( $user_id )){
            $this->redirect(array('action' => 'eigyou_section/'.$user['id']));
        }
        $this->set('user_id', $user_id);
        $data = $this->Section->find('first',array(
                'conditions'=>array('user_id' => $user_id, 'section_name' => 'eigyou')
        ));
        $this->set('data', $data);
		if ($this->request->is('post')) {
            $wdata = $this->request->data['Section'];
            $res = $this->Section->save( $wdata );
            if( $res === false ){
                $this->Session->setFlash(__('登録失敗しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else{
                $this->Session->setFlash(__('登録成功しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
                $this->redirect( array('action' =>'section'));
            }
		}
	}
	public function viewSection( $section_name = null ) {
        $this->set('title_for_layout', '担当者一覧');
        $this->set('section_name', $section_name);
        switch( $section_name ){
        case 'eigyou':
            $section_label = '【営業部門】';
            break;
        case 'tenpo_kaihatu':
            $section_label = '【店舗開発部門】';
            break;
        case 'keiri':
            $section_label = '【経理部門】';
            break;
        case 'kouhou':
            $section_label = '【広報部門】';
            break;
        case 'hannyuu':
            $section_label = '【搬入部門】';
            break;
        case 'remarks':
            $section_label = '【備考】';
            break;
        default:
            $section_label = '';
            break;
        }
        $this->set('section_label', $section_label );

        if( empty( $section_name )){
            $section_name = 'eigyou';
        }
		$this->Section->recursive = 0;
        $this->loadModel('User');
        $users = $this->paginate('User', unserialize( TENANT_USER_AUTHORIZED ));
        foreach( $users as $userKey => $user ){
            $sections[ $userKey ] = array();
			// 店名、重複しているデータを除く
			if( isset( $user['Signboard'])){
				$all_signboards = $user['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = array('shop_sign'=> $signboard_last['shop_sign']);
			else
				$shop_sign = array('shop_sign'=>null);
			$sections[ $userKey ]['Signboard'] = $shop_sign;
			// 部門
            if( count( $user['Section'])){
                foreach( $user['Section'] as $section ){
                    if( $section['section_name'] == $section_name ){
                        $sections[ $userKey ]['Section'] = $section;
                    }
                }
            }else{
                $sections[ $userKey ]['Section'] = array();
            }
			// 電話番号（御社情報）
			if( empty( $user['User']['phone'] )){
				$user['User']['phone'] = $user['User']['company_phone'];
			}
            $sections[ $userKey ]['User'] = $user['User'];
        }
        $this->set('sections', $sections);
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->Section->recursive = 0;
		$this->set('sections', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid %s', __('section')));
		}
		$this->set('section', $this->Section->read(null, $id));
	}
 */

/**
 * add method
 *
 * @return void
	public function add() {
		if ($this->request->is('post')) {
			$this->Section->create();
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$users = $this->Section->User->find('list');
		$this->set(compact('users'));
	}
 */

/**
 * edit method
 *
 * @param string $id
 * @return void
	public function edit($id = null) {
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid %s', __('section')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Section->read(null, $id);
		}
		$users = $this->Section->User->find('list');
		$this->set(compact('users'));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid %s', __('section')));
		}
		if ($this->Section->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('section')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('section')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

/**
 * admin_index method
 *
 * @return void
	public function admin_index() {
		$this->Section->recursive = 0;
		$this->set('sections', $this->paginate());
	}
 */

/**
 * admin_view method
 *
 * @param string $id
 * @return void
	public function admin_view($id = null) {
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid %s', __('section')));
		}
		$this->set('section', $this->Section->read(null, $id));
	}
 */

/**
 * admin_add method
 *
 * @return void
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Section->create();
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$users = $this->Section->User->find('list');
		$this->set(compact('users'));
	}
 */

/**
 * admin_edit method
 *
 * @param string $id
 * @return void
	public function admin_edit($id = null) {
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid %s', __('section')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Section->save($this->request->data)) {
				$this->Session->setFlash(
					__('The %s has been saved', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(
					__('The %s could not be saved. Please, try again.', __('section')),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->Section->read(null, $id);
		}
		$users = $this->Section->User->find('list');
		$this->set(compact('users'));
	}
 */

/**
 * admin_delete method
 *
 * @param string $id
 * @return void
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Section->id = $id;
		if (!$this->Section->exists()) {
			throw new NotFoundException(__('Invalid %s', __('section')));
		}
		if ($this->Section->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('section')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('section')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

	function download_csv( $section ){
	Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		// ファイル名
		$filename = SUB_DOMAIN . '_section_' . $section . '_list';
		// 表のヘッダー
		$section_fields = array('department','position','name','zipcode','address','phone','fax','cellphone','email');
		$user_fields = array('loginid','receipt_tenant_name','company');
		
		$this->Section->recursive = 0;
		$this->loadModel('User');
		// アソシエーションの条件
		$this->User->hasMany['Section']['conditions'] = "Section.section_name='" . $section . "'";
		$this->User->hasMany['Section']['fields'] = $section_fields;
		// データの読み出し
		$users = $this->User->find('all', array(
				'fields' => $user_fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		// 表データの整形
		foreach( $users as $user ){
			// 店名、重複しているデータを除く
			if( isset( $user['Signboard'])){
				$all_signboards = $user['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = array( $signboard_last['shop_sign']);
			else
				$shop_sign = array('');

			unset( $user['User']['id']);
			if( isset( $user['Section'][0])){
				unset( $user['Section'][0]['user_id'] ); // user_idは除く
				$td[] = array_merge( $user['User'], $shop_sign, $user['Section'][0]);
			} else{
				$td[] = array_merge( $user['User'], $shop_sign );
			}
		}
		// データをセット
		$th = array('ログインID','テナント名','会社名','店名','部署','役職','担当者名','郵便番号','住所','電話番号','FAX','携帯','メールアドレス');
		$this->set(compact('filename', 'th', 'td'));
	}

	function DownloadAllCsv(){
		//Configure::write('debug', 0); // 警告を出さない
		$section_names = array(
			'eigyou',
			'tenpo_kaihatu',
			'keiri',
			'kouhou',
			'hannyuu',
			'remarks');
		//$section_name['eigyou'] ='【営業部門】';
		//$section_name['tenpo_kaihatu'] ='【店舗開発部門】';
		//$section_name['keiri'] ='【経理部門】';
		//$section_name['kouhou'] ='【広報部門】';
		//$section_name['hannyuu'] ='【搬入部門】';
		//$section_name['remarks'] ='【備考】';
		$this->layout = false;
		// ファイル名
		$filename = SUB_DOMAIN . '_section_all_list';
		// 表のフィールド
		$user_fields = array('loginid','company','zipcode','address','company_phone','fax',
			'email','department','name_kana','name','phone','extension');
		$section_fields = array('section_name','department','position','name',
			'zipcode','address','phone','extension','fax','cellphone','email','remarks');
		$this->Section->recursive = 0;
		$this->loadModel('User');
		// アソシエーションの条件
		$this->User->hasMany['Section']['fields'] = $section_fields;
		// データの読み出し
		$users = $this->User->find('all', array(
				'fields' => $user_fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		//debug( $users );exit();

		// 表データの整形
		foreach( $users as $user ){
			unset( $user['User']['id'] ); // idは除く

			$sections = array();
			foreach( $user['Section'] as $section ){
				$section_name = $section['section_name'];
				unset( $section['id'] ); // 除く
				unset( $section['user_id'] ); // 除く
				unset( $section['section_name'] ); // 除く
				if( $section_name != 'remarks'){
					unset( $section['remarks'] ); // 除く
					$sections[ $section_name ] = array_values( $section );
				}else{
					$sections[ $section_name ] = array( $section['remarks']);
				}
			}
			//debug($sections);

			// 重複しているデータを除く
			if( isset( $user['Signboard'])){
				$all_signboards = $user['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';

			$sections_row = array();
			foreach( $section_names as $section_name ){
				if( ! isset( $sections[ $section_name ])){
					$sections_row = array_merge( $sections_row, array('','','','','','','','','',''));
				}else{
					$sections_row = array_merge( $sections_row, array_values( $sections[ $section_name ]));
				}
			}
			//debug($sections_row);exit();

			$td[] = array_merge(
						array_values( $user['User']),
						array( $shop_sign ),
						$sections_row
			);
		}
		//debug( $td );exit();
		// データをセット
		$th = array('ログインID','会社名','郵便番号','住所','会社電話番号','FAX',
			'メールアドレス','部署','フリガナ','お名前','電話番号','内線番号',
			'店名',

			'営業部署','営業役職','営業担当者',
			'営業郵便番号','営業住所','営業電話番号','営業内線','営業FAX','営業携帯','営業メールアドレス',

			'店舗部署','店舗役職','店舗担当者',
			'店舗郵便番号','店舗住所','店舗電話番号','店舗内線','店舗FAX','店舗携帯','店舗メールアドレス',

			'経理部署','経理役職','経理担当者',
			'経理郵便番号','経理住所','経理電話番号','経理内線','経理FAX','経理携帯','経理メールアドレス',

			'広報部署','広報役職','広報担当者',
			'広報郵便番号','広報住所','広報電話番号','広報内線','広報FAX','広報携帯','広報メールアドレス',

			'搬入部署','搬入役職','搬入担当者',
			'搬入郵便番号','搬入住所','搬入電話番号','搬入内線','搬入FAX','搬入携帯','搬入メールアドレス',

			'備考'
		);
		$this->set(compact('filename', 'th', 'td'));
	}
}
