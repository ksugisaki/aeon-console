<?php
class HearingsController extends AppController
{
    public $helpers = array('Html');
    public
        $uses = Array('Hearing'),
        $components = Array(
            'Mall',
            'Session'
        );
    public function beforeFilter()
    {
        parent::beforeFilter();
    }

	// 全ユーザーの予算リスト表示
	public function AllYosanList()
	{
		$this->Hearing->recursive = 0;
		$this->loadModel('User');
		// アソシエーションの条件
		$user_fields = array('id','receipt_tenant_name','company');
		$this->User->hasMany['Hearing']['fields'] = 'uriage_yosan';
		$hearings = $this->User->find('all',array(
			'fields'=> $user_fields,
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$start = 4; // 11,12,1,2月の後から次年度
			$getu_num = 16;
		}else{
			$start = 3; // 12,1,2月の後から次年度
			$getu_num = 15;
		}

		foreach( $hearings as $data ){
			$sum0=0; $sum1=0; $sum2=0; $sum3=0;
			$user_id = '';
			$tname = '';
			$company = '';
			if( isset( $data )){
				if( isset( $data['Hearing'][0] )){
					$uriage_yosan = unserialize( $data['Hearing'][0]['uriage_yosan'] );
					$uriage_yosan = array_slice( $uriage_yosan, 0, $getu_num);
					$sum0 = array_sum( array_slice( $uriage_yosan, 0, $start));
					$sum1 = array_sum( array_slice( $uriage_yosan, $start, 6));
					$sum2 = array_sum( array_slice( $uriage_yosan, $start +6, 6));
					$sum3 = $sum1 + $sum2;
				}else{
					$uriage_yosan = null;
				}
				$user_id = $data['User']['id'];
				$tname = $data['User']['receipt_tenant_name'];
				$company = $data['User']['company'];
			}
			$datas[] = array(
				'user_id'=> $user_id,
				'tname'=> $tname,
				'company'=> $company,
				'uriage_yosan'=> $uriage_yosan,
				'sum0' => $sum0,
				'sum1' => $sum1,
				'sum2' => $sum2,
				'sum3' => $sum3
			);
		}

		if( $this->request->is('get')) {
			$this->set('datas', $datas );
		}
	}

	// 全ユーザーの売上予算リストをエクスポート
	public function DownloadYosanCsv( $nendo )
	{
		$this->Hearing->recursive = 0;
		$this->loadModel('User');
		$user_fields = array('loginid','company','zipcode','address','company_phone','fax',
			'email','department','name_kana','name','phone','extension');
		$user_labels = array('ログインID','会社名','郵便番号','住所','会社電話番号','FAX',
			'メールアドレス','部署','フリガナ','お名前','電話番号','内線番号');
		// アソシエーションの条件
		$this->User->hasMany['Hearing']['fields'] = 'uriage_yosan';
		$hearings = $this->User->find('all',array(
			'fields'=> $user_fields,
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$uriage_yosan_label = array(
				'11月度予算',
				'12月度予算',
				'01月度予算',
				'02月度予算',
				'03月度予算',
				'04月度予算',
				'05月度予算',
				'06月度予算',
				'07月度予算',
				'08月度予算',
				'09月度予算',
				'10月度予算',
				'11月度予算',
				'12月度予算',
				'01月度予算',
				'02月度予算'
			);
			$start = 4; // 11,12,1,2月の後から次年度
			$getu_num = 16;
		}else{
			$start = 3; // 12,1,2月の後から次年度
			$getu_num = 15;
			$uriage_yosan_label = array(
				'12月度予算',
				'01月度予算',
				'02月度予算',
				'03月度予算',
				'04月度予算',
				'05月度予算',
				'06月度予算',
				'07月度予算',
				'08月度予算',
				'09月度予算',
				'10月度予算',
				'11月度予算',
				'12月度予算',
				'01月度予算',
				'02月度予算'
			);
		}

		foreach( $hearings as $data ){
			$sum0=0; $sum1=0; $sum2=0; $sum3=0; $sum4=0;
			$uriage_data = array();

			if( isset( $data['Hearing'][0] )){
				$uriage_yosan = unserialize( $data['Hearing'][0]['uriage_yosan'] );
				$uriage_yosan = array_slice( $uriage_yosan, 0, $getu_num);
				$sum0 = array_sum( array_slice( $uriage_yosan, 0, $start));
				$sum1 = array_sum( array_slice( $uriage_yosan, $start, 6));
				$sum2 = array_sum( array_slice( $uriage_yosan, $start +6, 6));
				$sum3 = $sum1 + $sum2;
				$sum4 = $sum0 + $sum1 + $sum2;
			}else{
				$uriage_yosan = null;
			}

			$user_data = $data['User'];
			$uriage_data['uriage_yosan'] = $uriage_yosan;
			$uriage_data['sum0'] = $sum0;
			$uriage_data['sum1'] = $sum1;
			$uriage_data['sum2'] = $sum2;
			$uriage_data['sum3'] = $sum3;
			$uriage_data['sum4'] = $sum4;
			$datas[] = array_merge( $user_data, $uriage_data );

		}

		//Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		// ファイル名
		if( $nendo == 2013 )
		  $filename = SUB_DOMAIN . '_uriage_yosan_2013_list';
		else if( $nendo == 2014 )
		  $filename = SUB_DOMAIN . '_uriage_yosan_2014_list';
		else
		  $filename = SUB_DOMAIN . '_uriage_yosan_all_list';
		// 表データの整形
		$td = array();
		$dummy = array('','','','','','','','','','','','','','','','');
		foreach( $datas as $data ){
			if( $data['uriage_yosan']){
			  if( $nendo == 2013 )
				$uriage = array_slice( $data['uriage_yosan'],0,$start); // 12月,1月,2月
			  else if( $nendo == 2014 )
				$uriage = array_slice( $data['uriage_yosan'],$start,12); // 3月〜2月
			  else
				$uriage = array_slice( $data['uriage_yosan'],0,$getu_num); // 12月,1月,2月,3月〜2月
			}else{
			  if( $nendo == 2013 )
				$uriage = array_slice( $dummy,0,$start);
			  else if( $nendo == 2014 )
				$uriage = array_slice( $dummy,$start,12);
			  else
				$uriage = $dummy;
			}
			if( $nendo == 2013 )
			  $uriage[] = $data['sum0']; // 最後に年度合計をを追加
			else if( $nendo == 2014 )
			  $uriage[] = $data['sum3']; // 最後に年度合計をを追加
			else
			  $uriage[] = $data['sum4']; // 最後に全合計をを追加
			unset( $data['uriage_yosan']);
			unset( $data['sum0']);
			unset( $data['sum1']);
			unset( $data['sum2']);
			unset( $data['sum3']);
			unset( $data['sum4']);
			unset( $data['id']);
			$td[] = $data + $uriage;
		}

		// データをセット
		if( $nendo == 2013 )
			$label = array_slice( $uriage_yosan_label,0,$start);
		else if( $nendo == 2014 )
			$label = array_slice( $uriage_yosan_label,$start,12);
		else
			$label = $uriage_yosan_label;
		$th = array_merge( $user_labels, $label);
		$th[] = '合計';
		$this->set(compact('filename', 'th', 'td'));

	}


	// あるユーザーの予算リスト表示
	public function YosanList( $user_id = null )
	{
		if ($this->request->is('get')) {
			$serial_data = $this->Hearing->find('first',array(
				'fields'=>'uriage_yosan',
				'conditions'=>array('user_id'=>$user_id,"Hearing.deleted"=> 0 )
			));
			$field ='uriage_yosan';
			if( isset( $serial_data )){
				$data = unserialize( $serial_data['Hearing'][ $field ] );
				foreach( $data as $key => $value ){
					$this->request->data['Hearing'][ $field.$key ] = $value;
				}
			}
			$sum0 = $this->request->data['Hearing'][ 'uriage_yosan0' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan1' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan2' ];
			$sum1 = $this->request->data['Hearing'][ 'uriage_yosan3' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan4' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan5' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan6' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan7' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan8' ];
			$sum2 = $this->request->data['Hearing'][ 'uriage_yosan9' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan10' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan11' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan12' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan13' ]
				+ $this->request->data['Hearing'][ 'uriage_yosan14' ];
			$sum3 = $sum1 + $sum2;
			$this->set('data', $data );
			$this->set('sum0', $sum0 );
			$this->set('sum1', $sum1 );
			$this->set('sum2', $sum2 );
			$this->set('sum3', $sum3 );
		}
	}

    public function index( $user_id = null, $print = null )
    {
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
        $this->set('title_for_layout', 'ヒアリングシート');
        $user = $this->Auth->user();
        if( empty( $user_id )){
            $this->redirect(array('action' => 'index/'.$user['id']));
        }
        $this->set('user_id', $user_id);
        // $user_id = $this->request->params['pass'][0];

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_hearing1'];
		$this->set('locked', $locked);

		if( $user_id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, $this->params['controller'])){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, $this->params['controller'])&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

		$this->Mall->setFieldsAndLabels( $labels, $fields, $uriage_yosan_label, $uriage_yosan_1nendo, $uriage_yosan_2nendo, $syutten_jyoukyou_label, $target_info, $data_bunseki_label, $kaiin_suu_label, $schedule_label, $schedule_example, $jyou_kokyaku_label );

        $this->set('labels', $labels);
        $this->set('fields', $fields);
        $this->set('uriage_yosan_label', $uriage_yosan_label);
        $this->set('uriage_yosan_1nendo', $uriage_yosan_1nendo);
        $this->set('uriage_yosan_2nendo', $uriage_yosan_2nendo);
        $this->set('syutten_jyoukyou_label', $syutten_jyoukyou_label);
        $this->set('target_info', $target_info);
        $this->set('data_bunseki_label', $data_bunseki_label);
        $this->set('kaiin_suu_label', $kaiin_suu_label);
        $this->set('schedule_label', $schedule_label);
        $this->set('schedule_example', $schedule_example);
        $this->set('jyou_kokyaku_label', $jyou_kokyaku_label);

        if ($this->request->is('get')) {
			$fields[] = 'updated';
            $this->request->data = $this->Hearing->find('first',array(
                'fields'=>$fields,
                'conditions'=>array('user_id'=>$user_id,"Hearing.deleted"=> 0 )
            ));
			$this->loadModel('User');
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$user_id )
			));
			$this->set('user_info', $user['User'] );
            // アンシリアライズ
            if( isset( $this->request->data['Hearing']['uriage_yosan']))
                       $this->request->data['Hearing']['uriage_yosan'] = unserialize( 
                       $this->request->data['Hearing']['uriage_yosan']);
            if( isset( $this->request->data['Hearing']['syutten_jyoukyou']))
                       $this->request->data['Hearing']['syutten_jyoukyou'] = unserialize( 
                       $this->request->data['Hearing']['syutten_jyoukyou']);
            if( isset( $this->request->data['Hearing']['main_target']))
                       $this->request->data['Hearing']['main_target'] = unserialize( 
                       $this->request->data['Hearing']['main_target']);
            if( isset( $this->request->data['Hearing']['sub_target']))
                       $this->request->data['Hearing']['sub_target'] = unserialize( 
                       $this->request->data['Hearing']['sub_target']);
            if( isset( $this->request->data['Hearing']['data_bunseki']))
                       $this->request->data['Hearing']['data_bunseki'] = unserialize( 
                       $this->request->data['Hearing']['data_bunseki']);
            if( isset( $this->request->data['Hearing']['kaiin_suu']))
                       $this->request->data['Hearing']['kaiin_suu'] = unserialize( 
                       $this->request->data['Hearing']['kaiin_suu']);
            if( isset( $this->request->data['Hearing']['schedule']))
                       $this->request->data['Hearing']['schedule'] = unserialize( 
                       $this->request->data['Hearing']['schedule']);
            if( isset( $this->request->data['Hearing']['jyou_kokyaku']))
                       $this->request->data['Hearing']['jyou_kokyaku'] = unserialize( 
                       $this->request->data['Hearing']['jyou_kokyaku']);
            if( isset( $this->request->data['Hearing']['communication_to_tenpo_hindo']))
                       $this->request->data['Hearing']['communication_to_tenpo_hindo'] = unserialize( 
                       $this->request->data['Hearing']['communication_to_tenpo_hindo']);
            if( isset( $this->request->data['Hearing']['communication_to_tenpo_syudan']))
                       $this->request->data['Hearing']['communication_to_tenpo_syudan'] = unserialize( 
                       $this->request->data['Hearing']['communication_to_tenpo_syudan']);
            if( isset( $this->request->data['Hearing']['communication_to_tenpo_naiyou']))
                       $this->request->data['Hearing']['communication_to_tenpo_naiyou'] = unserialize( 
                       $this->request->data['Hearing']['communication_to_tenpo_naiyou']);
            if( isset( $this->request->data['Hearing']['communication_to_honbu_hindo']))
                       $this->request->data['Hearing']['communication_to_honbu_hindo'] = unserialize( 
                       $this->request->data['Hearing']['communication_to_honbu_hindo']);
            if( isset( $this->request->data['Hearing']['communication_to_honbu_syudan']))
                       $this->request->data['Hearing']['communication_to_honbu_syudan'] = unserialize( 
                       $this->request->data['Hearing']['communication_to_honbu_syudan']);
            if( isset( $this->request->data['Hearing']['communication_to_honbu_naiyou']))
                       $this->request->data['Hearing']['communication_to_honbu_naiyou'] = unserialize( 
                       $this->request->data['Hearing']['communication_to_honbu_naiyou']);

			$this->set('modal_message', '' );

		}else{ // post
			if( $user_id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, $this->params['controller'])){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}
			if( $locked ){
				$message = __('<p>現在、ご登録情報の変更はできません。</p>');
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect('/');
			}

            $data = $this->request->data;

            $uriage_yosan = array();
            $main_target = array();
            $sub_target = array();
            $kaiin_suu = array();
            $schedule = array();
            $jyou_kokyaku = array();
            foreach( $data['Hearing'] as $key => $value ){
                if( strstr( $key, 'uriage_yosan')){ // 売上予算
                    $uriage_yosan[] = mb_convert_kana($value, "a", "UTF-8");
                    $data['Hearing'][$key] = null;
                }
                if( strstr( $key, 'main_target')){
                    $main_target[] = $value;
                    $data['Hearing'][$key] = null;
                }
                if( strstr( $key, 'sub_target')){
                    $sub_target[] = $value;
                    $data['Hearing'][$key] = null;
                }
                if( strstr( $key, 'kaiin_suu')){
                    $kaiin_suu[] = $value;
                    $data['Hearing'][$key] = null;
                }
                if( strstr( $key, 'schedule_1')){
                    $schedule[1][] = $value;
                    $data['Hearing'][$key] = null;
                }
                if( strstr( $key, 'schedule_2')){
                    $schedule[2][] = $value;
                    $data['Hearing'][$key] = null;
                }
                if( strstr( $key, 'jyou_kokyaku')){
                    $jyou_kokyaku[] = $value;
                    $data['Hearing'][$key] = null;
                }
            }

            // シリアライズ
            $data['Hearing']['uriage_yosan'] = serialize( $uriage_yosan );
            $data['Hearing']['syutten_jyoukyou'] = serialize( $data['Hearing']['syutten_jyoukyou']);
            $data['Hearing']['main_target'] = serialize( $main_target );
            $data['Hearing']['sub_target'] = serialize( $sub_target );
            $data['Hearing']['data_bunseki'] = serialize( $data['Hearing']['data_bunseki']);
            $data['Hearing']['kaiin_suu'] = serialize( $kaiin_suu );
            $data['Hearing']['schedule'] = serialize( $schedule );
            $data['Hearing']['jyou_kokyaku'] = serialize( $jyou_kokyaku );

            $data['Hearing']['communication_to_tenpo_hindo']  = serialize( $data['Hearing']['communication_to_tenpo_hindo']);
            $data['Hearing']['communication_to_tenpo_syudan'] = serialize( $data['Hearing']['communication_to_tenpo_syudan']);
            $data['Hearing']['communication_to_tenpo_naiyou'] = serialize( $data['Hearing']['communication_to_tenpo_naiyou']);
            $data['Hearing']['communication_to_honbu_hindo']  = serialize( $data['Hearing']['communication_to_honbu_hindo']);
            $data['Hearing']['communication_to_honbu_syudan'] = serialize( $data['Hearing']['communication_to_honbu_syudan']);
            $data['Hearing']['communication_to_honbu_naiyou'] = serialize( $data['Hearing']['communication_to_honbu_naiyou']);

			// Lock確認
			$this->loadModel('User');
			$my_user = $this->User->find('first', array('conditions' => array(
								'User.id' => $_SESSION['Auth']['User']['id'],
								'User.deleted' => '0',
			)));
			if( $my_user['User']['lock_hearing1'] != 0 ){
				$message = __('<p>現在、ご登録情報の変更はできません。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				//$this->redirect(array('action' => 'index/'.$user_id));
			}
			$res = $this->Hearing->save( $data );
			if ( $res ) {
				$this->request->data['Hearing']['id'] = $res['Hearing']['id'];
				$message = __('<p>登録成功しました。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message ,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect(array('action' => 'index/'.$user_id));
			} else {
				$message = __('<p>登録失敗しました。入力内容をお確かめください。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
        }
    }

	// イオンモール幕張新都心 各種制作物(PR用・店舗設置用)ヒアリング項目案  ( 使ってない )
	public function hearing2( $user_id = null )
	{
		$this->set('title_for_layout', 'イオンモール幕張新都心ヒアリングシート');
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$this->redirect(array('action' => 'hearing2/'.$user['id']));
		}
		$this->set('user_id', $user_id);
		// $user_id = $this->request->params['pass'][0];
	}

	// シリアライズされてるのは、ここでタグ除去
	private function unseri_and_strip( $data )
	{
		if( ! $unserialized = @unserialize( $data )){
			return false;
		}else{
			$k = 1; // 配列は１から
			foreach( $unserialized as $key => $value ){
				if( is_array( $value)){
					$ary = array();
					foreach( $value as $value1){
						$ary[] = strip_tags( $value1, '');
					}
					$rtn[$k++] = $ary;
				}else if( $value == ''){
					$rtn[$k++] = $value;
				}else{
					$rtn[$k++] = strip_tags( $value, '');
				}
			}
			return $rtn;
		}
	}
	// 全ユーザーのリスト（売上予算以外）をエクスポート
	public function DownloadAllCsv()
	{

		$labels = array('ログインID','売上予算','出店状況','他店出店状況','セールス・ポイント','メイン・ターゲット','サブ・ターゲット','競合店','差別化手法','顧客管理','顧客管理目的','顧客管理ツール','データ分析','販促アプローチ','上顧客','会員数','感謝デー特典','GG感謝デー特典','スケジュール','営業形体','販売代行・FC決定','販売代行・FC会社名','店舗従業員数','うち社員数','新規採用予定数','決定','決定時期','来店頻度','従業員教育','コミュニケーション頻度<br />（本部→店舗）','コミュニケーション手段<br />（本部→店舗）','コミュニケーション内容<br />（本部→店舗）','コミュニケーション頻度<br />（店舗→本部）','コミュニケーション手段<br />（店舗→本部）','コミュニケーション内容<br />（店舗→本部）');

		$fields = array('id','uriage_yosan','syutten_jyoukyou','taten_syutten_jyoukyou','sales_point','main_target','sub_target','kyougou_ten','sabetuka_syuhou','kokyaku_kanri','kokyaku_kanri_mokuteki','kokyaku_kanri_tool','data_bunseki','hansoku_approach','jyou_kokyaku','kaiin_suu','kansya_day_tokuten','gg_kansya_day_tokuten','schedule','eigyou_keitai','daikou_kettei','daikou_kaisya','tenpo_jyuugyouin_num','syain_num','sin_saiyou_num','kettei','kettei_yotei','raiten_hindo','jyuugyouin_kyouiku','communication_to_tenpo_hindo','communication_to_tenpo_syudan','communication_to_tenpo_naiyou','communication_to_honbu_hindo','communication_to_honbu_syudan','communication_to_honbu_naiyou','User.loginid');

		$this->set('labels', $labels);
		$this->set('fields', $fields);

		$uriage_yosan_label = array(
				  '12月度予算［千円］',
				  '01月度予算［千円］',
				  '02月度予算［千円］',
				  '03月度予算［千円］',
				  '04月度予算［千円］',
				  '05月度予算［千円］',
				  '06月度予算［千円］',
				  '07月度予算［千円］',
				  '08月度予算［千円］',
				  '09月度予算［千円］',
				  '10月度予算［千円］',
				  '11月度予算［千円］',
				  '12月度予算［千円］',
				  '01月度予算［千円］',
				  '02月度予算［千円］'
		);
		$this->set('uriage_yosan_label', $uriage_yosan_label);
		$syutten_jyoukyou_label = array(
				  '日本初出店',
				  '新業態',
'関東（千葉県・埼玉県・神奈川県・東京都）初出店',
				  '千葉県初出店'
		);
		$this->set('syutten_jyoukyou_label', $syutten_jyoukyou_label);
		$target_info = array();
		$target_info['label'] = array(
				  '年齢',
				  '性別等',
				  '備考'
		);
		$target_info['ages'] = array(
									 '0' => '0歳〜４歳',
									 '5' => '5歳〜9歳',
									 '10' => '10代前半',
									 '15' => '10代後半',
									 '20' => '20代前半',
									 '25' => '20代後半',
									 '30' => '30代前半',
									 '35' => '30代後半',
									 '40' => '40代前半',
									 '45' => '40代後半',
									 '50' => '50代以上',
									 );
		$target_info['sex'] = array(
									 'man' => '男',
									 'female' => '女',
									 'kids' => 'キッズ',
									 'family' => 'ファミリー',
									 );
		$target_info['note'] = '（例：ファミリーでの来店が多い）';
		$this->set('target_info', $target_info);
		$data_bunseki_label = array(
				  '店舗',
				  '本部（POSレジ連動による購買分析有り）',
				  '本部（POSレジ連動による購買分析無し）'
		);
		$this->set('data_bunseki_label', $data_bunseki_label);
		$kaiin_suu_label = array(
				  '商圏内会員数',
				  '当ＳＣ目標会員数',
				  '他店舗名',
				  '他店舗会員数'
		);
		$this->set('kaiin_suu_label', $kaiin_suu_label);
		$schedule_label = array(
				  '12月度',
				  '1月度',
				  '2月度',
				  '3月度',
				  '4月度',
				  '5月度',
				  '6月度',
				  '7月度',
				  '8月度',
				  '9月度',
				  '10月度',
				  '11月度',
				  '12月度',
				  '1月度',
				  '2月度',
				  '3月度'
		);
		$this->set('schedule_label', $schedule_label);
		$schedule_example = array(
				"オープニングフェア<br />
				クリスマスギフト<br />
				プレセール",		
				"福袋<br />
				バーゲン",		
				"スーツフェア<br />
				バレンタイン<br />
				新生活提案",		
				"ホワイトデー<br />
				春休み<br />
				新入学",		
				"新生活<br />
				新入学",		
				"GWフェア<br />
				母の日ギフト",		
				"父の日ギフト<br />
				顧客向けプレセール<br />
				バーゲン",		
				"クリアランス<br />
				夏休み",		
				"初秋プロパー打ち出し<br />
				服飾雑貨の提案",		
				"オケージョン<br />
				敬老の日",		
				"ジャケットフェア<br />
				運動会",		
				"コートフェア",		
				"顧客向けプレセール<br />
				クリスマスギフト強化",		
				"福袋<br />
				バーゲン",		
				"スーツフェア<br />
				バレンタイン",		
				"ホワイトデー<br />
				新入学強化"
		);
		$this->set('schedule_example', $schedule_example);
		$jyou_kokyaku_label = array(
				  '上客',
				  '定義',
				  '特典'
		);
		$this->set('jyou_kokyaku_label', $jyou_kokyaku_label);
		$hearings = $this->Hearing->find('all',array(
			'fields'=>$fields,
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		foreach( $hearings as $key => $hearing ){
			// アンシリアライズ
			if( isset( $hearings[$key]['Hearing']['uriage_yosan']))
					   $hearings[$key]['Hearing']['uriage_yosan'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['uriage_yosan']);
			if( isset( $hearings[$key]['Hearing']['syutten_jyoukyou']))
					   $hearings[$key]['Hearing']['syutten_jyoukyou'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['syutten_jyoukyou']);
			if( isset( $hearings[$key]['Hearing']['main_target']))
					   $hearings[$key]['Hearing']['main_target'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['main_target']);
			if( isset( $hearings[$key]['Hearing']['sub_target']))
					   $hearings[$key]['Hearing']['sub_target'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['sub_target']);
			if( isset( $hearings[$key]['Hearing']['data_bunseki']))
					   $hearings[$key]['Hearing']['data_bunseki'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['data_bunseki']);
			if( isset( $hearings[$key]['Hearing']['kaiin_suu']))
					   $hearings[$key]['Hearing']['kaiin_suu'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['kaiin_suu']);
			if( isset( $hearings[$key]['Hearing']['schedule']))
					   $hearings[$key]['Hearing']['schedule'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['schedule']);
			if( isset( $hearings[$key]['Hearing']['jyou_kokyaku']))
					   $hearings[$key]['Hearing']['jyou_kokyaku'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['jyou_kokyaku']);
			if( isset( $hearings[$key]['Hearing']['communication_to_tenpo_hindo']))
					   $hearings[$key]['Hearing']['communication_to_tenpo_hindo'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['communication_to_tenpo_hindo']);
			if( isset( $hearings[$key]['Hearing']['communication_to_tenpo_syudan']))
					   $hearings[$key]['Hearing']['communication_to_tenpo_syudan'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['communication_to_tenpo_syudan']);
			if( isset( $hearings[$key]['Hearing']['communication_to_tenpo_naiyou']))
					   $hearings[$key]['Hearing']['communication_to_tenpo_naiyou'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['communication_to_tenpo_naiyou']);
			if( isset( $hearings[$key]['Hearing']['communication_to_honbu_hindo']))
					   $hearings[$key]['Hearing']['communication_to_honbu_hindo'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['communication_to_honbu_hindo']);
			if( isset( $hearings[$key]['Hearing']['communication_to_honbu_syudan']))
					   $hearings[$key]['Hearing']['communication_to_honbu_syudan'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['communication_to_honbu_syudan']);
			if( isset( $hearings[$key]['Hearing']['communication_to_honbu_naiyou']))
					   $hearings[$key]['Hearing']['communication_to_honbu_naiyou'] = $this->unseri_and_strip( 
					   $hearings[$key]['Hearing']['communication_to_honbu_naiyou']);
		}
		$this->set('hearings', $hearings);
		//debug($hearings);exit;

		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;

		$filename = SUB_DOMAIN . '_hearing_list';
		$th = array('ログインID','出店状況','他店出店状況','セールス・ポイント','メイン・ターゲット','サブ・ターゲット','競合店','差別化手法','顧客管理','顧客管理目的','顧客管理ツール','データ分析','販促アプローチ','上顧客','会員数','感謝デー特典','GG感謝デー特典','スケジュール','営業形体','販売代行・FC決定','販売代行・FC会社名','店舗従業員数','うち社員数','新規採用予定数','決定','決定時期','来店頻度','従業員教育','コミュニケーション頻度（本部→店舗）','コミュニケーション手段（本部→店舗）','コミュニケーション内容（本部→店舗）','コミュニケーション頻度（店舗→本部）','コミュニケーション手段（店舗→本部）','コミュニケーション内容（店舗→本部）');

		$this->set(compact('filename', 'th'));

	}
}
?>
