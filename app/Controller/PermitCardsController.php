<?php
App::uses('AppController', 'Controller');
/**
 * PermitCardsController
 *
 * @property PermitCard $PermitCard
 */
class PermitCardsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
	
/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_permit_card'])?
		               $user_data['User']['lock_permit_card'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->PermitCard->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['PermitCard']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if( $lock != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->PermitCard->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->PermitCard->read(null, $id);
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->PermitCard->recursive = 0;
		$this->set('permitCards', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->PermitCard->id = $id;
		if (!$this->PermitCard->exists()) {
			throw new NotFoundException(__('Invalid %s', __('permit card')));
		}
		$this->set('permitCard', $this->PermitCard->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PermitCard->id = $id;
		if (!$this->PermitCard->exists()) {
			throw new NotFoundException(__('Invalid %s', __('permit card')));
		}
		if ($this->PermitCard->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('permit card')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('permit card')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

	public function download() {
		$this->layout = false;
		$this->set('filename', SUB_DOMAIN .'_permit_card.csv');
		$th = array(
				'ログインID',
				'店名',
				'区画番号',
				'会社名',
				'郵便番号',
				'住所',
				'電話番号',
				'FAX',
				'メールアドレス',
				'所属部署',
				'お名前（フリガナ）',
				'お名前',
				'ご担当者電話番号',
				'内線番号',

				'必要枚数',
				'送付先郵便番号',
				'送付先住所',
				'送付先部署',
				'送付先氏名',
				'送付先フリガナ',
				'送付先役職',
				'送付先連絡先'
		);

		$this->loadModel('User');
		$data_all = $this->User->find('all', array(
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		$td = array();
		foreach($data_all as $key => $data) {
			if( count( $data['PermitCard']) == 0 ) continue;
			
			// 店名、重複しているデータを除く
			if( isset( $data['Signboard'])){
				$all_signboards = $data['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			
			$result = array(
				$data['User']['loginid'],
				$shop_sign,
				$data['User']['block'],
				$data['User']['company'],
				$data['User']['zipcode'],
				$data['User']['address'],
				$data['User']['company_phone'],
				$data['User']['fax'],
				$data['User']['email'],
				$data['User']['department'],
				$data['User']['name_kana'],
				$data['User']['name'],
				$data['User']['phone'],
				$data['User']['extension'],

				$data['PermitCard'][0]['card_num'],
				$data['PermitCard'][0]['zipcode'],
				$data['PermitCard'][0]['address'],
				$data['PermitCard'][0]['department'],
				$data['PermitCard'][0]['name'],
				$data['PermitCard'][0]['name_kana'],
				$data['PermitCard'][0]['position'],
				$data['PermitCard'][0]['phone'],

			);
			$td[] = $result;
		}

		$this->set(compact('th', 'td'));
	}

}
