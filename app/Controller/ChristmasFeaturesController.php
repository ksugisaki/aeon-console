<?php
App::uses('AppController', 'Controller');
/**
 * ChristmasFeatures Controller
 *
 * @property ChristmasFeature $ChristmasFeature
 */
class ChristmasFeaturesController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator', 'UploadPack.Upload');
/**
 * Components
 *
 * @var array
 */
	public $uses = array('ChristmasFeature');
	public $components = array('Session');

/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_christmas_feature'])?
		               $user_data['User']['lock_christmas_feature'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->ChristmasFeature->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['ChristmasFeature']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {

			$empty_img = null;
			if( isset( $this->request->data['ChristmasFeature']['delete_syouhin1_image1'])
			&&( $this->request->data['ChristmasFeature']['delete_syouhin1_image1'] == 1 ))
				$this->request->data['ChristmasFeature']['syouhin1_image1'] = $empty_img;
			if( isset( $this->request->data['ChristmasFeature']['delete_syouhin2_image1'])
			&&( $this->request->data['ChristmasFeature']['delete_syouhin2_image1'] == 1 ))
				$this->request->data['ChristmasFeature']['syouhin2_image1'] = $empty_img;
			if( isset( $this->request->data['ChristmasFeature']['delete_syouhin3_image1'])
			&&( $this->request->data['ChristmasFeature']['delete_syouhin3_image1'] == 1 ))
				$this->request->data['ChristmasFeature']['syouhin3_image1'] = $empty_img;

			if( $lock != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->ChristmasFeature->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->ChristmasFeature->read(null, $id);
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->ChristmasFeature->recursive = 0;
		$this->set('ChristmasFeatures', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->ChristmasFeature->id = $id;
		if (!$this->ChristmasFeature->exists()) {
			throw new NotFoundException(__('Invalid %s', __('ChristmasFeature')));
		}
		$this->set('ChristmasFeature', $this->ChristmasFeature->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ChristmasFeature->id = $id;
		if (!$this->ChristmasFeature->exists()) {
			throw new NotFoundException(__('Invalid %s', __('ChristmasFeature')));
		}
		if ($this->ChristmasFeature->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('ChristmasFeature')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('ChristmasFeature')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */
}
