<?php
App::uses('AppController', 'Controller');
/**
 * SyokujyuItems Controller
 *
 * @property SyokujyuItem $SyokujyuItem
 */
class SyokujyuItemsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SyokujyuItem->recursive = 0;
		$this->set('syokujyuItems', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->SyokujyuItem->id = $id;
		if (!$this->SyokujyuItem->exists()) {
			throw new NotFoundException(__('Invalid %s', __('syokujyu item')));
		}
		$this->set('syokujyuItem', $this->SyokujyuItem->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$user = $this->Auth->user();
		$this->set('user_id', $user['id']);
		if ($this->request->is('post')) {
			$res = $this->SyokujyuItem->find('first', array('conditions' => array(
				'user_id' => $user['id']
			)));
			if( $res ){ // レコードがあったら、addしない。
				$this->Session->setFlash(
					__('登録失敗しました。もう一度お試しください。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}
			$this->SyokujyuItem->create();

			// Lock確認
			$this->loadModel('User');
			$my_user = $this->User->find('first', array('conditions' => array(
								'User.id' => $_SESSION['Auth']['User']['id'],
								'User.deleted' => '0',
			)));
			if( $my_user['User']['lock_syokujyu'] != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('add/'. $user['id']);
			}else if ($this->SyokujyuItem->save($this->request->data)) {
				$this->Session->setFlash(
					__('植樹祭参加者登録項目を登録しました。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$users = $this->SyokujyuItem->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit( $user_id = null, $print = null ) {
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$data = $this->SyokujyuItem->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) {
			$this->redirect('/');
		}

		$user = $this->Auth->user();
		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$id = $data['SyokujyuItem']['id'];

		$this->SyokujyuItem->id = $id;
		if (!$this->SyokujyuItem->exists()) {
			throw new NotFoundException(__('Invalid %s', __('syokujyu item')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			unset($this->request->data['SyokujyuItem']['image1_file_name']);
			unset($this->request->data['SyokujyuItem']['image2_file_name']);
			unset($this->request->data['SyokujyuItem']['image3_file_name']);
			unset($this->request->data['SyokujyuItem']['logo_file_name']);

			// Lock確認
			$this->loadModel('User');
			$my_user = $this->User->find('first', array('conditions' => array(
								'User.id' => $_SESSION['Auth']['User']['id'],
								'User.deleted' => '0',
			)));
			if( $my_user['User']['lock_syokujyu'] != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->SyokujyuItem->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else {
			$this->request->data = $this->SyokujyuItem->read(null, $id);
			$this->loadModel('User');
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$user_id )
			));
			$this->set('user_info', $user['User'] );
		}
	}

	public function check() {
		$user = $this->Auth->user();
		$data = $this->SyokujyuItem->find('first', array('conditions' => array(
			'user_id' => $user['id']
		)));
		if (empty($data)) {
			$this->redirect('/syokujyu_items/add');
		}
		$this->redirect('/syokujyu_items/edit/' . $user['id']);
	}

	public function checkprint( $user_id ) {
		$data = $this->SyokujyuItem->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) {
			$this->redirect('/syokujyu_items/emptySheet/' . $user_id);
		}
		$this->redirect('/syokujyu_items/edit/'. $user_id .'/print');
	}

	public function emptySheet($user_id = null) {
		Configure::write('debug', 0);
		$print = "print";
		$this->loadModel('User');
		$user = $this->User->find('first',array(
			'conditions'=>array('id'=>$user_id )
		));
		$this->set('user_info', $user['User'] );
		$this->set('print', true);
		$this->layout = 'default_print';
		$this->render('edit');
	}

	public function DownloadAllCsv()
	{
		$this->set('title_for_layout', '植樹祭登録エクスポート');

		$labels = array();				$fields = array();
		$labels[] = 'ログインID';		$fields[] = 'User.loginid';
		$labels[] = '会社名';			$fields[] = 'User.company';
		$labels[] = 'テナント名';

		if( SUB_DOMAIN =='toin'){
			$labels[] = '代表者氏名';
			$labels[] = '代表者フリガナ';
			$labels[] = '代表者役職';
			$labels[] = '代表者連絡先';
			$labels[] = '氏名１';
			$labels[] = '氏名１フリガナ';
			$labels[] = '氏名１役職';
			$labels[] = '氏名１連絡先';
			$labels[] = '氏名２';
			$labels[] = '氏名２フリガナ';
			$labels[] = '氏名２役職';
			$labels[] = '氏名２連絡先';
		}else{
			$labels[] = '参加者１';
			$labels[] = '参加者１フリガナ';
			$labels[] = '参加者１役職';
			$labels[] = '参加者１連絡先';
			$labels[] = '参加者２';
			$labels[] = '参加者２フリガナ';
			$labels[] = '参加者２役職';
			$labels[] = '参加者２連絡先';
			$labels[] = '参加者３';
			$labels[] = '参加者３フリガナ';
			$labels[] = '参加者３役職';
			$labels[] = '参加者３連絡先';
		}

		$this->loadModel('User');
		$all_users = $this->User->find('all',array(
			'fields'=>$fields,
			'conditions'=>array('User.role'=>'','User.user_status'=>'authorized')
		));

		$td = array();
		foreach( $all_users as $key => $user ){
			$row = array();
			$row[] = $user['User']['loginid'];
			$row[] = $user['User']['company'];

			// 重複しているデータを除く
			if( isset( $user['Signboard'])){
				$all_signboards = $user['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			$row[] = $shop_sign;

			if( count( $user['SyokujyuItem']) > 0 ){
				$row[] = $user['SyokujyuItem'][0]['name1'];
				$row[] = $user['SyokujyuItem'][0]['name_kana1'];
				$row[] = $user['SyokujyuItem'][0]['position1'];
				$row[] = $user['SyokujyuItem'][0]['contact1'];
				$row[] = $user['SyokujyuItem'][0]['name2'];
				$row[] = $user['SyokujyuItem'][0]['name_kana2'];
				$row[] = $user['SyokujyuItem'][0]['position2'];
				$row[] = $user['SyokujyuItem'][0]['contact2'];
				$row[] = $user['SyokujyuItem'][0]['name3'];
				$row[] = $user['SyokujyuItem'][0]['name_kana3'];
				$row[] = $user['SyokujyuItem'][0]['position3'];
				$row[] = $user['SyokujyuItem'][0]['contact3'];
			}else{
				for( $i=0; $i<12; $i++ ) $row[]='';
			}
			$td[] = $row;
		}

		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		$filename = SUB_DOMAIN . '_syokujyu_list';
		$th = $labels;
		$this->set(compact('filename', 'td', 'th'));

	}

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->SyokujyuItem->id = $id;
		if (!$this->SyokujyuItem->exists()) {
			throw new NotFoundException(__('Invalid %s', __('syokujyu item')));
		}
		if ($this->SyokujyuItem->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('syokujyu item')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('syokujyu item')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */
}
