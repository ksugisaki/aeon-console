<?php
App::uses('AppController', 'Controller');
/**
 * FireDrills Controller
 *
 * @property FireDrill $FireDrill
 */
class FireDrillsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
	
/**
 * edit/add method
 */
	public function edit( $user_id = null, $print = null ) {
		$user = $this->Auth->user();
		if( empty( $user_id )){
			$user_id = $user['id'];
			$this->redirect(array('action' => 'edit/'. $user_id));
		}

		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$this->loadModel('User');
		$user_data = $this->User->find('first', array('conditions' => array(
							'User.id' => $user_id,
							'User.deleted' => '0',
		)));
		$this->set('user_info', $user_data['User'] );

		// Lock確認
		$lock = isset( $user_data['User']['lock_fire_drill'])?
		               $user_data['User']['lock_fire_drill'] : 0;
		$this->set('lock', $lock);
		// プリント用フォーム
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('print', $print );
		// 読み出し
		$data = $this->FireDrill->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) { // 読み出しデータが無いなら、
			$this->set('add', true); // 追加
			$this->set('user_id', $user_id); // 追加
		}else{
			$id = $data['FireDrill']['id'];
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if( $lock != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->FireDrill->save($this->request->data)) {
				$this->Session->setFlash(
					__('保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} elseif( isset( $id )) {
			$this->request->data = $this->FireDrill->read(null, $id);
		}
	}

/**
 * index method
 *
 * @return void
	public function index() {
		$this->FireDrill->recursive = 0;
		$this->set('FireDrills', $this->paginate());
	}
 */

/**
 * view method
 *
 * @param string $id
 * @return void
	public function view($id = null) {
		$this->FireDrill->id = $id;
		if (!$this->FireDrill->exists()) {
			throw new NotFoundException(__('Invalid %s', __('FireDrill')));
		}
		$this->set('FireDrill', $this->FireDrill->read(null, $id));
	}
 */

/**
 * delete method
 *
 * @param string $id
 * @return void
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->FireDrill->id = $id;
		if (!$this->FireDrill->exists()) {
			throw new NotFoundException(__('Invalid %s', __('FireDrill')));
		}
		if ($this->FireDrill->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('FireDrill')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('FireDrill')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}
 */

	public function download() {
		$this->layout = false;
		$this->set('filename', SUB_DOMAIN .'_fire_drill.csv');
		$th = array(
				'ログインID',
				'店名',
				'区画番号',
				'会社名',
				'郵便番号',
				'住所',
				'電話番号',
				'FAX',
				'メールアドレス',
				'所属部署',
				'お名前（フリガナ）',
				'お名前',
				'ご担当者電話番号',
				'内線番号',

				'参加の有無',
				'12/6(金) 10:00',
				'12/6(金) 13:00',
				'12/6(金) 15:00',
				'12/7(土) 10:00',
				'12/7(土) 13:00',
				'12/7(土) 15:00',
				'参加者１部署',
				'参加者１氏名',
				'参加者１役職',
				'参加者１連絡先',
				'参加者２部署',
				'参加者２氏名',
				'参加者２役職',
				'参加者２連絡先',
				'参加者３部署',
				'参加者３氏名',
				'参加者３役職',
				'参加者３連絡先',
				'参加者４部署',
				'参加者４氏名',
				'参加者４役職',
				'参加者４連絡先',

		);

		$this->loadModel('User');
		$data_all = $this->User->find('all', array(
			'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));

		$td = array();
		foreach($data_all as $key => $data) {
			if( count( $data['FireDrill']) == 0 ) continue;
			
			// 店名、重複しているデータを除く
			if( isset( $data['Signboard'])){
				$all_signboards = $data['Signboard'];
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign']))
				$shop_sign = $signboard_last['shop_sign'];
			else
				$shop_sign = '';
			
			$result = array(
				$data['User']['loginid'],
				$shop_sign,
				$data['User']['block'],
				$data['User']['company'],
				$data['User']['zipcode'],
				$data['User']['address'],
				$data['User']['company_phone'],
				$data['User']['fax'],
				$data['User']['email'],
				$data['User']['department'],
				$data['User']['name_kana'],
				$data['User']['name'],
				$data['User']['phone'],
				$data['User']['extension'],

				empty( $data['FireDrill'][0]['attend_mode'])? '' : '参加',
				empty( $data['FireDrill'][0]['attend_day1'])? '' : '参加',
				empty( $data['FireDrill'][0]['attend_day2'])? '' : '参加',
				empty( $data['FireDrill'][0]['attend_day3'])? '' : '参加',
				empty( $data['FireDrill'][0]['attend_day4'])? '' : '参加',
				empty( $data['FireDrill'][0]['attend_day5'])? '' : '参加',
				empty( $data['FireDrill'][0]['attend_day6'])? '' : '参加',
				$data['FireDrill'][0]['department1'],
				$data['FireDrill'][0]['name1'],
				$data['FireDrill'][0]['position1'],
				$data['FireDrill'][0]['contact1'],
				$data['FireDrill'][0]['department2'],
				$data['FireDrill'][0]['name2'],
				$data['FireDrill'][0]['position2'],
				$data['FireDrill'][0]['contact2'],
				$data['FireDrill'][0]['department3'],
				$data['FireDrill'][0]['name3'],
				$data['FireDrill'][0]['position3'],
				$data['FireDrill'][0]['contact3'],
				$data['FireDrill'][0]['department4'],
				$data['FireDrill'][0]['name4'],
				$data['FireDrill'][0]['position4'],
				$data['FireDrill'][0]['contact4'],

			);
			$td[] = $result;
		}

		$this->set(compact('th', 'td'));
	}

}
