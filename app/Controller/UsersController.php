<?php
class UsersController extends AppController
{
    public $helpers = array('Html', 'UploadPack.Upload', 'My');
    public
        $uses = Array('User'),
        $components = Array(
            'Mall',
            'Session',
            'Auth' => Array(
                'loginAction'    => Array('controller' => 'users', 'action' => 'login'),
                'loginRedirect'  => Array('controller' => 'users', 'action' => 'AdminList'),
                'logoutRedirect' => Array('controller' => 'users', 'action' => 'login'),
				'authenticate' => array(
					'Form' => array(
						'fields' => array ('username'=>'loginid')
					),
				),
            )
        );
	public $paginate = array();

    public function beforeFilter()
    {
        parent::beforeFilter();
        // $this->layout = 'user';
        $this->Auth->allow('index','agreement','welcome','register','add','auth','authback','authorize','login','ForgotPassword','ForgotLoginid','ResetPassword');

		if( SUB_DOMAIN != 'wakayama') // 和歌山以外は、
			unset($this->User->validate['block']); // 区画番号バリデーションを無効にする
    }
    public function index()
    {
        $this->set('title_for_layout', 'インデックス');
        $this->set('userinfo',$this->Auth->user());
        $user = $this->Auth->user();
	$user_id = $user['id'];
	$current_user_data = $this->User->find('first', array('conditions' => array('User.id' => $user_id)));
	$user = empty($current_user_data['User']) ? $user : $current_user_data['User'];
        $this->set('user_id', $user['id']);
		if( SUB_DOMAIN =='wakayama'){
			$agreed = isset( $user['agreed'])? $user['agreed']: 0;
			if( empty( $agreed )){
				$this->redirect(array('controller'=>'users','action'=>'agreement2/' . $user_id));
			}

//			$res = $this->User->registered_user( $user['id'] );
//			if( ! $res )
//				$this->Session->setFlash(__('必須項目（※印）を入力の上、登録ボタンを押してください。'),
//					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
//				$this->redirect(array('controller'=>'users','action'=>'edit/'. $user['id']));
		}
    }
    public function welcome()
    {
        $this->set('title_for_layout', '登録完了');
        $this->set('userinfo',$this->Auth->user());
        $this->set('authkey', $this->Session->read('authkey'));
    }
    public function agreement()
    {
        $this->set('title_for_layout', '規約同意');
        $this->set('userinfo',$this->Auth->user());
        if($this->request->is('post')) {
            if( isset( $this->request['data']['agree'])):
                $this->redirect(array('action' => 'add'));
            else:
                $this->Session->setFlash(__('ユーザー登録には、規約同意の確認が必要です。<br />ページ下部の「同意する」にチェックを入れて「次ぎへ」ボタンを押してください。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            endif;
        }
    }
    public function agreement2( $id )
    {
        $this->set('id', $id);
		if( $this->request->is('get')){
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$id )
			));
			$this->request->data = $this->User->read();
		}else{ // post
			if( isset( $this->request->data['User']['agree'])
			 && $this->request->data['User']['agree'] =='1'):
				$this->User->agreedSave( $id );
                $this->redirect(array('action' => '/'));
            else:
                $this->Session->setFlash(__('ユーザー登録には、規約同意の確認が必要です。<br />ページ下部の「同意する」にチェックを入れて「次ぎへ」ボタンを押してください。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            endif;
        }
    }
    public function login()
    {
        $this->set('title_for_layout', 'ログイン');
        if($this->request->is('post')) {
			$user = array();
			if( isset( $this->request['data']['User']['loginid'])){
				$loginid = $this->request['data']['User']['loginid'];
				if( SUB_DOMAIN =='wakayama'){ // 和歌山は、コード＋区画番号
					$block_id = $this->request['data']['User']['loginid'];
					if( substr( $block_id, 0, 4 ) == MALL_CODE ){
						$block = substr( $block_id, 4 );
						$user = $this->User->find('first',array(
							'conditions'=>array('block' => $block )
						));
					}
				}else{
					$user = $this->User->find('first',array(
						'conditions'=>array('loginid' => $loginid )
					));
				}
			}
		// Emailでログイン
		//	}else if( isset( $this->request['data']['User']['email'])){
		//		$email = $this->request['data']['User']['email'];
		//		$user = $this->User->find('first',array(
		//			'conditions'=>array('email' => $email )
		//		));
            if( empty( $user )){
                $this->Session->setFlash(__('ログインID、またはパスワードが不一致です。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else if( $user['User']['user_status'] == ''){
                $this->Session->setFlash(__('メール認証が必要です'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else if( $user['User']['user_status'] == 'authbacked'){
                $this->Session->setFlash(__('ユーザー登録手続中です。メール認証でお送りしたメール本文のURLから登録をお願いいたします。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else if( $user['User']['user_status'] == 'registered'){
                $this->Session->setFlash(__('ユーザー認証の手続中です。認証完了時にメールでお知らせいたしますのでしばらくお待ちください。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else if( $user['User']['user_status'] == 'rejected'){
                $this->Session->setFlash(__('ユーザー認証は、却下されました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else if( $user['User']['user_status'] == 'authorized'){
				//$agreed = isset( $user['User']['agreed'])? $user['User']['agreed']: 0;
				//$agree_check = isset($this->request['data']['agree'])? $this->request['data']['agree']:'';
				//if( SUB_DOMAIN == 'wakayama'
				// && empty( $agreed )
				// && $agree_check !='on'){
				//    $this->redirect(array('type'=>'post','action' =>'login/agree'));
				//}
				if( $this->Auth->login()){
					return $this->redirect($this->Auth->redirect());
				}else{
					$this->Session->setFlash(__('ログイン失敗しました。'),
						'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				}
            }
        }
    }
    public function logout($id = null)
    {
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }
    public function add( $email = null )
    {
        $this->set('send_message', $this->Session->read('send_message'));
        $this->Session->delete('send_message');
        $this->set('title_for_layout', 'ユーザー登録');
        $this->loadModel('Administrations');
        $admin = $this->Administrations->find('first');
		if( SUB_DOMAIN =='wakayama'){ // 和歌山は、新規登録はなし
			$this->redirect(array('controller'=>'users', 'action'=>'index'));
		}
		if( isset( $admin['Administrations']['entry_expiration'])){
			$expiration = $admin['Administrations']['entry_expiration'];
			$expiration = strtotime( $expiration ) +24*60*60;
			if( $expiration < time()){
				$this->Session->setFlash(__('新規登録の受付期間を過ぎました。'),
						'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect(array('controller'=>'users', 'action'=>'index'));
			}
		}
        $labels = array('id','メールアドレス');
        $fields = array('id','email');

        $this->set('email', $email);
        $this->set('labels', $labels);
        $this->set('fields', $fields);
        if ($this->request->is('post')) {
            $this->User->create();
            $data = $this->request->data;
            //$data['User']['user_id'] = uniqid();
            $data['User']['authkey'] = md5( uniqid( rand(),1));
            $this->Session->write('authkey', $data['User']['authkey']);
            if ($this->User->save( $data )) {
                $this->redirect(array('action' => 'auth'));
            }
        }
    }
    public function ChangePassword( $id )
    {
        $this->set('title_for_layout', 'パスワード変更');
        $this->set('id', $id);

		$user = $this->Auth->user();
		if( $id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isWritePermitted( $user, 'users_edit')){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

        $user = $this->User->find('first',array(
                'fields'=>array('id','email','name'),
            'conditions'=>array('id' => $id )
        ));
        $this->set('userinfo', $user['User']);
        $this->User->id = $id;

        if ($this->request->is('post')) {
            $res = $this->User->save( $this->request->data );
            if( $res !== false) {
                $this->Session->destroy();
                $this->Session->setFlash(__('パスワードは変更されました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
                $this->redirect($this->Auth->logout());
            }
        }
    }
    public function auth()
    {
        $this->set('title_for_layout', 'メール認証');
        $authkey = $this->Session->read('authkey');
        $user = $this->User->find('first',array(
            'fields'=>array('id','email','authkey'),
            'conditions'=>array('authkey' => $authkey )
        ));
        if( !empty( $user['User'] )){
            $this->set('authkey', $authkey );
            $this->set('email', $user['User']['email']);
            App::uses('CakeEmail', 'Network/Email');
            $cakeemail = new CakeEmail();
            $cakeemail->config('auth'); //$authの設定を読み込み。
            $cakeemail->to( $user['User']['email']);
            $cakeemail->subject('['.SITE_NAME.'] ユーザー認証'); // 件名
            $cakeemail->viewVars( $user['User'] ); //テンプレートにデータを渡す。
            if( $cakeemail->send()){
                $this->Session->write('send_message', true);
                $this->redirect(array('action'=>'add', $user['User']['email']));
            }
        }else{
            $this->Session->setFlash(__('ユーザー登録失敗しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            $this->redirect(array('action' => 'add'));
        }
    }
    public function authback() // メールからのコールバック
    {
        $this->autoRender = false;
        if( empty( $this->request->query['authkey'] )){
            $this->Session->setFlash(__('ユーザー認証失敗しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            $this->redirect("/users/add");
        }
        $authkey = $this->request->query['authkey'];
        $user = $this->User->find('first',array(
            'fields'=>array('id','email','authkey','user_status'),
            'conditions'=>array('authkey' => $authkey )
        ));
		if( ! $user ){
		    $this->Session->setFlash(__('この仮登録URLからの登録はできません。新規登録からお手続きをお願いいたします。'),
		            'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
		    $this->redirect("/");
		}
		if( $user['User']['user_status'] =='registered'){
		    $this->Session->setFlash(__('申請中です。お待ちください。'),
		            'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
		    $this->redirect("/users/login");
		}
		if( $user['User']['user_status'] =='authorized'){
		    $this->Session->setFlash(__('認証済です。ログインID,パスワードをご入力ください。'),
		            'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
		    $this->redirect("/users/login");
		}
        if( $authkey ==  $user['User']['authkey'] ){
            $this->Session->write('authkey', $authkey);
            $res = $this->User->authBackSave( $user['User']['id']);
            //$this->Session->setFlash(__('ご登録メールの確認が出来ました。'),
            //        'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
            App::uses('CakeEmail', 'Network/Email');
            $cakeemail = new CakeEmail();
            $cakeemail->config('contact'); // $contactの設定を読み込み。
            $cakeemail->to( $user['email']); // 宛先
            $cakeemail->subject('['.SITE_NAME.'] ご登録の完了'); // 件名
            $cakeemail->template('authback'); // テンプレート
            //テンプレートにデータを渡す。
            $cakeemail->viewVars( $res['User'] );
            $this->redirect(array('action'=>'register'));
        }else{
            $this->Session->setFlash(__('ユーザー認証失敗しました。。認証トークンが不一致です。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
        }
        $this->redirect("/users/index");
    }
    public function delete( $id=null ){
        $this->User->id=$id;
        if($this->request->is('post')){
            $this->User->delete();
            $this->redirect('AdminList');
        }
    }
    public function authorize() // Adminが認証
    {
        $this->autoRender = false;
        if( isset( $this->request->data['reject'])){
            $res = $this->User->authSave( $this->request->data['id'],'rejected');
            if( $res === false ){
                $this->Session->setFlash(__('却下手続き失敗しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else{
                $user_email = h($res['User']['email']);
                $user_name = h($res['User']['name']);

                $mail = array();
                $mail['email'] = $user_email;
                $mail['title'] = 'ユーザー登録のご依頼について';
                $mail['vars']  = array();
                $mail['vars']['message']
                    = $user_name . "様\n"
                    . "\n"
                    . "お客さまのユーザー登録は却下されました。\n";
                App::uses('CakeEmail', 'Network/Email');
                $cakeemail = new CakeEmail();
                $cakeemail->config('remind'); //$remind の設定を読み込み。
                $cakeemail->to( $user_email );
                $cakeemail->subject('['.SITE_NAME.'] '. $mail['title']); // 件名
                $cakeemail->viewVars( $mail['vars'] ); //テンプレートにデータを渡す。
                if( ! is_array( $cakeemail->send())){
                    $this->Session->setFlash(__('メール発行失敗しました。'),
                        'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
                }
            }
        }else if( isset( $this->request->data['accept'])){
            $res = $this->User->authSave( $this->request->data['id'],'authorized');
            if( $res === false ){
                $this->Session->setFlash(__('認証手続き失敗しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }else{
                $user_email = h($res['User']['email']);
                $user_name = h($res['User']['name']);
            
                $mail = array();
                $mail['email'] = $user_email;
                $mail['title'] = 'ユーザー登録完了';
                $mail['vars']  = array();
                $mail['vars']['message']
                    = $user_name . "様\n"
                    . "\n"
                    . "お客さまのユーザー登録が完了いたしました。\n"
                    . "下記のユーザーIDでログイン可能です。\n"
                    . $res['User']['loginid'];
                App::uses('CakeEmail', 'Network/Email');
                $cakeemail = new CakeEmail();
                $cakeemail->config('remind'); //$remind の設定を読み込み。
                $cakeemail->to( $user_email );
                $cakeemail->subject('['.SITE_NAME.'] '. $mail['title']); // 件名
                $cakeemail->viewVars( $mail['vars'] ); //テンプレートにデータを渡す。
                if( ! is_array( $cakeemail->send())){
                    $this->Session->setFlash(__('メール発行失敗しました。'),
                        'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
                }
            }
        }
        $this->redirect(array('controller'=>'users','action'=>'AdminList'));
    }

	public function AddAuthorizedUser() // Adminが認証済のユーザーを追加
	{
		$user = $this->Auth->user();
		if( $user['role'] != 'admin'){
			$this->redirect("index");
		}
		$this->set('title_for_layout', 'ユーザー追加');
		if ($this->request->is('post')) {
			$this->User->create();
			$data = $this->request->data;
			$data['User']['user_status'] ='authorized';
			$res = $this->User->save( $data );

			if( ! $res ){
				$this->Session->setFlash(__('ユーザーを追加できませんでした。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
			}elseif( SUB_DOMAIN == 'wakayama'){
				$this->Session->setFlash('ユーザーを追加しました。',
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect(array('action' => 'edit/'.$res['User']['id']));
			}else{
				$res = $this->User->authSave( $res['User']['id'],'authorized');
				if( ! $res ){
					$this->Session->setFlash(__('ログインIDを追加できませんでした。'),
						'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				}else{
					$this->Session->setFlash('ユーザーを追加しました。<br />
						新ユーザーのログインIDをご記録ください。ログインID： '. $res['User']['loginid'],
						'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
					$this->redirect(array('action' => 'edit/'.$res['User']['id']));
				}
			}
		}
	}

    public function ForgotPassword()
    {
        $this->set('title_for_layout', 'パスワード再設定');
        if($this->request->is('post')) {
            $this->Session->destroy();
            $data = $this->request->data;
            $email = $data['User']['email'];
            $user = $this->User->find('first',array(
                    'fields'=>array('id','email','user_status','block'),
                'conditions'=>array('email' => $email )
            ));
            if( empty( $user['User'])){
                $this->Session->setFlash(__('メールアドレスは、登録されておりません。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
                $this->redirect(array('action' => 'ForgotPassword'));
            }
            switch( $user['User']['user_status']){
            case '':
            case 'authbacked':
                $this->Session->setFlash(__('ユーザー登録の途中です。パスワードは、設定されておりません。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
                $this->redirect(array('action' => 'ForgotPassword'));
            }
            $authkey = md5( uniqid( rand(),1));
            $this->Session->write('authkey', $authkey);
            $user['User']['authkey'] = $authkey;

            $this->set('authkey', $authkey );
            $this->set('email', $email );
            App::uses('CakeEmail', 'Network/Email');
            $cakeemail = new CakeEmail();
            $cakeemail->config('forgot_password'); //$forgot_passwordの設定を読み込み。
            $cakeemail->to( $email );
            $cakeemail->subject('['.SITE_NAME.'] パスワードのリセット'); // 件名
            $cakeemail->viewVars( $user['User'] ); //テンプレートにデータを渡す。
            if( $cakeemail->send()){
                $this->Session->setFlash(__('パスワード再設定のためのメールを送信いたしました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
                $this->redirect(array('action' => 'login'));
            }
        }
    }
	public function ResetPassword() // メールからのコールバック
	{
		if( $this->request->is('get')){
			if( empty( $this->request->query['authkey'])){
				$this->Session->setFlash(__('ユーザー認証失敗しました。 [Code=1]'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect(array('action' => 'login'));
			}else{
				$this->set('authkey', $this->request->query['authkey']);
			}
			if( empty( $this->request->query['email'])){
				$this->Session->setFlash(__('ユーザー認証失敗しました。 [Code=2]'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect(array('action' => 'login'));
			}else{
				$this->set('email', $this->request->query['email']);
			}
			$users = $this->User->find('all',array(
				'fields'=>array('id','email','name','loginid','block'),
				'conditions'=>array(
							'email'=> $this->request->query['email'],
							'user_status' => 'authorized'
				)
			));
			if( empty( $users )){
				$this->Session->setFlash(__('ユーザー認証失敗しました。 [Code=3]'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect(array('action' => 'login'));
			}else{
				$this->request->data = $users;
			}
		}else{ // request is post
			if( $this->Session->read('authkey') != $this->request->query['authkey']){
				$this->Session->setFlash(__('ユーザー認証失敗しました。認証トークンが不一致です。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect(array('action' => 'login'));
			}
			$this->set('authkey', $this->request->query['authkey']);
			$this->set('email', $this->request->query['email']);
			$user = $this->User->find('first',array(
				'fields'=>array('id','loginid','name'),
				'conditions'=>array('id'=> $this->request->data['User']['id'])
			));
			$this->request->data['User']['id'] = $user['User']['id'];
			$this->request->data['User']['loginid'] = $user['User']['loginid'];
			$this->request->data['User']['name'] = $user['User']['name'];
			$res = $this->User->save( $this->request->data );
			if( $res !== false) {
				$this->Session->delete('authkey');
				$this->Session->setFlash(__('パスワードは変更されました。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect(array('action' => 'login'));
			}
		}
	}
	public function ForgotLoginid()
	{
		$this->set('title_for_layout', 'ログインID確認');
		if($this->request->is('post')) {
			$this->Session->destroy();
			$data = $this->request->data;
			$email = $data['User']['email'];
			$users = $this->User->find('all',array(
					'fields'=>array('id','name','email','user_status','block','loginid'),
				'conditions'=>array('email'=> $email, 'user_status'=>'authorized')
			));
			if( empty( $users )){
				$this->Session->setFlash(__('メールアドレスは、登録されておりません。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect(array('action' => 'ForgotLoginid'));
			}
			//if(  ( $user['User']['user_status'] != 'authorized')
			//   ||( $user['User']['loginid'] == '' )){
			//	$this->Session->setFlash(__('ユーザー登録／認証の途中です。ログインIDは、設定されておりません。'),
			//		'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			//	$this->redirect(array('action' => 'ForgotLoginid'));
			//	
			//}
			App::uses('CakeEmail', 'Network/Email');
			$cakeemail = new CakeEmail();
			$cakeemail->config('forgot_loginid'); //$forgot_loginidの設定を読み込み。
			$cakeemail->to( $email );
			$cakeemail->subject('['.SITE_NAME.'] ログインIDのお知らせ'); // 件名
			$cakeemail->viewVars( array('users'=>$users)); //テンプレートにデータを渡す。
			if( $cakeemail->send()){
				$this->Session->setFlash(__('ログインIDをお知らせするメールを送信いたしました。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect(array('action' => 'login'));
			}
		}
	}
	
    public function contact()
    {
        $this->set('title_for_layout', 'お問い合わせ');
        App::uses('CakeEmail', 'Network/Email');
        if ($this->request->is('post')) {
            $cakeemail = new CakeEmail();
            $cakeemail->config('contact'); //$contactの設定を読み込みます。
            $cakeemail->viewVars($this->request->data['Contact']);  //テンプレートにデータを渡す。
            if($cakeemail->send()){
                    //メール送信が成功した場合ここで処理
            }
        }
        $this->render();
    }
    public function register()
    {
        $authkey = $this->Session->read('authkey');
        $user = $this->User->find('first',array(
            'fields'=>array('id','email','authkey','created'),
                'conditions'=>array('authkey' => $authkey,
								'user_status' => 'authbacked' )
        ));
		if( ! $user ){
		  $this->redirect(array('action'=>'login'));
		}
        $expr = strtotime( $user['User']['created']) +24*60*60;
        if( $expr > time()){
            $enable_register = true;
        }else{
            $enable_register = false;
			$this->User->delete( $user['User']['id'] );
		}
        $this->set('enable_register', $enable_register );
		$fields1 = array('id','authkey','company','zipcode','address','company_phone','fax');
		$fields2 = array('email','department','name_kana','name','phone','extension');
		$fields = array_merge( $fields1, $fields2 );
        if ($this->request->is('get')) {
            $this->request->data = $this->User->find('first',array(
                'fields'=>$fields,
                'conditions'=>array('authkey' => $authkey )
            ));
        }
        if ($this->request->is('post')) {
            if( $enable_register ){
                $res = $this->User->registerSave( $this->request->data );
                if( is_array( $res )){
                    $this->redirect(array('action'=>'welcome', $authkey));
                }
            }
        }
    }

    public function edit( $id = null, $print = null )
    {
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
        $this->set('title_for_layout', '御社情報');
        $user = $this->Auth->user();
        if( empty( $id )){
            $this->redirect(array('action' => 'edit/'.$user['id']));
        }
        $this->set('role', $user['role']);

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_user_profile'];
		$this->set('locked', $locked);

		if( $id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, 'users_edit')){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, 'users_edit')&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

        $this->User->id = $id;
        $this->set('id', $id );
        if( isset( $this->request->query['mode'])){
            $mode = $this->request->query['mode'];
        }else{
            $mode = '';
        }
        $this->set('mode', $mode);
        if ($this->request->is('get')) {
			$user = $this->User->read();
			$this->set('user_info', $user['User'] );
			$this->request->data = $user;
        }
        $mail_address_count = isset( $this->request->data['MailAddress'] )?
            count( $this->request->data['MailAddress']) : 0;
        //$this->set('mail_address_count', $mail_address_count);

        // 副メールアドレス
        $submails = array();
        for( $i=0; $i < $mail_address_count; $i++ ){
            array_push( $submails, 'MailAddress.'. $i .'.id');
            array_push( $submails, 'MailAddress.'. $i .'.email');
        }
        array_push( $submails, 'MailAddress.add.email');
        $this->set('submails', $submails );

        if ($this->request->is('post')&& $print == null ) {
			if( $id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, 'users_edit')){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}

			$this->loadModel('Administrations');
			$admin = $this->Administrations->find('first');
			if( isset( $admin['Administrations']['edit_expiration'])){
				$expiration = $admin['Administrations']['edit_expiration'];
				$expiration = strtotime( $expiration ) +24*60*60;
				if( $user['role'] != 'admin' && $expiration < time()){
					$this->Session->setFlash(__('ご登録情報の変更可能期間を過ぎました。'),
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}
			if( $user['role'] == 'admin' )
				$res = $this->User->profileSaveAdmin( $this->request->data );
			else
				$res = $this->User->profileSave( $this->request->data );
			if( ! $res ){
				$this->Session->setFlash(__('必須項目（※印）をご入力のうえ、登録ボタンを押してください。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
			}

			if( $res == 'locked' ){
				$this->Session->setFlash(__('現在、ご登録情報の変更はできません。'),
						'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect('/');
			}
            // メールアドレス追加
            if( !empty( $this->request['data']['MailAddress']['add']['email'])){
                if( $this->request['data']['MailAddress']['add']['email'] == $this->request['data']['User']['email']){
                    $res2 = '既に主メールアドレスとして登録されています。';
                }else{
                    $this->loadModel('MailAddress');
                    $res2 = $this->MailAddress->addEmailSave( $this->request->data );
                    if( ! is_array( $res2 )){ // NG?
                        $error = $this->validateErrors( $this->MailAddress );
                        $res2 = $error['email']['0'];
                    }
                }
            }else{
                $res2 = array();
            }
            // メールアドレス削除
            if( $mail_address_count ){
                foreach( $this->request['data']['MailAddress'] as $mail ){
                    if( $mail['email'] == 'delete'){
                        $this->loadModel('MailAddress');
                        if( $res_d = $this->MailAddress->deleteEmailSave( $mail['id'])){
                            $mail_address_count -= 1;
                        }
                    }
                }
            }
            if( is_array( $res )){
                $this->Session->setFlash(__('プロフィール情報登録完了しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
                $this->redirect(array('action'=>'edit/'.$id));
            }
        }
    }
    public function terminal( $id = null, $print = null )
    {
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
        $this->set('title_for_layout', '入力用端末申込書');
        $user = $this->Auth->user();
        if( empty( $id )){
            $this->redirect(array('action' => 'terminal/'.$user['id']));
        }
        $this->set('role', $user['role']);

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_user_terminal'];
		$this->set('locked', $locked);

		if( $id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, 'users_terminal')){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, 'users_terminal')&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

        $this->User->id = $id;
        $labels1 = array('id','持込みレジ台数','入力用端末機台数');
        $fields1 = array('id','regi_num','terminal_num');
		if( defined('TERMINAL_COMPANY')){
		// レシート用情報は別ページにする
			$labels2 = array();
			$fields2 = array();
		}else{
			$labels2 = array('レシート用<br />テナント名称','レシート用<br />電話番号');
			$fields2 = array('receipt_tenant_name','receipt_phone');
		}
		$labels3 = array('氏名 （必須）','フリガナ （必須）','部署','役職','〒郵便番号','住所','電話番号','内線番号','FAX','携帯番号','E-Mail');
		$fields3 = array(
				'contact_name',
				'contact_name_kana',
				'contact_department',
				'contact_position',
				'contact_zipcode',
				'contact_address',
				'contact_phone',
				'contact_extension',
				'contact_fax',
				'contact_cellphone',
				'contact_email');
		$labels = array_merge( $labels1, $labels2, $labels3 );
		$fields = array_merge( $fields1, $fields2, $fields3 );

		$this->set('labels', $labels);
		$this->set('fields', $fields);

		if( $user['role']=='admin' || $user['role']=='partner' ){ // 管理と協力会社のみ
			$fields[] = 'terminal_remark'; // 端末備考
		}
		if ($this->request->is('get')) {
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$id )
			));
			$this->set('user_info', $user['User'] );
			$this->request->data = $this->User->read();
		} else {
			if( $id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, 'users_terminal')){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}
			$this->loadModel('Administrations');
			$admin = $this->Administrations->find('first');
			if( isset( $admin['Administrations']['edit_expiration'])){
				$expiration = $admin['Administrations']['edit_expiration'];
				$expiration = strtotime( $expiration ) +24*60*60;
				if( $user['role'] != 'admin' && $expiration < time()){
					$message = __('<p>ご登録情報の変更可能期間を過ぎました。</p>');
					$this->set('modal_message', $message );
					$this->Session->setFlash( $message,
						'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}
			$res = $this->User->terminalSave( $this->request->data, $fields );
			if( $res ) {
				$message = __('<p>登録成功しました。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				//$this->redirect(array('controller'=>'users', 'action'=>'terminal'));
			}else{
				$message = __('<p>登録失敗しました。入力内容をお確かめください。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
        }
    }
	public function receipt($id = null, $print = null )
	{
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('title_for_layout', 'レシート用情報');
		$user = $this->Auth->user();
		if( empty( $id )){
			$this->redirect(array('action' => 'receipt/'.$user['id']));
		}
		$this->set('role', $user['role']);

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_receipt'];
		$this->set('locked', $locked);

		if( $id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, 'users_receipt')){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, 'users_receipt')&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

		$this->User->id = $id;
		$labels = array('id','レシート用<br />テナント名称','レシート用<br />電話番号');
		$fields = array('id','receipt_tenant_name','receipt_phone');
		$this->set('labels', $labels);
		$this->set('fields', $fields);
		if ($this->request->is('get')) {
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$id )
			));
			$this->set('user_info', $user['User'] );
			$this->request->data = $this->User->read();
		} else {
			if( $id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, 'users_receipt')){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}

			$this->loadModel('Administrations');
			$admin = $this->Administrations->find('first');
			if( isset( $admin['Administrations']['edit_expiration'])){
				$expiration = $admin['Administrations']['edit_expiration'];
				$expiration = strtotime( $expiration ) +24*60*60;
				if( $user['role'] != 'admin' && $expiration < time()){
					$this->Session->setFlash(__('ご登録情報の変更可能期間を過ぎました。'),
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect(array('controller'=>'users', 'action'=>'receipt'));
				}
			}
			$res = $this->User->receiptSave( $this->request->data, $fields );
			if( $res ) {
				$message = __('<p>登録成功しました。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				//$this->redirect(array('controller'=>'users', 'action'=>'receipt'));
			}else{
				$message = __('<p>登録失敗しました。入力内容をお確かめください。</p>');
				$this->set('modal_message', $message );
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
		}
	}
	public function settingGoogleMap( $id = null, $print = null )
	{
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('title_for_layout', 'Google Map設定');
		$user = $this->Auth->user();
		if( empty( $id )){
			$this->redirect(array('action' => 'settingGoogleMap/'.$user['id']));
		}

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_google_map'];
		$this->set('locked', $locked);

		if( $id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, 'users_setting_google_map')){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, 'users_setting_google_map')&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

		$this->User->id = $id;
		$this->set('id', $id );
		if( $this->request->is('get')){
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$id )
			));
			$this->set('user_info', $user['User'] );
			$this->request->data = $this->User->read();
		}else{ // post
			if( $id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, 'users_setting_google_map')){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}

			$res = $this->User->googleMapSave( $this->request->data );
			if( $res ) {
				$message = __('<p>登録成功しました。</p>');
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
			}else{
				$message = __('<p>登録失敗しました。入力内容をお確かめください。</p>');
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
		}
	}
	public function LastShopSign( $id = null, $print = null )
	{
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$this->set('title_for_layout', '店名最終確認');
		$user = $this->Auth->user();
		$this->set('user', $user );
		if( empty( $id )){
			$this->redirect(array('action' => 'LastShopSign/'.$user['id']));
		}

		if( $user['role'] == 'admin' || $user['role'] =='partner' )
			$locked = 0;
		else
			$locked = $user['lock_last_shop_sign'];
		$this->set('locked', $locked);
		if( $id !== $user['id']){ // 自分のIDではないなら
			if( $user['role'] =='partner' ){ // 協力会社なら
				if( ! $this->Permit->isReadPermitted( $user, 'users_last_shop_sign')){
					$this->Session->setFlash('閲覧権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
				if( ! $this->Permit->isWritePermitted( $user, 'users_last_shop_sign')&& $print == null ){
					$this->Session->setFlash('編集権限がありません',
							'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
					$this->redirect('/');
				}
			}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
					$this->redirect('/');
			}
		}

		$this->User->id = $id;
		$this->set('id', $id );
		if( $this->request->is('get')){
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$id )
			));
			$this->set('user_info', $user['User'] );
			$this->request->data = $this->User->read();
			foreach( $this->request->data['Hearing'] as $key => $hearing ){
				// アンシリアライズ
				if( isset( $this->request->data['Hearing'][$key]['syutten_jyoukyou']))
						   $this->request->data['Hearing'][$key]['syutten_jyoukyou'] = @unserialize(
						   $this->request->data['Hearing'][$key]['syutten_jyoukyou']);
			}
		}else{ // post
			if( $id !== $user['id']){ // 自分のIDではないなら
				if( $user['role'] =='partner' ){ // 協力会社なら
					if( ! $this->Permit->isWritePermitted( $user, 'users_last_shop_sign')){
						$this->Session->setFlash('編集権限がありません',
								'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
						$this->redirect('/');
					}
				}elseif( $user['role'] !='admin' ){ // 管理者でもないなら
						$this->redirect('/');
				}
			}

			$res = $this->User->lastShopSignSave( $this->request->data );
			if( $res ) {
				$message = __('<p>登録成功しました。</p>');
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect(array('action' => 'LastShopSign/'.$res['User']['id']));
			}else{
				$message = __('<p>登録失敗しました。入力内容をお確かめください。</p>');
				$this->Session->setFlash( $message,
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
		}
	}

	public function ImportIdPw()
	{
		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		$this->set('title_for_layout', 'CSVインポート和歌山向け、新規ユーザー作成');
		if( $this->request->is('get')){
			$users = $this->User->find('all',array(
					'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
			));
			$this->set('users', $users );
		}else{ // post
			foreach( $this->request->data['User'] as $data ){
				if( empty( $data['block'])) continue;
				if( empty( $data['password'])) continue;
				$this->User->create();
				$wdata = array();
				$wdata['User']['block'] = $data['block'];
				$wdata['User']['password'] = $data['password'];
				$wdata['User']['password_confirm'] = $data['password'];
				$res = $this->User->save( $wdata );
			}
		}
	}

	public function ImportUserProfile()
	{
		unset($this->User->validate); // バリデーションを無効にする
		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		$this->set('title_for_layout', 'CSVインポート、御社情報');
		if( $this->request->is('get')){
			$users = $this->User->find('all',array(
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
			));
			$this->set('users', $users );
		}else{ // post
			$message_at_post = array();
			foreach( $this->request->data['User'] as $data ){
				if( empty( $data['block'])) continue;
				$user = $this->User->find('first',array(
					'conditions'=>array('block' => $data['block'] )
				));
				$block = $data['block'];
				if( empty( $user['User']['block'])){
					$message_at_post[(string)$block] = '<p>##ERROR## The user not found. block='. $data['block'];
					continue;
				}
				$wdata = array();
				$wdata['User'] = array();
				$wdata['User']['id'] = $user['User']['id'];

				//$wdata['User']['user_status'] = 'authorized';
				//$wdata['User']['block'] = $data['block'];
				//$wdata['User']['company'] = $data['company'];
				//$wdata['User']['zipcode'] = $data['zipcode'];
				//$wdata['User']['address'] = $data['address'];
				//$wdata['User']['company_phone'] = $data['company_phone'];
				//$wdata['User']['fax'] = $data['fax'];
				//$wdata['User']['department'] = $data['department'];
				//$wdata['User']['name'] = $data['name'];
				
				$wdata['User']['terminal_num'] = $data['terminal_num'];
				$wdata['User']['regi_num'] = $data['regi_num'];
				$res = $this->User->save( $wdata );
				$message_at_post[(string)$block]= 'The user has saved. block='. $data['block'];
			}
			$this->set('message_at_post', $message_at_post );
			debug($message_at_post);
			exit();
		}
	}

	public function ListLastShopSign( $order = 'id' )
	{
		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		if( $order == 'id' ) $order .= ' DESC';
		if( $order == 'name' ) $order = 'name_kana';
		$this->set('title_for_layout', '店名最終確認リスト');
		$this->set('userinfo', $user );

		// アソシエーションのフィールドを狭める
		$this->User->hasMany['Hearing']['fields']		= 'created,updated';
		$this->User->hasMany['OptionHearing']['fields'] = 'created,modified';
		$this->User->hasMany['Section']['fields']		= 'created,modified';
		$this->User->hasMany['Signboard']['fields']		= 'created,modified,shop_sign_jpn,shop_sign_eng,shop_sign_main_type';
		$this->User->hasMany['SyokujyuItem']['fields']		= 'created,modified';
		$this->User->hasMany['PreCarPort']['fields']		= 'created,modified';
		$this->User->hasMany['CarPort']['fields']			= 'created,modified';
		$this->User->hasMany['Bicycle']['fields']			= 'created,modified';
		$this->User->hasMany['PermitCard']['fields']		= 'created,modified';
		$this->User->hasMany['Cashbox']['fields']			= 'created,modified';
		$this->User->hasMany['MockOperation']['fields']		= 'created,modified';
		$this->User->hasMany['Locker']['fields']			= 'created,modified';
		$this->User->hasMany['FireDrill']['fields']			= 'created,modified';
		$this->User->hasMany['OperationBriefing']['fields']	= 'created,modified';
		$this->User->hasMany['Fukubukuro']['fields']		= 'created,modified';
		$this->User->hasMany['MediaStation']['fields']		= 'created,modified';
		$this->User->hasMany['LastHansoku']['fields']		= 'created,modified';
		$this->User->hasMany['ChristmasFeature']['fields']	= 'created,modified';

		$allUsers = $this->User->find('all', array(
				'order' => array( $order ),
				'conditions' => unserialize( TENANT_USER_AUTHORIZED )));

		$this->set('all', $allUsers);
	}

    public function AdminIndex()
    {
        $user = $this->Auth->user();
        if( $user['role'] != 'admin' && $user['role'] != 'partner'){
            $this->redirect("index");
        }
        $this->set('title_for_layout', 'データ管理');
        $this->set('userinfo', $user );

		$permission_users_last_shop = $this->Permit->isReadPermitted( $user, 'users_last_shop');
		$permission_users_edit = $this->Permit->isReadPermitted( $user, 'users_edit');
		$permission_users_terminal = $this->Permit->isReadPermitted( $user, 'users_terminal');
		$permission_users_receipt = $this->Permit->isReadPermitted( $user, 'users_receipt');
		$permission_sections = $this->Permit->isReadPermitted( $user, 'sections');
		$permission_hearings = $this->Permit->isReadPermitted( $user, 'hearings');
		$permission_option_hearings = $this->Permit->isReadPermitted( $user, 'option_hearings');
		$permission_signboards = $this->Permit->isReadPermitted( $user, 'signboards');
		$permission_users_google_map = $this->Permit->isReadPermitted( $user, 'users_google_map');
		$permission_syokujyu_items = $this->Permit->isReadPermitted( $user, 'syokujyu_items');
		$permission_fukubukuros = $this->Permit->isReadPermitted( $user, 'fukubukuros');
		$permission_last_hansokus = $this->Permit->isReadPermitted( $user, 'last_hansokus');
		$permission_media_stations = $this->Permit->isReadPermitted( $user, 'media_stations');
		$permission_mock_operations = $this->Permit->isReadPermitted( $user, 'mock_operations');
		$permission_christmas_features = $this->Permit->isReadPermitted( $user, 'christmas_features');
		$permission_permit_cards = $this->Permit->isReadPermitted( $user, 'permit_cards');
		$permission_pre_car_ports = $this->Permit->isReadPermitted( $user, 'pre_car_ports');
		$permission_car_ports = $this->Permit->isReadPermitted( $user, 'car_ports');
		$permission_bicycles = $this->Permit->isReadPermitted( $user, 'bicycles');
		$permission_lockers = $this->Permit->isReadPermitted( $user, 'lockers');
		$permission_cashboxes = $this->Permit->isReadPermitted( $user, 'cashboxes');
		$permission_fire_drills = $this->Permit->isReadPermitted( $user, 'fire_drills');
		$permission_operation_briefing = $this->Permit->isReadPermitted( $user, 'operation_briefing');

		$this->set('permission_users_last_shop', $permission_users_last_shop);
		$this->set('permission_users_edit', $permission_users_edit);
		$this->set('permission_users_terminal', $permission_users_terminal);
		$this->set('permission_users_receipt', $permission_users_receipt);
		$this->set('permission_sections', $permission_sections);
		$this->set('permission_hearings', $permission_hearings);
		$this->set('permission_option_hearings', $permission_option_hearings);
		$this->set('permission_signboards', $permission_signboards);
		$this->set('permission_users_google_map', $permission_users_google_map);
		$this->set('permission_syokujyu_items', $permission_syokujyu_items);
		$this->set('permission_fukubukuros', $permission_fukubukuros);
		$this->set('permission_last_hansokus', $permission_last_hansokus);
		$this->set('permission_media_stations', $permission_media_stations);
		$this->set('permission_mock_operations', $permission_mock_operations);
		$this->set('permission_christmas_features', $permission_christmas_features);
		$this->set('permission_permit_cards', $permission_permit_cards);
		$this->set('permission_pre_car_ports', $permission_pre_car_ports);
		$this->set('permission_car_ports', $permission_car_ports);
		$this->set('permission_bicycles', $permission_bicycles);
		$this->set('permission_lockers', $permission_lockers);
		$this->set('permission_cashboxes', $permission_cashboxes);
		$this->set('permission_fire_drills', $permission_fire_drills);
		$this->set('permission_operation_briefing', $permission_operation_briefing);

    }
    public function AdminList( $order = 'id' )	/* routes from AdminEdit */
    {
		$edit_mode = stristr( $this->request->url, 'users/AdminEdit')? true : false;
		$this->set('edit_mode', $edit_mode);
		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		if( $order == 'id' ) $order .= ' DESC';
		if( $order == 'name' ) $order = 'name_kana';
		$this->set('title_for_layout', 'ユーザーリスト');
		$this->set('userinfo', $user );

		// アソシエーションのフィールドを狭める
		$this->User->hasMany['Hearing']['fields']       = 'created,updated';
		$this->User->hasMany['OptionHearing']['fields'] = 'created,modified';
		$this->User->hasMany['Section']['fields']       = 'created,modified';
		$this->User->hasMany['Signboard']['fields']     = 'created,modified,shop_sign_jpn,shop_sign_eng,shop_sign_main_type';
		$this->User->hasMany['SyokujyuItem']['fields']		= 'created,modified';
		$this->User->hasMany['PreCarPort']['fields']		= 'created,modified';
		$this->User->hasMany['CarPort']['fields']			= 'created,modified';
		$this->User->hasMany['Bicycle']['fields']			= 'created,modified';
		$this->User->hasMany['PermitCard']['fields']		= 'created,modified';
		$this->User->hasMany['Cashbox']['fields']			= 'created,modified';
		$this->User->hasMany['MockOperation']['fields']		= 'created,modified';
		$this->User->hasMany['Locker']['fields']			= 'created,modified';
		$this->User->hasMany['FireDrill']['fields']			= 'created,modified';
		$this->User->hasMany['OperationBriefing']['fields']	= 'created,modified';
		$this->User->hasMany['Fukubukuro']['fields']		= 'created,modified';
		$this->User->hasMany['MediaStation']['fields']		= 'created,modified';
		$this->User->hasMany['LastHansoku']['fields']		= 'created,modified';
		$this->User->hasMany['ChristmasFeature']['fields']	= 'created,modified';

		if( $user['role'] == 'admin' ) // 管理者は全部
			$allUsers = $this->User->find('all', array(
					'order' => array( $order ),
					'conditions' => array('deleted'=> 0 )));
		elseif( $user['role'] == 'partner' ) // 協力会社には、認証済テナントのみ
			$allUsers = $this->User->find('all', array(
					'order' => array( $order ),
					'conditions' => unserialize( TENANT_USER_AUTHORIZED )));

        $this->set('all', $allUsers);
        $this->set('authAction', h(Router::url('/users/authorize',true)));
        $this->set('deleteAction', h(Router::url('/users/delete',true)));

        //$this->loadModel('Histories');
        //foreach( $allUsers as $each ){
        //    $this->User->id = $each['User']['id'];
        //    $userinfo = $this->User->read();
		//
        //    $histories[] = $userinfo['History'];
        //}
        //$this->set('histories', $histories);
    }
    public function AdminPages( $order = 'id' )	/* routes from AdminEdit */
    {
		$edit_mode = stristr( $this->request->url, 'users/AdminEdit')? true : false;
		$this->set('edit_mode', $edit_mode);
		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		if( $order == 'id' ) $order .= ' DESC';
		if( $order == 'name' ) $order = 'name_kana';
		$this->set('title_for_layout', 'ユーザーリスト');
		$this->set('userinfo', $user );

		// アソシエーションのフィールドを狭める
		$this->User->hasMany['Hearing']['fields']       = 'created,updated';
		$this->User->hasMany['OptionHearing']['fields'] = 'created,modified';
		$this->User->hasMany['Section']['fields']       = 'created,modified';
		$this->User->hasMany['Signboard']['fields']     = 'created,modified,shop_sign_jpn,shop_sign_eng,shop_sign_main_type';
		$this->User->hasMany['SyokujyuItem']['fields']		= 'created,modified';
		$this->User->hasMany['PreCarPort']['fields']		= 'created,modified';
		$this->User->hasMany['CarPort']['fields']			= 'created,modified';
		$this->User->hasMany['Bicycle']['fields']			= 'created,modified';
		$this->User->hasMany['PermitCard']['fields']		= 'created,modified';
		$this->User->hasMany['Cashbox']['fields']			= 'created,modified';
		$this->User->hasMany['MockOperation']['fields']		= 'created,modified';
		$this->User->hasMany['Locker']['fields']			= 'created,modified';
		$this->User->hasMany['FireDrill']['fields']			= 'created,modified';
		$this->User->hasMany['OperationBriefing']['fields']	= 'created,modified';
		$this->User->hasMany['Fukubukuro']['fields']		= 'created,modified';
		$this->User->hasMany['MediaStation']['fields']		= 'created,modified';
		$this->User->hasMany['LastHansoku']['fields']		= 'created,modified';
		$this->User->hasMany['ChristmasFeature']['fields']	= 'created,modified';

		if( $user['role'] == 'admin' ) // 管理者は全部
			$this->paginate = array(
				'limit' => 50,
				'order' => $order,
				'conditions' => array('deleted'=> 0 )
			);
		elseif( $user['role'] == 'partner' ) // 協力会社には、認証済テナントのみ
			$this->paginate = array(
				'limit' => 50,
				'order' => $order,
				'conditions' => unserialize( TENANT_USER_AUTHORIZED )
			);
		$allUsers = $this->paginate(); // ページ取得
        $this->set('all', $allUsers);
        $this->set('authAction', h(Router::url('/users/authorize',true)));
        $this->set('deleteAction', h(Router::url('/users/delete',true)));
        $this->set('this_action', $this->action);

        //$this->loadModel('Histories');
        //foreach( $allUsers as $each ){
        //    $this->User->id = $each['User']['id'];
        //    $userinfo = $this->User->read();
		//
        //    $histories[] = $userinfo['History'];
        //}
        //$this->set('histories', $histories);
    }
	public function AdminControl( $order = 'id' )
	{
		$user = $this->Auth->user();
		if( $user['role'] != 'admin'){
			$this->redirect("index");
		}
		if( $order == 'id' ) $order .= ' DESC';
		if( $order == 'name' ) $order = 'name_kana';
		$this->set('title_for_layout', 'シートロック管理');
		$this->set('userinfo', $user );

		// アソシエーションのフィールドを狭める
		$this->User->hasMany['Hearing']['fields']       = 'created,updated';
		$this->User->hasMany['OptionHearing']['fields'] = 'created,modified';
		$this->User->hasMany['Section']['fields']       = 'created,modified';
		$this->User->hasMany['Signboard']['fields']     = 'created,modified,shop_sign_jpn,shop_sign_eng,shop_sign_main_type';
		$this->User->hasMany['SyokujyuItem']['fields']		= 'created,modified';
		$this->User->hasMany['PreCarPort']['fields']		= 'created,modified';
		$this->User->hasMany['CarPort']['fields']			= 'created,modified';
		$this->User->hasMany['Bicycle']['fields']			= 'created,modified';
		$this->User->hasMany['PermitCard']['fields']		= 'created,modified';
		$this->User->hasMany['Cashbox']['fields']			= 'created,modified';
		$this->User->hasMany['MockOperation']['fields']		= 'created,modified';
		$this->User->hasMany['Locker']['fields']			= 'created,modified';
		$this->User->hasMany['FireDrill']['fields']			= 'created,modified';
		$this->User->hasMany['OperationBriefing']['fields']	= 'created,modified';
		$this->User->hasMany['Fukubukuro']['fields']		= 'created,modified';
		$this->User->hasMany['MediaStation']['fields']		= 'created,modified';
		$this->User->hasMany['LastHansoku']['fields']		= 'created,modified';
		$this->User->hasMany['ChristmasFeature']['fields']	= 'created,modified';
		$allUsers = $this->User->find('all', array(
					'order' => array( $order ),
					'conditions' => unserialize( TENANT_USER_AUTHORIZED )));
		$this->set('all', $allUsers);
		$this->set('authAction', h(Router::url('/users/authorize',true)));
		$this->set('deleteAction', h(Router::url('/users/delete',true)));

		if ($this->request->is('post')) {
			unset( $this->request->data['User']['lock_last_shop_sign']);
			unset( $this->request->data['User']['lock_user_profile']);
			unset( $this->request->data['User']['lock_section']);
			unset( $this->request->data['User']['lock_hearing1']);
			unset( $this->request->data['User']['lock_hearing2']);
			unset( $this->request->data['User']['lock_signboard']);
			unset( $this->request->data['User']['lock_syokujyu']);
			unset( $this->request->data['User']['lock_google_map']);

			unset( $this->request->data['User']['lock_last_hansoku']);
			unset( $this->request->data['User']['lock_fukubukuro']);
			unset( $this->request->data['User']['lock_media_station']);
			unset( $this->request->data['User']['lock_christmas_feature']);

			unset( $this->request->data['User']['lock_permit_card']);
			unset( $this->request->data['User']['lock_pre_car_port']);
			unset( $this->request->data['User']['lock_car_port']);
			unset( $this->request->data['User']['lock_bicycle']);
			unset( $this->request->data['User']['lock_locker']);
			unset( $this->request->data['User']['lock_cashbox']);
			unset( $this->request->data['User']['lock_fire_drill']);
			unset( $this->request->data['User']['lock_bill']);
			unset( $this->request->data['User']['lock_mock_operation']);
			unset( $this->request->data['User']['lock_emergency_contact']);
			unset( $this->request->data['User']['lock_cashier']);
			unset( $this->request->data['User']['lock_operation_briefing']);

			unset( $this->request->data['User']['lock_user_terminal']);
			unset( $this->request->data['User']['lock_receipt']);
			if ($this->User->lockDataSave( $this->request->data )) {
				$this->Session->setFlash(__('登録しました。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect("AdminControl");
			}else{
				$this->Session->setFlash(__('登録失敗しました。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
			}
		}
	}

	public function ImageLogoList( $order = 'id', $print = null )
	{
		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}

		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		if( $order == 'id' ) $order .= ' DESC';
		$this->set('title_for_layout', 'イメージ／ロゴ・リスト');
		$this->set('userinfo', $user );
		$fields = array('loginid','block','company');
		$this->loadModel('Signboard');
		$this->Signboard->recursive = 0;
		$allUsers = $this->User->find('all', array(
					'fields' => $fields,
					'order' => array( $order ),
					'conditions' => unserialize( TENANT_USER_AUTHORIZED )));
		$this->set('fields', $fields);
		$this->set('all', $allUsers);
	}

	public function MailingList( $order = 'id' )
	{
		$user = $this->Auth->user();
		if( $user['role'] != 'admin' && $user['role'] != 'partner'){
			$this->redirect("index");
		}
		if( $order == 'id' ) $order .= ' DESC';
		$this->set('title_for_layout', 'メーリングリスト');
		$this->set('userinfo', $user );
		$labels = array('選択','ログインID','会社名','店名','ご担当者電話番号','お名前','メールアドレス');
		$fields = array('id','loginid','company','phone','name','email','role');
		$this->set('labels', $labels);
		$this->set('fields', $fields);
		$allUsers = $this->User->find('all', array(
					'fields' => $fields,
					'order' => array( $order ),
					'conditions' => array('user_status' => 'authorized')));
		$this->set('all', $allUsers);
		$this->loadModel('QueueMail');
		$queue_mails = $this->QueueMail->find('all',array(
			'conditions'=>array('has_completed'=>'0')
		));
		if( $this->request->is('post')){
			$queue_mails = array();
			$user_ids = $this->request->data['User'];
			$checked_num = 0;
			foreach( $allUsers as $user ){
				$id = $user['User']['id'];
				if( $user_ids[$id] =='1'){
					$checked_num++;
					$queue_mail['id']        = null;
					$queue_mail['mail_to']   = $user['User']['email'];
					$queue_mail['mail_from'] = $this->request->data['mail_from'];
					$queue_mail['mail_subject'] = $this->request->data['mail_subject'];
					$queue_mail['mail_body'] = $this->request->data['mail_body'];
					$save_data = array();
					$save_data['QueueMail'] = $queue_mail;
					$res = $this->QueueMail->save( $save_data );
				}
			}
			if( $checked_num > 0 ){
				$this->Session->setFlash(__('メールの送信を受け付けいたしました。※メールが送信されるまで時間がかかる事があります。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
				$this->redirect("AdminIndex");
			}else{
				$this->Session->setFlash(__('送信先の選択ボックスにチェックを入れて送信ボタンを押して下さい。'),
					'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
				$this->redirect("MailingList");
			}
		}
	}
    public function SyoubouKeikaku()
    {
        $this->set('title_for_layout', '消防提出書類');
    }
    public function export()
    {
        $this->autoRender = false;
        $modelClass = $this->modelClass;
        $this->response->type('Content-Type: text/csv');
        $this->response->download( SUB_DOMAIN . '_userlist.csv');
        $this->response->body( $this->$modelClass->exportCSV());
    }

	public function DownloadAllCsv()
	{
		$this->set('title_for_layout', 'ユーザーリストのエクスポート');
		$labels = array();				$fields = array();
		$labels[] = 'loginid';			$fields[] = 'loginid';
		$labels[] = 'shop_name';		$fields[] = 'id'; // 後で店名に書き換える
		$labels[] = 'block_num';		$fields[] = 'block';
		$labels[] = 'company';			$fields[] = 'company';
		$labels[] = 'zipcode';			$fields[] = 'zipcode';
		$labels[] = 'address';			$fields[] = 'address';
		$labels[] = 'company_phone';	$fields[] = 'company_phone';
		$labels[] = 'fax';				$fields[] = 'fax';
		$labels[] = 'email';			$fields[] = 'email';
		$labels[] = 'department';		$fields[] = 'department';
		$labels[] = 'name_kana';		$fields[] = 'name_kana';
		$labels[] = 'name';			$fields[] = 'name';
		$labels[] = 'phone';			$fields[] = 'phone';
		$labels[] = 'extension';		$fields[] = 'extension';
		$this->loadModel('Signboard');
		$this->Signboard->recursive = 0;
		$users = $this->User->find('all',array(
				'fields'=>$fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		$this->set('users', $users );
		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		$filename = SUB_DOMAIN . '_userlist';
		$th = $labels;
		$this->set(compact('filename', 'th'));
	}

	public function DownloadTenantInfo()
	{
		$this->set('title_for_layout', 'テナント情報のエクスポート');
		$labels = array();			$fields = array();
		$labels[] = 'ID';			$fields[] = 'loginid';
		$labels[] = '会社名';		$fields[] = 'company';
		$labels[] = '店名';			$fields[] = 'id'; // 後で店名に書き換える
		$labels[] = '区画番号';		$fields[] = 'block';
		$users = $this->User->find('all',array(
				'fields'=>$fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		$this->set('users', $users );
		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		$filename = SUB_DOMAIN . '_tenant_info';
		$th = $labels;
		$this->set(compact('filename', 'th'));
	}

	public function DownloadTerminalInfo()
	{
		$this->set('title_for_layout', '入力用端末／レシート用情報のエクスポート');
		$labels = array();			$fields = array();
		$labels[] = 'ID';			$fields[] = 'loginid';
		$labels[] = '会社名';		$fields[] = 'company';
		$labels[] = '店名';			$fields[] = 'id'; // 後で店名に書き換える
		$labels[] = '区画番号';		$fields[] = 'block';
		$labels[] = 'テナントコード';	$fields[] = 'tenant_code';

		$labels[] = '持込みレジ台数';	$fields[] = 'regi_num';
		$labels[] = '申し込み台数';	$fields[] = 'terminal_num';
		$labels[] = '担当氏名';		$fields[] = 'contact_name';
		$labels[] = '担当フリガナ';	$fields[] = 'contact_name_kana';
		$labels[] = '担当部署';		$fields[] = 'contact_department';
		$labels[] = '担当役職';		$fields[] = 'contact_position';
		
		$labels[] = '端末備考';		$fields[] = 'terminal_remark';
		
		$labels[] = 'レシート用テナント名称';	$fields[] = 'receipt_tenant_name';
		$labels[] = 'レシート用電話番号';		$fields[] = 'receipt_phone';

		$users = $this->User->find('all',array(
				'fields'=>$fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		
		$this->set('users', $users );
		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		$filename = SUB_DOMAIN . '_terminal_info';
		$th = $labels;
		$this->set(compact('filename', 'th'));
	}

	public function DownloadLastShopSign()
	{
		$this->set('title_for_layout', '店名最終確認情報のエクスポート');
		$labels = array();			$fields = array();
		$labels[] = 'ID';			$fields[] = 'loginid';
		
		$labels[] = '店名';			$fields[] = 'shop_sign_last';
		$labels[] = '店名確定';		$fields[] = 'shop_sign_kakutei';
		$labels[] = '店名時期';		$fields[] = 'shop_sign_yotei';
		$labels[] = '店名修正';		$fields[] = 'shop_sign_syuusei';
		$labels[] = '店名備考';		$fields[] = 'shop_sign_remark';
		
		$labels[] = '業種';			$fields[] = 'shop_category_last';
		$labels[] = '業種確定';		$fields[] = 'shop_category_kakutei';
		$labels[] = '業種時期';		$fields[] = 'shop_category_yotei';
		$labels[] = '業種修正';		$fields[] = 'shop_category_syuusei';
		$labels[] = '業種備考';		$fields[] = 'shop_category_remark';
		
		$labels[] = '出店状況';			$fields[] = 'syutten_jyoukyou_last';
		$labels[] = '出店状況確定';		$fields[] = 'syutten_jyoukyou_kakutei';
		$labels[] = '出店状況修正';		$fields[] = 'syutten_jyoukyou_syuusei';
		$labels[] = '出店状況備考';		$fields[] = 'syutten_jyoukyou_remark';

		$users = $this->User->find('all',array(
				'fields'=>$fields,
				'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
		));
		$this->set('users', $users );
		Configure::write('debug', 0); // 警告を出さない
		$this->layout = false;
		$filename = SUB_DOMAIN . '_last_shop_sign';
		$th = $labels;
		$this->set(compact('filename', 'th'));
	}

	public function testscr(){
		echo '********************';
		echo ' DEBUG LEVEL ';
		echo '********************';
		$debug_level = Configure::read('debug');
		debug( $debug_level );
		if( $debug_level == 0 ){
			$this->redirect("index");
		}
		echo '********************';
		echo ' SESSION ';
		echo '********************';
		debug( $this->Session->read());

		echo '********************';
		echo ' PHP INFO ';
		echo '********************';
		phpinfo();

		echo '********************';
		echo ' 実験';
		echo '********************';
		$data = 'a:2:{i:1;a:16:{i:0;s:40:"<p>スマホアクセサリー割引</p>";i:1;s:40:"<p>スマホアクセサリー福袋</p>";i:2;s:0:"";i:3;s:0:"";i:4;s:0:"";i:5;s:0:"";i:6;s:0:"";i:7;s:0:"";i:8;s:0:"";i:9;s:0:"";i:10;s:0:"";i:11;s:0:"";i:12;s:0:"";i:13;s:0:"";i:14;s:0:"";i:15;s:0:"";}i:2;a:16:{i:0;s:0:"";i:1;s:0:"";i:2;s:0:"";i:3;s:0:"";i:4;s:0:"";i:5;s:0:"";i:6;s:0:"";i:7;s:0:"";i:8;s:0:"";i:9;s:0:"";i:10;s:0:"";i:11;s:0:"";i:12;s:0:"";i:13;s:0:"";i:14;s:0:"";i:15;s:0:"";}}';
		debug( $data );

		$unserialized = @unserialize( $data);
		debug( $unserialized );
		
//		$unserialized = @unserialize( strip_tags( $data,'' ));
//		debug( $unserialized );

			$k = 1;
			foreach( $unserialized as $key => $value ){
				if( is_array( $value)){
					$ary = array();
					foreach( $value as $value1){
						$ary[] = strip_tags( $value1, '');
					}
					$rtn[$k++] = $ary;
				}else if( $value == ''){
					$rtn[$k++] = $value;
				}else{
					$rtn[$k++] = strip_tags( $value, '');
				}
			}
		debug( $rtn );

		exit();
	}

}
?>
