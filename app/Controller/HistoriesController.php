<?php
class HistoriesController extends AppController
{
    public $helpers = array('Html');
    public
        $uses = Array('History'),
        $components = Array(
            'Session'
        );
    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $this->set('title_for_layout', '更新履歴 | インデックス');
        $labels = array('項目','変更値','変更日時');
        $this->set('labels', $labels);
        $fields = array('item','value','updated');
        $user_id = $this->request->params['pass'][0];
        $this->set('histories', $this->History->find('all', array(
                     'fields' => $fields,
                     'conditions' => array(
                             "user_id" => $user_id,
                             "History.deleted" => 0))));
    }
}
?>
