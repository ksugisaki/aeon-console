<?php
App::uses('AppController', 'Controller');
/**
 * OptionHearings Controller
 *
 * @property OptionHearing $OptionHearing
 */
class OptionHearingsController extends AppController {

/**
 * Helpers
 *
 * @var array
 */
	public $helpers = array('TwitterBootstrap.BootstrapHtml', 'TwitterBootstrap.BootstrapForm', 'TwitterBootstrap.BootstrapPaginator', 'UploadPack.Upload');
/**
 * Components
 *
 * @var array
 */
	public $components = array('Session');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OptionHearing->recursive = 0;
		$this->set('optionHearings', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->OptionHearing->id = $id;
		if (!$this->OptionHearing->exists()) {
			throw new NotFoundException(__('Invalid %s', __('option hearing')));
		}
		$this->set('optionHearing', $this->OptionHearing->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$user = $this->Auth->user();
		$this->set('user_id', $user['id']);
		if ($this->request->is('post')) {
			$res = $this->OptionHearing->find('first', array('conditions' => array(
				'user_id' => $user['id']
			)));
			if( $res ){ // レコードがあったら、addしない。
				$this->Session->setFlash(
					__('登録失敗しました。もう一度お試しください。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}
			$this->OptionHearing->create();

			// Lock確認
			$this->loadModel('User');
			$my_user = $this->User->find('first', array('conditions' => array(
								'User.id' => $_SESSION['Auth']['User']['id'],
								'User.deleted' => '0',
			)));
			if( $my_user['User']['lock_hearing2'] != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('add/'. $user['id']);
			}else if ($this->OptionHearing->save($this->request->data)) {
				$this->Session->setFlash(
					__('ヒアリングシート2を保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		}
		$users = $this->OptionHearing->User->find('list');
		$this->set(compact('users'));
		if( SUB_DOMAIN == 'toin' || SUB_DOMAIN == 'testsite'){
			$this->render('add_toin');
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit( $user_id = null, $print = null ) {
		$this->disableCache(); // キャッシュオフ

		$this->set('print', $print );
		if( $print !== null ){
			$this->layout = 'default_print';
		}
		$data = $this->OptionHearing->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) {
			$this->redirect('/');
		}

		$user = $this->Auth->user();
		if ($user_id !== $user['id']) {
			if ($user['role'] != 'admin'&& $print == null ) {
				$this->Session->setFlash('編集権限がありません');
				$this->redirect('/');
			}
		}

		$id = $data['OptionHearing']['id'];

		$this->OptionHearing->id = $id;
		if (!$this->OptionHearing->exists()) {
			throw new NotFoundException(__('Invalid %s', __('option hearing')));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			unset($this->request->data['OptionHearing']['image1_file_name']);
			unset($this->request->data['OptionHearing']['image2_file_name']);
			unset($this->request->data['OptionHearing']['image3_file_name']);
			unset($this->request->data['OptionHearing']['logo_file_name']);

			$empty_img = null;

			if( isset( $this->request->data['OptionHearing']['delete_image1'])
			&&( $this->request->data['OptionHearing']['delete_image1'] == 1 ))
				$this->request->data['OptionHearing']['image1'] = $empty_img;

			if( isset( $this->request->data['OptionHearing']['delete_image2'])
			&&( $this->request->data['OptionHearing']['delete_image2'] == 1 ))
				$this->request->data['OptionHearing']['image2'] = $empty_img;

			if( isset( $this->request->data['OptionHearing']['delete_image3'])
			&&( $this->request->data['OptionHearing']['delete_image3'] == 1 ))
				$this->request->data['OptionHearing']['image3'] = $empty_img;

			if( isset( $this->request->data['OptionHearing']['delete_logo'])
			&&( $this->request->data['OptionHearing']['delete_logo'] == 1 ))
				$this->request->data['OptionHearing']['logo'] = $empty_img;

			if( isset( $this->request->data['OptionHearing']['delete_logo2'])
			&&( $this->request->data['OptionHearing']['delete_logo2'] == 1 ))
				$this->request->data['OptionHearing']['logo2'] = $empty_img;

			// Lock確認
			$this->loadModel('User');
			$my_user = $this->User->find('first', array('conditions' => array(
								'User.id' => $_SESSION['Auth']['User']['id'],
								'User.deleted' => '0',
			)));
			if( $my_user['User']['lock_hearing2'] != 0 ){
				$this->Session->setFlash(
					__('現在、ご登録情報の変更はできません。'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			}else if ($this->OptionHearing->save($this->request->data)) {
				$this->Session->setFlash(
					__('ヒアリングシート2を保存しました'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-success'
					)
				);
				$this->redirect('edit/'. $user['id']);
			} else {
				$this->Session->setFlash(
					__('入力内容を確認して下さい'),
					'alert',
					array(
						'plugin' => 'TwitterBootstrap',
						'class' => 'alert-error'
					)
				);
			}
		} else { // get
			$this->request->data = $this->OptionHearing->read(null, $id);
			$this->loadModel('User');
			$user = $this->User->find('first',array(
				'conditions'=>array('id'=>$user_id )
			));
			$this->set('user_info', $user['User'] );
		}
		if( SUB_DOMAIN == 'toin' || SUB_DOMAIN == 'testsite'){
			$this->render('edit_toin');
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->OptionHearing->id = $id;
		if (!$this->OptionHearing->exists()) {
			throw new NotFoundException(__('Invalid %s', __('option hearing')));
		}
		if ($this->OptionHearing->delete()) {
			$this->Session->setFlash(
				__('The %s deleted', __('option hearing')),
				'alert',
				array(
					'plugin' => 'TwitterBootstrap',
					'class' => 'alert-success'
				)
			);
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(
			__('The %s was not deleted', __('option hearing')),
			'alert',
			array(
				'plugin' => 'TwitterBootstrap',
				'class' => 'alert-error'
			)
		);
		$this->redirect(array('action' => 'index'));
	}

	public function check() {
		$user = $this->Auth->user();
		$data = $this->OptionHearing->find('first', array('conditions' => array(
			'user_id' => $user['id']
		)));
		if (empty($data)) {
			$this->redirect('/option_hearings/add');
		}
		$this->redirect('/option_hearings/edit/' . $user['id']);
	}

	public function checkprint( $user_id ) {
		$data = $this->OptionHearing->find('first', array('conditions' => array(
			'user_id' => $user_id
		)));
		if (empty($data)) {
			$this->redirect('/option_hearings/emptySheet/' . $user_id);
		}
		$this->redirect('/option_hearings/edit/'. $user_id .'/print');
	}

	public function emptySheet($user_id = null) {
		Configure::write('debug', 0);
		$print = "print";
		$this->loadModel('User');
		$user = $this->User->find('first',array(
			'conditions'=>array('id'=>$user_id )
		));
		$this->set('user_info', $user['User'] );
		$this->set('print', true);
		$this->layout = 'default_print';
		$this->render('edit');
	}



       public function download() {
               // Configure::write('debug', 0);

               $this->layout = false;
               $this->set('filename', SUB_DOMAIN .'_hearing2.csv');

               $th = array(
                       'ログインID',
					   '店名',
					   '区画番号',
					   'テナントコード',
                       '会社名',
                       '郵便番号',
                       '住所',
                       '電話番号',
                       'FAX',
                       'メールアドレス',
                       '所属部署',
                       'お名前（フリガナ）',
                       'お名前',
                       'ご担当者電話番号',
                       '内線番号',

                       'PRポイント',
                       '商品・サービス名',
                       '商品・サービス名 - 価格',
                       '商品・サービス名 - 備考',
                       'コト消費',
                       '初の取り組みポイント',
                       'レポーター体験ポイント',
                       '媒体掲載用画像1',
                       '媒体掲載用画像2',
                       '媒体掲載用画像3',
                       'ロゴ1',
                       'ロゴ2',
                       'ロゴ無し',
                       '指定書体',
                       '座席数',
                       '喫煙・禁煙',
                       '予約受付について',
                       '予約方法 - 電話',
                       '予約方法 - FAX',
                       '予約方法 - HP',
                       '予約方法 - その他',
                       'ラストオーダー時間',
                       'テイクアウトの可否',
                       'アルコール扱い',
                       'キッズ用椅子の有無',
                       'キッズメニューの有無',
                       'オフィシャルサイト以外のURL',

                       // TODO: 広報担当者もCSVに含める
                       /*
                       '広報担当: 氏名',
                       '広報担当: フリガナ',
                       '広報担当: 部署',
                       '広報担当: 役職',
                       '広報担当: 郵便番号',
                       '広報担当: 住所',
                       '広報担当: 電話番号',
                       '広報担当: 内線番号',
                       '広報担当: FAX',
                       '広報担当: 携帯番号',
                       '広報担当: Email',
                       */
               );


               // TODO: 広報担当者もCSVのリストに含める
               /*
                       '広報担当: 氏名',
                       '広報担当: フリガナ',
                       '広報担当: 部署',
                       '広報担当: 役職',
                       '広報担当: 郵便番号',
                       '広報担当: 住所',
                       '広報担当: 電話番号',
                       '広報担当: 内線番号',
                       '広報担当: FAX',
                       '広報担当: 携帯番号',
                       '広報担当: Email',

                       $data['Section']['name'],
                       $data['Section']['kana'],
                       $data['Section']['department'],
                       $data['Section']['position'],
                       $data['Section']['phone'],
                       $data['Section']['extension'],
                       $data['Section']['fax'],
                       $data['Section']['email'],

               */

				$this->loadModel('User');
				$data_all = $this->User->find('all', array(
					'conditions'=> unserialize( TENANT_USER_AUTHORIZED )
				));
               $td = array();
               foreach($data_all as $key => $data) {
						if( count( $data['OptionHearing']) == 0 ) continue;

						// 店名、重複しているデータを除く
						if( isset( $data['Signboard'])){
							$all_signboards = $data['Signboard'];
							$signboard_last = array();
							$prev_modified = 0;
							foreach( $all_signboards as $signboard){
								if( $prev_modified == 0 )
									$signboard_last = $signboard;
								else if( $signboard['modified'] > $prev_modified ) // 新しいなら
									$signboard_last = $signboard; // 上書き
								$prev_modified = $signboard['modified'];
							}
						}
						if( isset( $signboard_last['shop_sign']))
							$shop_sign = $signboard_last['shop_sign'];
						else
							$shop_sign = '';

						$food_yoyaku_tel = ($data['OptionHearing'][0]['food_yoyaku_tel']=='1')? '電話' : '';
						$food_yoyaku_fax = ($data['OptionHearing'][0]['food_yoyaku_fax']=='1')? 'FAX' : '';
						$food_yoyaku_hp = ($data['OptionHearing'][0]['food_yoyaku_hp']=='1')? 'HP' : '';
						$food_yoyaku_etc = ($data['OptionHearing'][0]['food_yoyaku_etc']=='1')? 'その他' : '';
                       $result = array(
                               $data['User']['loginid'],
							   $shop_sign,
							   $data['User']['block'],
							   $data['User']['tenant_code'],
                               $data['User']['company'],
                               $data['User']['zipcode'],
                               $data['User']['address'],
                               $data['User']['company_phone'],
                               $data['User']['fax'],
                               $data['User']['email'],
                               $data['User']['department'],
                               $data['User']['name_kana'],
                               $data['User']['name'],
                               $data['User']['phone'],
                               $data['User']['extension'],
                               $data['OptionHearing'][0]['haikei'],
                               $data['OptionHearing'][0]['osusume_service_name'],
                               $data['OptionHearing'][0]['osusume_service_price'],
                               $data['OptionHearing'][0]['osusume_comment'],
                               $data['OptionHearing'][0]['comment1'],
                               $data['OptionHearing'][0]['comment4'],
                               $data['OptionHearing'][0]['comment6'],
                               $data['OptionHearing'][0]['image1_file_name'],
                               $data['OptionHearing'][0]['image2_file_name'],
                               $data['OptionHearing'][0]['image3_file_name'],
                               $data['OptionHearing'][0]['logo_file_name'],
                               $data['OptionHearing'][0]['logo2_file_name'],
                               $data['OptionHearing'][0]['no_logo'],
                               $data['OptionHearing'][0]['logo_shotai'],
                               $data['OptionHearing'][0]['food_comment2'],
                               $data['OptionHearing'][0]['food_comment3'],
                               $data['OptionHearing'][0]['food_comment5'],
                               $food_yoyaku_tel,
                               $food_yoyaku_fax,
                               $food_yoyaku_hp,
                               $food_yoyaku_etc,
                               $data['OptionHearing'][0]['food_comment1'],
                               $data['OptionHearing'][0]['food_takeout'],
                               $data['OptionHearing'][0]['food_alcohol_ok'],
                               $data['OptionHearing'][0]['food_childseat'],
                               $data['OptionHearing'][0]['food_kidsmenu'],
                               $data['OptionHearing'][0]['unofficial_url'],
                       );
                       $td[] = $result;
               }

               $this->set(compact('th', 'td'));
       }

}
