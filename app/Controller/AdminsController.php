<?php

App::uses('AppController', 'Controller');

class AdminsController extends AppController {

/**
 * before
 */
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow();
        $role = $this->Session->read('Auth.User.role');
        if( $role != 'admin' ){
            $this->redirect('/');
        }
    }
/**
 * functions
 */
	function ShowFileLimit()
    {
        $this->autoRender = false;
        ?><pre>
        - memory_limit=<?php echo ini_get('memory_limit')."\n"; ?>
        - post_max_size=<?php echo ini_get('post_max_size')."\n"; ?>
        - upload_max_filesize=<?php echo ini_get('upload_max_filesize')."\n"; ?>
        - MAX_FILE_SIZE=<?php echo empty($_REQUEST['MAX_FILE_SIZE'])
                                   ? 'is not set' 
                                   : $_REQUEST['MAX_FILE_SIZE']."\n"; ?>
        </pre><?php
    }
	function ShowPhpInfo()
    {
        $this->autoRender = false;
        echo phpinfo();
    }
}
