<?php
App::uses('Component', 'Controller');
class PermitComponent extends Component {

	public function isReadPermitted( $user, $sheet ){
		$read_permission = isset(	$user['read_permission'])?
									$user['read_permission']: array();
		$write_permission = isset(	$user['write_permission'])?
									$user['write_permission']: array();
		$read_permission = is_array($read_permission)? $read_permission : array();
		$write_permission = is_array($write_permission)? $write_permission : array();
		$permitted = in_array( $sheet, $read_permission )
				  || in_array( $sheet, $write_permission );
		if(( $user['role'] == 'admin' )
		 ||( $user['role'] == 'partner' && $permitted )){
			return true;
		}else{
			return false;
		}
	}

	public function isWritePermitted( $user, $sheet ){
		$write_permission = isset(	$user['write_permission'])?
									$user['write_permission']: '';
		$write_permission = is_array($write_permission)? $write_permission : array();
		$permitted = in_array( $sheet, $write_permission );
		if(( $user['role'] == 'admin' )
		 ||( $user['role'] == 'partner' && $permitted )){
			return true;
		}else{
			return false;
		}
	}

}
