<?php
App::uses('Component', 'Controller');
class MallComponent extends Component {
	public function setFieldsAndLabels( &$labels, &$fields, &$uriage_yosan_label, &$uriage_yosan_1nendo, &$uriage_yosan_2nendo, &$syutten_jyoukyou_label, &$target_info, &$data_bunseki_label, &$kaiin_suu_label, &$schedule_label, &$schedule_example, &$jyou_kokyaku_label ){

		$labels = array();	$fields = array();

		$labels[] = 'id';	$fields[] = 'id';
		$labels[] = 'user_id';	$fields[] = 'user_id';

		//【１】月別売上予算をご記入ください。
		$labels[] = '売上予算';	$fields[] = 'uriage_yosan';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = '初年度営業計画';	$fields[] = 'syonendo_keikaku';
		}
		//【２】出店状況についてご記入ください。
		$labels[] = '出店状況';	$fields[] = 'syutten_jyoukyou';
		$labels[] = '他店出店状況';	$fields[] = 'taten_syutten_jyoukyou';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = '最上位店舗';	$fields[] = 'jyoui_tenpo';
			$labels[] = '位置付け';	$fields[] = 'itiduke';
		}
		//【３】営業計画・施策について
		$labels[] = 'セールス・ポイント';	$fields[] = 'sales_point';
		$labels[] = 'メイン・ターゲット';	$fields[] = 'main_target';
		$labels[] = 'サブ・ターゲット';	$fields[] = 'sub_target';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = '男女構成比';	$fields[] = 'target_danjyo';
			$labels[] = '年齢構成比';	$fields[] = 'target_nenrei';
			$labels[] = '商品構成';	$fields[] = 'syouhin_kousei';
			$labels[] = '商品構成変更点';	$fields[] = 'syouhin_kousei_henkou';
			$labels[] = '新商品';	$fields[] = 'sin_syouhin';
		}
		$labels[] = '競合店';	$fields[] = 'kyougou_ten';
		$labels[] = '差別化手法';	$fields[] = 'sabetuka_syuhou';
		//【４】顧客管理について
		$labels[] = '顧客管理';	$fields[] = 'kokyaku_kanri';
		$labels[] = '顧客管理目的';	$fields[] = 'kokyaku_kanri_mokuteki';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = '顧客管理方法';	$fields[] = 'kokyaku_kanri_houhou';
			$labels[] = 'その他のコミュニケーション方法';	$fields[] = 'kokyaku_kanri_tool';
		}else{
			$labels[] = '顧客管理ツール';	$fields[] = 'kokyaku_kanri_tool';
		}
		$labels[] = 'データ分析';	$fields[] = 'data_bunseki';
		$labels[] = '販促アプローチ';	$fields[] = 'hansoku_approach';
		$labels[] = '上顧客';	$fields[] = 'jyou_kokyaku';
		$labels[] = '会員数';	$fields[] = 'kaiin_suu';
		//【５】お客さま感謝デー特典について
		$labels[] = '感謝デー特典';	$fields[] = 'kansya_day_tokuten';
		$labels[] = 'GG感謝デー特典';	$fields[] = 'gg_kansya_day_tokuten';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = 'シネマdeプラス（仮称）';	$fields[] = 'cinema_de_plus_tokuten';
		}
		//【６】販促計画・営業のしかけについて
		$labels[] = 'スケジュール';	$fields[] = 'schedule';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = 'オープン販促';	$fields[] = 'open_hansoku';
			$labels[] = 'スマートフォン販促';	$fields[] = 'smart_phone_hansoku';
			$labels[] = 'スマートフォン販促内容';	$fields[] = 'smart_phone_hansoku_naiyou';
		}
		//【７】人員体制について
		$labels[] = '営業形体';	$fields[] = 'eigyou_keitai';
		$labels[] = '販売代行・FC決定';	$fields[] = 'daikou_kettei';
		$labels[] = '販売代行・FC会社名';	$fields[] = 'daikou_kaisya';
		$labels[] = '店舗従業員数';	$fields[] = 'tenpo_jyuugyouin_num';
		$labels[] = 'うち社員数';	$fields[] = 'syain_num';
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = 'パート社員数';	$fields[] = 'part_syain_num';
			$labels[] = '人員配置計画';	$fields[] = 'jinin_keikaku';
			$labels[] = '人事';	$fields[] = 'jinji1';
			$labels[] = '人事';	$fields[] = 'jinji2';
		}else{
			$labels[] = '新規採用予定数';	$fields[] = 'sin_saiyou_num';
		}
		$labels[] = '決定';	$fields[] = 'kettei';
		$labels[] = '決定時期';	$fields[] = 'kettei_yotei';
		$labels[] = '来店頻度';	$fields[] = 'raiten_hindo';
		//【８】店舗従業員の教育(および教育内容）について
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$labels[] = '従業員教育（オープン前）';	$fields[] = 'jyuugyouin_kyouiku_before_open';
			$labels[] = '従業員教育（オープン後）';	$fields[] = 'jyuugyouin_kyouiku_after_open';
		}else{
			$labels[] = '従業員教育';	$fields[] = 'jyuugyouin_kyouiku';
		}
		//【９】店舗と本部でのコミュニケーション手段
		$labels[] = 'コミュニケーション頻度<br />（本部→店舗）';	$fields[] = 'communication_to_tenpo_hindo';
		$labels[] = 'コミュニケーション手段<br />（本部→店舗）';	$fields[] = 'communication_to_tenpo_syudan';
		$labels[] = 'コミュニケーション内容<br />（本部→店舗）';	$fields[] = 'communication_to_tenpo_naiyou';
		$labels[] = 'コミュニケーション頻度<br />（店舗→本部）';	$fields[] = 'communication_to_honbu_hindo';
		$labels[] = 'コミュニケーション手段<br />（店舗→本部）';	$fields[] = 'communication_to_honbu_syudan';
		$labels[] = 'コミュニケーション内容<br />（店舗→本部）';	$fields[] = 'communication_to_honbu_naiyou';


		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$uriage_yosan_label = array(
				'11月度予算［千円］',
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］',
				'03月度予算［千円］',
				'04月度予算［千円］',
				'05月度予算［千円］',
				'06月度予算［千円］',
				'07月度予算［千円］',
				'08月度予算［千円］',
				'09月度予算［千円］',
				'10月度予算［千円］',
				'11月度予算［千円］',
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］'
			);
			$uriage_yosan_1nendo = 3;
			$uriage_yosan_2nendo = 9;
		}else{
			$uriage_yosan_label = array(
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］',
				'03月度予算［千円］',
				'04月度予算［千円］',
				'05月度予算［千円］',
				'06月度予算［千円］',
				'07月度予算［千円］',
				'08月度予算［千円］',
				'09月度予算［千円］',
				'10月度予算［千円］',
				'11月度予算［千円］',
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］'
			);
			$uriage_yosan_1nendo = 2;
			$uriage_yosan_2nendo = 8;
		}

		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$syutten_jyoukyou_label = array(
				'日本初出店',
				'新業態',
				'中部（愛知、三重、岐阜）初出店',
				'三重県初出店'
			);
		}else{
			$syutten_jyoukyou_label = array(
				'日本初出店',
				'新業態',
				'関東（千葉県・埼玉県・神奈川県・東京都）初出店',
				'千葉県初出店'
			);
		}

		$target_info = array();
		$target_info['label'] = array(
				'年齢',
				'性別等',
				'備考'
		);
		$target_info['ages'] = array(
									'0' => '0歳〜4歳',
									'5' => '5歳〜9歳',
									'10' => '10代前半',
									'15' => '10代後半',
									'20' => '20代前半',
									'25' => '20代後半',
									'30' => '30代前半',
									'35' => '30代後半',
									'40' => '40代前半',
									'45' => '40代後半',
									'50' => '50代以上',
									);
		$target_info['sex'] = array(
									'man' => '男',
									'female' => '女',
									'kids' => 'キッズ',
									'family' => 'ファミリー',
									);
		$target_info['note'] = '（例：ファミリーでの来店が多い）';

		$data_bunseki_label = array(
				'店舗',
				'本部（POSレジ連動による購買分析有り）',
				'本部（POSレジ連動による購買分析無し）'
		);

		$kaiin_suu_label = array(
				'商圏内会員数',
				'当ＳＣ目標会員数',
				'他店舗名',
				'他店舗会員数'
		);

		$schedule_label = array(
				'12月度<br /><br />',
				'1月度<br /><br />',
				'2月度<br /><br />',
				'3月度<br /><br />',
				'4月度<br /><br />',
				'5月度<br /><br />',
				'6月度<br /><br />',
				'7月度<br /><br />',
				'8月度<br /><br />',
				'9月度<br /><br />',
				'10月度<br /><br />',
				'11月度<br /><br />',
				'12月度<br /><br />',
				'1月度<br /><br />',
				'2月度<br /><br />',
				'3月度<br /><br />'
		);

		$schedule_example = array(
				"オープニングフェア<br />
				クリスマスギフト<br />
				プレセール",		
				"福袋<br />
				バーゲン",		
				"スーツフェア<br />
				バレンタイン<br />
				新生活提案",		
				"ホワイトデー<br />
				春休み<br />
				新入学",		
				"新生活<br />
				新入学",		
				"GWフェア<br />
				母の日ギフト",		
				"父の日ギフト<br />
				顧客向けプレセール<br />
				バーゲン",		
				"クリアランス<br />
				夏休み",		
				"初秋プロパー打ち出し<br />
				服飾雑貨の提案",		
				"オケージョン<br />
				敬老の日",		
				"ジャケットフェア<br />
				運動会",		
				"コートフェア",		
				"顧客向けプレセール<br />
				クリスマスギフト強化",		
				"福袋<br />
				バーゲン",		
				"スーツフェア<br />
				バレンタイン",		
				"ホワイトデー<br />
				新入学強化"
		);

		$jyou_kokyaku_label = array(
				'上客',
				'定義',
				'特典'
		);

		return true;
	}
}
