<?php
class AdministrationsController extends AppController
{
    public $helpers = array('Html');
    public
        $uses = Array('Administration'),
        $components = Array(
            'Session'
        );
    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function index()
    {
        $user = $this->Auth->user();
        if( $user['role'] != 'admin' && $user['id'] != $id ){
            $this->Session->setFlash(__('編集権限がありません。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            $this->redirect(array('controller'=>'Users', 'action'=>'index'));
        }
        if ($this->request->is('get')) {
            $this->request->data = $this->Administration->find('first');
        }
        $this->set('user', $this->Session->read('Auth.User'));
        $this->set('title_for_layout', '管理 | インデックス');
        $this->set('administration', $this->Administration->find('first'));

        if( $this->request->is('post')){
            $data = $this->request->data;

            $entry_expiration = implode("-", $data['Administration']['entry_expiration']);
            $data['Administration']['entry_expiration'] = $entry_expiration;
            $edit_expiration = implode("-", $data['Administration']['edit_expiration']);
            $data['Administration']['edit_expiration'] = $edit_expiration;


            $res = $this->Administration->save( $data );
            if( $res ) {
                $this->Session->setFlash(__('設定完了しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-success'));
            } else {
                $this->Session->setFlash(__('設定失敗しました。'),
                    'alert', array('plugin'=>'TwitterBootstrap', 'class'=>'alert-error'));
            }
        }
    }
}
?>
