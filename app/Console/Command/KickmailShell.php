<?php 
App::uses('CakeEmail', 'Network/Email');

class KickmailShell extends AppShell {

	public $uses = array('QueueMail');

	public function main(){
		// キューから、処理中でないもの、完了してないものを読み出す
		$queue_mails = $this->QueueMail->find('all',array(
			'conditions'=>array(
				'processing'=>'0',
				'has_completed'=>'0'
			)
		));
		//debug( $queue_mails );

		// まず処理中フラグを全部たてる
		foreach( $queue_mails as $queue_mail ){
			$save_data = $queue_mail;
			$save_data['QueueMail']['processing'] ='1';
			$res = $this->QueueMail->save( $save_data );
		}
		foreach( $queue_mails as $queue_mail ){
			$cakeemail = new CakeEmail();
			$cakeemail->config('remind'); //$remind の設定を読み込み。
			$cakeemail->to( $queue_mail['QueueMail']['mail_to']);
			$cakeemail->from( $queue_mail['QueueMail']['mail_from']);
			$cakeemail->subject( $queue_mail['QueueMail']['mail_subject']);
			$vars['message'] = $queue_mail['QueueMail']['mail_body'];
			$cakeemail->viewVars( $vars ); //テンプレートにデータを渡す。
			$this->out('KickmailShell Send to ' . $queue_mail['QueueMail']['mail_to']);
			$save_data = $queue_mail;
			if( $cakeemail->send()){ // メール送信
				$this->out('KickmailShell Success.');
				$save_data['QueueMail']['processing'] ='1';
				$save_data['QueueMail']['send'] = date('Y-m-d H:i:s');
				$save_data['QueueMail']['has_completed'] = '1'; // 完了フラグ
			}else{
				$this->out('KickmailShell Error.');
				$save_data['QueueMail']['processing'] ='1';
				$save_data['QueueMail']['send'] = date('Y-m-d H:i:s');
				$save_data['QueueMail']['has_completed'] = '1'; // 完了フラグ
				$save_data['QueueMail']['has_error'] = '1'; // エラーフラグ
			}
			$res = $this->QueueMail->save( $save_data );
			usleep(rand(1000000, 2000000)); // 1 - 2秒に1回
		}
		$this->out('KickmailShell End.');
	}
}
