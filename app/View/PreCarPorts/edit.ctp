<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>オープン前：臨時駐車場利用台数アンケート</h2>
<p>
2013年12月2日～2013年12月15日に車通勤される方には『オープン前臨時駐車許可証』を発行いたしますので、貴店の駐車台数をとりまとめ、ご入力下さい。
</p>
        <?php if( defined('PRE_CAR_PORT_LINK')):
            echo '<a class="" href="';
            echo $this->Html->url( PRE_CAR_PORT_LINK );
            echo '"><i class="icon-file"></i><large>駐車場ご案内(PDF)</large></a>';
        endif;?>
    </div>
<pre>
・原則、公共交通機関をご利用いただきますよう、お願い致します。
・収容台数に限りがございますので、上限台数を設定させていただきます。
・詳細は運営管理説明会資料19～23ページをご確認下さい。
</pre><br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>オープン前：臨時駐車場利用台数アンケート</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['PreCarPort']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['PreCarPort']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
    <tr><td width="200px">使用期間</td><td>12月2日（月）～12月15日（日）</td></tr>
    <tr><td>駐車料金</td><td>有料（1日上限1,000円）</td></tr>
    <tr><td>上限台数</td><td>大型店舗（店舗面積　500坪以上）　25台<br />
中型店舗（店舗面積　200坪以上）　15台<br />
一般店舗　8台</td></tr>
    <tr><td></td><td></td></tr>
</table>

<p class="clear"></p>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('PreCarPort',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
        <h3>利用台数</h3>
        </div>
        <?php echo $this->BootstrapForm->input('port_num', array('label'=>'希望台数','class' => 'span10')); ?>
        <p class="offset2">※ 半角数字で入力してください。</p>
        <br />
        <div class="alert alert-info">
        <h3>契約書送付先</h3>
        </div>
        <h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）261-8539</p>
        <?php echo $this->BootstrapForm->input('address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('position', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'ご連絡先','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
