<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>両替機・入金機運用説明会・売上会計説明会参加申込書</h2>
        <p>日々の売上報告処理等の説明を、実際に入力用端末機を操作して頂きながら、説明いたします。<br />
           説明会でお渡ししております「売上管理規程」を熟読の上、店長様とレジ担当者様を含め２～３名のご参加をお願い申し上げます。<br />
           なお、会場の都合及び業種ごとに説明内容が異なることから、貴店の該当する日時にご参加頂きますようお願い致します。</p>
    </div>
    <br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>両替機・入金機運用説明会・売上会計説明会参加申込書</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['Cashier']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['Cashier']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
<tr>
  <th></th><th>対象店舗</th><th>両替機・入金機運用説明会</th><th>売上管理説明会</th>
</tr><tr>
  <td>○○月○○日（曜）</td><td>1F</td><td>○○時○○分～○○時○○分</td><td>○○時○○分～○○時○○分</td>
</tr><tr>
  <td>○○月○○日（曜）</td><td>2F</td><td>○○時○○分～○○時○○分</td><td>○○時○○分～○○時○○分</td>
</tr><tr>
  <td>○○月○○日（曜）</td><td>請求制（別棟含む）</td><td>○○時○○分～○○時○○分</td><td>○○時○○分～○○時○○分</td>
</tr><tr>
  <td>○○月○○日（曜）</td><td>1F</td><td>○○時○○分～○○時○○分</td><td>○○時○○分～○○時○○分</td>
</tr><tr>
  <td></td><td></td><td></td><td></td>
</tr>
</table>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('Cashier',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>

    <fieldset>
        <div class="alert alert-info">
        <h3>参加の確認</h3>
        </div>
        <h4>参加の有無</h4>
        <?php echo $this->BootstrapForm->select('attend_mode',
                array('参加'=>'参加', '不参加'=>'不参加'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <p class="offset2">※必ず「参加」／「不参加」を選択してください。</p>
        <h4>参加日時</h4>
        <p>参加される日程にチェックをお付けください。</p>
        <?php echo $this->BootstrapForm->input('attend_day1', array(
            'label'     => '２０１３年○○月○１日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day2', array(
            'label'     => '２０１３年○○月○２日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day3', array(
            'label'     => '２０１３年○○月○３日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day4', array(
            'label'     => '２０１３年○○月○４日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <br />

        <div class="alert alert-info">
          <h3>参加者</h3>
        </div>
        <h4>参加者①</h4>
        <p>※店長様、または店長代理様</p>
        <?php echo $this->BootstrapForm->input('name1', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例） 山田太郎</p>
        <?php echo $this->BootstrapForm->input('position1', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例） 店長</p>
        <?php echo $this->BootstrapForm->input('contact1', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>
        <hr />

        <h4>参加者②</h4>
        <?php echo $this->BootstrapForm->input('name2', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例） 山田花子</p>
        <?php echo $this->BootstrapForm->input('position2', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例） 副店長</p>
        <?php echo $this->BootstrapForm->input('contact2', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>
        <hr />

        <h4>参加者③</h4>
        <?php echo $this->BootstrapForm->input('name3', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田良子</p>
        <?php echo $this->BootstrapForm->input('position3', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">　</p>
        <?php echo $this->BootstrapForm->input('contact3', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
