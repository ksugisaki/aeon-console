<style type="text/css">
    input[type="text"] {
        margin-bottom: 0px;
    }
    th, td {
        vertical-align: middle;
    }
</style>

<section id="tables">
<form>
  <div class="page-header">
    <h3>ヒアリングシート</h3>
  </div>
  
  <table class="table table-bordered">
    <tbody>
      <tr>
        <th style="vertical-align: middle">お名前</th>
        <td style="vertical-align: middle">
           <input type="text" />
           <input type="text" />
        </td>
      </tr>
      <tr>
        <th>月別予算</th>
        <td>
            <h4>2013年度</h4>
            <hr />
            <p>11月度: <input type="text" /></p>
            <p>12月度: <input type="text" /></p>
            <p>01月度: <input type="text" /></p>
            <p>02月度: <input type="text" /></p>
            <hr />
            <h4>2014年度</h4>
            <hr />
            <p>01月度: <input type="text" /></p>
            <p>02月度: <input type="text" /></p>
            <p>03月度: <input type="text" /></p>
            <p>04月度: <input type="text" /></p>
            <p>05月度: <input type="text" /></p>
            <p>06月度: <input type="text" /></p>
            <p>07月度: <input type="text" /></p>
            <p>08月度: <input type="text" /></p>
            <p>09月度: <input type="text" /></p>
            <p>10月度: <input type="text" /></p>
            <p>11月度: <input type="text" /></p>
            <p>12月度: <input type="text" /></p>
        </td>
      </tr>
    </tbody>
  </table>
</form>
</section>
