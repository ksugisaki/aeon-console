<?php
$uriage_yosan_label = array(
		'12月度予算',
		'01月度予算',
		'02月度予算',
		'03月度予算',
		'04月度予算',
		'05月度予算',
		'06月度予算',
		'07月度予算',
		'08月度予算',
		'09月度予算',
		'10月度予算',
		'11月度予算',
		'12月度予算',
		'01月度予算',
		'02月度予算'
);
		if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
			$uriage_yosan_label = array(
				'11月度予算［千円］',
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］',
				'03月度予算［千円］',
				'04月度予算［千円］',
				'05月度予算［千円］',
				'06月度予算［千円］',
				'07月度予算［千円］',
				'08月度予算［千円］',
				'09月度予算［千円］',
				'10月度予算［千円］',
				'11月度予算［千円］',
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］'
			);
			$start = 4;
		}else{
			$uriage_yosan_label = array(
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］',
				'03月度予算［千円］',
				'04月度予算［千円］',
				'05月度予算［千円］',
				'06月度予算［千円］',
				'07月度予算［千円］',
				'08月度予算［千円］',
				'09月度予算［千円］',
				'10月度予算［千円］',
				'11月度予算［千円］',
				'12月度予算［千円］',
				'01月度予算［千円］',
				'02月度予算［千円］'
			);
			$start = 3;

		}

$dummy = array('','','','','','','','','','','','','','','','');
?>

<h3>2013年度予算</h3>
<div class="span6">
<table class="communication table table-bordered" style="">
<?php
$header = array_slice( $uriage_yosan_label,0,$start);
array_unshift( $header, 'テナント名','会社名');
$header[] = '年度合計';
echo $this->Html->tableCells( $header );
foreach( $datas as $key => $data ){
	if( $data['uriage_yosan']){
		$uriage = array_slice( $data['uriage_yosan'],0,$start);
	}else{
		$uriage = array_slice( $dummy,0,$start);
	}
	array_unshift( $uriage, $data['tname'], $data['company']); // 先頭にテナント名、会社名を追加
	$uriage[] = $data['sum0']; // 最後に年度合計をを追加
	echo $this->Html->tableCells( $uriage );
}
?>
</table>
</div>
<br />
<div class="user-control clear">
  <?php
      echo $this->Html->link('2013年度予算情報のエクスポート',
           '/hearings/DownloadYosanCsv/2013',
           array('class'=>'btn btn-primary')
      );
  ?>
</div>
<hr />
<br />

<h3>2014年度予算</h3>
<div class="span12">
<table class="communication table table-bordered" style="">
<?php
$header = array_slice( $uriage_yosan_label,$start,12);
array_unshift( $header, 'テナント名','会社名');
$header[] = '年度合計';
echo $this->Html->tableCells( $header );
foreach( $datas as $key => $data ){
	if( $data['uriage_yosan']){
		$uriage = array_slice( $data['uriage_yosan'],$start,12); // 3月〜2月
	}else{
		$uriage = array_slice( $dummy,$start,12);
	}
	array_unshift( $uriage, $data['tname'], $data['company']); // 先頭にテナント名、会社名を追加
	$uriage[] = $data['sum3']; // 最後に年度合計を追加
	echo $this->Html->tableCells( $uriage );
}
?>
</table>
</div>
<br />
<div class="user-control clear">
  <?php
      echo $this->Html->link('2014年度予算情報のエクスポート',
           '/hearings/DownloadYosanCsv/2014',
           array('class'=>'btn btn-primary')
      );
  ?>
</div>

<?php // debug($datas);?>
