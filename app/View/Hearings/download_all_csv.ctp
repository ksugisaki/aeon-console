<?php
	foreach( $hearings as $h_key => $hearing ){

		/* *** */
		$loginid = $hearings[$h_key]['User']['loginid'];
		$hearings[$h_key]['Hearing']['id'] = $loginid;
		/* *** */
		$array = $hearings[$h_key]['Hearing']['syutten_jyoukyou'];
		$text = '';
		foreach( $array as $key => $value ){
		    $text .= $syutten_jyoukyou_label[ $value ] . "\n";
		}
		$hearings[$h_key]['Hearing']['syutten_jyoukyou'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['main_target'];
		$text = '';
		$text .=  $array[0] . '代' .  "\n";
		$text .=  $array[1] . "\n";
		$text .=  $array[2] . "\n";
		$hearings[$h_key]['Hearing']['main_target'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['sub_target'];
		$text = '';
		$text .=  $array[0] . '代' .  "\n";
		$text .=  $array[1] . "\n";
		$text .=  $array[2] . "\n";
		$hearings[$h_key]['Hearing']['sub_target'] = $text;
		
		/* *** */
		$key = $hearings[$h_key]['Hearing']['kokyaku_kanri'];
		$select = array('1'=>'はい','0'=>'いいえ');
		$text = $select[ $key ];
		$hearings[$h_key]['Hearing']['kokyaku_kanri'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['data_bunseki'];
		$text = '';
		foreach( $array as $key => $value ){
		    $text .= $data_bunseki_label[ $value ] . "\n";
		}
		$hearings[$h_key]['Hearing']['data_bunseki'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['jyou_kokyaku'];
		$select = array('1'=>'区分あり','0'=>'区分なし');
		$text = '';
		$text .= $select[ $array[0]] . "\n";
		$text .= $array[1] . "\n";
		$text .= $array[2] . "\n";
		$hearings[$h_key]['Hearing']['jyou_kokyaku'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['kaiin_suu'];
		$text = '';
		$text .= $kaiin_suu_label[0] . $array[0] . "\n";
		$text .= $kaiin_suu_label[1] . $array[1] . "\n";
		$text .= $kaiin_suu_label[2] . $array[2] . "\n";
		$text .= $kaiin_suu_label[3] . $array[3] . "\n";
		$hearings[$h_key]['Hearing']['kaiin_suu'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['schedule'];
		$text = '';
		foreach( $array[1] as $key => $value ){
			$text .= $schedule_label[ $key ] .":"
					. $array[1][$key] .":"
					. $array[2][$key] ."\n";
		}
		$hearings[$h_key]['Hearing']['schedule'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['communication_to_tenpo_hindo'];
		$label = array('毎日','２～３回／週','１回／週','1回／月');
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$hearings[$h_key]['Hearing']['communication_to_tenpo_hindo'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['communication_to_tenpo_syudan'];
		$label = array('電話','FAX（書面）','メール','面談（直接）');
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$hearings[$h_key]['Hearing']['communication_to_tenpo_syudan'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['communication_to_tenpo_naiyou'];
		$label = array('自社販促','その他');
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$hearings[$h_key]['Hearing']['communication_to_tenpo_naiyou'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['communication_to_honbu_hindo'];
		$label = array('毎日','２～３回／週','１回／週','1回／月');
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$hearings[$h_key]['Hearing']['communication_to_honbu_hindo'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['communication_to_honbu_syudan'];
		$label = array('電話','FAX（書面）','メール','面談（直接）');
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$hearings[$h_key]['Hearing']['communication_to_honbu_syudan'] = $text;
		
		/* *** */
		$array = $hearings[$h_key]['Hearing']['communication_to_honbu_naiyou'];
		$label = array('売上','コンペジ状況','モール状況','その他');
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$hearings[$h_key]['Hearing']['communication_to_honbu_naiyou'] = $text;
		
		$row = $hearings[$h_key]['Hearing'];
		// unset( $row['id']);
		unset( $row['uriage_yosan']);
		$td[] = $row;
	}

	//ファイル名設定
	$this->Csv->setFilename( $filename ); 

	//ヘッダー行を追加
	 $this->Csv->addRow( $th );

	//表の各行を追加していく
	foreach( $td as $t ){
		$this->Csv->addRow( $t );
	}

	//文字コードをUTF-8からSJISに変換して出力。
	echo mb_convert_encoding( $this->Csv->render(), 'SJIS', 'UTF-8');

