<script type="text/javascript">
  // ignore_enter_to_submit.jsに渡す変数
  var print_mode = "<?php echo empty( $print )? '' : $print; ?>";
</script>
<?php echo $this->Html->script(array('scrollback','ignore_enter_to_submit'), array('inline'=>false)); ?>

<?php if( $print == null ){ ?>
<div class="hero-unit">
	<h2><?php echo $title_for_layout ?></h2>
	<p>出店に関するヒアリングシートです</p>
</div>

<?php }else{ ?>
<?php echo $this->Html->script(array('set_tiny_mce'), array('inline'=>false)); ?>
<div class="row-fluid">
	<div class="span6">
		<h2><?php echo $title_for_layout ?></h2>
	</div>
	<div class="span6">
		<?php if( isset( $this->request->data['Hearing']['updated'])){ ?>
			<p>最終更新日： <?php echo( $this->request->data['Hearing']['updated']);?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
<?php } ?>

<?php echo $this->Session->flash(); ?>
<div class="hearing form">
    <?php
        echo $this->Form->create('Hearing', Array('class'=>'form-horizontal span11','url' =>'index/'. $user_id ));
        foreach ( $fields as $key => $field ){
        if( $field == 'id' ):
            echo $this->Form->input('id', array('type'=>'hidden'));
        elseif( $field == 'user_id' ):
            echo $this->Form->input('user_id', array('type'=>'hidden', 'value'=>$user_id));

        elseif( $field == 'uriage_yosan' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【１】月別売上予算をご記入ください。</h3>';
            echo '＊月度の売上は、毎月1日～末日の売上予算を記入してください。';
            echo '</div>';
            echo '<h4 class="">2013年度</h4>';
            if( isset( $this->request->data['Hearing']['uriage_yosan']))
                $uriage_yosan = $this->request->data['Hearing']['uriage_yosan'];
            $sum = 0;
            $sum1 = 0;
            foreach( $uriage_yosan_label as $key => $value ){
                if( ! isset( $uriage_yosan[$key])) $uriage_yosan[$key] = 0;
                $sum += $uriage_yosan[$key];
                echo $this->Form->input('uriage_yosan' . $key, array('label'=>$value,'type'=>'text','value'=>$uriage_yosan[$key]));
                if( $key == $uriage_yosan_1nendo ){
                    if( $locked == 0)
                        echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
                    else
                        echo MESSAGE_AT_LOCKED;
                    echo '<br />';
                    echo '<hr /><p>2013年度合計 ' .  $sum . '［千円］</p>';
                    $sum = 0;
                    echo '<hr /><h4 class="">2014年度</h4>';
                }
                if( $key == $uriage_yosan_2nendo ){
                    if( $locked == 0 )
                        echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
                    else
                        echo MESSAGE_AT_LOCKED;
                    echo '<br />';
                    echo '<hr /><p>上期合計 ' .  $sum . '［千円］</p>'; $sum1 = $sum; $sum = 0;
                    echo '<hr />';
                }
            }
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br />';
            echo '<hr /><p>下期合計 '. $sum .'［千円］</p>';
            echo '<hr /><p>2014年度合計 '.( $sum + $sum1 ).'［千円］</p>';
            echo '<hr />';

        elseif( $field == 'syutten_jyoukyou' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3 id="syutten_jyoukyou">【２】出店状況についてご記入ください。</h3>';
            echo '</div>';
            echo '<h4>'.MALL_NAME.'への出店状況にチェックを入れてください。</h4>';
            echo $this->Form->input( $field, array(
                'label'     => $labels[ $key ],
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => $syutten_jyoukyou_label
            ));
        elseif( $field == 'taten_syutten_jyoukyou' ):
            echo '<pre class="span10">';
			if( SUB_DOMAIN == 'toin' || SUB_DOMAIN == 'testsite'):
				echo '<dl><dt>＊店名および出店状況については、9月初旬予定の全体概要リリースで使用させていただきます。</dt>';
			else:
				echo '<dl><dt>＊店名および出店状況については、9月末予定の全体概要リリースで使用させていただきます。</dt>';
			endif;
            echo '<dd>店名が未決定の場合は、8月末までにご決定いただきますようよろしくお願いいたします。</dd>';
            echo '<dd>なお、2013年12月に同じ屋号で店舗がオープンする場合は、その店舗のオープン日を下記に記載してください。</dd>';
            echo '<dd>急遽、「●●初出店」に該当しなくなった場合には、お手数ではございますが、ご連絡をお願いいたします。</dd></dl>';
            echo '</pre>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';

        elseif( $field == 'sales_point' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【３】営業計画・施策について</h3>';
            echo '</div>';
            echo '<h4>①店舗・商品について教えてください。</h4>';
            echo '<p>◆自店のコンセプト、セールスポイント（強み）についてご記入ください。</p>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
        elseif( $field == 'main_target' or $field == 'sub_target'):
            echo '<p>◆' . $labels[$key]. '</p>';
            if( isset( $this->request->data['Hearing'][$field]))
                $target = $this->request->data['Hearing'][$field];
            foreach( $target_info['label'] as $key => $label ){
                if( ! isset( $target[$key])) $target[$key] = ' ';
                if( $label == '年齢'){
                    echo $this->Form->select( $field . $key, $target_info['ages'],
                        array('empty' => $label .'を選んでください','class'=>'offset2','value'=>$target[$key]));
                    echo '<br />';
                }else
                if( $label == '性別等'){
                    echo $this->Form->select( $field . $key, $target_info['sex'],
                        array('empty' => $label .'を選んでください','class'=>'offset2','value'=>$target[$key]));
                    echo '<br />';
                }else{
                    echo '<br />';
                    echo '<p class="offset2">' . $target_info['note'] . '</p>';
                    echo $this->Form->input( $field . $key, array('label'=>'備考','type'=>'text','class'=>'span8','rows'=>'2','value'=>$target[$key]));

                }
            }
        elseif( $field == 'kyougou_ten' ):
            echo '<p>◆競合店（自社競合・他社競合）／競合ブランド</p>';
            echo $this->Form->input( $field, array('label'=>'競合店とその理由','type'=>'text','class'=>'span8','rows'=>'2'));
        elseif( $field == 'sabetuka_syuhou' ):
            echo '<p>◆競合店との差別化手法（自社競合・他社競合）</p>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';


        elseif( $field == 'kokyaku_kanri' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【４】顧客管理について</h3>';
            echo '</div>';
            echo '<h4>１．顧客管理はされていますか？ </h4>';
            //echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span2'));
            echo $this->Form->select( $field, array('1'=>'はい','0'=>'いいえ'),
                        array('empty' => '選択してください','class'=>'offset2'));


        elseif( $field == 'kokyaku_kanri_mokuteki' ):
            echo '<h4>２．顧客管理の目的・考え方について教えてください。</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
            echo '<h4>３．その顧客管理ツール（お客様とコネクションを作るツール）は何ですか？内容をご記入ください。</h4>';
        elseif( $field == 'kokyaku_kanri_tool' ):
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
            echo '<pre class="span10">';
            echo '<dl>';
            echo '<dt>例１）</dt><dd>スタンプカードを発行し、●●円ごとに１スタンプ、スタンプ●個で住所等を記入いただき、回収、<br />次回お買上時に10%OFFの特典。</dd>';
            echo '<dt>例２）</dt><dd>接客をして印象深かった方や複数購入の方など、商品への支持度を見て顧客カードに直接記入していただく。</dd>';
            echo '<dt>例３）</dt><dd>インターネット上で住所等を登録頂いてメールマガジン配信。</dd>';
            echo '</dl>';
            echo '</pre>';
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
        elseif( $field == 'data_bunseki' ):
            echo '<h4>４．会員リスト等データの管理や分析はどちらでされていますか？</h4>';
            echo $this->Form->input( $field, array(
                'label'     => $labels[ $key ],
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => $data_bunseki_label
            ));
        elseif( $field == 'hansoku_approach' ):
            echo '<h4>５．会員様へのサービス・特典内容や販促でのアプローチ方法を教えてください。</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
        elseif( $field == 'jyou_kokyaku' ):
            echo '<h4>６．一般顧客と上顧客との区別はありますか？</h4>';
            if( isset( $this->request->data['Hearing'][$field]))
                $jyou_kokyaku = $this->request->data['Hearing'][$field];
            foreach( $jyou_kokyaku_label as $key => $value ){
                if( ! isset( $jyou_kokyaku[$key])) $jyou_kokyaku[$key] = ' ';
                if( $key == 0){
                    echo $this->Form->select( $field, array('1'=>'区分あり','0'=>'区分なし'),
                            array('empty' => '区分の有無を選択してください','class'=>'offset2',
                                  'value'=>$jyou_kokyaku[$key]));
                    echo '<br /><br />';
                    echo '<p class="">また、ある場合は、どのような定義で分けられますか？その特典内容も教えてください。</p>';
                }else{
                    echo $this->Form->input( $field . $key, array('label'=>$value,'type'=>'text','value'=>$jyou_kokyaku[$key],'class'=>'span8','rows'=>'2'));
                }
            }

        elseif( $field == 'kaiin_suu' ):
            echo '<h4>７．商圏内会員数および当ＳＣ店で目標とする会員数をご記入ください。その根拠となる他店会員数の事例を教えてください。</h4>';
            if( isset( $this->request->data['Hearing'][$field]))
                $kaiin_suu = $this->request->data['Hearing'][$field];
            foreach( $kaiin_suu_label as $key => $value ){
                if( ! isset( $kaiin_suu[$key])) $kaiin_suu[$key] = ' ';
                echo $this->Form->input( $field . $key, array('label'=>$value,'type'=>'text','value'=>$kaiin_suu[$key]));
            }
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';

        elseif( $field == 'kansya_day_tokuten' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【５】お客さま感謝デー特典について</h3>';
			if( defined('THANKSDAY_LINK')):
				echo '<a class="pull-right" target="_blank" href="';
				echo $this->Html->url(THANKSDAY_LINK);
				echo '"><i class="icon-file"></i>お客様感謝デー・ＧＧ感謝デーについて(PDF)</a>';
				$link_title = "お客様感謝デー・ＧＧ感謝デーについて";
			endif;
			if( defined('HANSOKU_LINK')):
				echo '<a class="pull-right" target="_blank" href="';
				echo $this->Html->url( HANSOKU_LINK );
				echo '"><i class="icon-file"></i>販促企画について(PDF)</a>';
				$link_title = "販促企画について";
			endif;
            echo '<p>&nbsp;</p>';
            echo '</div>';
			if( isset( $link_title ))
				echo '<h4>　＊別紙の「'. $link_title .'」のご案内をご参照・ご確認のうえ、ご記入をお願いいたします。</h4>';
            echo '<p class="">～特典内容について～</p>';
            echo '<p class="">・感謝デーの特典については、各店のレジで操作をしていただくことになります。</p>';
            echo '<p class="">　クレジット請求時の割引ではありませんので、ご注意ください。</p>';
            echo '<pre class="span10">';
            echo '<dl><dt>【記入例】</dt>';
            echo '<dd>全品5%OFF ・・・店内商品全てが5%OFFの対象となります。</dd>';
            echo '<dd>5%OFF（一部商品除外） ・・・セール品除外の場合は、一部商品除外という表現になります。</dd>';
            echo '<dd>自店ポイントカード2倍 ・・・自社の発行しているポイントカードが対象です。</dd>';
            echo '<dd>●円以上お買上げの方にノベルティプレゼント</dd>';
            echo '<dd>ソフトドリンクサービス</dd>';
            echo '</dl></pre><br />';
            echo '<h4>【お客さま感謝デー特典内容】</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));

        elseif( $field == 'gg_kansya_day_tokuten' ):
            echo '<h4>【G.G.感謝デー特典内容】</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
 
        elseif( $field == 'schedule' ):
           if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';
            echo '<div class="alert alert-info span10">';
            echo '<h3>【６】販促計画・営業のしかけについて</h3>';
			if( defined('SCHEDULE_LINK')):
				echo '<a class="pull-right" href="';
				echo $this->Html->url(SCHEDULE_LINK);
				echo '"><i class="icon-file"></i>SCスケジュール(PDF)</a>';
			endif;
            echo '<p>予定の範囲で結構ですのでご記入ください。</p>';
            echo '</div>';
            if( isset( $this->request->data['Hearing']['schedule']))
                $schedule = $this->request->data['Hearing']['schedule'];
            $schdule_table = array();
            // 例
            $schdule_table[] = array( '例<br />（１２月）', 'オープニング・フェア','','オープン告知→雑誌●●にて告知予定' );
            // 入力エリア
            foreach( $schedule_label as $key => $value ){
                if( ! isset( $schedule[1][$key] )) $schedule[1][$key] = '';
                if( ! isset( $schedule[2][$key] )) $schedule[2][$key] = '';
                $input1 = '<textarea rows="5" type="text" name="data[Hearing][schedule_1_'. $key .']">'
                        . $schedule[1][$key]
                        . '</textarea>';
                $example = '<p class="">' . $schedule_example[ $key ] . '</p>';
                $input2 = '<textarea rows="5" type="text" name="data[Hearing][schedule_2_'. $key .']">'
                        . $schedule[2][$key]
                        . '</textarea>';
                $schdule_table[] = array( $value, $input1,$example,$input2 );
            }
            echo '<table class="table table-condensed">';
            echo $this->Html->tableHeaders( array('','貴店のスケジュール','記入例','CM・雑誌等での告知予定') );
            echo $this->Html->tableCells( $schdule_table );
            echo '</table>';
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';

        elseif( $field == 'eigyou_keitai' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【７】人員体制について</h3>';
            echo '</div>';
            echo '<h4>営業形体は、直営でしょうか?  販売代行・FCを利用されるでしょうか?</h4>';
            echo $this->Form->select( $field, array('直営'=>'直営','販売代行・FC'=>'販売代行・FC'),
                                      array('empty' => '営業形体を選択してください','class'=>'offset2'));
            echo '<br /><br />';
        elseif( $field == 'daikou_kettei' ):
            echo '<h4>販売代行・FCは、未定でしょうか? 決定済みでしょうか?</h4>';
            echo $this->Form->select( $field, array('未定'=>'未定','決定済み'=>'決定済み'),
                                      array('empty' => '決定済みか否か','class'=>'offset2'));
            echo '<br /><br />';
        elseif( $field == 'daikou_kaisya' ):
            echo '<h4>販売代行・FCの候補先・決定先会社名をご記入ください。</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));

        elseif( $field == 'tenpo_jyuugyouin_num' ):
            echo '<h4>店舗従業員数</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span3'));
        elseif( $field == 'syain_num' ):
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span3'));
        elseif( $field == 'sin_saiyou_num' ):
            echo '<h4>新規採用予定数</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span3'));
        elseif( $field == 'kettei' ):
            echo '<h4>店長様の決定時期がわかればご記入ください。</h4>';
            echo $this->Form->select( $field, array('未定'=>'未定','決定済み'=>'決定済み'),
                                      array('empty' => '決定済みか否か','class'=>'offset2'));
            echo '<br /><br />';
        elseif( $field == 'kettei_yotei' ):
            echo '<h4>未決定の場合は、決定時期をご記入下さい。</h4>';
            echo '<p class="offset2">（何月頃決定予定かをお書き下さい。）</p>';
            echo '<p class="offset2">※イオンモールとしては、９月末までに決定をお願いしております。</p>';
            echo $this->Form->input( $field, array('label'=>'','type'=>'text','class'=>'span8','rows'=>'2'));
        elseif( $field == 'raiten_hindo' ):
            echo '<h4>営業担当者様（エリアマネージャー）の店舗来店頻度はどのくらいでしょうか？</h4>';
            echo $this->Form->select( $field, array('常駐'=>'常駐','週１回以上'=>'週１回以上','週１回'=>'週１回','月２〜３回'=>'月２〜３回','月１回'=>'月１回','２〜３ヶ月に１回'=>'２〜３ヶ月に１回'),
                                      array('empty' => '来店頻度を選択してください','class'=>'offset2'));
            echo '<br /><br />';
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';

        elseif( $field == 'jyuugyouin_kyouiku' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【８】店舗従業員の教育(および教育内容）について</h3>';
            echo '<p>（オープン前の研修等について）</p>';
            echo '</div>';
            echo '<pre class="span10">';
            echo '<dl><dt>（例）</dt><dd>アルバイトについては、オープンまでの2ヶ月間●●店で研修予定。</dd><dl>';
            echo '</pre>';
            echo $this->Form->input( $field, array('label'=>'','type'=>'text','class'=>'span8','rows'=>'2'));
            if( $locked == 0 )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
            else
                echo MESSAGE_AT_LOCKED;
            echo '<br /><br /><br />';

        elseif( $field == 'communication_to_tenpo_hindo' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【９】店舗と本部でのコミュニケーション手段</h3>';
            echo '</div>';
            $cell_a1 = $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('毎日','２～３回／週','１回／週','1回／月')
            ));
        elseif( $field == 'communication_to_tenpo_syudan' ):
            $cell_a2 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('電話','FAX（書面）','メール','面談（直接）')
            ));
        elseif( $field == 'communication_to_tenpo_naiyou' ):
            $cell_a3 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('自社販促','その他')
            ));
        elseif( $field == 'communication_to_honbu_hindo' ):
            $cell_b1 = $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('毎日','２～３回／週','１回／週','1回／月')
            ));
        elseif( $field == 'communication_to_honbu_syudan' ):
            $cell_b2 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('電話','FAX（書面）','メール','面談（直接）')
            ));
        elseif( $field == 'communication_to_honbu_naiyou' ):
            $cell_b3 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('売上','コンペジ状況','モール状況','その他')
            ));
            echo '<table class="communication table table-condensed">';
            echo '<h4>本部　→　店舗</h4>';
            echo $this->Html->tableHeaders( array('頻度','コミュニケーション手段','コミュニケーションの主な内容') );
            echo $this->Html->tableCells( array($cell_a1,$cell_a2,$cell_a3) );
            echo '</table>';
            echo '<table class="communication table table-condensed">';
            echo '<h4>店舗　→　本部</h4>';
            echo $this->Html->tableHeaders( array('頻度','コミュニケーション手段','コミュニケーションの主な内容') );
            echo $this->Html->tableCells( array($cell_b1,$cell_b2,$cell_b3) );
            echo '</table>';
		elseif( $field == 'syonendo_keikaku' ):
			echo '<p>◆ オープン初年度の営業計画(売上予算を達成するためのより具体的な計画をご記入下さい。</p>';
			echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
		elseif( $field == 'jyoui_tenpo' ):
			echo '<p>◆全国での最上位店舗の年間売上・面積を教えて下さい。</p>';
			echo $this->Form->input( 'Hearing.'.$field .'.basyo', array('label'=>'場所','type'=>'text','class'=>'span8'));
			echo $this->Form->input( 'Hearing.'.$field .'.sisetu', array('label'=>'施設名','type'=>'text','class'=>'span8'));
			echo $this->Form->input( 'Hearing.'.$field .'.uriage', array('label'=>'売上／年間［千円］','type'=>'text','class'=>'span8'));
			echo $this->Form->input( 'Hearing.'.$field .'.menseki1', array('label'=>'面積［m&#178;］','type'=>'text','class'=>'span8'));
			echo $this->Form->input( 'Hearing.'.$field .'.menseki2', array('label'=>'面積［坪］','type'=>'text','class'=>'span8'));
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
		elseif( $field == 'itiduke' ):
			echo '<p>◆貴社における(仮称)イオンモール東員の位置付けをご回答ください。</p>';
			echo '<p>下記の項目の中で、適合する項目の数値をチェックしてください。</p>';
			echo '<p>該当するものが無い場合は、その他の欄に簡単にで結構ですので、ご記入ください。</p>';
			echo $this->Form->input('Hearing.'. $field .'.checked', array(
				'label'	=> '位置付け',
				'type'		=> 'select' ,
				'multiple'	=> 'checkbox',
				'options'	=> array(
					'１．　社内で将来の主力店舗として考えている',
					'２．　全国での最有力店舗として考えている',
					'３．　東海地方での最有力店舗として考えている',
					'４．　その他')
			));
			echo $this->Form->input('Hearing.'. $field .'.other', array('label'=>'その他','type'=>'text','class'=>'span8'));
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
		elseif( $field == 'target_danjyo' ):
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
			echo '<p>◆男女構成比</p>';
			echo '<p>（男性　％　+　女性　％　+　子供　％　=　１００％）</p>';
			echo $this->Form->input( 'Hearing.'.$field .'.man', array('label'=>'男性［％］','type'=>'text','class'=>'span1'));
			echo $this->Form->input( 'Hearing.'.$field .'.female', array('label'=>'女性［％］','type'=>'text','class'=>'span1'));
			echo $this->Form->input( 'Hearing.'.$field .'.kids', array('label'=>'子供［％］','type'=>'text','class'=>'span1'));
		elseif( $field == 'target_nenrei' ):
			echo '<p>◆年齢構成比</p>';
			echo $this->Form->input( 'Hearing.'.$field .'.ov20', array('label'=>'20代［％］','type'=>'text','class'=>'span1'));
			echo $this->Form->input( 'Hearing.'.$field .'.ov30', array('label'=>'30代［％］','type'=>'text','class'=>'span1'));
			echo $this->Form->input( 'Hearing.'.$field .'.ov40', array('label'=>'40代［％］','type'=>'text','class'=>'span1'));
			echo $this->Form->input( 'Hearing.'.$field .'.ov50', array('label'=>'50代［％］','type'=>'text','class'=>'span1'));
			echo $this->Form->input( 'Hearing.'.$field .'.ov60', array('label'=>'60代［％］','type'=>'text','class'=>'span1'));
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
		elseif( $field == 'syouhin_kousei' ):
			echo '<p>◆貴社モデル店舗（当SC入店ブランド）の商品構成をご記入ください。</p>';
			$syouhin_kousei_table = array();
			for( $num=0; $num<10; $num++ ){
				$item  = $this->Form->input( 'Hearing.'.$field.'.'.$num.'.item',
							array('label'=>false,'div'=>false,'type'=>'text','class'=>'span6','rows'=>'3'));
				$ratio = $this->Form->input( 'Hearing.'.$field.'.'.$num.'.raio',
							array('label'=>false,'div'=>false,'type'=>'text','class'=>'span1','rows'=>'3'));
				$plice1 = $this->Form->input( 'Hearing.'.$field.'.'.$num.'.plice1',
							array('label'=>false,'div'=>false,'type'=>'text','class'=>'span2','rows'=>'3'));
				$plice2 = $this->Form->input( 'Hearing.'.$field.'.'.$num.'.plice2',
							array('label'=>false,'div'=>false,'type'=>'text','class'=>'span2','rows'=>'3'));
				$syouhin_kousei_table[] = array( $item, $ratio, $plice1, $plice2 );
			}
			echo '<table class="table table-condensed">';
			echo $this->Html->tableHeaders( array('品種(メニュー）或いは商品','構成比［％］','プライスゾーン下限［円］','プライスゾーン上限［円］'));
			echo $this->Html->tableCells( $syouhin_kousei_table );
			echo '</table>';
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
		elseif( $field == 'syouhin_kousei_henkou' ):
			echo '<p>◆ アパレル、雑貨店舗・・・当SC商圏に落とし込んだ場合の、商品構成等の変更点をご記入下さい。</p>';
			echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
		elseif( $field == 'sin_syouhin' ):
			echo '<p>◆飲食、食品、サービス店舗・・・当SC出店にあたり、新メニュー、新商品の提案があればご記入ください。</p>';
			echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
		elseif( $field == 'kokyaku_kanri_houhou' ):
			echo '<p>通常期の貴社の顧客コミュニケーションに関してご記入下さい</p>';
			$kokyaku_kanri_houhou_table = array();
			$houhous = array(
				'ＤＭ',
				'メルマガ',
				'ＴＥＬ',
				'ＦＡＸ　',
				'その他');
			foreach( $houhous as $h_key => $houhou ){
				$houhou_box = $this->Form->input('Hearing.'. $field .'.'. $h_key .'.checked', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array( $houhou ),
					'class' => 'span2'
				));
				$hindo_box = $this->Form->select('Hearing.'. $field .'.'. $h_key .'.hindo',
					array('毎週'=>'毎週','隔週'=>'隔週','月2〜3回'=>'月2〜3回','月1回'=>'月1回','2〜3ヶ月に1回'=>'2〜3ヶ月に1回','年に2〜3回'=>'年に1〜2回','その他'=>'その他'),
					array(
						'empty' => '選択して下さい',
						'class'=>'offset2',
					'label'	=> false,
					'div'	=> false)
				);
				$other = $this->Form->input('Hearing.'. $field .'.'. $h_key .'.other',array(
						'label'=>false,'div'=>false,'type'=>'text','class'=>'span2'));
				$kokyaku_kanri_houhou_table[] = array( $houhou_box, $hindo_box, $other );
			}
			echo '<table class="table kokyaku_kanri table-condensed">';
			echo $this->Html->tableHeaders( array('コミュニケーション方法','実施頻度','その他の頻度'));
			echo $this->Html->tableCells( $kokyaku_kanri_houhou_table );
			echo '</table>';
		elseif( $field == 'cinema_de_plus_tokuten' ):
			echo '<h4>【シネマdeプラス（仮称）特典内容】</h4>';
			echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
		elseif( $field == 'open_hansoku' ):
			echo '<p>◆貴店（貴社）オープン販促に関してご記入下さい。</p>';
			echo '<p>※予定でも結構です。オープン販促は若干の規制を設ける場合がございます。ご了承下さい。</p>';
				$baitai = $this->Form->input('Hearing.'. $field .'.tvcm', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('TVCM'=>'TVCM'),
					'class' => 'span3'
				));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tvcm_start', array('label'=>'開始時期','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tvcm_end', array('label'=>'終了時期','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tv_station', array('label'=>'TV局','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tv_grp', array('label'=>'GRP','type'=>'text','class'=>'span3'));

				$baitai .= $this->Form->input('Hearing.'. $field .'.news', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('新聞広告'=>'新聞広告'),
					'class' => 'span3'
				));
				$baitai .= $this->Form->input('Hearing.'. $field .'.news_start', array('label'=>'開始時期','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.news_end', array('label'=>'終了時期','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.news_name', array('label'=>'新聞名','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.news_num', array('label'=>'部数','type'=>'text','class'=>'span3'));

				$baitai .= $this->Form->input('Hearing.'. $field .'.tirasi', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('折込チラシ'=>'折込チラシ'),
					'class' => 'span3'
				));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tirasi_start', array('label'=>'開始時期','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tirasi_end', array('label'=>'終了時期','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tirasi_name', array('label'=>'新聞名','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.tirasi_num', array('label'=>'部数','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.magazine', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('雑誌'=>'雑誌'),
					'class' => 'span3'
				));
				$baitai .= $this->Form->input('Hearing.'. $field .'.magazine_name', array('label'=>'雑誌名','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.magazine_day', array('label'=>'発行日','type'=>'text','class'=>'span3'));
				$baitai .= $this->Form->input('Hearing.'. $field .'.other', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('その他'=>'その他'),
					'class' => 'span3'
				));
				$baitai .= $this->Form->input('Hearing.'. $field .'.other_naiyou', array('label'=>'内容','type'=>'text','class'=>'span3'));


				$hansoku = $this->Form->input('Hearing.'. $field .'.dm', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('ＤＭ'=>'ＤＭ'),
					'class' => 'span3'
				));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.dm_num', array('label'=>'部数','type'=>'text','class'=>'span3'));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.mail_magazine', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('メルマガ'=>'メルマガ'),
					'class' => 'span3'
				));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.mail_magazine_num', array('label'=>'講読数','type'=>'text','class'=>'span3'));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.tel', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('ＴＥＬ'=>'ＴＥＬ'),
					'class' => 'span3'
				));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.tel_num', array('label'=>'件数','type'=>'text','class'=>'span3'));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.fax', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('ＦＡＸ'=>'ＦＡＸ'),
					'class' => 'span3'
				));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.fax_num', array('label'=>'件数','type'=>'text','class'=>'span3'));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.hansoku_other', array(
					'label'	=> false,
					'div'	=> false,
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('その他'=>'その他'),
					'class' => 'span3'
				));
				$hansoku .= $this->Form->input('Hearing.'. $field .'.hansoku_other_naiyou', array('label'=>'内容','type'=>'text','class'=>'span3'));
			echo '<table class="table kokyaku_kanri table-bordered">';
			echo $this->Html->tableHeaders( array('媒体','顧客販促'));
			echo $this->Html->tableCells( array( $baitai, $hansoku ));
			echo '</table>';

			$kokuti = $this->Form->input('Hearing.'. $field .'.kokuti', array('label'=>'内容','type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<table class="table kokyaku_kanri table-bordered">';
			echo $this->Html->tableHeaders( array('告知・仕掛内容（目玉・Sale・ノベルティ）'));
			echo $this->Html->tableCells( array( $kokuti ));
			echo '</table>';

			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';
		elseif( $field == 'smart_phone_hansoku' ):
			echo '<p>◆貴社にて導入されているスマートフォンを利用した販促企画についてお教え下さい。</p>';
			echo '<p>ある場合、その内容（利用ツール、お客さまへ提供している内容・特典等）</p>';
			echo '<pre class="span10">
例：・自社アプリを展開している
　　・LINEに公式アカウントを持っている　等
</pre>';
			echo $this->Form->input( $field, array(
					'label'	=> '有無',
					'type'		=> 'select' ,
					'multiple'	=> 'checkbox',
					'options'	=> array('あり'=>'あり'),
					'class' => 'span3'
				));
		elseif( $field == 'smart_phone_hansoku_naiyou' ):
			echo $this->Form->input( $field, array('label'=>'内容','type'=>'text','class'=>'span8','rows'=>'2'));
		elseif( $field == 'part_syain_num' ):
			echo $this->Form->input( $field, array('label'=>'うちパート・アルバイト数','type'=>'text','class'=>'span3'));
		elseif( $field == 'jinin_keikaku' ):
			echo '<h4>オープン期間（平日）</h4>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.heijitu', array('label'=>'従業員数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.heijitu_stuff', array('label'=>'うち 店頭スタッフ数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.heijitu_ouen', array('label'=>'うち 応援数','type'=>'text','class'=>'span2','div'=>false));
			echo '<table class="table kokyaku_kanri table-bordered">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';

			echo '<h4>オープン期間（土日祝）</h4>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.doniti', array('label'=>'従業員数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.doniti_stuff', array('label'=>'うち 店頭スタッフ数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.doniti_ouen', array('label'=>'うち 応援数','type'=>'text','class'=>'span2','div'=>false));
			echo '<table class="table kokyaku_kanri table-bordered">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';

			echo '<h4>平常期の1日あたりの従業員数（平日）</h4>';
			echo '<p>１日８時間勤務者を１名と数えた場合の人数（例：４時間パート２名で１名と数えて下さい）</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.heijitu_a_day', array('label'=>'従業員数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.heijitu_syain_a_day', array('label'=>'うち 社員数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.heijitu_part_a_day', array('label'=>'うち パート・アルバイト数','type'=>'text','class'=>'span2','div'=>false));
			echo '<table class="table kokyaku_kanri table-bordered">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';

			echo '<h4>平常期の1日あたりの従業員数（土日祝）</h4>';
			echo '<p>１日８時間勤務者を１名と数えた場合の人数（例：４時間パート２名で１名と数えて下さい）</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.doniti_a_day', array('label'=>'従業員数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.doniti_syain_a_day', array('label'=>'うち 社員数','type'=>'text','class'=>'span2','div'=>false));
			$row[] .= $this->Form->input('Hearing.'. $field .'.doniti_part_a_day', array('label'=>'うち パート・アルバイト数','type'=>'text','class'=>'span2','div'=>false));
			echo '<table class="table kokyaku_kanri table-bordered">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';

			echo '<h4>従業員１人が直接接客する売場面積</h4>';
			echo '<p>売場面積(坪) ÷ （〔従業員数〕 - レジ・後方・厨房人員）</p>';
			echo '<p>※売場面積は、接客可能な場所の面積とします。よって、厨房・バックヤード等は売場面積に含まないものとします。</p>';
			echo $this->Form->input('Hearing.'. $field .'.heijitu_menseki', array('label'=>'平日［坪］','type'=>'text','class'=>'span2'));
			echo $this->Form->input('Hearing.'. $field .'.doniti_menseki', array('label'=>'土日祝［坪］','type'=>'text','class'=>'span2'));
			if( $locked == 0 )
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';

		elseif( $field == 'jinji1' ):
			echo '<h4>人事について</h4>';
			echo '<p>決まっている部分で結構ですので、ご記入をお願いします。</p>';
			echo '<p>◆店長</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_name', array('label'=>'店長名','type'=>'text','class'=>'span3','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_age', array('label'=>'年齢','type'=>'text','class'=>'span2','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_zaisyoku', array('label'=>'貴社での在職年数','type'=>'text','class'=>'span2','div'=>false));
			$row1 = $this->Form->input('Hearing.'. $field .'.tentyou_keireki', array('label'=>'これまでの経歴','type'=>'text','class'=>'span10','rows'=>'3','div'=>false));
			echo '<table class="table kokyaku_kanri table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '<tr><td colspan="3">'. $row1 .'</td></tr>';
			echo '</table>';

			echo '<p>◆副店長</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.fuku_tentyou_name', array('label'=>'副店長名','type'=>'text','class'=>'span3','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.fuku_tentyou_age', array('label'=>'年齢','type'=>'text','class'=>'span2','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.fuku_tentyou_zaisyoku', array('label'=>'貴社での在職年数','type'=>'text','class'=>'span2','div'=>false));
			$row1 = $this->Form->input('Hearing.'. $field .'.fuku_tentyou_keireki', array('label'=>'これまでの経歴','type'=>'text','class'=>'span10','rows'=>'3','div'=>false));
			echo '<table class="table kokyaku_kanri table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '<tr><td colspan="3">'. $row1 .'</td></tr>';
			echo '</table>';

		elseif( $field == 'jinji2' ):
			echo '<p>◆他スタッフ1</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff1_name', array('label'=>'氏名','type'=>'text','class'=>'span3','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff1_age', array('label'=>'年齢','type'=>'text','class'=>'span2','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff1_zaisyoku', array('label'=>'貴社での在職年数','type'=>'text','class'=>'span2','div'=>false));
			$row1 = $this->Form->input('Hearing.'. $field .'.stuff1_keireki', array('label'=>'これまでの経歴','type'=>'text','class'=>'span10','rows'=>'3','div'=>false));
			echo '<table class="table kokyaku_kanri table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '<tr><td colspan="3">'. $row1 .'</td></tr>';
			echo '</table>';

			echo '<p>◆他スタッフ2</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff2_name', array('label'=>'氏名','type'=>'text','class'=>'span3','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff2_age', array('label'=>'年齢','type'=>'text','class'=>'span2','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff2_zaisyoku', array('label'=>'貴社での在職年数','type'=>'text','class'=>'span2','div'=>false));
			$row1 = $this->Form->input('Hearing.'. $field .'.stuff2_keireki', array('label'=>'これまでの経歴','type'=>'text','class'=>'span10','rows'=>'3','div'=>false));
			echo '<table class="table kokyaku_kanri table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '<tr><td colspan="3">'. $row1 .'</td></tr>';
			echo '</table>';

			echo '<p>◆他スタッフ3</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff3_name', array('label'=>'氏名','type'=>'text','class'=>'span3','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff3_age', array('label'=>'年齢','type'=>'text','class'=>'span2','div'=>false));
			$row[] = $this->Form->input('Hearing.'. $field .'.stuff3_zaisyoku', array('label'=>'貴社での在職年数','type'=>'text','class'=>'span2','div'=>false));
			$row1 = $this->Form->input('Hearing.'. $field .'.stuff3_keireki', array('label'=>'これまでの経歴','type'=>'text','class'=>'span10','rows'=>'3','div'=>false));
			echo '<table class="table kokyaku_kanri table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '<tr><td colspan="3">'. $row1 .'</td></tr>';
			echo '</table>';
			if( $locked == 0)
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';

		elseif( $field == 'jyuugyouin_kyouiku_before_open' ):
			echo '<div class="alert alert-info span10">';
			echo '<h3>【８】店舗従業員の教育(および教育内容）について</h3>';
			echo '</div>';
			echo '<h4>◆(仮称)イオンモール東員 に配属予定の従業員（ﾊﾟｰﾄ・ｱﾙﾊﾞｲﾄ含む）の オープン前 貴社教育(研修)についてご記入下さい。</h4>';
			echo '<p>【　店　　長　】</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_jiki', array('label'=>'時期','type'=>'text','class'=>'span3','div'=>false,'rows'=>'2'));
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_naiyou', array('label'=>'研修内容','type'=>'text','class'=>'span7','div'=>false,'rows'=>'2'));
			echo '<table class="table table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';
			echo '<p>【　従　業　員　】</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.jyuugyouin_jiki', array('label'=>'時期','type'=>'text','class'=>'span3','div'=>false,'rows'=>'2'));
			$row[] = $this->Form->input('Hearing.'. $field .'.jyuugyouin_naiyou', array('label'=>'研修内容','type'=>'text','class'=>'span7','div'=>false,'rows'=>'2'));
			echo '<table class="table table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';
			echo '<p>【　新規採用の従業員　】</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.sinki_jiki', array('label'=>'時期','type'=>'text','class'=>'span3','div'=>false,'rows'=>'2'));
			$row[] = $this->Form->input('Hearing.'. $field .'.sinki_naiyou', array('label'=>'研修内容','type'=>'text','class'=>'span7','div'=>false,'rows'=>'2'));
			echo '<table class="table table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';


		elseif( $field == 'jyuugyouin_kyouiku_after_open' ):
			echo '<h4>◆オープン後　貴社フォロー教育（定期研修含む）についてご記入ください。</h4>';
			echo '<p>【　店　　長　】</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_jiki', array('label'=>'時期','type'=>'text','class'=>'span3','div'=>false,'rows'=>'2'));
			$row[] = $this->Form->input('Hearing.'. $field .'.tentyou_naiyou', array('label'=>'研修内容','type'=>'text','class'=>'span7','div'=>false,'rows'=>'2'));
			echo '<table class="table table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';
			echo '<p>【　従　業　員　】</p>';
			$row = array();
			$row[] = $this->Form->input('Hearing.'. $field .'.jyuugyouin_jiki', array('label'=>'時期','type'=>'text','class'=>'span3','div'=>false,'rows'=>'2'));
			$row[] = $this->Form->input('Hearing.'. $field .'.jyuugyouin_naiyou', array('label'=>'研修内容','type'=>'text','class'=>'span7','div'=>false,'rows'=>'2'));
			echo '<table class="table table-condensed">';
			echo $this->Html->tableCells( array( $row ));
			echo '</table>';
			if( $locked == 0)
				echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			else
				echo MESSAGE_AT_LOCKED;
			echo '<br /><br /><br />';


		else:
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
        endif;
        }
        if( $locked == 0)
            echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
        else
            echo MESSAGE_AT_LOCKED;
        echo $this->Form->end();
    ?>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>

<!-- モーダル -->
<div class="modal hide" id="mymodal">
  <div class="modal-header">
    <div>登録結果</div>
    <br />
  </div>
  <div class="modal-body">
    <p id="modal_message"></p>
  </div>
  <div class="modal-footer">
    <button id="close_mymodal" data-dismiss="modal">OK</button>
  </div>
</div>

<?php
if( isset( $script_in_get )){
  echo '<script type="text/javascript">';
  echo $script_in_get;
  echo '</script>';
}
?>
