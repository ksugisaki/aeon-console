<div class="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
  <p>出店に関するヒアリングシートです</p>
</div>

<div class="hearing form">
    <?php
        echo $this->Form->create('Hearing', Array('class'=>'form-horizontal span11','url' =>'index/'. $user_id ));
        foreach ( $fields as $key => $field ){
        if( $field == 'id' ):
            echo $this->Form->input('id', array('type'=>'hidden'));
        elseif( $field == 'user_id' ):
            echo $this->Form->input('user_id', array('type'=>'hidden', 'value'=>$user_id));

        elseif( $field == 'uriage_yosan' ): ?>
        
            <div class="alert alert-info span10">
            <h3>【１】月別売上予算をご記入ください。</h3>
            ＊月度の売上は、毎月1日～末日の売上予算を記入してください。
            </div>

            <h4 class="">2013年度</h4>
			<table class="communication table table-bordered" style="">
			<tbody>
				
	            <?php if( isset( $this->request->data['Hearing']['uriage_yosan']))
	            $uriage_yosan = $this->request->data['Hearing']['uriage_yosan'];
	            $sum = 0;
	            $sum1 = 0;
	            foreach( $uriage_yosan_label as $key => $value ){
	                if( ! isset( $uriage_yosan[$key])) $uriage_yosan[$key] = 0;
	                $sum += $uriage_yosan[$key];
	                echo $this->Form->input('uriage_yosan' . $key, array('label'=>$value,'type'=>'text','value'=>$uriage_yosan[$key]));
	                if( $key == 2 ){
	                    if( $is_myid )
	                        echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
	                    else
	                        echo '<p class="btn pull-right">※更新は出来ません。</p>';
		                    echo '<br />';
		                    echo '<p class="offset6">2013年度合計 ' .  $sum . '［千円］</p>';
		                    $sum = 0;
		                    echo '<h4 class="">2014年度</h4>';
	                }
	
	                if( $key == 8 ){
	                    if( $is_myid )
	                        echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
	                    else
	                        echo '<p class="btn pull-right">※更新は出来ません。</p>';
		                    echo '<br />';
		                    echo '<hr /><p class="offset6">上期合計 ' .  $sum . '［千円］</p>'; $sum1 = $sum; $sum = 0;
	                }
	            }
	            ?>
			</tbody>
			</table>
			
            
            <?php 
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br />';
            echo '<hr /><p class="offset6">下期合計 '. $sum .'［千円］</p>';
            echo '<hr /><p class="offset6">2014年度合計 '.( $sum + $sum1 ).'［千円］</p>';
			?>

			<?php  elseif( $field == 'syutten_jyoukyou' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【２】出店状況についてご記入ください。</h3>';
            echo '</div>';
            echo '<h4>'.MALL_NAME.'への出店状況にチェックを入れてください。</h4>';
            echo $this->Form->input( $field, array(
                'label'     => $labels[ $key ],
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => $syutten_jyoukyou_label
            ));
        elseif( $field == 'taten_syutten_jyoukyou' ):
            echo '<pre class="span10">';
            echo '<dl><dt>＊店名および出店状況については、9月末予定の全体概要リリースで使用させていただきます。</dt>';
            echo '<dd>店名が未決定の場合は、8月末までにご決定いただきますようよろしくお願いいたします。</dd>';
            echo '<dd>なお、2013年12月に同じ屋号で店舗がオープンする場合は、その店舗のオープン日を下記に記載してください。</dd>';
            echo '<dd>急遽、「●●初出店」に該当しなくなった場合には、お手数ではございますが、ご連絡をお願いいたします。</dd></dl>';
            echo '</pre>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';

        elseif( $field == 'sales_point' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【３】営業計画・施策について</h3>';
            echo '</div>';
            echo '<h4>①店舗・商品について教えてください。</h4>';
            echo '<p>◆自店のセールスポイント（強み）についてご記入ください。</p>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
        elseif( $field == 'main_target' or $field == 'sub_target'):
            echo '<p>◆' . $labels[$key]. '</p>';
            if( isset( $this->request->data['Hearing'][$field]))
                $target = $this->request->data['Hearing'][$field];
            foreach( $target_info['label'] as $key => $label ){
                if( ! isset( $target[$key])) $target[$key] = ' ';
                if( $label == '年齢'){
                    echo $this->Form->select( $field . $key, $target_info['ages'],
                        array('empty' => $label .'を選んでください','class'=>'offset2','value'=>$target[$key]));
                    echo '<br />';
                }else
                if( $label == '性別等'){
                    echo $this->Form->select( $field . $key, $target_info['sex'],
                        array('empty' => $label .'を選んでください','class'=>'offset2','value'=>$target[$key]));
                    echo '<br />';
                }else{
                    echo '<br />';
                    echo '<p class="offset2">' . $target_info['note'] . '</p>';
                    echo $this->Form->input( $field . $key, array('label'=>'備考','type'=>'text','class'=>'span8','rows'=>'2','value'=>$target[$key]));

                }
            }
        elseif( $field == 'kyougou_ten' ):
            echo '<p>◆競合店（自社競合・他社競合）／競合ブランド</p>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
        elseif( $field == 'sabetuka_syuhou' ):
            echo '<p>◆競合店との差別化手法（自社競合・他社競合）</p>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';


        elseif( $field == 'kokyaku_kanri' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【４】顧客管理について</h3>';
            echo '</div>';
            echo '<h4>１．顧客管理はされていますか？ </h4>';
            //echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span2'));
            echo $this->Form->select( $field, array('1'=>'はい','0'=>'いいえ'),
                        array('empty' => '選択してください','class'=>'offset2'));


        elseif( $field == 'kokyaku_kanri_mokuteki' ):
            echo '<h4>２．顧客管理の目的・考え方について教えてください。</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
        elseif( $field == 'kokyaku_kanri_tool' ):
            echo '<h4>３．その顧客管理ツール（お客様とコネクションを作るツール）は何ですか？内容をご記入ください。</h4>';
            echo '<pre class="span10">';
            echo '<dl>';
            echo '<dt>例１）</dt><dd>スタンプカードを発行し、●●円ごとに１スタンプ、スタンプ●個で住所等を記入いただき、回収、<br />次回お買上時に10%OFFの特典。</dd>';
            echo '<dt>例２）</dt><dd>接客をして印象深かった方や複数購入の方など、商品への支持度を見て顧客カードに直接記入していただく。</dd>';
            echo '<dt>例３）</dt><dd>インターネット上で住所等を登録頂いてメールマガジン配信。</dd>';
            echo '</dl>';
            echo '</dl>';
            echo '</pre>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
        elseif( $field == 'data_bunseki' ):
            echo '<h4>４．会員リスト等データの管理や分析はどちらでされていますか？</h4>';
            echo $this->Form->input( $field, array(
                'label'     => $labels[ $key ],
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => $data_bunseki_label
            ));
        elseif( $field == 'hansoku_approach' ):
            echo '<h4>５．会員様へのサービス・特典内容や販促でのアプローチ方法を教えてください。</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
        elseif( $field == 'jyou_kokyaku' ):
            echo '<h4>６．一般顧客と上顧客との区別はありますか？</h4>';
            //$labels[ $key ] = "区別：<br /><br />定義：<br /><br />特典：";
            //echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'5'));

            if( isset( $this->request->data['Hearing'][$field]))
                $jyou_kokyaku = $this->request->data['Hearing'][$field];
            foreach( $jyou_kokyaku_label as $key => $value ){
                if( ! isset( $jyou_kokyaku[$key])) $jyou_kokyaku[$key] = ' ';
                if( $key == 0){
                    echo $this->Form->select( $field, array('1'=>'区分あり','0'=>'区分なし'),
                                              array('empty' => '区分の有無を選択してください','class'=>'offset2'));
                    echo '<br /><br />';
                    echo '<p class="">また、ある場合は、どのような定義で分けられますか？その特典内容も教えてください。</p>';
                }else{
                    echo $this->Form->input( $field . $key, array('label'=>$value,'type'=>'text','value'=>$jyou_kokyaku[$key],'class'=>'span8','rows'=>'2'));
                }
            }

        elseif( $field == 'kaiin_suu' ):
            echo '<h4>７．商圏内会員数および当ＳＣ店で目標とする会員数をご記入ください。その根拠となる他店会員数の事例を教えてください。</h4>';
            if( isset( $this->request->data['Hearing'][$field]))
                $kaiin_suu = $this->request->data['Hearing'][$field];
            foreach( $kaiin_suu_label as $key => $value ){
                if( ! isset( $kaiin_suu[$key])) $kaiin_suu[$key] = ' ';
                echo $this->Form->input( $field . $key, array('label'=>$value,'type'=>'text','value'=>$kaiin_suu[$key]));
            }
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';

        elseif( $field == 'kansya_day_tokuten' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【５】お客さま感謝デー特典について</h3>';
            echo '</div>';
            echo '<h4>　＊別紙の「お客さま感謝デー・GG感謝デー」のご案内をご参照・ご確認のうえ、ご記入をお願いいたします。</h4>';
            echo '<p class="">～特典内容について～</p>';
            echo '<p class="">・感謝デーの特典については、各店のレジで操作をしていただくことになります。</p>';
            echo '<p class="">　クレジット請求時の割引ではありませんので、ご注意ください。</p>';
            echo '<pre class="span10">';
            echo '<dl><dt>【記入例】</dt>';
            echo '<dd>全品5%OFF ・・・店内商品全てが5%OFFの対象となります。</dd>';
            echo '<dd>5%OFF（一部商品除外） ・・・セール品除外の場合は、一部商品除外という表現になります。</dd>';
            echo '<dd>自店ポイントカード2倍 ・・・自社の発行しているポイントカードが対象です。</dd>';
            echo '<dd>●円以上お買上げの方にノベルティプレゼント</dd>';
            echo '<dd>ソフトドリンクサービス</dd>';
            echo '</dl></pre><br />';
            echo '<h4>【お客さま感謝デー特典内容】</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));

        elseif( $field == 'gg_kansya_day_tokuten' ):
            echo '<h4>【G.G.感謝デー特典内容】</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'3'));
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';

        elseif( $field == 'schedule' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【６】販促計画・営業のしかけについて</h3>';
            echo $this->Html->link('感謝デーとSCスケジュールはこちら', SCHEDULE_LINK,
                                   array('target'=>'_blank','class'=>'pull-right'));
            echo '<p>予定の範囲で結構ですのでご記入ください。</p>';
            echo '</div>';
            if( isset( $this->request->data['Hearing']['schedule']))
                $schedule = $this->request->data['Hearing']['schedule'];
            $schdule_table = array();
            // 例
            $schdule_table[] = array( '例<br />（１２月）', 'オープニング・フェア','','オープン告知→雑誌●●にて告知予定' );
            // 入力エリア
            foreach( $schedule_label as $key => $value ){
                if( ! isset( $schedule[1][$key] )) $schedule[1][$key] = '';
                if( ! isset( $schedule[2][$key] )) $schedule[2][$key] = '';
                $input1 = '<textarea class="span4" type="text" name="data[Hearing][schedule_1_'. $key .']">'
                        . $schedule[1][$key]
                        . '</textarea>';
                $example = '<p class="span2">' . $schedule_example[ $key ] . '</p>';
                $input2 = '<textarea class="span3" type="text" name="data[Hearing][schedule_2_'. $key .']">'
                        . $schedule[2][$key]
                        . '</textarea>';
                $schdule_table[] = array( $value, $input1,$example,$input2 );
            }
            echo '<table class="table table-condensed">';
            echo $this->Html->tableHeaders( array('','貴店のスケジュール','記入例','CM・雑誌等での告知予定') );
            echo $this->Html->tableCells( $schdule_table );
            echo '</table>';
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';

        elseif( $field == 'eigyou_keitai' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【７】人員体制について</h3>';
            echo '</div>';
            echo '<h4>営業形体は、直営でしょうか?  販売代行・FCを利用されるでしょうか?</h4>';
            echo $this->Form->select( $field, array('直営'=>'直営','販売代行・FC'=>'販売代行・FC'),
                                      array('empty' => '営業形体を選択してください','class'=>'offset2'));
            echo '<br /><br />';
        elseif( $field == 'daikou_kettei' ):
            echo '<h4>販売代行・FCは、未定でしょうか? 決定済みでしょうか?</h4>';
            echo $this->Form->select( $field, array('未定'=>'未定','決定済み'=>'決定済み'),
                                      array('empty' => '決定済みか否か','class'=>'offset2'));
            echo '<br /><br />';
        elseif( $field == 'daikou_kaisya' ):
            echo '<h4>販売代行・FCの候補先・決定先会社名をご記入ください。</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));

        elseif( $field == 'tenpo_jyuugyouin_num' ):
            echo '<h4>店舗従業員数</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span3'));
        elseif( $field == 'syain_num' ):
            echo '<h4>うち社員数</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span3'));
        elseif( $field == 'sin_saiyou_num' ):
            echo '<h4>新規採用予定数</h4>';
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span3'));
        elseif( $field == 'kettei' ):
            echo '<h4>店長様の決定時期がわかればご記入ください。</h4>';
            echo $this->Form->select( $field, array('未定'=>'未定','決定済み'=>'決定済み'),
                                      array('empty' => '決定済みか否か','class'=>'offset2'));
            echo '<br /><br />';
        elseif( $field == 'kettei_yotei' ):
            echo '<h4>未決定の場合は、決定時期をご記入下さい。</h4>';
            echo '<p class="offset2">（何月頃決定予定かをお書き下さい。）</p>';
            echo '<p class="offset2">※イオンモールとしては、９月末までに決定をお願いしております。</p>';
            echo $this->Form->input( $field, array('label'=>'','type'=>'text','class'=>'span8','rows'=>'2'));
        elseif( $field == 'raiten_hindo' ):
            echo '<h4>営業担当者様（エリアマネージャー）の店舗来店頻度はどのくらいでしょうか？</h4>';
            //echo '<p class="offset2">（  常駐　・　1回 / 週以上　・　1回/週　・　1回/月　・　1回以下/月     ）</p>';
            //echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));

            echo $this->Form->select( $field, array('常駐'=>'常駐','1回 / 週以上'=>'1回 / 週以上','1回 / 週'=>'1回 / 週','1回 / 月'=>'1回 / 月','1回以下 / 月'=>'1回以下 / 月'),
                                      array('empty' => '来店頻度を選択してください','class'=>'offset2'));
            echo '<br /><br />';
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';

        elseif( $field == 'jyuugyouin_kyouiku' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【８】店舗従業員の教育(および教育内容）について</h3>';
            echo '<p>（オープン前の研修等について）</p>';
            echo '</div>';
            echo '<pre class="span10">';
            echo '<dl><dt>（例）</dt><dd>アルバイトについては、オープンまでの2ヶ月間●●店で研修予定。</dd><dl>';
            echo '</pre>';
            echo $this->Form->input( $field, array('label'=>'','type'=>'text','class'=>'span8','rows'=>'3'));
            if( $is_myid )
                echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
            else
                echo '<p class="btn pull-right">※更新は出来ません。</p>';
            echo '<br /><br /><br />';

        elseif( $field == 'communication_to_tenpo_hindo' ):
            echo '<div class="alert alert-info span10">';
            echo '<h3>【９】店舗と本部でのコミュニケーション手段</h3>';
            echo '</div>';
            $cell_a1 = $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('毎日','２～３回／週','１回／週','1回／月')
            ));
        elseif( $field == 'communication_to_tenpo_syudan' ):
            $cell_a2 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('電話','FAX（書面）','メール','面談（直接）')
            ));
        elseif( $field == 'communication_to_tenpo_naiyou' ):
            $cell_a3 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('自社販促','その他')
            ));
        elseif( $field == 'communication_to_honbu_hindo' ):
            $cell_b1 = $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('毎日','２～３回／週','１回／週','1回／月')
            ));
        elseif( $field == 'communication_to_honbu_syudan' ):
            $cell_b2 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('電話','FAX（書面）','メール','面談（直接）')
            ));
        elseif( $field == 'communication_to_honbu_naiyou' ):
            $cell_b3 =  $this->Form->input( $field, array(
                'label'     => '',
                'type'      => 'select' ,
                'multiple'  => 'checkbox',
                'options'   => array('売上','コンペジ状況','モール状況','その他')
            ));
            echo '<table class="communication table table-condensed">';
            echo '<h4>本部　→　店舗</h4>';
            echo $this->Html->tableHeaders( array('頻度','コミュニケーション手段','コミュニケーションの主な内容') );
            echo $this->Html->tableCells( array($cell_a1,$cell_a2,$cell_a3) );
            echo '</table>';
            echo '<table class="communication table table-condensed">';
            echo '<h4>店舗　→　本部</h4>';
            echo $this->Html->tableHeaders( array('頻度','コミュニケーション手段','コミュニケーションの主な内容') );
            echo $this->Html->tableCells( array($cell_b1,$cell_b2,$cell_b3) );
            echo '</table>';


        else:
            echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span8','rows'=>'2'));
        endif;
        }
        if( $is_myid )
            echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-right'));
        else
            echo '<p class="btn pull-right">※更新は出来ません。</p>';
        echo $this->Form->end();
    ?>
</div>
