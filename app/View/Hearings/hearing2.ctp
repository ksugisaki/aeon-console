<?php echo $this->Html->script(array('scrollback'), array('inline'=>false)); ?>
<?php //echo $this->Html->css(array('style1', 'style2'), false, array('inline'=>false)); ?>

<div class="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
</div>
<?php echo $this->Session->flash(); ?>

<div class="alert alert-info span10">
<h3>各種制作物 (PR用・店舗設置用) ヒアリング項目案</h3>
<br />
※回答内容は、事前ＰＲ活動や取材に来場する報道関係者（記者・ＴＶディレクター・アナウンサー）に対し配布する資料「ファクトブック」や各種製作物に反映、活用させて頂きます。<br />
※より多くの施設取材・個別店舗取材を誘致するため、できる限り多くの情報提供のご協力をお願いします。<br />
※回答内容がそのまま記事化される事もありますので、その点ご留意ください。また、ご回答いただいた内容が全て製作物に反映されない場合もございますことも合わせてご留意願います。<br />
※「制作」の項目はフロアガイド、ブランドブック、体験ガイドにて、「ＰＲ」の項目はファクトブックにて使用を想定しております。<br />
<br />
</div>
<p class="clear"><p>


<h4>　広報ご担当者様情報をご登録ください。</h4>
<div class="offset1">
  <p>下のリンクから広報ご担当者様情報をご登録ください。</p>
  <a href="/sections/addSection/<?php echo $user_id;?>/3">広報ご担当者様情報</a>
</div>
<br />
<br />



<?php $field="";?>
<p class="clear"><p>

<div class="hearing2 form">
	<?php
		echo $this->Form->create('Hearing', Array('class'=>'form-horizontal span11','url' =>'index/'. $user_id ));
			echo $this->Form->input('id', array('type'=>'hidden'));
/*
			echo '<h4>法人名</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8'));
			echo '<h4>店舗名</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8'));
			echo '<h4>広報担当者様</h4>';
			echo $this->Form->input( $field, array('label'=>'氏名','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'フリガナ','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'部署名','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'役職','type'=>'text','class'=>'span8'));
			echo '<h4>広報ご担当者様連絡先</h4>';
			echo $this->Form->input( $field, array('label'=>'住所','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'電話','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'FAX','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'メールアドレス','type'=>'text','class'=>'span8'));
*/
			echo '<h4>業態</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8'));
/*
			echo '<h4>オフィシャルサイトURL</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8'));
			echo '<h4>店舗紹介文章</h4>';
			echo '<p class="offset2">（30～150文字以内）</p>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
*/
			echo '<h4>幕張出店の背景・戦略</h4>';
			echo '<p class="offset2">（30～150文字前後）</p>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>代表商品の商品名、スペック、価格等</h4>';
			echo '<p class="">御社を代表する商品・サービスについてご回答ください。</p>';
			echo $this->Form->input( $field, array('label'=>'商品名','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'スペック','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'価格','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'その他','type'=>'text','class'=>'span8'));
			echo '<h4>おススメ商品・サービス</h4>';
			echo '<p class="">新（リニューアル）商品・新サービス等についてご回答ください。</p>';
			echo $this->Form->input( $field, array('label'=>'おススメ商品・サービス','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'同価格（価格込み）','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'新（リニューアル）商品<br />新サービス','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'同価格（価格込み）','type'=>'text','class'=>'span8'));
			echo '<h4>年末商戦に向けのおススメ商品・サービス</h4>';
			echo '<p class="">期間限定商品等についてご回答ください。</p>';
			echo $this->Form->input( $field, array('label'=>'おススメ商品・サービス','type'=>'text','class'=>'span8'));
			echo $this->Form->input( $field, array('label'=>'同価格（価格込み）','type'=>'text','class'=>'span8'));
			echo '<h4>”コト消費”とリンクするポイント</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>これまでの店舗展開との違い</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>競合他社との違い・アピールポイント</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>（業界・イオンで）初の取り組み　ポイント</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>直接的な商品・サービス以外に体験できるコト、体験して頂きたいお勧めポイント</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>ＴＶ取材の場合、レポーターが体験できるポイント</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>今後の予定（イベントスケジュールや新商品・サービス展開予定等）</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			echo $this->Form->end();
?>
</div>
<p class="clear"><p>
<div class="hearing2 form">
<?php
			echo '<div class="alert alert-info span10">';
			echo '<h3>サービスに必要な画像を３点、ご登録ください。</h3>';
			echo '</div>';
			echo '<p class="clear"><p>';
			echo '<h4>代表商品画像・サービスイメージ</h4>';
			$options = array(
				'action'=>'upload',
				'type'=>'file',
				'class'=>'offset1'
			);
			echo $this->Form->create('Upload', $options);
			echo $this->Form->file('file_name');
			echo $this->Form->error('file_name');
			echo $this->Form->submit('登録',array('class' => 'btn btn-primary'));
			echo $this->Form->end();
?>
</div>
<p class="clear"><p>
<div class="hearing2 form">
<?php
			echo '<h4>店舗内パース、代表商品画像・サービスイメージ</h4>';
			$options = array(
				'action'=>'upload',
				'type'=>'file',
				'class'=>'offset1'
			);
			echo $this->Form->create('Upload', $options);
			echo $this->Form->file('file_name');
			echo $this->Form->error('file_name');
			echo $this->Form->submit('登録',array('class' => 'btn btn-primary'));
			echo $this->Form->end();
?>
</div>
<p class="clear"><p>
<div class="hearing2 form">
<?php
			echo '<h4>代表商品画像・サービスイメージ</h4>';
			$options = array(
				'action'=>'upload',
				'type'=>'file',
				'class'=>'offset1'
			);
			echo $this->Form->create('Upload', $options);
			echo $this->Form->file('file_name');
			echo $this->Form->error('file_name');
			echo $this->Form->submit('登録',array('class' => 'btn btn-primary'));
			echo $this->Form->end();
?>
</div>
<p class="clear"><p>
<div class="hearing2 form">
<?php
			echo '<div class="alert alert-info span10">';
			echo '<h3>店舗ロゴをご登録ください。</h3>';
			echo '</div>';
			echo '<p class="clear"><p>';
			$options = array(
				'action'=>'upload',
				'type'=>'file',
				'class'=>'offset1'
			);
			echo $this->Form->create('Upload', $options);
			echo $this->Form->file('file_name');
			echo $this->Form->error('file_name');
			echo $this->Form->submit('登録',array('class' => 'btn btn-primary'));
			echo $this->Form->end();
?>
</div>
<p class="clear"><p>
<div class="hearing2 form">
<?php
			echo '<div class="alert alert-info span10">';
			echo '<h3>飲食業態に対するヒアリング</h3>';
			echo '</div>';
			echo '<p class="clear"><p>';

			echo '<h4>ラストオーダー時間（フード・ドリンク）</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>座席数</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>喫煙席・禁煙席</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>分煙・禁煙時間</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>予約（電話・FAX・FAX・HP・その他）</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>アルコール扱い</h4>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo '<h4>チャイルドシート有無</h4>';
			echo $this->Form->select( $field, array(
					'有り'=>'有り','無し'=>'無し'
			));
			echo '<h4>キッズメニュー有無</h4>';
			echo $this->Form->select( $field, array(
					'有り'=>'有り','無し'=>'無し'
			));
			echo '<h4>オフィシャルサイト以外の外部サイトURL</h4>';
			echo '<p>（例） ぐるなびURL、食べログURL等</p>';
			echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'2'));
			echo $this->Form->button('登録',array('class' => 'btn btn-primary pull-left'));
			echo $this->Form->end();

?>
</div>
