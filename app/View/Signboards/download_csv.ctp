<?php

	// カテゴリー１〜５のチェックボックスを文字列にする
	for( $cnum = 0; $cnum < 5; $cnum++ ){
		/* *** */
		$array = $signboard['shop_category' . ($cnum +1)];
		$label = $category_options[ $cnum ];
		$text = '';
		foreach( $array as $key => $value ){
		  $text .= $label[ $value ] ."\n";
		}
		$signboard['shop_category' . ($cnum +1)] = $text;
	}

	$td[] = $signboard;

	//ファイル名設定
	$this->Csv->setFilename( $filename ); 

	//ヘッダー行を追加
	 $this->Csv->addRow( $th );

	//表の各行を追加していく
	foreach( $td as $t ){
		$this->Csv->addRow( $t );
	}

	//文字コードをUTF-8からSJISに変換して出力。
	echo mb_convert_encoding( $this->Csv->render(), 'SJIS', 'UTF-8');

