<?php
	foreach( $signboards as $s_key => $signboard ){
		// カテゴリー１〜５のチェックボックスを文字列にする
		for( $cnum = 0; $cnum < 5; $cnum++ ){
			/* *** */
			$array = $signboards[$s_key]['Signboard']['shop_category' . ($cnum +1)];
			$label = $category_options[ $cnum ];
			$text = '';
			foreach( $array as $key => $value ){
			  $text .= $label[ $value ] ."\n";
			}
			$signboards[$s_key]['Signboard']['shop_category' . ($cnum +1)] = $text;
		}

		// メインの標記方法を文字にする
		$main = $signboards[$s_key]['Signboard']['shop_sign_main_type'];
		if( $main == '1' ){
			$signboards[$s_key]['Signboard']['shop_sign_main_type'] = '英語標記';
		}else{
			$signboards[$s_key]['Signboard']['shop_sign_main_type'] = '日本語標記';
		}

		// 会社名を追加
		if(isset($signboard['User']['company']))
			$signboards[$s_key]['Signboard']['company'] = $signboard['User']['company'];
		else
			$signboards[$s_key]['Signboard']['company'] = '';

		// ログインIDにする
		$signboards[$s_key]['Signboard']['id'] = $signboard['User']['loginid'];
		unset($signboards[$s_key]['Signboard']['user_id']);
		$td[] = $signboards[$s_key]['Signboard'];
//debug( $td );exit();
	}

	//ファイル名設定
	$this->Csv->setFilename( $filename ); 

	//ヘッダー行を追加
	$th[0] = 'ログインID';
	unset($th[1]);
	 $this->Csv->addRow( $th );

	//表の各行を追加していく
	foreach( $td as $t ){
		$this->Csv->addRow( $t );
	}

	//文字コードをUTF-8からSJISに変換して出力。
	echo mb_convert_encoding( $this->Csv->render(), 'SJIS', 'UTF-8');

