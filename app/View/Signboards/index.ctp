<?php if( $print == null ){ ?>
<div class="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
  <p>出店に関するヒアリングシートです</p>
  <p>本フォームは、館内サイン・フロアガイド・HP及び販促告知媒体などのカテゴリー・店名の標記確認書です。</p>
  <p>媒体等のカテゴリー・店名は、全て記載いたしますので、くれぐれもお間違いの無い様、御社本部確認の上、ご入力をお願いいたします。</p>
</div>

<?php }else{ ?>
<div class="row-fluid">
	<div class="span6">
		<h2><?php echo $title_for_layout ?></h2>
		<p>出店に関するヒアリングシートです</p>
	</div>
	<div class="span6">
		<?php if( isset( $this->request->data['Signboard']['modified'])){ ?>
			<p>最終更新日： <?php echo( $this->request->data['Signboard']['modified']);?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php if( SUB_DOMAIN == 'makuharishintoshin' || SUB_DOMAIN == 'testsite'):?>
<p class="clear"></p>
<h4>【 Indoor　google　map　導入について 】</h4>
<p>イオンモールにおいて、indoor　google　mapを導入いたします。</p>
<p>google　map上に、当施設が表示される際、拡大していくと施設内の区画まで表示されます。</p>
<ul>
<li>1. テナント名</li>
<li>2. 業種カテゴリー</li>
<li>3. 営業時間</li>
<li>4. 電話番号</li>
<li>5. テナント様URL</li>
</ul>
<p>が表示されます。</p>
<p>1.～5.の情報は、イオンモール幕張新都心のウェブサイト上の情報をそのまま転用いたしますので、その旨ご了承ください。</p>
<br /><hr />
<?php endif;?>

<?php echo $this->Session->flash(); ?>

<div class="signboard form">
<?php
echo $this->Form->create('Signboard', Array('class'=>'form-horizontal span12','url' =>'index/'. $user_id ));
$option = 0;
foreach ( $fields as $key => $field ){
    switch( $field ){
    case 'id':
        echo $this->Form->input('id', array('type'=>'hidden'));
        break;
    case 'user_id':
        echo $this->Form->input('user_id', array('type'=>'hidden', 'value'=>$user_id));
        echo '<h3>【本件に関する御社連絡先】</h3>';
        break;
    case 'name':
        echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 山田太郎'
		));
        break;
    case 'name_kana':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） ヤマダタロウ'
		));
        break;
    case 'department':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 営業部'
		));
        break;
    case 'position':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 営業マネージャー'
		));
		echo '</table>';

		if( empty( $locked )){
			echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
		}elseif( $print == null ){
			echo MESSAGE_AT_LOCKED;
		}
		echo '<br />';
		echo '<br />';
		echo '<h5>【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>';
		echo $this->Html->link('御社情報登録はこちら',
				 '/users/edit',array('class'=>'pull-right'));
		echo '<br />';
		echo '<br />';
		echo '<table class="table table-bordered">';
        break;
    case 'zipcode':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 2618539 ・・・７文字の半角数字で入力してください。'
		));
        break;
    case 'address':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 千葉県千葉市美浜区中瀬一丁目5番地1'
		));
        break;
    case 'phone':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 031234567 ・・・１４文字以内の半角数字で入力してください。'
		));
        break;
    case 'fax':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 031234568 ・・・１４文字以内の半角数字で入力してください。'
		));
        break;
    case 'extension':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 1234 ・・・１４文字以内の半角数字で入力してください。'
		));
        break;
    case 'cellphone':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 1234 ・・・１４文字以内の半角数字で入力してください。'
		));
        break;
    case 'email':
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） yamada@xxxx.com ・・・メールアドレスを入力してください。'
		));
        echo '</table>';
        break;

    case 'shop_sign_jpn':
		if( empty( $locked )){
			echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
		}elseif( $print == null ){
			echo MESSAGE_AT_LOCKED;
		}
        echo '<br /><hr />';
        echo '<h3 id="shop_sign">【基本情報ご記入欄」</h3>';
        echo '<h4>標記方法</h4>';

        echo '<table class="table table-bordered">';
        echo '<tr><td rowspan="2">';
        echo $labels[ $key ];
        echo '</td><td>';
        echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'));
        echo '<div>（例） 東京ファッション</div>';
        echo '</td></tr>';
        break;
    case 'shop_sign_kana':
        echo '<tr><td style="background-color: white;">';
        echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'));
        echo '<div>（例） トウキョウファッション ・・・フリガナを全角カタカナで入力してください。</div>';
        echo '</td></tr>';
        break;
    case 'shop_sign_eng':
        echo '<tr><td>';
        echo $labels[ $key ];
        echo '</td><td>';
        echo $this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'));
        echo '<div>（例） tokyo fashion</div>';
        echo '</td></tr>';
        echo '</table>';
        break;

    case 'shop_sign_main_type':
        echo '<h4>メインの標記方法</h4>';
        echo '<table class="table table-bordered">';
        echo '<tr><td>メインの標記方法';
        echo '</td><td>';
        echo $this->Form->select( $field, array( 'カナor日本語標記', '英語標記'),
                                  array('empty' => '選択してください',));
        echo '<p>メインの標記方法を選択してください。</p>';
        echo '</td></tr>';
        echo '</table>';
        break;
    case 'shop_category1':
		if( empty( $locked )){
			echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
		}elseif( $print == null ){
			echo MESSAGE_AT_LOCKED;
		}
        echo '<hr />';
        echo '<h4>分類</h4>';
    case 'shop_category2':
    case 'shop_category3':
    case 'shop_category4':
    case 'shop_category5':
        $cells[ $option ] =  $this->Form->input( $field, array(
            'label'     => '',
            'type'      => 'select' ,
            'multiple'  => 'checkbox',
            'options'   => $category_options[ $option++ ]
        ));
        break;
    case 'shop_detail':
        echo '<h5>カテゴリー（該当するものに、しるしをつけてください。　複数可）</h5>';
        echo '<table class="category table table-condensed">';
        echo $this->Html->tableHeaders( $category_labels );
        echo $this->Html->tableCells( $cells );
        echo '</table>';
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			'詳細業種',
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 酒類 ・・・御社が希望する業種標記をご記入ください。'
		));
		echo '</table>';
        break;
    case 'shop_remarks':
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			'備考',
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10','rows'=>'3'))
			. 'その他、注意すべき点があります場合は、この「備考欄」にご記入をお願いします。'
		));
		echo '</table>';
		if( empty( $locked )){
			echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
		}elseif( $print == null ){
			echo MESSAGE_AT_LOCKED;
		}
        echo '<hr />';
        break;
    case 'shop_phone':
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			'テナント電話番号',
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） 031234567 ・・・１４文字以内の半角数字で入力してください。'
		));
		echo '</table>';
        break;
    case 'shop_open_time':
        //echo '<hr />';
        //echo '<h4>営業時間</h4>';
        //echo '<div class="caution">';
        //echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span2'));
        //echo '<p class="offset2">４桁の半角数字でご記入をお願いします。（例： １０時００分の場合→ <strong>1000</strong> ）</p>';
        //echo '</div>';
        break;
    case 'shop_close_time':
        //echo '<div class="caution">';
        //echo $this->Form->input( $field, array('label'=>$labels[ $key ],'type'=>'text','class'=>'span2'));
        //echo '<p class="offset2">４桁の半角数字でご記入をお願いします。（例： ２０時００分の場合→ <strong>2000</strong> ）</p>';
        //echo '</div>';
        break;
    case 'shop_url':
		if( empty( $locked )){
			echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
		}elseif( $print == null ){
			echo MESSAGE_AT_LOCKED;
		}
        echo '<br /><hr />';
        echo '<h3>【ショップページ基本情報ご記入欄】</h3>';
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） https://com.aeonmall.com/ 　・・・サイトURLを入力してください。'
		));
		echo '</table>';
        break;
	case 'blog_url':
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
			. '（例） http://shopname.xxxblog.com/ 　・・・ブログURLを入力してください。'
		));
		echo '</table>';
        break;
    case 'shop_copy':
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10'))
		));
		echo '</table>';
        break;
    case 'shop_concept':
		echo '<table class="table table-bordered">';
		echo $this->Html->tableCells(array(
			$labels[ $key ],
			$this->Form->input( $field, array('label'=>false,'type'=>'text','class'=>'span10','rows'=>'5'))
		));
		echo '</table>';
        break;
    }
}
echo '<br />';
if( empty( $locked )){
	echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
}elseif( $print == null ){
	echo MESSAGE_AT_LOCKED;
}
echo $this->Form->end();
?>
<hr />
<p class="clear"></p><br />

<?php if( SUB_DOMAIN == 'toin' || SUB_DOMAIN == 'testsite'):?>
<p class="clear"></p>
<h4>【 事務局 】</h4>
<pre class="span9">
	〒460-8445　名古屋市中区栄4-16-36（株）電通中部支社　6階
	イオンモール東員ご出店社様対応事務局　担当：佐竹/小﨑 宛
	TEL：052-263-8482　FAX：052-263-8444
	Email： aeontouin@dentsutec.co.jp
	</pre>
<p class="clear"></p><br />
<br /><hr />
<?php endif;?>

<!-- モーダル -->
<div class="modal hide" id="mymodal">
  <div class="modal-header">
    <div>登録結果</div>
    <br />
  </div>
  <div class="modal-body">
    <p id="modal_message"></p>
  </div>
  <div class="modal-footer">
    <button id="close_mymodal" data-dismiss="modal">OK</button>
  </div>
</div>

<?php
if( isset( $script_in_get )){
  echo '<script type="text/javascript">';
  echo $script_in_get;
  echo '</script>';
}
?>
