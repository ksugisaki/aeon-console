<script type="text/javascript">
  // ignore_enter_to_submit.jsに渡す変数
  var print_mode = "<?php echo empty( $print )? '' : $print; ?>";
</script>
<?php echo $this->Html->script(array('ignore_enter_to_submit'), array('inline'=>false)); ?>

<?php if( $print == null ){ ?>
<div class="hero-unit">
	<h2>植樹祭参加者登録</h2>
	<p>植樹祭参加者の情報をご入力ください。</p>
	<?php if( defined('SYOKUJYU_LINK')):
		echo '<a class="" href="';
		echo $this->Html->url( SYOKUJYU_LINK );
		echo '"><i class="icon-file"></i><large>植樹祭のご案内(PDF)</large></a>';
	endif;?>
</div>

<?php }else{ ?>
<div class="row-fluid">
	<div class="span6">
		<h2>植樹祭参加者登録</h2>
	</div>
	<div class="span6">
		<?php if( isset( $this->request->data['SyokujyuItem']['modified'])){ ?>
			<p>最終更新日： <?php echo( $this->request->data['SyokujyuItem']['modified']);?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('SyokujyuItem', array('class' =>'myform form-horizontal', 'type' => 'file'));?>
		<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>
		<?php echo $this->BootstrapForm->input('user_id', array('type' => 'hidden')); ?>
		<fieldset>

		<?php $title = array('参加者①','参加者②','参加者③');?>
		<?php if( SUB_DOMAIN =='toin') $title = array('代表者名','氏名1','氏名2');?>

		<?php for( $num = 1; $num <= 3; $num++ ): ?>
			<h3><?php echo $title[ $num -1 ];?></h3>
			<?php echo $this->BootstrapForm->input('name'. $num, array('label'=>'氏名','class' => 'span10')); ?>
			<p class="offset2"> 例）山田花子</p>
			<?php echo $this->BootstrapForm->input('name_kana'. $num, array('label'=>'フリガナ','class' => 'span10')); ?>
			<p class="offset2"> 例）ヤマダハナコ</p>
			<?php echo $this->BootstrapForm->input('position'. $num, array('label'=>'役職','class' => 'span10')); ?>
			<p class="offset2"> 例）店長</p>
			<?php echo $this->BootstrapForm->input('contact'. $num, array('label'=>'当日連絡の取れる連絡先','class' => 'span10')); ?>
			<p class="offset2"> 例） 090-1234-5678</p>
			<hr />
		<?php endfor; ?>

		<?php if( $_SESSION['Auth']['User']["lock_syokujyu"] == 0): ?>
			<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>'btn btn-primary'));?>
		<?php else: ?>
			<?php echo MESSAGE_AT_LOCKED; ?>
		<?php endif; ?>
		</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
