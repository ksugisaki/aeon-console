<?php if( $print == null ): ?>
    <div class="hero-unit">
    <?php if( SUB_DOMAIN =='toin'): ?>
        <h2>従業員ロッカー利用台数申込書</h2>
    <?php else: ?>
        <h2>従業員ロッカー利用台数アンケート</h2>
        <p>
          ＜グランドモール、ファミリーモール、アクティブモールの専門店さま限定＞<br />
          従業員ロッカーの希望台数をご入力下さい。
        </p>
    <?php endif; ?>
        <?php if( defined('LOCKER_LINK')):
            echo '<a class="" href="';
            echo $this->Html->url( LOCKER_LINK );
            echo '"><i class="icon-file"></i><large>駐車場ご案内(PDF)</large></a>';
        endif;?>
    </div>
<pre>
・希望数に添えない場合もございますので、予めご了承下さい。
<?php if( SUB_DOMAIN =='makuharishintoshin'): ?>
・ペットモールには、スペースの都合上、ご用意できませんでした。
・詳細は運営管理説明会資料24～25ページをご確認下さい。
<?php endif; ?>
</pre><br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>従業員ロッカー利用希望台数アンケート</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['Locker']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['Locker']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
<?php if( SUB_DOMAIN =='toin'):  ?>
  <tr><th>費用</th><th>サイズ</th></tr>
  <tr><td>月額　<strong>1,000円</strong>（税別）</td><td>幅193㎜ × 奥行き450㎜ × 高さ1,670㎜</td></tr>
<?php else: ?>
  <tr><th width="200px"></th>
    <th>費用</th><th>サイズ</th></tr>
  <tr><td width="200px">【グランドモール】</td>
    <td>月額　<strong>1,000円</strong>（税別）</td><td>幅193mm × 奥行き450mm × 高さ1,670mm</td></tr>
  <tr><td width="200px">【ファミリーモール】</td>
    <td>月額　　<strong>600円</strong>（税別）</td><td>幅264mm × 奥行き450mm × 高さ820mm</td></tr>
  <tr><td width="200px">【アクティブモール】</td>
    <td>月額　　<strong>600円</strong>（税別）</td><td>幅264mm × 奥行き450mm × 高さ820mm</td></tr>
  <tr><td colspan="3">※建物構造上、棟ごとにサイズが異なります。それにより、料金も棟ごとに異なっております。</td></tr>
<?php endif; ?>
</table>
<p class="clear"></p>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('Locker',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
        <h3>利用台数</h3>
        </div>
        <?php echo $this->BootstrapForm->input('locker_num', array('label'=>'希望台数','class' => 'span10')); ?>
        <?php if( SUB_DOMAIN =='makuharishintoshin'): ?>
            <p class="offset2">※ お一人様1台の利用とさせていただきます。<br />
                               　 また、鍵の複製は禁止いたしております。予め、ご了承下さい。</p>
        <?php endif; ?>
        <p class="offset2">※ 半角数字で入力してください。</p>
        <p class="offset2">※ 数に限りがございますので、ご希望に添えない場合もあります。
        <br />　 最低限の必要台数を申請願います。</p>

        <br />
        <div class="alert alert-info">
        <h3>契約書送付先</h3>
        </div>
        <h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）261-8539</p>
        <?php echo $this->BootstrapForm->input('address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('position', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'ご連絡先','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
