<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>クリスマスギフト特集・クリスマスグルメ特集</h2>
        <p>ホームページにて「クリスマスギフト特集・クリスマスグルメ特集」を掲載いたします。
           掲載を希望される商品についての情報をご記入ください（最大３件）。</p>
    </div>

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>クリスマスギフト特集・クリスマスグルメ特集</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['ChristmasFeature']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['ChristmasFeature']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="row-fluid">

    <div style="margin-left: 50px;">
      <div style="margin-left: 10px; font-size: 120%; line-height: 150%;">
        イオンモール東員ホームページにて、２種類のクリスマス特集ページを作成致します。<br />
        下記テーマに沿った、各店舗さまの商品やサービスをご紹介するページでございます。<br />
        上記テーマに沿った商品やサービスを提案できる店舗さまは、下記に必要情報をご登録下さい。<br />
        （ご登録頂いたものを、イオンモール東員 営業担当にて、掲載の振り分けをさせて頂きます。）<br />
        お客さまへの情報発信に、ぜひ積極的にご活用下さい。<br />
      </div>
      <br />

      <h4>【設定テーマ】</h4>
      <div style="margin-left: 10px;">
        <h5>①心も体もほっこりクリスマスギフト</h5>
        <p>家族・お友達・恋人・・・に贈る、心も体もあたたまるハッピーなクリスマスギフトをご提案下さい。</p>
        <h5>②みんなでわいわいクリスマスグルメ</h5>
        <p>家族や、友達同士でわいわい食べられる、クリスマス(冬季)限定グルメをご提案下さい。</p>
      </div>
      <br />

      <h4>【掲載期間】</h4>
      <div style="margin-left: 10px;">
        <p>11月19日(火)～12月25日(水)</p>
      </div>
      <br />

      <h4>【注意事項】</h4>
      <div style="margin-left: 10px;">
        <h5>①掲載期間中に在庫のある商品のご提案をお願い致します。</h5>
        <p>(在庫がなくなった場合は、掲載を削除致しますので、イオンモール東員営業担当までご連絡下さい。)</p>
        <h5>②掲載できる画像は最大で3枚までです。</h5>
        <p>商品が目立つような、アップで撮影した画像をご登録下さい。</p>
        <h5>③ご登録頂いた画像が必ずしも全て掲載されるとは限りません。</h5>
        <h5>④上記掲載期間中は、商品画像の追加・変更・削除が可能ですので、その都度イオンモール東員 営業担当までご相談下さい。</h5>
      </div>
      <br /><br />
    </div>

    <div class="">
    <?php
        echo $this->BootstrapForm->create('ChristmasFeature',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <?php for( $num = 1; $num <= 3; $num++ ): ?><!-- 商品の数だけ繰り返し -->

            <div class="alert alert-info">
                <h3>商品情報 <?php echo $num; ?></h3>
            </div>

          <?php // 画像の入力フォームと登録画像表示

            if( empty( $this->request->data['ChristmasFeature']['syouhin'. $num .'_image1_file_name'])){
                echo '<p>画像：　未入力</p>';
            }elseif( empty( $this->request->data['ChristmasFeature']['syouhin'. $num .'_image1_modified'] )){
                echo '<p class="alert">登録ファイルの保存に失敗しました。すみませんが、もう一度登録をやりなおして下さい。</p>';
            }else{
                $filename = $this->request->data['ChristmasFeature']['syouhin'. $num .'_image1_file_name'];
                $pathinfo = pathinfo( $this->upload->url(
                            $this->request->data, 'ChristmasFeature.syouhin'. $num .'_image1', array()));
                switch( $pathinfo['extension']):
                case 'jpg':
                case 'JPG':
                case 'png':
                case 'PNG':
                    echo $this->upload->image(
                        $this->request->data, 'ChristmasFeature.syouhin'. $num .'_image1', array(), array('width' => '400'));
                    echo '<p>'. $filename . '</p>';
                    echo $this->BootstrapForm->input('delete_syouhin'. $num .'_image1', array(
                                    'label'=> '削除','type'=>'checkbox','div'=>false));
                    break;
                case 'eps':
                case 'EPS':
                    echo '<p class="alert alert-info">※「eps」画像のプレビューは表示されません。</p>';
                    echo '<p>商品画像：　'. $filename.'<p>';
                    echo $this->BootstrapForm->input('delete_syouhin'. $num .'_image1', array(
                                    'label'=> '削除','type'=>'checkbox','div'=>false));
                    break;
                default:
                    echo '<p>商品画像：　'. $filename.'<p>';
                    echo '<p class="alert">※画像ファイルは .jpg .png .eps いずれかの形式でご入力ください。</p>';
                    break;
                endswitch;
            }
            echo $this->BootstrapForm->input('syouhin'. $num .'_image1', array('label'=>false,'div'=>false,'type' => 'file'));
            echo '<p>※画像ファイルは .jpg .png .eps いずれかの形式でご入力ください。</p>';
            echo '<p class="clear"></p>';
            echo '<hr />';

          ?>


            <?php echo $this->BootstrapForm->input('syouhin'. $num .'_name',
                                             array('label'=>'商品名','class'=>'span10')); ?><br />
            <?php echo $this->BootstrapForm->input('syouhin'.$num.'_info',
                                             array('label'=>'商品情報','class'=>'span10','rows'=>'6')); ?><br />
            <?php echo $this->BootstrapForm->input('syouhin'.$num.'_tanka',
                                             array('label'=>'価格(税込)','class'=>'span3 yen')); ?><br />
            <?php echo $this->BootstrapForm->input('syouhin'.$num.'_comment',
                                             array('label'=>'オススメコメント','class'=>'span10','rows'=>'6')); ?>

            <?php if( ! $lock ): ?>
                <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
            <?php else: ?>
                <?php echo MESSAGE_AT_LOCKED; ?>
            <?php endif; ?>

        <?php endfor; ?><!-- ここまで繰り返し -->

    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>

<script type="text/javascript">
$(document).ready(function(){
    // 「円」を追加
    $('.yen').after(' 円');

    // 「個」を追加
    $('.ko').after(' 個');
})
</script>
