<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>販促企画最終確認</h2>
        <p>
                こちらのシートへの入力をもって、最終確認とさせて頂き、
                館内ポスターや、イオンモール東員ＨＰへ、ご入力頂いたとおりに掲載させて頂きますので、
                正確にご入力をお願い致します。
        </p>
    </div>

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>販促企画最終確認</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['LastHansoku']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['LastHansoku']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

        <strong class="offset1">【記入例】</strong>
        <div class="offset2">
        <ul>
        <li style="list-style: square;">全品5%OFF ・・・店内商品全てが5%OFFの対象となります。</li>
        <li style="list-style: square;">5%OFF（一部商品除外） ・・・セール品除外の場合は、一部商品除外という表現になります。</li>
        <li style="list-style: square;">自店ポイントカード2倍 ・・・自社の発行しているポイントカードが対象です。</li>
        <li style="list-style: square;">●円以上お買上げの方にノベルティプレゼント</li>
        <li style="list-style: square;">ソフトドリンクサービス</li>
        <li style="list-style: square;">対象カード、<u>自店ポイントカード</u>をご利用 または ご提示の方、5％ＯＦＦ(一部商品除く)</li>
        </ul>

        </div>
        <p class="offset1"><strong>※</strong>対象カードのご利用 または ご提示にて特典を付与して頂く形となります。<br />
                           　上記条件以外で付与されることがあるようでしたら、下記枠内に対象者も明記下さい。<br />
        </p>

        <br />


<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('LastHansoku',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
        <h3>オープン販促</h3>
        </div>
        <pre>※オープン販促期間は12/1（日）までと致します。</pre>
        <?php echo $this->BootstrapForm->input('open_hansoku', array('label'=>'オープン販促','class' => 'span12','rows'=>'5')); ?>
        <br />

        <div class="alert alert-info">
        <h3>感謝デー　特典内容</h3>
        </div>
        <?php echo $this->BootstrapForm->input('kansya_day_tokuten', array('label'=>'感謝デー<br />特典内容','class' => 'span12','rows'=>'5')); ?>
        <br />

        <div class="alert alert-info">
        <h3>Ｇ．Ｇ感謝デー　特典内容</h3>
        </div>
        <?php echo $this->BootstrapForm->input('gg_kansya_day_tokuten', array('label'=>'Ｇ．Ｇ感謝デー<br />特典内容','class' => 'span12','rows'=>'5')); ?>
        <br />


        <div class="alert alert-info">
        <h3>シネマdeおトク　特典内容</h3>
        </div>
        <pre>※（仮）イオンシネマ東員での当日の映画チケットまたは半券を持参のお客さま限定のサービスです。</pre>
        <?php echo $this->BootstrapForm->input('cinema_de_plus_tokuten', array('label'=>'シネマdeおトク<br />特典内容','class' => 'span12','rows'=>'5')); ?>
        <br />


        <div class="alert alert-info">
        <h3>従業員サービス　特典内容</h3>
        </div>
        <pre>※イオンモール東員内で働く、従業員様へ向けたサービスです。従業員証の提示でサービスを受けられます。</pre>
        <?php echo $this->BootstrapForm->input('juugyouin_tokuten', array('label'=>'従業員サービス<br />特典内容','class' => 'span12','rows'=>'5')); ?>
        <br />


        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>

