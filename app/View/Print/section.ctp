<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<?php echo $this->Html->css('bootstrap.min'); ?>
	<?php echo $this->Html->css('bootstrap_for_aeon'); ?>
	<style type="text/css">
		table {
			background-color: #FFF;
		}
		.break {
			page-break-before: always;
		}
	</style>
</head>
<body>

<?php foreach($td as $key => $val): ?>
<div class="container">
	<h4>店舗カルテ</h4>
	<h5>【基本情報】</h5>
	<table class="table">
		<tr>
			<th>テナントコード</th>
			<td></td>
			<th>テナント名</th>
			<td><?php echo $val[12] ?></td>
		</tr>
		<tr>
			<th>会社名</th>
			<td><?php echo $val[1] ?></td>
			<th>出店場所</th>
			<td></td>
		</tr>
		<tr>
			<th>住所</th>
			<td colspan="3">〒<?php echo $val[2] ?><br /><?php echo $val[3] ?></td>
		</tr>
		<tr>
			<th>電話番号</th>
			<td><?php echo $val[10] ?></td>
			<th>FAX</th>
			<td><?php echo $val[5] ?></td>
		</tr>
	</table>

	<hr />
<p class="clear"></p>
	<h5>【担当者情報】</h5>
	<table class="table">
		<tr>
			<th nowrap>担当部門</th>
			<th nowrap>部署</th>
			<th nowrap>役職</th>
			<th nowrap>担当者名</th>
			<th nowrap>連絡先</th>
		</tr>
				<?php $vi=13;?>
		<tr>
			<th>営業</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td>
				<?php echo $val[$vi++];?><br />
				<?php echo $val[$vi++];?><br />
			</td>
			<td>
				〒: <?php echo $val[$vi++];?><br />
				住所: <?php echo $val[$vi++];?><br />
				電話: <?php echo $val[$vi++];?><br />
				FAX: <?php echo $val[$vi++];?><br />
				携帯: <?php echo $val[$vi++];?><br />
				Email: <?php echo $val[$vi++];?><br />
			</td>
		</tr>
		<tr>
			<th>店舗開発</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td>
				<?php echo $val[$vi++];?><br />
				<?php echo $val[$vi++];?><br />
			</td>
			<td>
				〒: <?php echo $val[$vi++];?><br />
				住所: <?php echo $val[$vi++];?><br />
				電話: <?php echo $val[$vi++];?><br />
				FAX: <?php echo $val[$vi++];?><br />
				携帯: <?php echo $val[$vi++];?><br />
				Email: <?php echo $val[$vi++];?><br />
			</td>
		</tr>
		<tr>
			<th>経理</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td>
				<?php echo $val[$vi++];?><br />
				<?php echo $val[$vi++];?><br />
			</td>
			<td>
				〒: <?php echo $val[$vi++];?><br />
				住所: <?php echo $val[$vi++];?><br />
				電話: <?php echo $val[$vi++];?><br />
				FAX: <?php echo $val[$vi++];?><br />
				携帯: <?php echo $val[$vi++];?><br />
				Email: <?php echo $val[$vi++];?><br />
			</td>
		</tr>
		<tr>
			<th>広報</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td>
				<?php echo $val[$vi++];?><br />
				<?php echo $val[$vi++];?><br />
			</td>
			<td>
				〒: <?php echo $val[$vi++];?><br />
				住所: <?php echo $val[$vi++];?><br />
				電話: <?php echo $val[$vi++];?><br />
				FAX: <?php echo $val[$vi++];?><br />
				携帯: <?php echo $val[$vi++];?><br />
				Email: <?php echo $val[$vi++];?><br />
			</td>
		</tr>
		<tr>
			<th>搬入</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td>
				<?php echo $val[$vi++];?><br />
				<?php echo $val[$vi++];?><br />
			</td>
			<td>
				〒: <?php echo $val[$vi++];?><br />
				住所: <?php echo $val[$vi++];?><br />
				電話: <?php echo $val[$vi++];?><br />
				FAX: <?php echo $val[$vi++];?><br />
				携帯: <?php echo $val[$vi++];?><br />
				Email: <?php echo $val[$vi++];?><br />
			</td>
		</tr>
		<tr>
			<th>備考</th>
			<td colspan="10"><?php echo $val[63] ?></td>
		</tr>
	</table>
</div>
<div class="break"></div>
<?php endforeach ?>

</body>
</html>
