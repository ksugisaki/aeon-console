<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
	<?php echo $this->Html->css('bootstrap.min'); ?>
	<?php echo $this->Html->css('bootstrap_for_aeon'); ?>
	<style type="text/css">
		table {
			background-color: #FFF;
		}
		.break {
			page-break-before: always;
		}
	</style>
</head>
<body>

<?php foreach($td as $key => $val): ?>
<div class="container">
	<h4>店舗カルテ</h4>
	<h5>【基本情報】</h5>
	<table class="table">
		<tr>
			<th>テナントコード</th>
			<td></td>
			<th>テナント名</th>
			<td><?php echo $val[12] ?></td>
		</tr>
		<tr>
			<th>会社名</th>
			<td><?php echo $val[1] ?></td>
			<th>出店場所</th>
			<td></td>
		</tr>
		<tr>
			<th>住所</th>
			<td colspan="3">〒<?php echo $val[2] ?><br /><?php echo $val[3] ?></td>
		</tr>
		<tr>
			<th>電話番号</th>
			<td><?php echo $val[10] ?></td>
			<th>FAX</th>
			<td><?php echo $val[5] ?></td>
		</tr>
	</table>

	<hr />

	<h5>【担当者情報】</h5>
	<table class="table">
		<tr>
			<th>担当部門</th>
			<th>部署</th>
			<th>役職</th>
			<th>担当者名</th>
			<th>カナ</th>
			<th>郵便</th>
			<th>住所</th>
			<th>電話番号</th>
			<th>FAX</th>
			<th>携帯番号</th>
			<th>Email</th>
		</tr>
				<?php $vi=13;?>
		<tr>
			<th>営業</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
		</tr>
		<tr>
			<th>店舗開発</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
		</tr>
		<tr>
			<th>経理</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
		</tr>
		<tr>
			<th>広報</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
		</tr>
		<tr>
			<th>搬入</th>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
			<td><?php echo $val[$vi++];?></td>
		</tr>
		<tr>
			<th>備考</th>
			<td colspan="10"><?php echo $val[63] ?></td>
		</tr>
	</table>
</div>
<div class="break"></div>
<?php endforeach ?>

</body>
</html>
