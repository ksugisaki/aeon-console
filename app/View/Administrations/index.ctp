<div class="hero-unit">
  <h2>管理設定</h2>
</div>
<?php echo $this->Session->flash(); ?>
<div class="span8">
<?php
echo $this->Form->create('Administration',
                  array('class'=>'form-horizontal span7 pull-right','action'=>'index'));

echo $this->Form->input('id', array('type'=>'hidden'));
echo $this->Html->tag('h3','ユーザー登録期限');
echo $this->Html->tag('p','ユーザー登録を許す期限を設定できます。');
echo $this->Html->tag('p','これを過ぎるとユーザー登録は出来なくなります。');
echo $this->Form->year('entry_expiration', date('Y'), date(2043),
                       array('class'=>'span2','empty'=>false,'orderYear'=>'asc'));
echo $this->Html->tag('span',' 年 ', null);
echo $this->Form->month('entry_expiration', array('class'=>'span2','empty'=>false, 'monthNames'=>false));
echo $this->Html->tag('span',' 月 ', null);
echo $this->Form->day('entry_expiration', array('class'=>'span2','empty'=>false));
echo $this->Html->tag('span',' 日 ', null);
echo $this->Html->tag('p','', null);

echo '<br />';

echo $this->Html->tag('h3','御社情報／入力用端末申込書 編集期限');
echo $this->Html->tag('p','御社情報／入力用端末申込書の編集を許す期限を設定できます。');
echo $this->Html->tag('p','これを過ぎると御社情報／入力用端末申込書の編集は出来なくなります。');
echo $this->Form->year('edit_expiration', date('Y'), date(2043),
                       array('class'=>'span2','empty'=>false,'orderYear'=>'asc'));
echo $this->Html->tag('span',' 年 ', null);
echo $this->Form->month('edit_expiration', array('class'=>'span2','empty'=>false, 'monthNames'=>false));
echo $this->Html->tag('span',' 月 ', null);
echo $this->Form->day('edit_expiration', array('class'=>'span2','empty'=>false));
echo $this->Html->tag('span',' 日 ', null);
echo $this->Html->tag('p','', null);

echo '<br />';

echo $this->Form->button('設定',array('class'=>'btn btn-primary'));
echo $this->Form->end();
?>
</div>
<br />
<br />
<br />
