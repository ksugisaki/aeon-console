<?php
    if( SUB_DOMAIN =='toin'):
        $name = 'オープン時臨時入館証';
    else:
        $name = 'オープン時仮入館証';
    endif;
?>

<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2><?php echo $name; ?>発行願</h2>
    <?php if( SUB_DOMAIN =='toin'): ?>
        <p>
        2013年11月5日～2013年12月1日に入退館する全ての方に必要となる入館証です。
        セキュリティ確保のため必要最小限の枚数をご入力下さい。<br />
        </p>
    <?php else: ?>
        <p>
        2013年12月2日～2014年1月13日（応援者の方は1月31日まで）に入退館する全ての方に必要となる入館証です。
        セキュリティ確保のため必要最小限の枚数をご入力下さい。
        </p>
    <?php endif; ?>
        <?php if( defined('PERMIT_CARD_LINK')):
            echo '<a class="" href="';
            echo $this->Html->url( PERMIT_CARD_LINK );
            echo '"><i class="icon-file"></i><large>駐車場ご案内(PDF)</large></a>';
        endif;?>
    </div>
<pre>
・入館時に『<?php echo $name; ?>』をお持ちでない方は、一切入館をお断りさせていただきます。
・『<?php echo $name; ?>』は期間終了後全て回収させていただきます。
<?php if( SUB_DOMAIN =='makuharishintoshin'): ?>
・詳細は運営管理説明会資料12～18ページをご確認下さい。
<?php endif; ?>
</pre>

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2><?php echo $name; ?>発行願</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['PermitCard']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['PermitCard']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
    <tr><td width="200px">新規発行料</td>
      <td>無料</td></tr>
    <tr><td width="200px">紛失による再発行</td>
      <td>５００円（税別）／１枚</td></tr>
    <tr><td width="200px">期間終了後未返却分</td>
      <td>５００円（税別）／１枚</td></tr>
    <tr><td></td><td></td></tr>
</table>
<p class="clear"></p>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('PermitCard',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
            <h3><?php echo $name; ?>発行願</h3>
        </div>
        <?php echo $this->BootstrapForm->input('card_num', array('label'=>'必要枚数','class' => 'span10')); ?>
        <p class="offset2">※ 半角数字で入力してください。</p>
        <br />
        <br />
        <div class="alert alert-info">
        <h3>ご担当者情報</h3>
        </div>
        <h5>※<?php echo $name; ?>の送付先となります。<br />
            　【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />
            　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('zipcode', array('label'=>'送付先郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）123-4567</p>
        <?php echo $this->BootstrapForm->input('address', array('label'=>'送付先住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市南区山田一丁目5番地1</p>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('position', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'ご連絡先','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
