<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>ソフトオープン前 模擬営業（オペレーション確認）について</h2>
        <p>きたるオープンを万全の態勢で迎えるため、飲食・食品店舗を対象にオペレーション練習（模擬営業）を行います。
        趣旨としましては、オープニング従業員に経験を持ってもらうこと、
        オープンに向けて不備がないかどうかの最終確認を行うことでございます。</p>
        <p>下記をお読みくださり、趣旨ご理解の上、参加の程宜しくお願い致します。</p>
        <?php if( defined('MOCK_OPERATION_LINK')):
            echo '<a class="" href="';
            echo $this->Html->url( MOCK_OPERATION_LINK );
            echo '"><i class="icon-file"></i><large>ソフトオープン前 模擬営業（オペレーション確認）ご案内(PDF)</large></a>';
        endif;?>
    </div>

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>ソフトオープン前 模擬営業（オペレーション確認）について</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['MockOperation']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['MockOperation']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
  <tr><td>１．日時   </td><td> ２０１３年○月○日（）～２０１３年○月○日（）</td></tr>
  <tr><td>２．場所   </td><td> イオンモール幕張新都心　専門店各店（飲食店）</td></tr>
  <tr><td>３．内容   </td><td> オープン時の営業を想定した無料及びサービス価格での飲食提供会</td></tr>
  <tr><td>４．対象店舗</td><td>     フードコート・レストラン・食品店及び一部サービス店舗</td></tr>
  <tr><td rowspan="2">５．注意事項</td>
      <td>①売上金は、入金する必要はありません。※自社レジの締めは忘れずに行ってください。</td></tr>
  <tr><td>②本番を想定した練習を実施するべく多くの方にご参加いただくため、サービス価格（もしくは無料）での提供をお願いします。</td></tr>
  <tr><td></td><td></td></tr>
</table>
<p class="clear"></p>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('MockOperation',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
        <h3>ご担当者様連絡先</h3>
        </div>
        <h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）261-8539</p>
        <?php echo $this->BootstrapForm->input('address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('position', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'ご連絡先','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>


        <div class="alert alert-info">
        <h3>参加の確認</h3>
        </div>
        <h4>参加の有無</h4>
        <?php echo $this->BootstrapForm->select('attend_mode',
                array('参加'=>'参加', '不参加'=>'不参加'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <p class="offset2">※必ず「参加」／「不参加」を選択してください。</p>
        <h4>参加日時</h4>
        <p>参加される日程にチェックをお付けください。</p>
        <?php echo $this->BootstrapForm->input('attend_day1', array(
            'label'     => '２０１３年○○月○１日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day2', array(
            'label'     => '２０１３年○○月○２日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day3', array(
            'label'     => '２０１３年○○月○３日（曜）<br />XX時XX分 ～ XX時XX分',
            'type'      => 'checkbox'
        )); ?>
        <br />
        <h4>商品の限定</h4>
        <?php echo $this->BootstrapForm->select('gentei_mode',
                array('有り'=>'有り', '無し'=>'無し'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?><br /><br />
        <?php echo $this->BootstrapForm->input('gentei_menu',
                array('label'=>'提供メニュー・数量等','class' => 'span10','rows'=>'3')); ?>
        <br />
        <h4>価格設定</h4>
        <?php echo $this->BootstrapForm->select('kakaku_mode',
                array('無料'=>'無料', 'サービス価格'=>'サービス価格'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?><br /><br />
        <?php echo $this->BootstrapForm->input('kakaku_detail',
                array('label'=>'サービス価格の詳細','class' => 'span10','rows'=>'3')); ?>
        <br />
        <h4>お持ち帰り</h4>
        <?php echo $this->BootstrapForm->select('takeout_mode',
                array('有り'=>'有り', '無し'=>'無し'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?><br /><br />
        <br />
        <h4>その他のサービス</h4>
        <?php echo $this->BootstrapForm->input('other_service',
                array('label'=>'その他のサービス','class' => 'span10','rows'=>'3')); ?>
        <br />
        <h4>懸念点</h4>
        <?php echo $this->BootstrapForm->input('kenen_detail',
                array('label'=>'（混雑・列など）','class' => 'span10','rows'=>'5')); ?>
        <br />
        <h4>従業員さんへのメッセージ</h4>
        <?php echo $this->BootstrapForm->input('message',
                array('label'=>'メッセージ','class' => 'span10','rows'=>'5')); ?>
        <br />









        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
