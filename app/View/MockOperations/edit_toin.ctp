<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>【飲食・食物販店舗のみ】<br />模擬営業、店頭準備についてのアンケート</h2>
        <?php if( defined('MOCK_OPERATIONS_LINK')):
            echo '<a class="" href="';
            echo $this->Html->url( MOCK_OPERATIONS_LINK );
            echo '"><i class="icon-file"></i><large>模擬営業についてのご案内(PDF)</large></a>';
        endif;?>
    </div>

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>【飲食・食物販店舗のみ】模擬営業、店頭準備についてのアンケート</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['MockOperation']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['MockOperation']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('MockOperation',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
        <h3>ご担当者様連絡先</h3>
        </div>
        <h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'電話','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>
        <?php echo $this->BootstrapForm->input('email', array('label'=>'メールアドレス','class' => 'span10')); ?>
        <p class="offset2">（例）hanako_yamada@xxxx.co.jp</p>


        <div class="alert alert-info">
        <h3>模擬営業、参加の確認</h3>
        <p>◆ソフトオープン前　模擬営業（オペレーション確認）について</p>
        </div>

<p class="span10">
グランドオープンを万全の態勢で迎える為、飲食店（レストラン・フードコート・外部棟・食物販店舗）を対象に模擬営業（オペレーション確認）を行います。オープニング従業員に接客の実戦経験を積んでもらうこと、オープンに向けてお客さま整理対応や店舗内オペレーションに不備がないか確認するべく、行うものとなります。<br />つきましては、趣旨をご理解の上、ご参加頂きます様お願い申し上げます。
</p>
<p class="clear"></p>
<table class="admin_list table table-condensed">
  <tr><td>１．日時    </td><td>２０１３年１１月１４日（木）・１５日（金）　１１：００～１４：００</td></tr>
  <tr><td>２．場所    </td><td>イオンモール東員　専門店各店</td></tr>
  <tr><td>３．内容    </td><td>ソフトオープン時の営業を想定した試食提供会（ワンコインなどサービスとして企画下さい。）</td></tr>
  <tr><td>４．実施店舗</td><td>フードコート・レストラン街・食物販店舗</td></tr>
  <tr><td>５．告知方法</td><td>
                         参加店舗一覧を１１月７日（木）より休憩室前掲示板に提示致します。<br />
                         模擬営業期間中は館内スタッフが自由に来店できるものとします。
  </td></tr>
  <tr><td>６．その他連絡事項</td><td></td></tr>
  <tr><td>　・売上金について</td><td>売上は入金する必要はございません。自社処理にてお願い致します。</td></tr>
  <tr><td>　・価格設定について</td><td>ワンコイン提供などをご検討下さい。<bt />
                                ※現金のみ可とします。クレジットカード・電子マネー・ギフト券での支払いは不可です。</td></tr>
  <tr><td>　・両替について</td><td>当日は両替機は稼動しておりません。自社にてご準備をお願い致します。</td></tr>
  <tr><td>　・混雑時の整備について</td><td>整理列用テープパーテーションは自社にてご準備下さい。<br />
                       混雑時の誘導オペレーション対策を事前にスタッフ間でご調整をお願い致します</td></tr>
  <tr><td colspan="2">※館内ののぼり旗の使用は禁止です。店頭のＰＯＰスタンドについては安全面を十分に考慮した展開をお願いします。</td></tr>
  <tr><td></td><td></td></tr>
</table>

<p class="clear"></p>

        <h4>参加日程</h4>
        <?php echo $this->BootstrapForm->select('attend_mode',
                array('11/14（木）'=>'11/14（木）','11/15（金）'=>'11/15（金）','両日参加'=>'両日参加','不参加'=>'不参加'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <p class="offset2">※必ず日程／参加／不参加を選択してください。</p>
        <br />
        <h4>商品の限定</h4>
        <?php echo $this->BootstrapForm->select('gentei_mode',
                array('有り'=>'有り', '無し'=>'無し'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?><br /><br />
        <?php echo $this->BootstrapForm->input('gentei_menu',
                array('label'=>'提供メニュー・数量等','class' => 'span10','rows'=>'3')); ?>
        <br />
        <h4>価格設定</h4>
        <?php echo $this->BootstrapForm->select('kakaku_mode',
                array('無料'=>'無料', 'サービス価格'=>'サービス価格', '定額'=>'定額'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?><br /><br />
        <?php echo $this->BootstrapForm->input('kakaku_detail',
                array('label'=>'サービス価格の詳細','class' => 'span10','rows'=>'3')); ?>
        <br />
        <h4>お持ち帰り</h4>
        <?php echo $this->BootstrapForm->select('takeout_mode',
                array('有り'=>'有り', '無し'=>'無し'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?><br /><br />
        <br />
        <h4>その他のサービス</h4>
        <?php echo $this->BootstrapForm->input('other_service',
                array('label'=>'その他のサービス','class' => 'span10','rows'=>'3')); ?>
        <br />
        <h4>懸念点</h4>
        <?php echo $this->BootstrapForm->input('kenen_detail',
                array('label'=>'（混雑・列など）','class' => 'span10','rows'=>'5')); ?>
        <br />
        <h4>従業員さんへのメッセージ</h4>
        <?php echo $this->BootstrapForm->input('message',
                array('label'=>'メッセージ','class' => 'span10','rows'=>'5')); ?>
        <br />
        <br />

        <div class="alert alert-info">
        <h3>店頭準備、希望日時の確認</h3>
        </div>
        <div class="span10">
        <p>
10月1日の運営管理説明会でご案内させて頂きましたが、
ウェイティングゾーンや、イーゼルの設置を、各店舗ごとに、
各店舗の責任者さまと、イオンモール担当者の立会いで実施致します。
        </p><p>
つきましては、<strong>11月14日(木)～16日(土)</strong>の間で、ご希望の日時を下記にご入力をお願い致します。
設置完了後、写真撮影を行いますので店頭はきれいに整えておいて下さい。<br />
(ダンボールの放置や資材が店頭に置いてあることのないようにお願い致します。)
        </p><p>
日時は決まり次第、ご連絡させて頂きます。
下記のご希望に沿えない可能性もございますので、ご了承下さい。
        </p>
        </div>
        <p class="clear"></p>

        <hr />
        <h5>第１希望</h5>
        <hr />
        <?php
            $date_list = array('11月14日(木)'=>'11月14日(木)',
                               '11月15日(金)'=>'11月15日(金)',
                               '11月16日(土)'=>'11月16日(土)');
            $start_list = array('7:00'=>'7:00','8:00'=>'8:00','9:00'=>'9:00','10:00'=>'10:00','11:00'=>'11:00','12:00'=>'12:00','13:00'=>'13:00','14:00'=>'14:00','15:00'=>'15:00','16:00'=>'16:00','17:00'=>'17:00','18:00'=>'18:00','19:00'=>'19:00','20:00'=>'20:00');
            $end_list = array('7:00'=>'7:00','8:00'=>'8:00','9:00'=>'9:00','10:00'=>'10:00','11:00'=>'11:00','12:00'=>'12:00','13:00'=>'13:00','14:00'=>'14:00','15:00'=>'15:00','16:00'=>'16:00','17:00'=>'17:00','18:00'=>'18:00','19:00'=>'19:00','20:00'=>'20:00');
        ?>
        <table>
        <tr><td>日程</td><td>
        <?php echo $this->BootstrapForm->select('prepare_date1',
                $date_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        <tr><td>開始時刻</td><td>
        <?php echo $this->BootstrapForm->select('prepare_start1',
                $start_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        <tr><td>終了時刻</td><td>
        <?php echo $this->BootstrapForm->select('prepare_end1',
                $end_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        </table>

        <hr />
        <h5>第２希望</h5>
        <hr />
        <table>
        <tr><td>日程</td><td>
        <?php echo $this->BootstrapForm->select('prepare_date2',
                $date_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        <tr><td>開始時刻</td><td>
        <?php echo $this->BootstrapForm->select('prepare_start2',
                $start_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        <tr><td>終了時刻</td><td>
        <?php echo $this->BootstrapForm->select('prepare_end2',
                $end_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        </table>

        <hr />
        <h5>第３希望</h5>
        <hr />
        <table>
        <tr><td>日程</td><td>
        <?php echo $this->BootstrapForm->select('prepare_date3',
                $date_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        <tr><td>開始時刻</td><td>
        <?php echo $this->BootstrapForm->select('prepare_start3',
                $start_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        <tr><td>終了時刻</td><td>
        <?php echo $this->BootstrapForm->select('prepare_end3',
                $end_list,
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?></td></tr>
        </table>


        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>

<pre>
【問い合わせ先】
イオンモール東員　営業担当　池田　
ＴＥＬ：0594-87-7161
</pre>

<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
