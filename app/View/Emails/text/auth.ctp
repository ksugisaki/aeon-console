<?php
echo "仮登録が完了いたしました。\n";
echo "引き続き下記URLより" . SITE_NAME . "サイトへの本登録手続きをお願いいたします。\n";
echo SITE_URL . 'users/authback?authkey=' . urlencode( $authkey ). "\n";
echo "\n";
echo "【重要】\n";
echo "・24時間以内にご登録を完了して頂きますようよろしくお願い致します。\n";
echo "\n";
echo "【本メールについて】\n";
echo "・本メールに心当たりがない方はお手数ですが削除くださいますようお願いいたします。\n";
echo "・本メールは送信専用アドレスからお送りしております。ご返信いただいてもお答えできませんのでご了承ください。\n";
?>
