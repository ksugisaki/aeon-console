<?php
App::uses('AppHelper', 'View/Helper');
class MyHelper extends Helper {
    function sheet_link( $title, $link, $link_option, $permission ){
		if( $permission )
			return '<a href="' . $link . '" target="_blank">' . $title . '</a>';
		else
			return $title;
    }
}
