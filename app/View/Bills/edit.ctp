<?php echo $this->Html->script(array('scrollback'), array('inline'=>false)); ?>
<?php if( $print == null ): ?><!-- 編集ページ -->
    <div class="hero-unit">
        <h2>請求書送付先登録届</h2>
        <p>ご請求書の送付先住所をご入力ください。</p>
    </div>

<?php else: ?><!-- 印刷用ページ -->
  <div class="row-fluid">
    <div class="span6">
        <h2>請求書送付先登録届</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['Bill']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['Bill']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<?php echo $this->Session->flash(); ?>

<div class="row-fluid">
    <div class="">
    <?php // 新規add／更新edit の切替
        echo $this->BootstrapForm->create('Bill',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
    <?php if( $print == null ): ?><!-- 編集ページ -->
        <div class="alert alert-info">
        <h3>1. 請求書送付先の選択</h3>
        <strong class="">既に他のページでご入力頂いている情報から請求書送付先の登録欄に入力できます。</strong><br />
        <strong class="">直接ご入力される場合は、次の「2. 請求書送付先の登録」へお進み下さい。</strong><br />
        </div>
        <?php echo $this->BootstrapForm->select('use_data_from',
                array('御社情報'=>'御社情報', '営業部門'=>'営業部門', '経理部門'=>'経理部門'),
                array('empty'=>'選択してください', 'class'=>'','id'=>'selector'));
        ?>
        <br />
        <hr />
        <div style="display: none;" class="bill_address offset1" id="user">
          <h5 class="title">御社情報</h5>
          <table class="table">
            <tr><td width="100px">郵便番号</td><td></td></tr>
            <tr><td width="100px">住所</td><td></td></tr>
            <tr><td width="100px">部署</td><td></td></tr>
            <tr><td width="100px">氏名</td><td></td></tr>
            <tr><td width="100px">電話</td><td></td></tr>
            <tr><td width="100px">FAX</td><td></td></tr>
          </table>
        </div>
        <p class="clear"></p>
        <hr />
        <div style="display: none;" class="bill_address offset1" id="eigyou">
          <h5 class="title">営業部門</h5>
          <table class="table">
            <tr><td width="100px">郵便番号</td><td></td></tr>
            <tr><td width="100px">住所</td><td></td></tr>
            <tr><td width="100px">部署</td><td></td></tr>
            <tr><td width="100px">氏名</td><td></td></tr>
            <tr><td width="100px">電話</td><td></td></tr>
            <tr><td width="100px">FAX</td><td></td></tr>
          </table>
        </div>
        <p class="clear"></p>
        <hr />
        <div style="display: none;" class="bill_address offset1" id="keiri">
          <h5 class="title">経理部門</h5>
          <table class="table">
            <tr><td width="100px">郵便番号</td><td></td></tr>
            <tr><td width="100px">住所</td><td></td></tr>
            <tr><td width="100px">部署</td><td></td></tr>
            <tr><td width="100px">氏名</td><td></td></tr>
            <tr><td width="100px">電話</td><td></td></tr>
            <tr><td width="100px">FAX</td><td></td></tr>
          </table>
        </div>
        <p class="clear"></p>
        <br />
        <div class="alert alert-info" id="input">
        <h3>2. 請求書送付先の登録</h3>
        <strong class="">各入力欄の内容をお確かめの上、登録ボタンを押してください。</strong>
        </div>
    <?php endif; ?>
        <?php echo $this->BootstrapForm->input('zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">※（例）261-8539</p>
        <?php echo $this->BootstrapForm->input('address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">※（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">※（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">※（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'電話','class' => 'span10')); ?>
        <p class="offset2">※（例）090-1234-5678</p>
        <?php echo $this->BootstrapForm->input('fax', array('label'=>'FAX','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5679</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>

<!-- モーダル -->
<div class="modal hide" id="mymodal">
  <div class="modal-header">
    <div>登録結果</div>
    <br />
  </div>
  <div class="modal-body">
    <p id="modal_message"></p>
  </div>
  <div class="modal-footer">
    <button id="close_mymodal" data-dismiss="modal">OK</button>
  </div>
</div>

<?php
    $user_data_txt = $user_info['zipcode'] .','
                   . $user_info['address'] .','
                   . $user_info['department'] .','
                   . $user_info['name'] .','
                   . $user_info['phone'] .','
                   . $user_info['fax']; // javascriptに渡す、御社情報
    $section_eigyou_txt = '';
    $section_keiri_txt = '';
    foreach( $section_data as $section ){
        if( $section['Section']['section_name'] == 'eigyou')
            $section_eigyou_txt
                   = $section['Section']['zipcode'] .','
                   . $section['Section']['address'] .','
                   . $section['Section']['department'] .','
                   . $section['Section']['name'] .','
                   . $section['Section']['phone'] .','
                   . $section['Section']['fax']; // javascriptに渡す、営業部門
        elseif( $section['Section']['section_name'] == 'keiri')
            $section_keiri_txt
                   = $section['Section']['zipcode'] .','
                   . $section['Section']['address'] .','
                   . $section['Section']['department'] .','
                   . $section['Section']['name'] .','
                   . $section['Section']['phone'] .','
                   . $section['Section']['fax']; // javascriptに渡す、経理部門
    }
?>
<script type="text/javascript">
    //console.log('テスト開始');
    // 御社情報を取得
    $("div#user").css("display","block"); // 表示を出す
    var user_data_txt = "<?php echo $user_data_txt ?>";
    user_data = user_data_txt.split(",");
    //console.log( user_data );
    $("div#user tr:eq(0) td:eq(1)").text(user_data[0]);
    $("div#user tr:eq(1) td:eq(1)").text(user_data[1]);
    $("div#user tr:eq(2) td:eq(1)").text(user_data[2]);
    $("div#user tr:eq(3) td:eq(1)").text(user_data[3]);
    $("div#user tr:eq(4) td:eq(1)").text(user_data[4]);
    $("div#user tr:eq(5) td:eq(1)").text(user_data[5]);

    // 営業部門の情報を取得
    var section_eigyou_txt = "<?php echo $section_eigyou_txt ?>";
    $("div#eigyou").css("display","block"); // 表示を出す
    if( section_eigyou_txt != ''){
        section_eigyou = section_eigyou_txt.split(",");
        //console.log( section_eigyou );
        $("div#eigyou tr:eq(0) td:eq(1)").text(section_eigyou[0]);
        $("div#eigyou tr:eq(1) td:eq(1)").text(section_eigyou[1]);
        $("div#eigyou tr:eq(2) td:eq(1)").text(section_eigyou[2]);
        $("div#eigyou tr:eq(3) td:eq(1)").text(section_eigyou[3]);
        $("div#eigyou tr:eq(4) td:eq(1)").text(section_eigyou[4]);
        $("div#eigyou tr:eq(5) td:eq(1)").text(section_eigyou[5]);
   }else{
       section_eigyou = Array('','','','','','');
       $("div#eigyou table").html("<p>＜未登録＞</p>")
   }

    // 経理部門の情報を取得
    var section_keiri_txt = "<?php echo $section_keiri_txt ?>";
    $("div#keiri").css("display","block"); // 表示を出す
    if( section_keiri_txt != ''){
        section_keiri = section_keiri_txt.split(",");
        //console.log( section_keiri );
        $("div#keiri tr:eq(0) td:eq(1)").text(section_keiri[0]);
        $("div#keiri tr:eq(1) td:eq(1)").text(section_keiri[1]);
        $("div#keiri tr:eq(2) td:eq(1)").text(section_keiri[2]);
        $("div#keiri tr:eq(3) td:eq(1)").text(section_keiri[3]);
        $("div#keiri tr:eq(4) td:eq(1)").text(section_keiri[4]);
        $("div#keiri tr:eq(5) td:eq(1)").text(section_keiri[5]);
   }else{
       section_keiri = Array('','','','','','');
       $("div#keiri table").html("<p>＜未登録＞</p>")
   }

$(document).ready(function(){
    // 選択されたらその情報をフォームに挿入する
    $("#selector").change( function(){
        var insert_data = new Array(6);
        if( $(this).val() == "御社情報"){
            //console.log( $(this).val() );
            //console.log( $('#BillZipcode').val());
            insert_data = user_data;
        }else if( $(this).val() == "営業部門"){
            insert_data = section_eigyou;
        }else if( $(this).val() == "経理部門"){
            insert_data = section_keiri;
        }else if( $(this).val() == "その他"){
            insert_data = Array('','','','','','');
        }
        $('#BillZipcode').val(insert_data[0]);
        $('#BillAddress').val(insert_data[1]);
        $('#BillDepartment').val(insert_data[2]);
        $('#BillName').val(insert_data[3]);
        $('#BillPhone').val(insert_data[4]);
        $('#BillFax').val(insert_data[5]);

        // 入力フォームにスクロール
       var dist = $("div#input").offset().top;
       $('html,body').animate({ scrollTop: dist }, 'fast');
    });
})
</script>
