<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>消防訓練説明会の参加申込</h2>
<p>
消防訓練事前説明会の参加者（消防訓練に参加される方）の情報をご入力下さい。
</p></div>
<pre>
・消防訓練には、各専門店様から2名以上の参加をお願いします。当日参加される方は、この説明会にも必ずご参加下さい。
・定員を超えた場合は、日時の調整をお願いすることがございます。予めご了承下さい。
・詳細は運営管理説明会資料45～49ページをご確認下さい。
</pre><br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>消防訓練説明会の参加申込</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['FireDrill']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['FireDrill']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
<tr>
  <td></td><td>消防訓練日程（予定）</td>
</tr><tr>
  <td>グランドモール・ペットモール</td><td>12月8日（日）9:00～11:00</td>
</tr><tr>
  <td>ファミリーモール・アクティブモール</td><td>12月8日（日）14:00～16:00</td>
</tr><tr>
  <td></td><td></td><td></td>
</tr>
</table>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('FireDrill',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>

    <fieldset>
        <div class="alert alert-info">
        <h3>参加の確認</h3>
        </div>
        <h4>参加の有無</h4>
        <?php echo $this->BootstrapForm->select('attend_mode',
                array('参加'=>'参加', '不参加'=>'不参加'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <p class="offset2">※必ず「参加」／「不参加」を選択してください。</p>
        <br />

        <h4>グランドモール・ペットモールの消防訓練説明会に参加される方は、<br />下記日程にチェックをお付けください。</h4>
        <?php echo $this->BootstrapForm->input('attend_day1', array(
            'label'     => '２０１３年１２月６日（金）<br />10時00分 ～ 11時00分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day2', array(
            'label'     => '２０１３年１２月６日（金）<br />13時00分 ～ 14時00分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day3', array(
            'label'     => '２０１３年１２月６日（金）<br />15時00分 ～ 16時00分',
            'type'      => 'checkbox'
        )); ?>
        <br />

        <h4>ファミリーモール・アクティブモールの消防訓練説明会に参加される方は、<br />下記日程にチェックをお付けください。</h4>
        <?php echo $this->BootstrapForm->input('attend_day4', array(
            'label'     => '２０１３年１２月７日（土）<br />10時00分 ～ 11時00分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day5', array(
            'label'     => '２０１３年１２月７日（土）<br />13時00分 ～ 14時00分',
            'type'      => 'checkbox'
        )); ?>
        <?php echo $this->BootstrapForm->input('attend_day6', array(
            'label'     => '２０１３年１２月７日（土）<br />15時00分 ～ 16時00分',
            'type'      => 'checkbox'
        )); ?>
        <br />

        <div class="alert alert-info">
          <h3>参加者</h3>
        </div>
        <h4>参加者①</h4>
        <?php echo $this->BootstrapForm->input('department1', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name1', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田太郎</p>
        <?php echo $this->BootstrapForm->input('position1', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('contact1', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>
        <hr />

        <h4>参加者②</h4>
        <?php echo $this->BootstrapForm->input('department2', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name2', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('position2', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）副店長</p>
        <?php echo $this->BootstrapForm->input('contact2', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>
        <hr />

        <h4>参加者③</h4>
        <?php echo $this->BootstrapForm->input('department3', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name3', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田良子</p>
        <?php echo $this->BootstrapForm->input('position3', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">　</p>
        <?php echo $this->BootstrapForm->input('contact3', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>

        <h4>参加者④</h4>
        <?php echo $this->BootstrapForm->input('department4', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name4', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田良子</p>
        <?php echo $this->BootstrapForm->input('position4', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">　</p>
        <?php echo $this->BootstrapForm->input('contact4', array('label'=>'連絡先','class' => 'span10')); ?>
        <p class="offset2">（例） 090-1234-5678</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
