<?php
$tab_labels = array(
                        '【営業部門】',
                        '【店舗開発部門】',
                        '【経理部門】',
                        '【広報部門】',
                        '【搬入部門】',
                        '【備考欄】'
                        );
$section_labels = array(
                        '【営業部門】担当範囲　・・・・　売り場・商品MD・販売促進など',
                        '【店舗開発部門】担当範囲　・・・・　契約内容・出店者協議会案内など',
                        '【経理部門】担当範囲　・・・・　売上控除・請求などについて',
                        '【広報部門】担当範囲　・・・・　ﾛｺﾞ使用、チラシ・HPなど媒体関係について（営業と異なる場合のみ）',
                        '【搬入部門】担当範囲　・・・・　商品搬入時のご担当',
                        '【備考欄】　・・・・　上記の担当範囲が大きく異なる場合などは詳細をご記入願います。'
                        );
$section_fields = array(
                       'eigyou',
                       'tenpo_kaihatu',
                       'keiri',
                       'kouhou',
                       'hannyuu');
$labels = array('氏名','部署','役職','〒郵便番号','住所','電話番号','FAX','携帯番号','E-Mail');
$address_caution ='※部門により住所が異なる場合のみ、全ての欄にご記入ください。';
$zipcode_caution = '※７桁の半角数字で入力';
$phone_caution = '※１４文字以内の半角数字で入力してください。';
$mail_caution = 'メールアドレスの形式（@を含む半角英数記号）で入力してください。';
$fields = array(
                'name',
                'department',
                'position',
                'zipcode',
                'address',
                'phone',
                'fax',
                'cellphone',
                'email',
                'remarks');
$table_labels = array('担当責任者','住所','連絡先');

// タブのアクティブ設定
$active = array('','','','','','');
if( isset( $this->request->params['pass'][1])){
    $tab = $this->request->params['pass'][1];
    $active[ $tab ] = ' active';
}else{
    $tab = 0;
    $active[ 0 ] = 'active';
}
?>
<div class="hero-unit">
  <h2><?php echo $title_for_layout . $tab_labels[ $tab ] ?></h2>
  <h4 class="offset1"><?php echo $section_labels[ $tab ] ?></h4>
  <dl><dt>ご担当者さまの連絡先について</dt><br />
  <dd><p>貴店における部門別の担当者様の連絡先についてお伺いさせて頂きます。</p>
      <p>この担当部門の分類が大きく異なる組織体の場合、備考欄にご記入頂けますよう、お願い致します。</p>
      <p>なお、本書式とは別に緊急連絡先届出書を頂戴いたします。</p>
  </dd></dl>
</div>
<?php echo $this->Session->flash(); ?>

<div class="section tabbale">

<?php if( false ){ // タブのインデックスは表示しない。?>
<h4>部門毎に５つの入力シートがございますので、部門タブを選択してご入力下さい。</h4>
<ul class="nav nav-tabs">
    <li class="<?php echo $active[0];?>"><a href="#tab0" data-toggle="tab"><?php echo $tab_labels[0];?></a></li>
	<li class="<?php echo $active[1];?>"><a href="#tab1" data-toggle="tab"><?php echo $tab_labels[1];?></a></li>
	<li class="<?php echo $active[2];?>"><a href="#tab2" data-toggle="tab"><?php echo $tab_labels[2];?></a></li>
	<li class="<?php echo $active[3];?>"><a href="#tab3" data-toggle="tab"><?php echo $tab_labels[3];?></a></li>
	<li class="<?php echo $active[4];?>"><a href="#tab4" data-toggle="tab"><?php echo $tab_labels[4];?></a></li>
	<li class="<?php echo $active[5];?>"><a href="#tab5" data-toggle="tab"><?php echo $tab_labels[5];?></a></li>
</ul>
<?php } ?>

<div class="tab-content">
<?php
    foreach( $section_fields as $sec_key => $sec_field ){
        echo '<div id="tab'. $sec_key .'" class="tab-pane '. $active[ $sec_key ] .'">';
            echo '<div class="row-fluid">';
                echo '<div class="span12">';
                    echo '<h4>'. $section_labels[$sec_key] .'</h4>';
                    $mark = '/'. $sec_key;
                    echo $this->BootstrapForm->create('Section',
                        array('class'=>'section form','url'=>'section/'. $user_id . $mark));

                    if( isset( $data[ $sec_key ]['Section'] )){
                        $sec_data = $data[ $sec_key ]['Section'];
                        $id = $sec_data['id'];
                        echo $this->BootstrapForm->input('id',
                                  array('type'=>'hidden','value'=>$id ));
                    }else{
                        $sec_data = null;
                    }
                    echo $this->BootstrapForm->input('user_id',
                              array('type'=>'hidden','value'=>$user_id));
                    echo $this->BootstrapForm->input('section_name',
                              array('type' => 'hidden','value'=>$sec_field));

                    $rows[] = array();
                    foreach( $labels as $key => $label ){
                        if( $sec_data ){
                            $value = $sec_data[ $fields[ $key ]];
                        }else{
                            $value = '';
                        }
                        if( $key < 3 ){
                            if( $key == 0 ){
                                $rows[0] = '<td width="80px" class="gray"><strong>'
                                         . $table_labels[0]
                                         . '</strong></td>';
                            }
                            $input = $this->BootstrapForm->input( $fields[ $key ],array(
                                                          'class' => 'span12',
                                                          'label' => $label,
                                                          'value' => $value));
                            $rows[0] .= '<td width="200px">'. $input .'</td>';
                        }else if( $key < 5 ){
                            if( $key == 3 ){
                                $rows[1] = '<td class="gray"><strong>'
                                         . $table_labels[1]
                                         . '</strong></td>';
                            }
                            $input = $this->BootstrapForm->input( $fields[ $key ],array(
                                                          'class' => 'span12',
                                                          'label' => $label,
                                                          'value' => $value));
                            if( $key == 3 )
                                $rows[1] .= '<td>'. $input . $zipcode_caution .'</td>';
                            if( $key == 4 )
                                $rows[1] .= '<td colspan="2">'. $input .'</td>';
                        }else if( $key < 7 ){
                            if( $key == 5 ){
                                $rows[2] = '<td rowspan="2" class="gray"><strong>' 
                                         . $table_labels[2]
                                         . '</strong></td>';
                            }
                            $input = $this->BootstrapForm->input( $fields[ $key ],array(
                                                          'class' => 'span12',
                                                          'label' => $label,
                                                          'value' => $value));
                            if( $key == 6 )
                                $rows[2] .= '<td>'. $input . $phone_caution .'</td><td></td>';
                            else
                                $rows[2] .= '<td>'. $input . $phone_caution .'</td>';

                        }else{
                            $input = $this->BootstrapForm->input( $fields[ $key ],array(
                                                          'class' => 'span12',
                                                          'label' => $label,
                                                          'value' => $value));
                            if( $key == 7 ):
                                $rows[3] = '<td>'. $input . $phone_caution .'</td>';
                            else:
                                $rows[3] .= '<td colspan="2">'. $input . $mail_caution .'</td>';
                            endif;
                        }
                    }
                    echo '<table class="communication table table-bordered" style="">';
                    echo '<tr>'. $rows[0];
                    echo '</tr><tr>'. $rows[1];
                    echo '</tr><tr>'. $rows[2];
                    echo '</tr><tr>'. $rows[3];
                    echo '</tr>';
                    echo '</table>';
                    echo '<p class="pull-right">' .$address_caution .'</p>';
                    echo '<div class="clear"></div>'; // フロートをクリア
                    echo $this->BootstrapForm->submit(__('登録'),array('class'=>'btn btn-primary pull-right'));
                    echo $this->BootstrapForm->end();
                    echo '<div class="clear"></div>'; // フロートをクリア
                    echo '<hr />';
                    echo '<br />';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    }
?>

<?php
    echo '<div id="tab5" class="tab-pane '. $active[ 5 ] .'">';
        echo '<div class="row-fluid">';
            echo '<div class="span12">';
            // 備考欄
                echo '<h4>'. $section_labels[5] .'</h4>';
                $mark = '/5';
                echo $this->BootstrapForm->create('Section',
                        array('class'=>'section form','url'=>'section/'. $user_id . $mark));
                $key = count( $section_labels ); // 最後のセクションが備考
                if( isset( $data[ $key ]['Section'])){
                    $remarks = $data[ $key ]['Section'];
                    $id = $remarks['id'];
                    echo $this->BootstrapForm->input('id',
                              array('type'=>'hidden','value'=>$id ));
                    $value = $remarks['remarks'];
                }else{
                    $value = '';
                }
                echo $this->BootstrapForm->input('user_id',
                            array('type'=>'hidden','value'=>$user_id));
                echo $this->BootstrapForm->input('section_name',
                            array('type'=>'hidden','value'=>'remarks'));
                echo $this->BootstrapForm->input('email',
                            array('type'=>'hidden','value'=>'xx@xx.xx'));

                echo $this->BootstrapForm->input('remarks', array(
                                                          'rows'  => '5',
                                                          'class' => 'span12',
                                                          'label' => '',
                                                          'value' => $value));
                echo $this->BootstrapForm->submit(__('登録'),array('class'=>'btn btn-primary pull-right'));
                echo $this->BootstrapForm->end();

            echo '</div>';
        echo '</div>';
    echo '</div>';
echo '</div>'; // .tab-content
echo '</div>'; // .tabbale
