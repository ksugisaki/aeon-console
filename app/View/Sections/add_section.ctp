<?php
$tab_labels = array(
				'【営業部門】',
				'【店舗開発部門】',
				'【経理部門】',
				'【広報部門】',
				'【搬入部門】',
				'【備考欄】'
				);
$section_labels = array(
				'【営業部門】担当範囲　・・・・　売り場・商品MD・販売促進など',
				'【店舗開発部門】担当範囲　・・・・　契約内容・出店者協議会案内など',
				'【経理部門】担当範囲　・・・・　売上控除・請求などについて',
				'【広報部門】担当範囲　・・・・　ﾛｺﾞ使用、チラシ・HPなど媒体関係について（営業と異なる場合のみ）',
				'【搬入部門】担当範囲　・・・・　商品搬入時のご担当',
				'【備考欄】　・・・・　各部門の担当範囲が大きく異なる場合などは詳細をご記入願います。'
				);
$labels = array('氏名 （必須）','フリガナ （必須）','部署','役職','〒郵便番号','住所','電話番号','内線番号','FAX','携帯番号','E-Mail');
$comments = array(
				'※ （例）山田太郎',
				'※ （例）ヤマダタロウ',
				'※ （例）営業部',
				'※ （例）営業マネージャー',
				'※ （例）2618539 ・・・７文字の半角数字で入力してください。',
				'※ （例）千葉県千葉市美浜区中瀬一丁目5番地1',
				'※ （例）0312345678 ・・・１４文字以内の数字で入力してください。',
				'※ （例）0123 ・・・１４文字以内の数字で入力してください。',
				'※ （例）0312345679 ・・・１４文字以内の数字で入力してください。',
				'※ （例）09012345678 ・・・１４文字以内の数字で入力してください。',
				'※ （例）yamada@xxxx.com ・・・メールアドレスを入力してください。'
				);
$address_caution ='※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。';
$zipcode_caution = '※７桁の半角数字で入力してください。';
$phone_caution = '※１４文字以内の半角数字で入力してください。';
$mail_caution = 'メールアドレスの形式（@を含む半角英数記号）で入力してください。';
$section_fields = array(
				'eigyou',
				'tenpo_kaihatu',
				'keiri',
				'kouhou',
				'hannyuu');
$fields = array(
				'name',
				'name_kana',
				'department',
				'position',
				'zipcode',
				'address',
				'phone',
				'extension',
				'fax',
				'cellphone',
				'email',
				'remarks');
$table_labels = array('担当責任者','住所','連絡先');

// タブのアクティブ設定
$active = array('','','','','','');
if( isset( $tab )){
    $active[ $tab ] = ' active';
}else if( isset( $this->request->params['pass'][1])){
    $tab = $this->request->params['pass'][1];
    $active[ $tab ] = ' active';
}else{
    $tab = 0;
    $active[ 0 ] = 'active';
}

?>
<div class="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
  <dl><dt>ご担当者さまの連絡先について</dt><br />
  <dd><p>貴店における部門別の担当者様の連絡先についてお伺いさせて頂きます。</p>
      <p>この担当部門の分類が大きく異なる組織体の場合、備考欄にご記入頂けますよう、お願い致します。</p>
      <p>なお、本書式とは別に緊急連絡先届出書を頂戴いたします。</p>
  </dd></dl>
</div>
<?php echo $this->Session->flash(); ?>

<?php if( $tab_index ){ // タブのインデックス表示 ?>
<div>
    <h3>以下のリンクから各、担当者確認シートにお進み下さい。</h3>
    <hr />
    <ul>
      <li><?php echo $this->Html->link('　【営業部門】',
            array('controller'=>'sections','action'=>'addSection',$user_id,0)); ?></li>
      <li><?php echo $this->Html->link('　【店舗開発部門】',
            array('controller'=>'sections','action'=>'addSection',$user_id,1)); ?></li>
      <li><?php echo $this->Html->link('　【経理部門】',
            array('controller'=>'sections','action'=>'addSection',$user_id,2)); ?></li>
      <li><?php echo $this->Html->link('　【広報部門】',
            array('controller'=>'sections','action'=>'addSection',$user_id,3)); ?></li>
      <li><?php echo $this->Html->link('　【搬入部門】',
            array('controller'=>'sections','action'=>'addSection',$user_id,4)); ?></li>
      <li><?php echo $this->Html->link('　【備考】',
            array('controller'=>'sections','action'=>'addSection',$user_id,5)); ?></li>
    </ul>
</div>

<?php }else{ // not タブのインデックス表示 ?>

<div class="section tabbale">

<?php if( false ){ // タブ表示（使わない） ?>
<h4>部門毎に５つの入力シートがございますので、部門タブを選択してご入力下さい。</h4>
<ul class="nav nav-tabs">
	<li class="<?php echo $active[0];?>"><a href="#tab0" data-toggle="tab"><?php echo $tab_labels[0];?></a></li>
	<li class="<?php echo $active[1];?>"><a href="#tab1" data-toggle="tab"><?php echo $tab_labels[1];?></a></li>
	<li class="<?php echo $active[2];?>"><a href="#tab2" data-toggle="tab"><?php echo $tab_labels[2];?></a></li>
	<li class="<?php echo $active[3];?>"><a href="#tab3" data-toggle="tab"><?php echo $tab_labels[3];?></a></li>
	<li class="<?php echo $active[4];?>"><a href="#tab4" data-toggle="tab"><?php echo $tab_labels[4];?></a></li>
	<li class="<?php echo $active[5];?>"><a href="#tab5" data-toggle="tab"><?php echo $tab_labels[5];?></a></li>
</ul>
<?php } ?>

<div class="tab-content">

<?php
	foreach( $section_fields as $sec_key => $sec_field ){
		echo '<div id="tab'. $sec_key .'" class="tab-pane '. $active[ $sec_key ] .'">';
			echo '<div class="row-fluid">';
				echo '<div class="span12">';
					echo '<h4>'. $section_labels[$sec_key] .'</h4>';
					$mark = '/'. $sec_key;
					echo $this->BootstrapForm->create('Section',
						array('class'=>'section form','url'=>'addSection/'. $user_id . $mark));

					if( isset( $data[ $sec_key ]['Section'] )){
						$sec_data = $data[ $sec_key ]['Section'];
						if( isset( $sec_data['id'])){
							$id = $sec_data['id'];
							echo $this->BootstrapForm->input('id',
									  array('type'=>'hidden','value'=>$id ));
						}
					}else{
						$sec_data = null;
					}

					echo $this->BootstrapForm->input('user_id',
							  array('type'=>'hidden','value'=>$user_id));
					echo $this->BootstrapForm->input('section_name',
							  array('type' => 'hidden','value'=>$sec_field));
					echo '<table class="communication table table-bordered" style="">';
					foreach( $labels as $key => $label ){
						if( $sec_data ){
							$value = $sec_data[ $fields[ $key ]];
						}else{
							$value = '';
						}
						if( $fields[ $key ] == 'zipcode'):
							echo '</table>';
							echo '<h5>' . $address_caution . '</h5>';
							echo $this->Html->link('御社情報登録はこちら',
								 '/users/edit',array('class'=>'pull-right'));
							echo '<br />';
							echo '<br />';
							echo '<table class="communication table table-bordered" style="">';
						endif;
						echo $this->Html->tableCells(array(
							$label,
							$this->BootstrapForm->input( $fields[ $key ],array(
								'class' => 'span12',
								'label' => '',
								'value' => $value)) . $comments[ $key ]
						));
					}
					echo '</table>';

					echo '<div class="clear"></div>'; // フロートをクリア
					if( empty( $locked )){
						echo $this->BootstrapForm->submit(__('登録'),array('class'=>'btn btn-primary'));
					}else{
						echo MESSAGE_AT_LOCKED;
					}
					echo $this->BootstrapForm->end();
					echo '<div class="clear"></div>'; // フロートをクリア
					echo '<br />';
				echo '</div>';
			echo '</div>';
		echo '</div>';
	}
?>

<?php
	echo '<div id="tab5" class="tab-pane '. $active[ 5 ] .'">';
		echo '<div class="row-fluid">';
			echo '<div class="span12">';
			// 備考欄
				echo '<h4>'. $section_labels[5] .'</h4>';
				$mark = '/5';
				$key = 5;
				echo $this->BootstrapForm->create('Section',
						array('class'=>'section form','url'=>'addSection/'. $user_id . $mark));

				if( isset( $data[ $key ]['Section'])){
					$remarks = $data[ $key ]['Section'];
					$id = $remarks['id'];
					echo $this->BootstrapForm->input('id',
							  array('type'=>'hidden','value'=>$id ));
					$value = $remarks['remarks'];
				}else{
					$value = '';
				}

				echo $this->BootstrapForm->input('user_id',
							array('type'=>'hidden','value'=>$user_id));
				echo $this->BootstrapForm->input('section_name',
							array('type'=>'hidden','value'=>'remarks'));
				echo $this->BootstrapForm->input('remarks', array(
														  'rows'  => '5',
														  'class' => 'span12',
														  'label' => '',
														  'value' => $value));
				if( empty( $locked )){
					echo $this->BootstrapForm->submit(__('登録'),array('class'=>'btn btn-primary'));
				}else{
					echo MESSAGE_AT_LOCKED;
				}
				echo $this->BootstrapForm->end();

			echo '</div>';
		echo '</div>';
	echo '</div>';
?>

</div><!-- .tab-content -->
</div><!-- .tabbale -->

<?php } // end タブのインデックス表示 ?>
