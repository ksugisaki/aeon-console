<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __('List %s', __('Sections'));?></h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('user_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('section_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('department');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('position');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('zipcode');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('address');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('phone');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('fax');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('cellphone');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('email');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('remarks');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($sections as $section): ?>
			<tr>
				<td><?php echo h($section['Section']['id']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($section['User']['name'], array('controller' => 'users', 'action' => 'view', $section['User']['id'])); ?>
				</td>
				<td><?php echo h($section['Section']['section_name']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['name']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['department']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['position']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['zipcode']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['address']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['phone']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['fax']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['cellphone']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['email']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['remarks']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $section['Section']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $section['Section']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $section['Section']['id']), null, __('Are you sure you want to delete # %s?', $section['Section']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Section')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>