<div class="hero-unit">
	 <h2><?php echo $title_for_layout;?> <?php echo $section_label;?></h2>
</div>
<div class="row-fluid">
	<div class="span12">
		<p class="pull-right">※ グレーの枠は、担当者情報が未入力であり、御社情報の内容を表示しています。</p>
		<table class="table table-bordered">
			<tr>
				<th><?php echo 'テナントコード';?></th>
				<th><?php echo $this->BootstrapPaginator->sort('receipt_tenant_name','テナント名');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('company','会社名');?></th>
				<th><?php echo '店名';?></th>
				<th><?php echo '部署';?></th>
				<th><?php echo '役職';?></th>
				<th><?php echo '担当者名';?></th>
				<th><?php echo '住所';?></th>
				<th width="160"><?php echo '電話番号／FAX／携帯';?></th>
				<th><?php echo 'メールアドレス';?></th>
				<!--<th class="actions"><?php echo __('操作');?></th>-->
			</tr>
		<?php foreach ($sections as $section): ?>
		  <?php if( isset($section['Section']) && count( $section['Section'])): ?>
			<tr>
				<td>&nbsp;</td>
				<td><?php echo h($section['User']['receipt_tenant_name']); ?>&nbsp;</td>
				<td><?php echo h($section['User']['company']); ?>&nbsp;</td>
				<td><?php echo h($section['Signboard']['shop_sign']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['department']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['position']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['name']); ?>&nbsp;</td>
				<td>〒<?php echo h($section['Section']['zipcode']); ?>
				<br /><?php echo h($section['Section']['address']); ?>&nbsp;</td>
				<td>電話 : <?php echo h($section['Section']['phone']); ?>
				<br />FAX : <?php echo h($section['Section']['fax']); ?>
				<br />携帯 : <?php echo h($section['Section']['cellphone']); ?>&nbsp;</td>
				<td><?php echo h($section['Section']['email']); ?>&nbsp;</td>
			<!--<td class="actions">
					<?php echo $this->Html->link(__('編集'),
						  array('action' => 'edit', $section['Section']['id'])); ?>
					<br />
					<?php echo $this->Form->postLink(__('削除'),
						  array('action' => 'delete', $section['Section']['id']), null,
								__('Are you sure you want to delete # %s?', $section['Section']['id'])); ?>
				</td>
			-->
			</tr>
		  <?php else:?>
			<tr>
				<td>&nbsp;</td>
				<td><?php echo h($section['User']['receipt_tenant_name']); ?>&nbsp;</td>
				<td><?php echo h($section['User']['company']); ?>&nbsp;</td>
				<td><?php echo h($section['Signboard']['shop_sign']); ?>&nbsp;</td>
				<td class="gray"><?php echo h($section['User']['department']); ?>&nbsp;</td>
				<td class="gray"></td>
				<td class="gray"><?php echo h($section['User']['name']); ?>&nbsp;</td>
				<td class="gray">〒<?php echo h($section['User']['zipcode']); ?>
				  <br /><?php echo h($section['User']['address']); ?>&nbsp;</td>
                <td class="gray">電話 : <?php echo h($section['User']['phone']); ?>
                  <br />FAX : <?php echo h($section['User']['fax']); ?>
                  <br />内線 : <?php echo h($section['User']['extension']); ?>&nbsp;</td>
                <td class="gray"><?php echo h($section['User']['email']); ?>&nbsp;</td>
			</tr>
		  <?php endif;?>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
</div>

<div class="user-control">
  <?php
      echo $this->Html->link('担当者情報のエクスポート',
           '/sections/download_csv/' . $section_name,
           array('class'=>'btn btn-primary')
      );
  ?>
</div>
