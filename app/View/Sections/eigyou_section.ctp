<div class="hero-unit">
  <h2><?php echo $title_for_layout . $tab_labels[ $tab ] ?></h2>
  <h4 class="offset1"><?php echo $section_labels[ $tab ] ?></h4>
  <dl><dt>ご担当者さまの連絡先について</dt><br />
  <dd><p>貴店における部門別の担当者様の連絡先についてお伺いさせて頂きます。</p>
      <p>この担当部門の分類が大きく異なる組織体の場合、備考欄にご記入頂けますよう、お願い致します。</p>
      <p>なお、本書式とは別に緊急連絡先届出書を頂戴いたします。</p>
  </dd></dl>
</div>

<div id="eigyou_section">
    <div class="span12">
        <h4>【営業部門】担当範囲　・・・・　売り場・商品MD・販売促進など</h4>
        <?php echo $this->BootstrapForm->create('Section',
                   array('class'=>'section form','url'=>'section/'. $user_id . $mark));?>
        <?php echo $this->BootstrapForm->input('user_id',
                              array('type'=>'hidden','value'=>$user_id));?>
        <?php echo $this->BootstrapForm->input('section_name',
                              array('type' => 'hidden','value'=>$sec_field));?>

		<?php echo $this->BootstrapForm->input('name');?>
		<?php echo $this->BootstrapForm->input('department');?>
		<?php echo $this->BootstrapForm->input('position');?>
		<?php echo $this->BootstrapForm->input('zipcode');?>
		<?php echo $this->BootstrapForm->input('address');?>
		<?php echo $this->BootstrapForm->input('phone');?>
		<?php echo $this->BootstrapForm->input('fax');?>
		<?php echo $this->BootstrapForm->input('cellphone');?>
		<?php echo $this->BootstrapForm->input('email');?>
		<?php echo $this->BootstrapForm->input('remarks');?>

		<?php echo $this->BootstrapForm->submit(__('Submit'));?>
