<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Section');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($section['Section']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('User'); ?></dt>
			<dd>
				<?php echo $this->Html->link($section['User']['name'], array('controller' => 'users', 'action' => 'view', $section['User']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Section Name'); ?></dt>
			<dd>
				<?php echo h($section['Section']['section_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($section['Section']['name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Department'); ?></dt>
			<dd>
				<?php echo h($section['Section']['department']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Position'); ?></dt>
			<dd>
				<?php echo h($section['Section']['position']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Zipcode'); ?></dt>
			<dd>
				<?php echo h($section['Section']['zipcode']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Address'); ?></dt>
			<dd>
				<?php echo h($section['Section']['address']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Phone'); ?></dt>
			<dd>
				<?php echo h($section['Section']['phone']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Fax'); ?></dt>
			<dd>
				<?php echo h($section['Section']['fax']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Cellphone'); ?></dt>
			<dd>
				<?php echo h($section['Section']['cellphone']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Email'); ?></dt>
			<dd>
				<?php echo h($section['Section']['email']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Remarks'); ?></dt>
			<dd>
				<?php echo h($section['Section']['remarks']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Section')), array('action' => 'edit', $section['Section']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Section')), array('action' => 'delete', $section['Section']['id']), null, __('Are you sure you want to delete # %s?', $section['Section']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Sections')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Section')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

