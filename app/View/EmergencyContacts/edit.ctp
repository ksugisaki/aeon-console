<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>緊急連絡先及び火元責任者届出書</h2>
        <p>本書は緊急連絡時に使用するものです。</p>
        <p>又、火元責任者は自衛消防隊組織図にも掲出します。</p>
    </div>
    <br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>緊急連絡先及び火元責任者届出書</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['EmergencyContact']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['EmergencyContact']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('EmergencyContact',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
          <h3>緊急連絡先</h3>
        </div>
        <h4>店責任者自宅</h4>
        <?php echo $this->BootstrapForm->input('manager_name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('manager_phone', array('label'=>'電話番号','class' => 'span10')); ?>
        <p class="offset2">（例）03-1234-5678</p>
        <?php echo $this->BootstrapForm->input('manager_cellphone', array('label'=>'携帯番号','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>
        <?php echo $this->BootstrapForm->input('manager_zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）2618539 ・・・７文字の半角数字で入力してください。</p>
        <?php echo $this->BootstrapForm->input('manager_address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <hr />

        <h4>店副責任者自宅</h4>
        <?php echo $this->BootstrapForm->input('vicemanager_name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('vicemanager_phone', array('label'=>'電話番号','class' => 'span10')); ?>
        <p class="offset2">（例）03-1234-5678</p>
        <?php echo $this->BootstrapForm->input('vicemanager_cellphone', array('label'=>'携帯番号','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>
        <?php echo $this->BootstrapForm->input('vicemanager_zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）2618539 ・・・７文字の半角数字で入力してください。</p>
        <?php echo $this->BootstrapForm->input('vicemanager_address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <hr />

        <h4>本社（店）</h4>
        <?php echo $this->BootstrapForm->input('company_name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('company_phone', array('label'=>'電話番号','class' => 'span10')); ?>
        <p class="offset2">（例）03-1234-5678</p>
        <?php echo $this->BootstrapForm->input('company_fax', array('label'=>'FAX','class' => 'span10')); ?>
        <p class="offset2">（例）03-1234-5679</p>
        <?php echo $this->BootstrapForm->input('company_cellphone', array('label'=>'携帯番号','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>
        <?php echo $this->BootstrapForm->input('company_zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）2618539 ・・・７文字の半角数字で入力してください。</p>
        <?php echo $this->BootstrapForm->input('company_address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <hr />

        <pre>その他、連絡先があれば下記にご記入願います。</pre>
        <h4>その他連絡先</h4>
        <?php echo $this->BootstrapForm->input('other_name', array('label'=>'名称','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('other_department', array('label'=>'担当部署','class' => 'span10')); ?>
        <p class="offset2">（例）総務部</p>
        <?php echo $this->BootstrapForm->input('other_phone', array('label'=>'電話番号','class' => 'span10')); ?>
        <p class="offset2">（例）03-1234-5678</p>
        <?php echo $this->BootstrapForm->input('other_fax', array('label'=>'FAX','class' => 'span10')); ?>
        <p class="offset2">（例）03-1234-5679</p>
        <?php echo $this->BootstrapForm->input('other_cellphone', array('label'=>'携帯番号','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>
        <?php echo $this->BootstrapForm->input('other_zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）2618539 ・・・７文字の半角数字で入力してください。</p>
        <?php echo $this->BootstrapForm->input('other_address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <hr />


        <div class="alert alert-info">
          <h3>火元責任者</h3>
        </div>
        <?php echo $this->BootstrapForm->input('firewarden1', array('label'=>'氏名１','class' => 'span10')); ?>
        <br />
        <?php echo $this->BootstrapForm->input('firewarden2', array('label'=>'氏名２','class' => 'span10')); ?>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
