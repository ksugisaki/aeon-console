<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>メディアステーション・イオンモールメンバーズ説明会</h2>
        <p>メディアステーション・イオンモールメンバーズ説明会、参加者の情報をご入力ください。</p>
        <?php if( defined('MEDIA_STATION_LINK')) :?>
            <li><a target="_blank" href="<?php echo $this->Html->url( MEDIA_STATION_LINK ) ?>">
            <i class="icon-file"></i>
            メディアステーション・イオンモールメンバーズ 説明会開催のご案内(PDF)</a>
            </li>
        <?php endif; ?>
    </div>
    <br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>メディアステーション・イオンモールメンバーズ説明会</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['MediaStation']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['MediaStation']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('MediaStation',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <table class="admin_list table table-condensed">
        <tr>
          <td colspan="2"><strong>説明会概要</strong></td>
        </tr><tr>
          <td>日時</td>
          <td><div style="font-size : larger">11月9日(土)</div><br />
            <div style="font-size : larger">
            <?php $time[1] ='①11：00～12：00'; echo $time[1]; ?><br />
            <?php $time[2] ='②13：30～14：30'; echo $time[2]; ?><br />
            <?php $time[3] ='③16：00～17：00'; echo $time[3]; ?><br />
            </div>
          </td>
        </tr><tr>
          <td>場所</td>
          <td><div style="font-size : larger">イオンモール東員 2階 会議室</div></td>
        </tr>
          <td></td><td></td>
    </table>
    <fieldset>
        <div class="alert alert-info">
          <h3>説明会参加者情報</h3>
        </div>
        <?php for( $tnum = 1; $tnum <= 3; $tnum++ ): ?>
            <h4>時間帯 <?php echo $time[ $tnum ]; ?></h4>
            <table class="admin_list table table-bordered table-condensed">
            <?php for( $pnum = 1; $pnum <= 5; $pnum++ ): ?>
                <tr>
                  <td rowspan="3" width="100px"><?php echo $pnum ?></td>
                  <td><p class="span2">氏名</p>
                    <?php echo $this->BootstrapForm->input('time'. $tnum .'_name'.$pnum,
                               array('label'=>false,'div'=>false,'class'=>'span10')); ?>
                  </td>
                </tr><tr>
                  <td><p class="span2">フリガナ</p>
                    <?php echo $this->BootstrapForm->input('time'. $tnum .'_name_kana'.$pnum,
                               array('label'=>false,'div'=>false,'class'=>'span10')); ?>
                  </td>
                </tr><tr>
                  <td><p class="span2">役職</p>
                    <?php echo $this->BootstrapForm->input('time'. $tnum .'_position'.$pnum,
                               array('label'=>false,'div'=>false,'class'=>'span10')); ?>
                  </td>
                </tr>
            <?php endfor; ?>
            </table>
            <?php if( ! $lock ): ?>
                <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
            <?php else: ?>
                <?php echo MESSAGE_AT_LOCKED; ?>
            <?php endif; ?>
            <br />
        <?php endfor; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
