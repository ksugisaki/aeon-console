
<div class="hero-unit">
  <h2>ヒアリングシート２</h2>
</div>

<hr />
<div class="row-fluid">
	<div class="span12">
<h5>各種制作物 (PR用・店舗設置用) ヒアリング項目案</h5>
回答内容は、事前ＰＲ活動や取材に来場する報道関係者（記者・ＴＶディレクター・アナウンサー）に対し配布する資料「ファクトブック」や各種製作物に反映、活用させて頂きます。<br><br />
より多くの施設取材・個別店舗取材を誘致するため、できる限り多くの情報提供のご協力をお願いします。<br><br />
回答内容がそのまま記事化される事もありますので、その点ご留意ください。また、ご回答いただいた内容が全て製作物に反映されない場合もございますことも併せてご留意願います。<br><br />
<?php /*「制作」の項目はフロアガイド、ブランドブック、体験ガイドにて、「ＰＲ」の項目はファクトブックにて使用を想定しております。 */ ?>
	</div>
</div>
<hr />

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('OptionHearing', array('class' => 'form-horizontal', 'type' => 'file'));?>
			<?php echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id)); ?>
			<fieldset>
				<?php // echo $this->BootstrapForm->input('gyotai', array('class' => 'span10')); ?>
				<h5>当モール出店にあたり、ニュース性の高いPRポイント（1行で簡潔にお書き下さい）</h5>
				<p> 例）イタリアで大人気のアクセサリーを日本初取り扱い</p>
				<hr />
				<?php echo $this->BootstrapForm->input('haikei', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>代表商品の商品名、スペック、価格等</h5><p>御社を代表する商品・サービスについてご回答ください。 </p>
				<hr />
				<?php 
				echo $this->BootstrapForm->input('daihyo_shohin_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_spec', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_price', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_etc', array('class' => 'span10'));
				?>
				<hr />
				*/ ?>

				<hr />
				<h5>オススメ商品・サービス</h5><p>オススメ商品・サービス等についてご回答ください</p>
				<hr />
				<?php 
				echo $this->BootstrapForm->input('osusume_service_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('osusume_service_price', array('class' => 'span10'));
				// echo $this->BootstrapForm->input('osusume_renewal_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('osusume_comment', array('class' => 'span10'));
				?>
				<hr />
				<h5>”コト消費”とリンクするポイント</h5>
				<p>回答は必須ではありません</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment1', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>これまでの店舗との違い</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment2', array('class' => 'span10')); ?>
				<hr />
				<h5>競合他社との違い・アピールポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment3', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>（業界・イオングループで）初の取り組みポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment4', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>直接的な商品・サービス以外に体験できるコト、体験して頂きたいお勧めポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment5', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>ＴＶ取材の場合、レポーターが体験できるポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment6', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>今後の予定（イベントスケジュールや新商品・サービス展開予定等）</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment7', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>媒体掲載用の画像を3点まで、ご登録下さい。</h5>
				<p>
				■媒体掲載用画像に関して<br /><br />

				１. データは、<span style="color:red">「eps」 もしくは 「jpg」 形式（350dpi以上、カラー設定CMYK）</span>でご提出ください。<br />
				様々な媒体で使用しますので、<span style="color:red">鮮明で解像度の高いデータ</span>をご提出ください。  <br /><br />

				※ 店舗イメージ写真画像は、他のショッピングセンターの店舗外観やパースなどはＮＧとなります。<br />
				ご出店社様のイメージ写真としてください。また、他店舗様やお客様が映り込んでいない写真にしてください。<br />
				キャラクターやタレントなどが含まれる場合は、あらかじめ肖像権・版権をクリアにした上でご提出ください。<br />
				<br />
				※ 代表商品画像については、開業時の季節（11月下旬）をご考慮の上、ご提出ください。  <br />
				※ 紙媒体（カタログや、雑誌の切り抜きなど）でのご提出はお受け取りできません。<br />
				</p>

				<hr />
				<?php echo $this->BootstrapForm->input('image1', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('image2', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('image3', array('type' => 'file')); ?>
				<hr />
				<h5>店舗ロゴをご登録ください</h5>
				<p>
				■ロゴに関して<br />
				<br />
				１. データは、<span style="color:red">「ai」（バージョンCS以降） もしくは 「eps」（350dpi以上）、カラー設定はCMYK形式</span>でお願いします。 <br />
				２. <span style="color:red">カラーとモノクロ</span>のデータをお持ちの場合は、双方のデータをご提出ください。<br />
				３.<span style="color:red">縦組み、横組みなど複数バリエーション</span>をお持ちの場合も、使用可能なものは全てご提出ください。<br />
				<br />
				 ※<span style="color:red"> ロゴの無いご出店社様</span>は写植文字で作成いたしますので、“ロゴ無し”にチェックし、下の欄に<span style="color:red">指定書体</span>をご記入ください。<br />
				</p>
				<hr />
				<?php echo $this->BootstrapForm->input('logo', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('logo2', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('no_logo', array('type' => 'checkbox')); ?>
				<?php echo $this->BootstrapForm->input('logo_shotai'); ?>
				<hr />
				<h5>飲食業態に関するヒアリング</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('food_comment2', array('class' => 'span10', 'after' => '<span class="help-block">記入例 : 全体○○席　（禁煙○○席、喫煙○○席）</span>
				')); ?>

				<?php echo $this->BootstrapForm->input('food_comment3', array('type' => 'radio', 'options' => array(
					'喫煙' => '喫煙',
					'全面禁煙' => '全面禁煙',
					'空間分煙' => '空間分煙',
					'完全分煙' => '完全分煙',
					'時間分煙' => '時間分煙',

))); ?>
				<?php // echo $this->BootstrapForm->input('food_comment4', array('class' => 'span10')); ?>
				<?php echo $this->BootstrapForm->input('food_comment5', array('type' => 'radio', 'options' => array('可' => '可', '不可' => '不可'))); ?><!-- 予約受付について -->


				<div class="control-group">
					<label for="" class="control-label">予約方法</label>
					<div class="controls">
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_tel', array('div' => false, 'label' => '電話', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_fax', array('div' => false, 'label' => 'FAX', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_hp', array('div' => false, 'label' => 'HP', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_etc', array('div' => false, 'label' => 'その他', 'type' => 'checkbox')); ?>
					</div>
				</div>

				<?php echo $this->BootstrapForm->input('food_comment1', array('class' => 'span10', 'after' => '
                <!-- ラストオーダー時間 -->
				<span class="help-block">記入例: （閉店の○○分前迄、ランチ：○○時○○分、ディナー：○○時○○分）</span>
				')); ?>


				<?php echo $this->BootstrapForm->input('food_takeout', array('type' => 'radio', 'options' => array('可能' => '可能', '不可能' => '不可能'))); ?>

				<?php echo $this->BootstrapForm->input('food_alcohol_ok', array('label'=>'アルコール扱い','type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<?php echo $this->BootstrapForm->input('food_alcohol', array('label'=>'備考<br />(アルコール扱い)','class' => 'span10')); ?>

				<?php echo $this->BootstrapForm->input('food_childseat', array('type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<?php echo $this->BootstrapForm->input('food_kidsmenu', array('type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<hr />
				<h5>オフィシャルサイト以外の外部サイトURL</h5>
				<p>（例） ぐるなびURL、食べログURL等</p>
				<hr />
				<?php echo $this->BootstrapForm->input('unofficial_url', array('class' => 'span10')); ?>
				<hr />
				<?php if( $_SESSION['Auth']['User']["lock_hearing1"] == 0 ): ?>
				<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>'btn btn-primary'));?>
				<?php else: ?>
				<?php echo MESSAGE_AT_LOCKED; ?>
				<?php endif; ?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>

<?php if( SUB_DOMAIN == 'toin' || SUB_DOMAIN == 'testsite'):?>
	<p class="clear"></p>
	<pre class="span9">
	<h4>事務局</h4>
	〒460-8445　名古屋市中区栄4-16-36（株）電通中部支社　6階
	イオンモール東員ご出店社様対応事務局　担当：佐竹/小﨑 宛
	TEL：052-263-8482　FAX：052-263-8444
	Email： aeontouin@dentsutec.co.jp
	</pre>
	<p class="clear"></p><br />
<?php endif;?>

