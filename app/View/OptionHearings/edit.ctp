<script type="text/javascript">
  // ignore_enter_to_submit.jsに渡す変数
  var print_mode = "<?php echo empty( $print )? '' : $print; ?>";
</script>
<?php echo $this->Html->script(array('ignore_enter_to_submit'), array('inline'=>false)); ?>

<?php if( $print == null ){ ?>
<div class="hero-unit">
	<h2>ヒアリングシート２</h2>
</div>
<?php }else{ ?>
<?php echo $this->Html->script(array('set_tiny_mce'), array('inline'=>false)); ?>
<div class="row-fluid">
	<div class="span6">
		<h2>ヒアリングシート２</h2>
	</div>
	<div class="span6">
		<?php if( isset( $this->request->data['OptionHearing']['modified'])){ ?>
			<p>最終更新日： <?php echo( $this->request->data['OptionHearing']['modified']);?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<hr />
<div class="row-fluid">
	<div class="span12">
<h5>各種制作物 (PR用・店舗設置用) ヒアリング項目案</h5>
回答内容は、事前ＰＲ活動や取材に来場する報道関係者（記者・ＴＶディレクター・アナウンサー）に対し配布する資料「ファクトブック」や各種製作物に反映、活用させて頂きます。<br><br />
より多くの施設取材・個別店舗取材を誘致するため、できる限り多くの情報提供のご協力をお願いします。<br><br />
回答内容がそのまま記事化される事もありますので、その点ご留意ください。また、ご回答いただいた内容が全て製作物に反映されない場合もございますことも併せてご留意願います。<br><br />
<?php /*「制作」の項目はフロアガイド、ブランドブック、体験ガイドにて、「ＰＲ」の項目はファクトブックにて使用を想定しております。 */ ?>
	</div>
</div>
<hr />

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('OptionHearing', array('class' => 'form-horizontal', 'type' => 'file'));?>
			<?php echo $this->BootstrapForm->input('id', array('type' => 'hidden')); ?>
			<?php echo $this->BootstrapForm->input('user_id', array('type' => 'hidden')); ?>
			<fieldset>
				<?php // echo $this->BootstrapForm->input('gyotai', array('class' => 'span10')); ?>
				<h5>当モール出店にあたり、ニュース性の高いPRポイント（1行で簡潔にお書き下さい）</h5>
				<p> 例）イタリアで大人気のアクセサリーを日本初取り扱い</p>
				<hr />
				<?php echo $this->BootstrapForm->input('haikei', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>代表商品の商品名、スペック、価格等</h5><p>御社を代表する商品・サービスについてご回答ください。 </p>
				<hr />
				<?php 
				echo $this->BootstrapForm->input('daihyo_shohin_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_spec', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_price', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_etc', array('class' => 'span10'));
				?>
				<hr />
				*/ ?>

				<hr />
				<h5>オススメ商品・サービス</h5><p>オススメ商品・サービス等についてご回答ください。（開業時点、店舗で必ず取り扱っている商品）</p>
				<hr />
				<?php 
				echo $this->BootstrapForm->input('osusume_service_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('osusume_service_price', array('class' => 'span10'));
				// echo $this->BootstrapForm->input('osusume_renewal_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('osusume_comment', array('class' => 'span10'));
				?>
				<hr />
				<h5>”コト消費”とリンクするポイント</h5>
				<p>必要文字数30～50文字程度。ブランドブックで使用します。</p>
				<p>回答は必須ではありません</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment1', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>これまでの店舗との違い</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment2', array('class' => 'span10')); ?>
				<hr />
				<h5>競合他社との違い・アピールポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment3', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>（業界・イオングループで）初の取り組みポイント</h5>
				<p>必要文字数30～50文字程度。ブランドブックで使用します。</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment4', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>直接的な商品・サービス以外に体験できるコト、体験して頂きたいお勧めポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment5', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>ＴＶ取材の場合、レポーターが体験できるポイント</h5>
				<p>必要文字数30～50文字程度。ブランドブックで使用します。</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment6', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>今後の予定（イベントスケジュールや新商品・サービス展開予定等）</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment7', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>媒体掲載用の画像を3点、ご登録下さい。</h5>

				<p>
				「eps」もしくは「jpg」形式（350dpi以上、カラー設定CMYK）<br />
				※様々な媒体で使用しますので、鮮明で解像度の高いデータをご提出下さい。
				</p>
				<hr />
		<?php
		$img = array('image1','image2','image3');
		$img_jp = array('<strong>画像１</strong>','<strong>画像２</strong>','<strong>画像３</strong>');
		for( $num=0; $num<3; $num++){
            if( empty( $this->request->data['OptionHearing'][ $img[$num] .'_file_name'])){
                echo '<p>'.$img_jp[$num].'：　未入力</p>';
            }elseif( empty( $this->request->data['OptionHearing'][ $img[$num] .'_modified'] )){
                echo '<p>'. $img_jp[$num] .'</p>';
                echo '<p class="alert">登録ファイルの保存に失敗しました。すみませんが、もう一度登録をやりなおして下さい。</p>';
            }else{
				$filename = $this->request->data['OptionHearing'][ $img[$num] .'_file_name'];
				$pathinfo = pathinfo( $this->upload->url($this->request->data, 'OptionHearing.'. $img[$num], array()));
				switch( $pathinfo['extension']):
				case 'jpg':
				case 'JPG':
					echo '<p>'. $img_jp[$num] .'</p>';
					echo $this->upload->image(
						$this->request->data, 'OptionHearing.'. $img[$num], array(), array('width' => '400'));
					echo '<p>'. $filename . '</p>';
					echo $this->BootstrapForm->input('delete_'. $img[$num], array(
									'label'=> '削除','type'=>'checkbox','div'=>false));
					break;
				case 'eps':
				case 'EPS':
					echo '<p>'.$img_jp[$num].'：　'. $filename.'<p>';
					echo $this->BootstrapForm->input('delete_'. $img[$num], array(
									'label'=> '削除','type'=>'checkbox','div'=>false));
					echo '<p class="alert alert-info">※「eps」画像のプレビューは表示されません。</p>';
					break;
				default:
					echo '<p>'.$img_jp[$num].'：　'. $filename.'<p>';
					echo '<p class="alert">※画像ファイルは、「eps」もしくは「jpg」形式でご入力ください。</p>';
					break;
				endswitch;
			}
			echo $this->BootstrapForm->input( $img[$num], array('label'=>false,'div'=>false,'type' => 'file'));
			echo '<p class="clear"></p><br />';
			echo '<hr />';
		}
		?>
				<h5>店舗ロゴをご登録ください</h5>

				<p>
				「ai」（バージョンCS以降）もしくは「eps」（350dpi以上）、カラー形式はCMYK形式<br />
				※カラー、モノクロのデータをお持ちの場合は両方<br />
				※複数バリエーションお持ちの場合も両方
				</p>
				<hr />
					<?php if( empty( $this->request->data['OptionHearing']['logo_file_name'] )): ?>
						<p><strong>店舗ロゴ１</strong>：　未入力</p>
					<?php elseif( empty( $this->request->data['OptionHearing']['logo_modified'])): ?>
						<p><strong>店舗ロゴ１</strong></p>
						<p class="alert">登録ファイルの保存に失敗しました。すみませんが、もう一度登録をやりなおして下さい。</p>
					<?php else: ?>
						<p><strong>店舗ロゴ１</strong>：　
						<?php echo $this->request->data['OptionHearing']['logo_file_name'] ?>
						</p>
						<?php echo $this->BootstrapForm->input('delete_logo', array(
									'label'=> '削除','type'=>'checkbox','div'=>false));?>
					<?php endif; ?>
					<?php echo $this->BootstrapForm->input('logo', array('label'=>false,'div'=>false,'type' => 'file')); ?>
				<hr />
				<p class="clear"></p><br />
					<?php if( empty( $this->request->data['OptionHearing']['logo2_file_name'])): ?>
						<p><strong>店舗ロゴ２</strong>：　未入力</p>
					<?php elseif( empty( $this->request->data['OptionHearing']['logo2_modified'])): ?>
						<p><strong>店舗ロゴ２</strong></p>
						<p class="alert">登録ファイルの保存に失敗しました。すみませんが、もう一度登録をやりなおして下さい。</p>
					<?php else: ?>
						<p><strong>店舗ロゴ２</strong>：　
						<?php echo $this->request->data['OptionHearing']['logo2_file_name'] ?>
						</p>
						<?php echo $this->BootstrapForm->input('delete_logo2', array(
									'label'=> '削除','type'=>'checkbox','div'=>false));?>
					<?php endif; ?>
					<?php echo $this->BootstrapForm->input('logo2', array('label'=>false,'div'=>false,'type' => 'file')); ?>
				<hr />
				<h5>飲食業態に関するヒアリング</h5>
				<hr />
				<?php // echo $this->BootstrapForm->input('food_comment1', array('class' => 'span10')); ?>
				<?php echo $this->BootstrapForm->input('food_comment2', array('class' => 'span2','type'=>'text')); ?>
				<?php // echo $this->BootstrapForm->input('food_comment3', array('class' => 'span10')); ?>
				<?php // echo $this->BootstrapForm->input('food_comment4', array('class' => 'span10')); ?>
				<?php echo $this->BootstrapForm->input('food_comment5', array('type' => 'radio', 'options' => array('可' => '可', '不可' => '不可'))); ?>

				<div class="control-group">
					<label for="" class="control-label">予約方法</label>
					<div class="controls">
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_tel', array('div' => false, 'label' => '電話', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_fax', array('div' => false, 'label' => 'FAX', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_hp', array('div' => false, 'label' => 'HP', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_etc', array('div' => false, 'label' => 'その他', 'type' => 'checkbox')); ?>
					</div>
				</div>

				<?php echo $this->BootstrapForm->input('food_alcohol_ok', array('label'=>'アルコール扱い','type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>

				<?php switch( SUB_DOMAIN ){
					case 'makuharishintoshin':
					case 'toin':
					case 'testsite':
					case 'aeon-console':
				?>
				<?php echo $this->BootstrapForm->input('food_alcohol', array('label'=>'備考<br />(アルコール扱い)','class' => 'span10')); ?>
				<?php break;?>
				<?php } ?>

				<?php echo $this->BootstrapForm->input('food_childseat', array('type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<?php echo $this->BootstrapForm->input('food_kidsmenu', array('type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<hr />
				<h5>オフィシャルサイト以外の外部サイトURL</h5>
				<p>（例） ぐるなびURL、食べログURL等</p>
				<hr />
				<?php echo $this->BootstrapForm->input('unofficial_url', array('class' => 'span10')); ?>
				<hr />
				<?php if( $_SESSION['Auth']['User']["lock_hearing2"] == 0 ): ?>
				<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>'btn btn-primary'));?>
				<?php elseif( $print == null ): ?>
				<?php echo MESSAGE_AT_LOCKED;?>
				<?php endif; ?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
