
<div class="hero-unit">
  <h2>ヒアリングシート２</h2>
</div>

<hr />
<div class="row-fluid">
	<div class="span12">
<h5>各種制作物 (PR用・店舗設置用) ヒアリング項目案</h5>
回答内容は、事前ＰＲ活動や取材に来場する報道関係者（記者・ＴＶディレクター・アナウンサー）に対し配布する資料「ファクトブック」や各種製作物に反映、活用させて頂きます。<br><br />
より多くの施設取材・個別店舗取材を誘致するため、できる限り多くの情報提供のご協力をお願いします。<br><br />
回答内容がそのまま記事化される事もありますので、その点ご留意ください。また、ご回答いただいた内容が全て製作物に反映されない場合もございますことも併せてご留意願います。<br><br />
<?php /*「制作」の項目はフロアガイド、ブランドブック、体験ガイドにて、「ＰＲ」の項目はファクトブックにて使用を想定しております。 */ ?>
	</div>
</div>
<hr />

<div class="row-fluid">
	<div class="span12">
		<?php echo $this->BootstrapForm->create('OptionHearing', array('class' => 'form-horizontal', 'type' => 'file'));?>
			<?php echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id)); ?>
			<fieldset>
				<?php // echo $this->BootstrapForm->input('gyotai', array('class' => 'span10')); ?>
				<h5>当モール出店にあたり、ニュース性の高いPRポイント（1行で簡潔にお書き下さい）</h5>
				<p> 例）イタリアで大人気のアクセサリーを日本初取り扱い</p>
				<hr />
				<?php echo $this->BootstrapForm->input('haikei', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>代表商品の商品名、スペック、価格等</h5><p>御社を代表する商品・サービスについてご回答ください。 </p>
				<hr />
				<?php 
				echo $this->BootstrapForm->input('daihyo_shohin_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_spec', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_price', array('class' => 'span10'));
				echo $this->BootstrapForm->input('daihyo_shohin_etc', array('class' => 'span10'));
				?>
				<hr />
				*/ ?>

				<hr />
				<h5>オススメ商品・サービス</h5><p>オススメ商品・サービス等についてご回答ください。（開業時点、店舗で必ず取り扱っている商品）</p>
				<hr />
				<?php 
				echo $this->BootstrapForm->input('osusume_service_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('osusume_service_price', array('class' => 'span10'));
				// echo $this->BootstrapForm->input('osusume_renewal_name', array('class' => 'span10'));
				echo $this->BootstrapForm->input('osusume_comment', array('class' => 'span10'));
				?>
				<hr />
				<h5>”コト消費”とリンクするポイント</h5>
				<p>必要文字数30～50文字程度。ブランドブックで使用します。</p>
				<p>回答は必須ではありません</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment1', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>これまでの店舗との違い</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment2', array('class' => 'span10')); ?>
				<hr />
				<h5>競合他社との違い・アピールポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment3', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>（業界・イオングループで）初の取り組みポイント</h5>
				<p>必要文字数30～50文字程度。ブランドブックで使用します。</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment4', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>直接的な商品・サービス以外に体験できるコト、体験して頂きたいお勧めポイント</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment5', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>ＴＶ取材の場合、レポーターが体験できるポイント</h5>
				<p>必要文字数30～50文字程度。ブランドブックで使用します。</p>
				<hr />
				<?php echo $this->BootstrapForm->input('comment6', array('class' => 'span10')); ?>
				<?php /*
				<hr />
				<h5>今後の予定（イベントスケジュールや新商品・サービス展開予定等）</h5>
				<hr />
				<?php echo $this->BootstrapForm->input('comment7', array('class' => 'span10')); ?>
				<hr />
				*/ ?>
				<hr />
				<h5>媒体掲載用の画像を3点、ご登録下さい。</h5>

				<p>
				「eps」もしくは「jpg」形式（350dpi以上、カラー設定CMYK）<br />
				※様々な媒体で使用しますので、鮮明で解像度の高いデータをご提出下さい。
				</p>

				<hr />
				<?php echo $this->BootstrapForm->input('image1', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('image2', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('image3', array('type' => 'file')); ?>
				<hr />
				<h5>店舗ロゴをご登録ください</h5>

				<p>
				「ai」（バージョンCS以降）もしくは「eps」（350dpi以上）、カラー形式はCMYK形式<br />
				※カラー、モノクロのデータをお持ちの場合は両方<br />
				※複数バリエーションお持ちの場合も両方
				</p>
				<hr />

				<?php echo $this->BootstrapForm->input('logo', array('type' => 'file')); ?>
				<?php echo $this->BootstrapForm->input('logo2', array('type' => 'file')); ?>
				<hr />
				<h5>飲食業態に関するヒアリング</h5>
				<hr />
				<?php // echo $this->BootstrapForm->input('food_comment1', array('class' => 'span10')); ?>
				<?php echo $this->BootstrapForm->input('food_comment2', array('class' => 'span2','type'=>'text')); ?>
				<?php // echo $this->BootstrapForm->input('food_comment3', array('class' => 'span10')); ?>
				<?php // echo $this->BootstrapForm->input('food_comment4', array('class' => 'span10')); ?>
				<?php echo $this->BootstrapForm->input('food_comment5', array('type' => 'radio', 'options' => array('可' => '可', '不可' => '不可'))); ?>


				<div class="control-group">
					<label for="" class="control-label">予約方法</label>
					<div class="controls">
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_tel', array('div' => false, 'label' => '電話', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_fax', array('div' => false, 'label' => 'FAX', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_hp', array('div' => false, 'label' => 'HP', 'type' => 'checkbox')); ?>
					<?php echo $this->Form->input('OptionHearing.food_yoyaku_etc', array('div' => false, 'label' => 'その他', 'type' => 'checkbox')); ?>
					</div>
				</div>

				<?php echo $this->BootstrapForm->input('food_alcohol_ok', array('label'=>'アルコール扱い','type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>

				<?php switch( SUB_DOMAIN ){
					case 'makuharishintoshin':
					case 'toin':
					case 'testsite':
					case 'aeon-console':
				?>
				<?php echo $this->BootstrapForm->input('food_alcohol', array('label'=>'備考<br />(アルコール扱い)','class' => 'span10')); ?>
				<?php break;?>
				<?php } ?>

				<?php echo $this->BootstrapForm->input('food_childseat', array('type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<?php echo $this->BootstrapForm->input('food_kidsmenu', array('type' => 'radio', 'options' => array('あり' => 'あり', 'なし' => 'なし'))); ?>
				<hr />
				<h5>オフィシャルサイト以外の外部サイトURL</h5>
				<p>（例） ぐるなびURL、食べログURL等</p>
				<hr />
				<?php echo $this->BootstrapForm->input('unofficial_url', array('class' => 'span10')); ?>
				<hr />
				<?php if( $_SESSION['Auth']['User']["lock_hearing1"] == 0 ): ?>
				<?php echo $this->BootstrapForm->submit(__('Submit'),array('class'=>'btn btn-primary'));?>
				<?php else: ?>
				<?php echo MESSAGE_AT_LOCKED; ?>
				<?php endif; ?>
			</fieldset>
		<?php echo $this->BootstrapForm->end();?>
	</div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
