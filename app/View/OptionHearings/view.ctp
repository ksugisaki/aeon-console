<div class="row-fluid">
	<div class="span9">
		<h2><?php  echo __('Option Hearing');?></h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('User'); ?></dt>
			<dd>
				<?php echo $this->Html->link($optionHearing['User']['name'], array('controller' => 'users', 'action' => 'view', $optionHearing['User']['id'])); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Gyotai'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['gyotai']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Haikei'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['haikei']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Created'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['created']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Modified'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['modified']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Daihyo Shohin Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['daihyo_shohin_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Daihyo Shohin Spec'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['daihyo_shohin_spec']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Daihyo Shohin Price'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['daihyo_shohin_price']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Daihyo Shohin Etc'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['daihyo_shohin_etc']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Osusume Service Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['osusume_service_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Osusume Service Price'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['osusume_service_price']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Osusume Renewal Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['osusume_renewal_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Osusume Renewal Price'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['osusume_renewal_price']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment1'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment1']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment2'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment2']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment3'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment3']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment4'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment4']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment5'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment5']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment6'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment6']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Comment7'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['comment7']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Image1 File Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['image1_file_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Image2 File Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['image2_file_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Image3 File Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['image3_file_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Logo File Name'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['logo_file_name']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Comment1'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_comment1']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Comment2'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_comment2']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Comment3'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_comment3']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Comment4'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_comment4']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Comment5'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_comment5']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Alcohol'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_alcohol']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Childseat'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_childseat']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Food Kidsmenu'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['food_kidsmenu']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Unofficial Url'); ?></dt>
			<dd>
				<?php echo h($optionHearing['OptionHearing']['unofficial_url']); ?>
				&nbsp;
			</dd>
		</dl>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('Edit %s', __('Option Hearing')), array('action' => 'edit', $optionHearing['OptionHearing']['id'])); ?> </li>
			<li><?php echo $this->Form->postLink(__('Delete %s', __('Option Hearing')), array('action' => 'delete', $optionHearing['OptionHearing']['id']), null, __('Are you sure you want to delete # %s?', $optionHearing['OptionHearing']['id'])); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Option Hearings')), array('action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('Option Hearing')), array('action' => 'add')); ?> </li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>

