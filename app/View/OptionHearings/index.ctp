<div class="row-fluid">
	<div class="span9">
		<h2><?php echo __('List %s', __('Option Hearings'));?></h2>

		<p>
			<?php echo $this->BootstrapPaginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>
		</p>

		<table class="table">
			<tr>
				<th><?php echo $this->BootstrapPaginator->sort('id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('user_id');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('gyotai');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('haikei');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('created');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('modified');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('daihyo_shohin_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('daihyo_shohin_spec');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('daihyo_shohin_price');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('daihyo_shohin_etc');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('osusume_service_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('osusume_service_price');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('osusume_renewal_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('osusume_renewal_price');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment1');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment2');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment3');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment4');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment5');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment6');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('comment7');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('image1_file_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('image2_file_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('image3_file_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('logo_file_name');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_comment1');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_comment2');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_comment3');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_comment4');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_comment5');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_alcohol');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_childseat');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('food_kidsmenu');?></th>
				<th><?php echo $this->BootstrapPaginator->sort('unofficial_url');?></th>
				<th class="actions"><?php echo __('Actions');?></th>
			</tr>
		<?php foreach ($optionHearings as $optionHearing): ?>
			<tr>
				<td><?php echo h($optionHearing['OptionHearing']['id']); ?>&nbsp;</td>
				<td>
					<?php echo $this->Html->link($optionHearing['User']['name'], array('controller' => 'users', 'action' => 'view', $optionHearing['User']['id'])); ?>
				</td>
				<td><?php echo h($optionHearing['OptionHearing']['gyotai']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['haikei']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['created']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['modified']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['daihyo_shohin_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['daihyo_shohin_spec']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['daihyo_shohin_price']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['daihyo_shohin_etc']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['osusume_service_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['osusume_service_price']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['osusume_renewal_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['osusume_renewal_price']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment1']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment2']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment3']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment4']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment5']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment6']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['comment7']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['image1_file_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['image2_file_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['image3_file_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['logo_file_name']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_comment1']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_comment2']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_comment3']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_comment4']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_comment5']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_alcohol']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_childseat']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['food_kidsmenu']); ?>&nbsp;</td>
				<td><?php echo h($optionHearing['OptionHearing']['unofficial_url']); ?>&nbsp;</td>
				<td class="actions">
					<?php echo $this->Html->link(__('View'), array('action' => 'view', $optionHearing['OptionHearing']['id'])); ?>
					<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $optionHearing['OptionHearing']['id'])); ?>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $optionHearing['OptionHearing']['id']), null, __('Are you sure you want to delete # %s?', $optionHearing['OptionHearing']['id'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>

		<?php echo $this->BootstrapPaginator->pagination(); ?>
	</div>
	<div class="span3">
		<div class="well" style="padding: 8px 0; margin-top:8px;">
		<ul class="nav nav-list">
			<li class="nav-header"><?php echo __('Actions'); ?></li>
			<li><?php echo $this->Html->link(__('New %s', __('Option Hearing')), array('action' => 'add')); ?></li>
			<li><?php echo $this->Html->link(__('List %s', __('Users')), array('controller' => 'users', 'action' => 'index')); ?> </li>
			<li><?php echo $this->Html->link(__('New %s', __('User')), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
		</div>
	</div>
</div>