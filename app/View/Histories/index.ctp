<div class="hero-unit">
  <h2>更新履歴</h2>
</div>
<table class="table table-striped table-bordered table-condensed">
<?php
  echo $this->Html->tableHeaders( $labels );
  foreach( $histories as $hisory ){
    echo $this->Html->tableCells( $hisory['History'] );
  }
?>
</table>
