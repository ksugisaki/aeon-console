<?php if( $print == null ): ?>
    <div class="hero-unit">
        <h2>福袋について</h2>
        <p>福袋の販売の有無などの情報をご入力ください。</p>
    </div>
    <br />

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>福袋について</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['Fukubukuro']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['Fukubukuro']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('Fukubukuro',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
        <div class="alert alert-info">
          <h3>ご担当者様連絡先</h3>
        </div>
        <h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('position', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'ご連絡先','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>
        <br />
        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>

        <div class="alert alert-info">
          <h3>1. 福袋の販売について</h3>
        </div>
        <h4>福袋の販売の実施について選択して下さい。</h4>
        <?php echo $this->BootstrapForm->select('fukubukuro_hanbai',
                array('する'=>'する', 'しない'=>'しない', '未定'=>'未定'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <h4>実施しない場合、理由をご記入下さい</h4>
        <?php echo $this->BootstrapForm->input('fukubukuro_riyuu',
                                               array('label'=>'しない理由','class' => 'span10')); ?>
        <h4>未定の場合、決定時期をご記入下さい</h4>
        <?php echo $this->BootstrapForm->input('fukubukuro_jiki',
                                               array('label'=>'決定時期','class' => 'span10')); ?>
        <p class="offset2">（例）〇月〇日頃</p>
        <br />
        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>

        <div class="alert alert-info">
          <h3>2. 福袋の販売計画</h3>
        </div>
        <h4>販売個数総数をご記入下さい。</h4>
        <?php echo $this->BootstrapForm->input('hanbai_suu',
                                               array('label'=>false,'class' => 'span3 ko')); ?>
        <br />
        <h4>販売金額総額をご記入下さい。</h4>
        <?php echo $this->BootstrapForm->input('hanbai_sou_gaku',
                                               array('label'=>false,'class' => 'span3 yen')); ?>
        <br />
        <h3>福袋詳細情報</h3>
		<pre>
※チラシやイオンモール東員ＨＰ等に、ご入力頂いたとおりに掲載致しますので、正確にご入力下さい。
 内容に関しては、表記されない部分もございますのでご了承下さい。
 入力期限を過ぎた後に変更が発生した際は、イオンモール東員 営業担当まで直接ご連絡下さい。
</pre>

        <?php for( $num = 1; $num <= 8; $num++ ): ?>
            <h4>福袋<?php echo $num; ?></h4>
            <?php echo $this->BootstrapForm->input('fukubukuro'.$num.'_name',
                                               array('label'=>'福袋名前','class'=>'span10')); ?><br />
            <?php echo $this->BootstrapForm->input('fukubukuro'.$num.'_tanka',
                                               array('label'=>'福袋単価(税込)','class'=>'span3 yen')); ?><br />
            <?php echo $this->BootstrapForm->input('fukubukuro'.$num.'_suu',
                                               array('label'=>'用意個数','class'=>'span3 ko')); ?><br />
            <?php echo $this->BootstrapForm->input('fukubukuro'.$num.'_naiyou',
                                               array('label'=>'内容','class'=>'span10','rows'=>'4')); ?>
            <p class="offset2">（例）アウターが必ず入っており、他カットソー、ボトムス計○点　○円相当　○円<br />
                               　　　雑貨の詰め合わせ　食器、エプロン、キッチン用具　計○点　○円相当　○円</p>
        <?php endfor; ?>
        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>

        <div class="alert alert-info">
          <h3>3. 予約の有無</h3>
        </div>
        <h4>予約の有無について選択して下さい。</h4>
        <?php echo $this->BootstrapForm->select('yoyaku',
                array('あり'=>'あり', 'なし'=>'なし'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <h4>予約ありの場合、ご入力ください。</h4>
        <?php echo $this->BootstrapForm->input('yoyaku_kaisi',
                                               array('label'=>'予約開始日','class'=>'span4')); ?><br />
        <p class="offset2">（例）〇月〇日</p>
        <h4>個数制限について選択して下さい。</h4>
        <?php echo $this->BootstrapForm->select('yoyaku_seigen',
                array('あり'=>'あり', 'なし'=>'なし'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <h4>個数制限ありの場合、ご入力ください。</h4>
        <?php echo $this->BootstrapForm->input('yoyaku_seigen_suu',
                                               array('label'=>'制限個数','class'=>'span3 ko')); ?><br />
        <h4>予約方法について、ご入力ください。</h4>
        <?php echo $this->BootstrapForm->input('yoyaku_houhou',
                                       array('label'=>'予約方法','class'=>'span10','rows'=>'4')); ?>
        <p class="offset2 span8">（例）店舗での受付。受付票にご記入頂き、控えを渡す。その控えを持って、1月1日より店舗で引き換える。会計は、商品お渡し時に全額頂き、予約時点では受け取らない。</p>
        <p class="clear"></p>
        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>

        <div class="alert alert-info">
          <h3>4. 臨時倉庫利用の有無</h3>
        </div>
<pre>
福袋専用となります。使用場所・使用方法・搬入・撤去時間などイオンモールの指示に従って頂く事を前提でご要望下さい。
※要望数にお答えできない場合もございます。必要数を頂いてからの調整となりますのでご了承下さい。		
</pre>
        <table class="admin_list table table-condensed">
        <tr>
          <td>倉庫サイズ</td><td>1坪(3.3㎡＝約1.8m×約1.8m)</td>
        </tr><tr>
          <td>倉庫利用料金</td><td>1坪　800円/日</td>
        </tr><tr>
          <td>貸出期間</td><td>2013年12月20日(金)～2014年1月6日(月)</td>
        </tr><tr>
          <td></td><td></td>
        </tr>
        </table>
        <h4>臨時倉庫利用について選択して下さい。</h4>
        <?php echo $this->BootstrapForm->select('souko_riyou',
                array('利用希望する'=>'利用希望する', '利用しない'=>'利用しない'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <h4>利用希望される場合、ご入力ください。</h4>
        <?php echo $this->BootstrapForm->input('souko_tubo_suu',
                                               array('label'=>'希望坪数','class'=>'span3 tubo')); ?><br />
        <h4>利用希望される場合、希望利用期間をご入力ください。</h4>
        <?php echo $this->BootstrapForm->input('souko_riyou_kaisi',
                                               array('label'=>'利用開始日','class'=>'span4')); ?><br />
        <?php echo $this->BootstrapForm->input('souko_riyou_saigo',
                                               array('label'=>'利用最終日','class'=>'span4')); ?><br />

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>


<script type="text/javascript">
$(document).ready(function(){
    // 「円」を追加
    $('.yen').after(' 円');

    // 「個」を追加
    $('.ko').after(' 個');

    // 「坪」を追加
    $('.tubo').after(' 坪');

    // アラート・メッセージを追記。
    $('div.alert-error').append('。<strong>「福袋の販売の実施について」</strong>は必ず選択入力してください。');

})
</script>
