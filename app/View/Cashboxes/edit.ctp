<?php if( $print == null ): ?>
    <div class="hero-unit">
    <?php if( SUB_DOMAIN =='toin'): ?>
        <h2>釣銭金庫利用申込書</h2>
    <?php else: ?>
        <h2>釣銭金庫使用申込書</h2>
    <?php endif; ?>
<p>
釣銭金を保管する金庫をお貸しします。希望台数をご入力下さい。
</p>
    </div>
<?php if( SUB_DOMAIN =='makuharishintoshin'): ?>
<pre>
詳細は運営管理説明会資料24～25ページをご確認下さい。
</pre><br />
<?php endif; ?>

<?php else: ?>
  <div class="row-fluid">
    <div class="span6">
        <h2>釣銭金庫使用申込書</h2>
    </div>
    <div class="span6">
        <?php if( isset( $this->request->data['Cashbox']['modified'])){ ?>
            <p>最終更新日： <?php echo( $this->request->data['Cashbox']['modified']);?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] );?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] );?></p>
        <?php } ?>
    </div>
  </div>
<?php endif; ?>

<table class="admin_list table table-condensed">
    <tr><td width="200px">１ヶ所につき</td>
      <td><strong>月額２，０００円（税別）</strong></td></tr>
    <tr><td></td><td></td></tr>
</table>
<p class="clear"></p>

<div class="row-fluid">
    <div class="">
    <?php
        echo $this->BootstrapForm->create('Cashbox',
                      array('class' => 'myform form-horizontal', 'type' => 'file'));
        if( isset( $add )){ // 新規追加
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden', 'value' => $user_id));
        }else{ // 更新
            echo $this->BootstrapForm->input('id', array('type' => 'hidden'));
            echo $this->BootstrapForm->input('user_id', array('type' => 'hidden'));
        }
    ?>
    <fieldset>
    <?php if( SUB_DOMAIN =='toin'):  ?>
        <div class="alert alert-info">
            <h3>利用有無</h3>
        </div>
        <?php echo $this->BootstrapForm->select('cashbox_num',
                array('1'=>'有', '0'=>'無'),
                array('empty'=>'選択してください', 'class'=>'offset2','id'=>'selector'));
        ?>
        <p class="offset2">※ 利用の有無を選択してください。</p>
    <?php else: ?>
        <div class="alert alert-info">
            <h3>利用台数</h3>
        </div>
        <?php echo $this->BootstrapForm->input('cashbox_num',
                   array('label'=>'釣銭金庫希望台数','class' => 'span10')); ?>
        <p class="offset2">※ 半角数字で入力してください。</p>
    <?php endif; ?>
        <br />
        <div class="alert alert-info">
        <h3>契約書送付先</h3>
        </div>
        <h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>
        <?php echo $this->BootstrapForm->input('zipcode', array('label'=>'郵便番号','class' => 'span10')); ?>
        <p class="offset2">（例）261-8539</p>
        <?php echo $this->BootstrapForm->input('address', array('label'=>'住所','class' => 'span10')); ?>
        <p class="offset2">（例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
        <?php echo $this->BootstrapForm->input('department', array('label'=>'部署','class' => 'span10')); ?>
        <p class="offset2">（例）営業部</p>
        <?php echo $this->BootstrapForm->input('name', array('label'=>'氏名','class' => 'span10')); ?>
        <p class="offset2">（例）山田花子</p>
        <?php echo $this->BootstrapForm->input('name_kana', array('label'=>'フリガナ','class' => 'span10')); ?>
        <p class="offset2">（例）ヤマダハナコ</p>
        <?php echo $this->BootstrapForm->input('position', array('label'=>'役職','class' => 'span10')); ?>
        <p class="offset2">（例）店長</p>
        <?php echo $this->BootstrapForm->input('phone', array('label'=>'ご連絡先','class' => 'span10')); ?>
        <p class="offset2">（例）090-1234-5678</p>

        <?php if( ! $lock ): ?>
            <?php echo $this->BootstrapForm->submit('登録',array('class'=>'btn btn-primary'));?>
        <?php else: ?>
            <?php echo MESSAGE_AT_LOCKED; ?>
        <?php endif; ?>
    </fieldset>
    <?php echo $this->BootstrapForm->end();?>
    </div>
</div>
<p class="clear"></p>
<hr />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
