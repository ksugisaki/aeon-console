<div class ="hero-unit">
  <h2>パスワードの再設定</h2>
</div>
<?php echo $this->Session->flash(); ?>
<h4>ご登録いただいたメールアドレスにパスワード再設定のURLをお知らせするメールを発行いたします。</h4>
<div class="user form">
    <?php
        echo $this->Form->create('User', array('url' => 'ForgotPassword'));
        echo $this->Form->input('email', array('label' => 'メールアドレス'));
        echo $this->Form->end('パスワードの再設定');
    ?>
</div>
