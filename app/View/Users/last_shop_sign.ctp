<?php if( $print == null ){ ?>
<div class ="hero-unit">
<h2><?php echo $title_for_layout; ?></h2>
<p>11月中旬～下旬発表予定の出店店舗名の最終リリース及び館内フロアマップ、フロアガイド等に
使用いたしますので、店名の最終確認をお願い致します。<br />
<strong>これ以降の修正はできませんのでご注意ください。</strong><br />
<h4><span style="color:red">提出期限：10月25日（金）</span></h4>
</p>
</div>
<?php echo $this->Session->flash(); ?>

<?php }else{ ?>
<div class="row-fluid">
    <div class="span1">
    </div>
    <div class="span5">
        <h2><?php echo $title_for_layout; ?></h2>
    </div>
    <div class="span6">
        <?php if( isset( $user_info['last_shop_sign_modified'])){ ?>
            <p>最終更新日： <?php echo( $user_info['last_shop_sign_modified'] ); ?></p>
        <?php } ?>
        <?php if( isset( $user_info['company'])){ ?>
            <p>会社名： <?php echo( $user_info['company'] ); ?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>テナント名： <?php echo( $user_info['receipt_tenant_name'] ); ?></p>
        <?php } ?>
        <?php if( isset( $user_info['receipt_tenant_name'])){ ?>
            <p>部署： <?php echo( $user_info['department'] ); ?></p>
        <?php } ?>
        <?php if( isset( $user_info['name'])){ ?>
            <p>担当者名： <?php echo( $user_info['name'] ); ?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>電話番号： <?php echo( $user_info['phone'] ); ?></p>
        <?php } ?>
        <?php if( isset( $user_info['phone'])){ ?>
            <p>Email： <?php echo( $user_info['email'] ); ?></p>
        <?php } ?>
    </div>
</div>
<?php } ?>

<div class="row-fluid">
    <?php echo $this->BootstrapForm->create('User', Array('class'=>'form-horizontal', 'url' => 'LastShopSign/'.$id )); ?>
    <fieldset>
        <?php echo $this->BootstrapForm->input('id', Array('type' => 'hidden')); ?>

        <div class="alert alert-info">
        <h3>店名</h3>
        <p>（”イオンモール幕張新都心店”　”幕張新都心店” などは不要です）</p>
        </div>
        <?php // 最新の店名を探す（重複があったから）
			if( isset( $this->request->data['Signboard'])){
				$all_signboards = $this->request->data['Signboard'];
				// user_id重複しているデータを除く
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			// echo $signboard_last['shop_sign'];
        ?>
        <p class="clear">　◆ 店名をご確認ください。</p>
<?php if( $user['role']=='admin'): ?>
        <?php echo $this->BootstrapForm->input('shop_sign_last',
              array('label'=>'店名','class'=>'span6')); ?>
<?php else: ?>
        <?php echo $this->BootstrapForm->input('shop_sign_last',
              array('label'=>'店名','class'=>'span6','readonly'=>true)); ?>
<?php endif; ?>

<?php if(false): ?>
        <p class="clear">　（更新は、「店名確認シート」からの更新になります）</p>
        <table class="admin_list table table-bordered span7"><tr>
          <td width="40px" style="background-color: whitesmoke;">店名</td>
          <td width="500px" style="background-color: whitesmoke;">
            <?php echo isset( $signboard_last['shop_sign'])? $signboard_last['shop_sign']:''; ?>
          </td>
          <td width="40px"><?php echo $this->Html->link('更新','/signboards/index/'.$this->request->data['User']['id'].'#shop_sign', true); ?></td>
        </tr>
        </table>
<?php endif; ?>
        <p class="clear">　◆ 店名が上記で確定しているか未定か修正するかを選択してください。</p>
        <?php echo $this->BootstrapForm->input('shop_sign_kakutei', Array(
            'label' => false,
            'type' => 'radio',
            'default' => 0,
            'options' => array('確定'=>'確定','未定'=>'未定','修正'=>'修正')
        )); ?>
        <div id="shop_sign_mitei">
          <p class="clear">　◆ 未定の場合は、決定時期をご入力ください。</p>
          <?php echo $this->BootstrapForm->input('shop_sign_yotei',
              array('label'=>'決定時期','class'=>'span6')); ?>
        </div>
        <div id="shop_sign_syuusei">
          <p class="clear">　◆ 修正の場合は、以下にご入力ください。</p>
          <?php echo $this->BootstrapForm->input('shop_sign_syuusei',
                     array('label'=>'店名（修正）','class'=>'span6')); ?>
        </div>
        <p class="clear">　◆ 備考</p>
        <?php echo $this->BootstrapForm->input('shop_sign_remark', array('label'=>false,'class'=>'span6')); ?>
    <?php
        if( empty( $locked )){
            echo $this->BootstrapForm->button('登録',array('class' => 'btn btn-primary'));
        }else{
            echo MESSAGE_AT_LOCKED;
        }
        echo '<br /><br />';
    ?>
        <div class="alert alert-info">
        <h3>業種カテゴリ</h3>
        </div>
        <p class="clear"></p>
        <p class="clear">　◆ 業種カテゴリをご確認ください。</p>
<?php if( $user['role']=='admin'): ?>
        <?php echo $this->BootstrapForm->input('shop_category_last',
                   array('label'=>'業種カテゴリ','class'=>'span6')); ?>
<?php else: ?>
        <?php echo $this->BootstrapForm->input('shop_category_last',
                   array('label'=>'業種カテゴリ','class'=>'span6','readonly'=>true)); ?>
<?php endif; ?>

        <p class="clear">　◆ 業種カテゴリが上記で確定しているか未定か修正するかを選択してください。</p>
        <?php echo $this->BootstrapForm->input('shop_category_kakutei', Array(
            'label' => false,
            'type' => 'radio',
            'default' => 0,
            'options' => array('確定'=>'確定','未定'=>'未定','修正'=>'修正')
        )); ?>
        <div id="category_mitei">
          <p class="clear">　◆ 未定の場合は、決定時期をご入力ください。</p>
          <?php echo $this->BootstrapForm->input('shop_category_yotei',
              array('label'=>'決定時期','class'=>'span6')); ?>
        </div>
        <div id="category_syuusei">
          <p class="clear">　◆ 修正の場合は、以下にご入力ください。</p>
          <?php echo $this->BootstrapForm->input('shop_category_syuusei',
              array('label'=>'業種カテゴリ（修正）','class'=>'span6')); ?>
        </div>
        <p class="clear">　◆ 備考</p>
        <?php echo $this->BootstrapForm->input('shop_category_remark', array('label'=>false,'class'=>'span6')); ?>

    <?php
        if( empty( $locked )){
            echo $this->BootstrapForm->button('登録',array('class' => 'btn btn-primary'));
        }else{
            echo MESSAGE_AT_LOCKED;
        }
        echo '<br /><br />';
    ?>
        <div class="alert alert-info">
        <h3>出店状況</h3>
        <p>（日本初・・・日本初上陸の外資、新業態・・・既存店で展開していない新業態）</p>
        </div>
        <?php
			$syutten_jyoukyou_label = array(
				'日本初出店'=>'日本初出店',
				'新業態'=>'新業態',
				'関東初出店'=>'関東初出店',
				'千葉県初出店'=>'千葉県初出店',
				'該当なし'=>'該当なし'
			);
			// $syutten_input = $this->BootstrapForm->input('Hearing.0.syutten_jyoukyou', array(
			$syutten_syuusei = $this->BootstrapForm->select('syutten_jyoukyou_syuusei',
				$syutten_jyoukyou_label,
				array('empty'=>'出店状況を選択してください','class'=>'offset2')
			);
        ?>
        <p class="clear">　◆ 出店状況をご確認ください。</p>
<?php if( $user['role']=='admin'): ?>
        <?php echo $this->BootstrapForm->input('syutten_jyoukyou_last',
              array('label'=>'出店状況','class'=>'span6')); ?>
<?php else: ?>
        <?php echo $this->BootstrapForm->input('syutten_jyoukyou_last',
              array('label'=>'出店状況','class'=>'span6','readonly'=>true)); ?>
<?php endif; ?>

<?php if(false): ?>
        <table class="admin_list table table-bordered span7"><tr>
          <td width="500px" style="background-color: whitesmoke;"><?php echo $syutten_input; ?></td>
          <td width="50px"><?php echo $this->Html->link('更新','/hearings/index/'.$this->request->data['User']['id'].'#syutten_jyoukyou', true); ?></td>
        </tr>
        </table>
<?php endif; ?>
        <p class="clear">　◆ 出店状況が上記で確定しているか修正するかを選択してください。</p>
        <?php echo $this->BootstrapForm->input('syutten_jyoukyou_kakutei', Array(
            'label' => false,
            'type' => 'radio',
            'default' => 0,
            'options' => array('確定'=>'確定','修正'=>'修正')
        )); ?>
        <div id="syutten_syuusei">
          <p class="clear">　◆ 修正の場合は、以下から選択してください。</p>
          <?php echo $syutten_syuusei; ?>
        </div>
        <p class="clear">　◆ 備考</p>
        <?php echo $this->BootstrapForm->input('syutten_jyoukyou_remark',
                                               array('label'=>false,'class'=>'span6')); ?>


    </fieldset>
    <?php
        if( empty( $locked )){
            echo $this->BootstrapForm->button('登録',array('class' => 'btn btn-primary'));
        }else{
            echo MESSAGE_AT_LOCKED;
        }
        echo $this->BootstrapForm->end();
    ?>
</div>

<!--
  $(function() {
    // 出店状況のチェックボックスを入力無効にする
    $("input#Hearing0SyuttenJyoukyou0").attr("disabled", true);
    $("input#Hearing0SyuttenJyoukyou1").attr("disabled", true);
    $("input#Hearing0SyuttenJyoukyou2").attr("disabled", true);
    $("input#Hearing0SyuttenJyoukyou3").attr("disabled", true);
 });
-->
<script type="text/javascript">

  if( $("#UserShopSignKakutei修正").is(':checked')){
      $("div#shop_sign_mitei").css("display",'none');
      $("div#shop_sign_syuusei").css("display",'inline');
  }
  if( $("#UserShopSignKakutei未定").is(':checked')){
      $("div#shop_sign_mitei").css("display",'inline');
      $("div#shop_sign_syuusei").css("display",'none');
  }
  if( $("#UserShopSignKakutei確定").is(':checked')){
      $("div#shop_sign_mitei").css("display",'none');
      $("div#shop_sign_syuusei").css("display",'none');
  }

  if( $("#UserShopCategoryKakutei修正").is(':checked')){
      $("div#category_mitei").css("display",'none');
      $("div#category_syuusei").css("display",'inline');
  }
  if( $("#UserShopCategoryKakutei未定").is(':checked')){
      $("div#category_mitei").css("display",'inline');
      $("div#category_syuusei").css("display",'none');
  }
  if( $("#UserShopCategoryKakutei確定").is(':checked')){
      $("div#category_mitei").css("display",'none');
      $("div#category_syuusei").css("display",'none');
  }

  if( $("#UserSyuttenJyoukyouKakutei修正").is(':checked')){
      $("div#syutten_syuusei").css("display",'inline');
  }
  if( $("#UserSyuttenJyoukyouKakutei確定").is(':checked')){
      $("div#syutten_syuusei").css("display",'none');
  }

  $(function() {
    // 店名、ラジオボタンで修正を押されたら修正欄を表示
    $( 'input[name="data[User][shop_sign_kakutei]"]:radio' ).change( function() {
        if( $(this).val()=='修正' ){
            $("div#shop_sign_mitei").css("display",'none');
            $("div#shop_sign_syuusei").css("display",'inline');
        }
        if( $(this).val()=='未定' ){
            $("div#shop_sign_mitei").css("display",'inline');
            $("div#shop_sign_syuusei").css("display",'none');
        }
        if( $(this).val()=='確定' ){
            $("div#shop_sign_mitei").css("display",'none');
            $("div#shop_sign_syuusei").css("display",'none');
        }
    });

    // カテゴリ、ラジオボタンで修正を押されたら修正欄を表示
    $( 'input[name="data[User][shop_category_kakutei]"]:radio' ).change( function() {
        if( $(this).val()=='修正' ){
            $("div#category_mitei").css("display",'none');
            $("div#category_syuusei").css("display",'inline');
        }
        if( $(this).val()=='未定' ){
            $("div#category_mitei").css("display",'inline');
            $("div#category_syuusei").css("display",'none');
        }
        if( $(this).val()=='確定' ){
            $("div#category_mitei").css("display",'none');
            $("div#category_syuusei").css("display",'none');
        }
    });

    // 出店状況、ラジオボタンで修正を押されたら修正欄を表示
    $( 'input[name="data[User][syutten_jyoukyou_kakutei]"]:radio' ).change( function() {
        if( $(this).val()=='修正' ){
            $("div#syutten_syuusei").css("display",'inline');
        }
        if( $(this).val()=='確定' ){
            $("div#syutten_syuusei").css("display",'none');
        }
    });

  });
</script>
