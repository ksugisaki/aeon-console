<div class ="hero-unit">
  <h2>パスワード変更</h2>
  <p><?php echo h( $userinfo['name']);?> さんのパスワードを変更します。</p>
  <p>登録メールアドレス: <?php echo h( $userinfo['email']);?> </p>
</div>
<?php echo $this->Session->flash(); ?>
<div class="user form">
<?php
echo $this->Form->create('User', array('url'=>'/Users/ChangePassword/'. $id ));
echo $this->Form->input('id', array('type'=>'hidden','value'=> $id ));
echo $this->Form->input('password',
     array('type'=>'password','label'=>'パスワード'));
echo $this->Form->input('password_confirm',
     array('type'=>'password','label'=>'パスワード（再）'));
echo $this->Form->button('パスワード変更',array('class' => 'btn btn-primary'));
echo $this->Form->end();
?>
</div>
