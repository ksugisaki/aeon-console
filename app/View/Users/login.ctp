<div class ="hero-unit">
  <h2>ログイン認証</h2>
</div>
<?php echo $this->Session->flash(); ?>
<div class="row-fluid">
    <div class="span12">
    <?php echo $this->BootstrapForm->create('User', Array('url' => 'login')); ?>
        <fieldset>
        <?php echo $this->Session->flash('auth'); ?>
        <?php echo $this->BootstrapForm->input('loginid', Array('label' => 'ログインID')); ?>
        <?php echo $this->BootstrapForm->input('password', Array('label' => 'パスワード')); ?>
        </fieldset>
    <?php echo $this->BootstrapForm->button('ログイン',array('class' => 'btn btn-primary'));?>
    <?php echo $this->BootstrapForm->end();?>
    </div>

</div>
<div class="user-control">
  <?php echo $this->Html->link('パスワードを忘れた方はこちら','ForgotPassword',array());?><br />
  <?php echo $this->Html->link('ログインIDを忘れた方はこちら','ForgotLoginid',array());?>
</div>
<div class="user-control">
  <?php // echo $this->Html->link('新規ユーザ登録','add',array());?>
</div>
