<?php if( $print == null ){ ?>
<div class ="hero-unit">
<h2><?php echo $title_for_layout;?></h2>
</div>
<?php echo $this->Session->flash(); ?>

<?php }else{ ?>
<div class="row-fluid">
	<div class="span1">
	</div>
	<div class="span5">
		<h2><?php echo $title_for_layout ?></h2>
	</div>
	<div class="span6">
		<?php if( isset( $user_info['google_map_modified'])){ ?>
			<p>最終更新日： <?php echo( $user_info['google_map_modified'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<h3>Google Map掲載可否の設定</h3>
<p class="">下記のPDF資料をご参照の上、掲載の可否を設定してください。</p>
<a class="" target="_blank" href="<?php echo $this->Html->url( GOOGLEMAP_LINK );?>">
<i class="icon-file"></i>indoor google map 導入のご連絡(PDF)</a>
<br /><br />

<div class="row-fluid">
	<div class="span9">
	<?php echo $this->BootstrapForm->create('User', Array('url' => 'settingGoogleMap/'.$id )); ?>
		<fieldset><?php
			echo $this->BootstrapForm->input('id', Array('type' => 'hidden'));
			echo $this->BootstrapForm->input('allow_google_map', Array(
				'label' => false,
				'type' => 'radio',
				'default' => 0,
				'options' => array( 1=>'掲載可', 0=>'掲載不可')
			));
		?></fieldset>
		<?php
            if( empty( $locked )){
                echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
            }else{
                echo MESSAGE_AT_LOCKED;
            }
			echo $this->Form->end();
		?>
	</div>
</div>
