<div class ="hero-unit">
  <h2><?php echo $title_for_layout;?></h2>
</div>
<?php echo $this->Session->flash(); ?>
<h4>ご登録いただいたメールアドレスにログインIDをお知らせするメールを発行いたします。</h4>
<div class="user form">
	<?php
		echo $this->Form->create('User', array('url' => 'ForgotLoginid'));
		echo $this->Form->input('email', array('label' => 'メールアドレス'));
		echo $this->Form->end('ログインIDの確認');
	?>
</div>
