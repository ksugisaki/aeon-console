<div class ="hero-unit">
  <h2>ユーザー追加</h2>
</div>

<?php echo $this->Session->flash(); ?>

<div class="user form myform">
    <?php echo $this->Form->create('User', Array('class'=>'form-horizontal span10','url' =>'AddAuthorizedUser')); ?>

    <?php
        $must_comment = '<p class="offset2">※必須</p>';
    ?>

    <?php echo $this->Form->input('company', array('label'=>'会社名','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('zipcode', array('label'=>'郵便番号','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('address', array('label'=>'住所','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('company_phone', array('label'=>'電話番号','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('fax', array('label'=>'FAX','type'=>'text','class'=>'span8')); ?>
    <br />
    <hr />

    <?php echo $this->Form->input('email', array('label'=>'メールアドレス','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('department', array('label'=>'所属部署','type'=>'text','class'=>'span8')); ?>
    <br />
    <?php echo $this->Form->input('name_kana', array('label'=>'氏名（フリガナ）','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('name', array('label'=>'氏名','type'=>'text','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('phone', array('label'=>'ご担当者電話番号','type'=>'text','class'=>'span8')); ?>
    <br />
    <?php echo $this->Form->input('extension', array('label'=>'内線番号','type'=>'text','class'=>'span8')); ?>
    <br />

    <?php if( SUB_DOMAIN == 'wakayama'): ?>
      <?php echo $this->Form->input('block', array('label'=>'区画番号','type'=>'text','class'=>'span8')); ?>
      <p class="offset2">※必須。ログインIDに反映されます。</p>
    <?php endif; ?>

    <?php echo $this->Form->input('password', array('label'=>'パスワード','type'=>'password','class'=>'span8')); ?>
    <?php echo $must_comment; ?>
    <?php echo $this->Form->input('password_confirm',
               Array('type' => 'password','class'=>'span8', 'label'=>'パスワード（再）')); ?>
    <?php echo $must_comment; ?>

    <?php echo $this->Form->button('登録',array('class' => 'btn btn-primary offset2')); ?>
    <?php echo $this->Form->end(); ?>
</div>
