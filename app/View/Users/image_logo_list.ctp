<div class ="hero-unit">
  <h2>イメージ／ロゴ・リスト</h2>
</div>

<table class="table table-striped table-bordered table-condensed">
<?php
	$labels = array('ログインID','区画番号','会社名','店名','店舗ロゴ<br />ダウンンロード','媒体掲載用画像<br />ダウンンロード');
    //$labels[1] .= ' [プロフィール]';
    //$labels[5] .= ' [端末申込書]';
    // テーブル・ヘッダー
    foreach( $fields as $key => $field ){
	if( $field =='loginid'|| $field =='company'|| $field =='block'){
        $header_link = $this->Html->url('/Users/ImageLogoList/'.$field, true);
        $labels[ $key ] = $this->Html->link( $labels[ $key ], $header_link );
	}
    }

	//echo $this->Html->tableHeaders( $labels );
	echo '<tr>';
	foreach( $labels as $ix => $item ){
		echo '<th nowrap>';
		echo $item;
		echo '</th>';
	}
	echo '</tr>';

    // テーブル・ボディー
    for( $i = 0; $i < count( $all ); $i++ ){
        $users[] = $all[$i]['User'];

        // プロフィール編集へのリンクを追加
        if( true ){
            // プロフィールへのリンク
            $myrole = $this->Session->read('Auth.User.role');
            $edit_link = $this->Html->url('/Users/edit/'.$all[$i]['User']['id'].'?mode='. $myrole, true);
            $all[$i]['User']['company'] = $this->Html->link( $all[$i]['User']['company'], $edit_link);
            // IDの表示
            if( ! $all[$i]['User']['loginid']) $all[$i]['User']['loginid'] = '未設定';
			// 「フォーム」へのリンクを追加
			$user_link  = $this->Html->url('/users/edit/'.$all[$i]['User']['id'].'/print', true);
			$hearing_link  = $this->Html->url('/Hearings/index/'.$all[$i]['User']['id'].'/print', true);
			$hearing_link2 = $this->Html->url('/OptionHearings/checkprint/'.$all[$i]['User']['id'], true);
			$signboard_link = $this->Html->url('/Signboards/index/'.$all[$i]['User']['id'].'/print', true);

			// 店名を追加
			if( isset( $all[$i]['Signboard'])){
				$all_signboards = $all[$i]['Signboard'];
				// user_id重複しているデータを除く
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			if( isset( $signboard_last['shop_sign'])){
				$all[$i]['User']['shop_sign'] = $signboard_last['shop_sign'];
			}else{
				$all[$i]['User']['shop_sign'] = '';
			}

			// ダウンロードボタン
			$not_found_msg = '<span class="label label-important">要再登録</span>';
			if( isset( $all[$i]['OptionHearing'][0])){
				$option_hearing = $all[$i]['OptionHearing'][0];
				$oid = $option_hearing['id'];// OptionHearing.id
				$lid = $all[$i]['User']['loginid'];// LoginID
				$program = SITE_URL .'download.php' .'?oid='. $oid .'&lid='. $lid;
				$fname = basename( $this->upload->url($option_hearing, 'OptionHearing.image1', array()));
				$all[$i]['User']['logos'] = '';
				$all[$i]['User']['images'] = '';
                if( strstr( $fname, '.')):
                    $modified = isset( $option_hearing['image1_modified'])?
                      date('m/d(H:i)', $option_hearing['image1_modified']) : $not_found_msg;
                    $all[$i]['User']['images'] .=
                    '<a href="'. $program .'&fname='. $fname .'">画像１</a><br />'. $modified .'<br />';
                endif;
                $fname = basename( $this->upload->url($option_hearing, 'OptionHearing.image2', array()));
                if( strstr( $fname, '.')):
                    $modified = isset( $option_hearing['image2_modified'])?
                      date('m/d(H:i)', $option_hearing['image2_modified']) : $not_found_msg;
                    $all[$i]['User']['images'] .=
                    '<a href="'. $program .'&fname='. $fname .'">画像２</a><br />'. $modified .'<br />';
                endif;
                $fname = basename( $this->upload->url($option_hearing, 'OptionHearing.image3', array()));
                if( strstr( $fname, '.')):
                    $modified = isset( $option_hearing['image3_modified'])?
                      date('m/d(H:i)', $option_hearing['image3_modified']) : $not_found_msg;
                    $all[$i]['User']['images'] .=
                    '<a href="'. $program .'&fname='. $fname .'">画像３</a><br />'. $modified .'<br />';
                endif;
                $fname = basename( $this->upload->url($option_hearing, 'OptionHearing.logo', array()));
                if( strstr( $fname, '.')):
                    $modified = isset( $option_hearing['logo_modified'])?
                      date('m/d(H:i)', $option_hearing['logo_modified']) : $not_found_msg;
                    $all[$i]['User']['logos'] .=
                    '<a href="'. $program .'&fname='. $fname .'">ロゴ１</a><br />'. $modified .'<br />';
                endif;
                $fname = basename( $this->upload->url($option_hearing, 'OptionHearing.logo2', array()));
                if( strstr( $fname, '.')):
                    $modified = isset( $option_hearing['logo2_modified'])?
                      date('m/d(H:i)', $option_hearing['logo2_modified']) : $not_found_msg;
                    $all[$i]['User']['logos'] .=
                    '<a href="'. $program .'&fname='. $fname .'">ロゴ２</a><br />'. $modified .'<br />';
                endif;
			}else{
				$all[$i]['User']['logos'] = '';
				$all[$i]['User']['images'] = '';
			}
        }
        // 表の一行を作成
        unset( $all[$i]['User']['id']); // idは除く
        echo $this->Html->tableCells( $all[$i]['User']);
    }
?>
</table>
<!-- テーブルの5,6桁目（フォーム）のセルを折り返し無しにする -->
<script type="text/javascript">
$(function() {
    $("td:eq(4)").attr("nowrap", true);
    $("td:eq(5)").attr("nowrap", true);
});
</script>

<?php if( $print != null ): ?>
<script type="text/javascript">
    $(function() {
        $("a").contents().unwrap();
    });
</script>
<?php endif; ?>
