<div class ="hero-unit">
    <h2><?php echo $title_for_layout; ?></h2>
</div>
<table class="admin_list table table-striped table-bordered table-condensed">
<tr>
  <th>ID</th><th colspan="4">店名</th><th colspan="4">業種カテゴリー</th><th colspan="4">出店状況</th>
</tr>
<?php
    $labels = array('','店名<br />（最終）','決定/<br />未定/<br />修正','修正後','備考',
                    '業種カテゴリー<br />（最終）','決定/<br />未定/<br />修正','修正後','備考',
                    '出店状況<br />（最終）','決定/<br />未定/<br />修正','修正後','備考',);

    echo '<tr>';
    foreach( $labels as $ix => $item ){
        echo '<th nowrap>';
        echo $item;
        echo '</th>';
    }
    echo '</tr>';

    // テーブル・ボディー
    for( $i = 0; $i < count( $all ); $i++ ){
        $users[] = $all[$i]['User'];

        // プロフィール編集へのリンクを追加
        if( $userinfo['role'] == 'admin' || $userinfo['role'] == 'partner' ){
            // プロフィールへのリンク
            //$myrole = $this->Session->read('Auth.User.role');
            //$edit_profile = $this->Html->url('/users/edit/'.$all[$i]['User']['id'].'?mode='. $myrole, true);
            //if( $userinfo['role'] == 'admin')
            //    $all[$i]['User']['name'] = $this->Html->link( $all[$i]['User']['name'], $edit_profile);

            // IDの表示
            if( ! $all[$i]['User']['loginid']){
                $all[$i]['User']['loginid'] = '未設定';
            }else{
                // 最終確認ページへのリンク
                $last_shop_link = $this->Html->url('/users/LastShopSign/'.$all[$i]['User']['id'], true);
                $all[$i]['User']['loginid'] = $this->Html->link( $all[$i]['User']['loginid'], $last_shop_link);
            }

            $email = $all[$i]['User']['email'];
            $role = $all[$i]['User']['role'];

            // カラムの順番を変更
            $row = array(
                'loginid'=> $all[$i]['User']['loginid'],

                'shop'=> $all[$i]['User']['shop_sign_last'],
                'shop_kakutei'=> $all[$i]['User']['shop_sign_kakutei'],
                'shop_syuusei'=> $all[$i]['User']['shop_sign_syuusei'],
                'shop_remark'=> $all[$i]['User']['shop_sign_remark'],

                'category'=> $all[$i]['User']['shop_category_last'],
                'category_kakutei'=> $all[$i]['User']['shop_category_kakutei'],
                'category_syuusei'=> $all[$i]['User']['shop_category_syuusei'],
                'category_remark'=> $all[$i]['User']['shop_category_remark'],

                'syutten'=> $all[$i]['User']['syutten_jyoukyou_last'],
                'syutten_kakutei'=> $all[$i]['User']['syutten_jyoukyou_kakutei'],
                'syutten_syuusei'=> $all[$i]['User']['syutten_jyoukyou_syuusei'],
                'syutten_remark'=> $all[$i]['User']['syutten_jyoukyou_remark'],

                'id'=> $all[$i]['User']['id']);
                // 'name'=> $all[$i]['User']['name'],
            $all[$i]['User'] = $row;
        }
        // 表を作成
        unset( $all[$i]['User']['id']); // idは除く
        unset( $all[$i]['User']['role']); // roleは除く
        if( $userinfo['role'] != 'admin') // 削除は管理者のみ
            unset( $all[$i]['User']['delete']);
        $tr = $this->Html->tableCells( $all[$i]['User']);
        if( $role == 'partner' )
            $tr = str_replace( '<tr>', '<tr class="partner">', $tr);
        else if( ! empty( $role))
            $tr = str_replace( '<tr>', '<tr class="admin">', $tr);
        // 表1行を出力
        echo( $tr );
    }
?>
</table>
