<div class ="hero-unit">
  <h2>パスワード変更</h2>
</div>
<?php echo $this->Session->flash(); ?>
<div class="user form">
<?php
    echo $this->Form->create('User', array('url'=>'/Users/ResetPassword?email='. $email .'&authkey='. $authkey));
    if( isset( $this->request->data['User'])){ // postから戻ってきた場合
        echo $this->Form->input('loginid',array('type'=>'hidden'));
        echo 'ID:'. $this->request->data['User']['loginid'] .' 　 '. $this->request->data['User']['name'] .' 様';
    }else{
        echo '<h5>ID選択</h5>';
        echo '<p>複数のIDをお持ちの場合は、パスワード変更対象のIDを選択してください。</p>';
        $attributes = array('legend' => false);
        $options = array();
        foreach( $this->request->data as $key => $user ){
            $id      = $user['User']['id'];
            $loginid = $user['User']['loginid'];
            if( empty( $loginid )) $loginid = '(未登録)';
            $options[ $id ] = 'ID:'. $loginid .' 　 '. $user['User']['name'] .' 様';
            if( $key == 0 ) $attributes['value'] = $id;
        }
        echo $this->Form->radio('id', $options, $attributes);
    }
    echo '<br />';

    echo $this->Form->input('authkey',  array('type'=>'hidden'));
    echo $this->Form->input('password',         
        array('type'=>'password','label'=>'パスワード','value'=>''));
    echo $this->Form->input('password_confirm',
        array('type'=>'password','label'=>'パスワード（再）','value'=>''));
    echo $this->Form->end('パスワード変更');
?>
</div>
<?php //debug($this->request); ?>