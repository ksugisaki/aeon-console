<?php
    //データ
    foreach( $users as $key => $user ){
        unset( $user['User']['id']); // IDは除く
        $td[] = $user['User'];
    }
    //ファイル名設定
    $this->Csv->setFilename( $filename );

    //ヘッダー行を追加
    $this->Csv->addRow( $th );

    //表の各行を追加していく
    foreach( $td as $t ){
        $this->Csv->addRow( $t );
    }

    //文字コードをUTF-8からSJISに変換して出力。
    echo mb_convert_encoding( $this->Csv->render(), 'SJIS', 'UTF-8');

