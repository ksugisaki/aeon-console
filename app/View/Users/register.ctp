<?php
    $labels1 = array('id','authkey','会社名','郵便番号','住所','電話番号','FAX');
    $fields1 = array('id','authkey','company','zipcode','address','company_phone','fax');
    $labels2 = array('メールアドレス','所属部署','お名前（フリガナ）','お名前','ご担当者電話番号','内線番号');
    $fields2 = array('email','department','name_kana','name','phone','extension');
    $labels3 = array('パスワード','パスワード（再入力）');
    $fields3 = array('password','password_confirm');
    $labels = array_merge( $labels1, $labels2, $labels3 );
    $fields = array_merge( $fields1, $fields2, $fields3 );
    $key = 0;
?>

<div class="hero-unit">
  <div>
    <img src="<?php echo $this->Html->url('/img/step3.jpg') ?>" />
  </div>
  <br />
  <h2>3.情報登録・更新</h2>
</div>
<?php echo $this->Session->flash(); ?>

<?php if( ! $enable_register ): ?>
    <div class="span11 alert alert-block">
	<strong>
	<p>情報登録・更新可能な期間（２４時間）を過ぎましたので、ご登録いただいたメールアドレスは削除いたしました。</p>
	<p>お手数ですがもう一度、新規登録からお手続をお願いいたします。</p>
	</strong>
    </div>'
<?php else: ?>

<?php echo $this->Form->create('User', array('class'=>'register form-horizontal span12','action'=>'register'));?>
<div class="alert alert-info span10">
<h3>御社情報</h3>
</div>

<?php echo $this->Form->input( $fields[ $key++ ], array('type'=>'hidden'));?>
<?php echo $this->Form->input( $fields[ $key++ ], array('type'=>'hidden'));?>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）イオンモール株式会社</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）2618539 ・・・７文字の半角数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）0312345678 ・・・１４文字以内の数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">（例）0312345678 ・・・１４文字以内の数字で入力してください。</p>

<br /><br />
<div class="alert alert-info span10">
<h3>ご担当者情報</h3>
</div>

<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8','readonly'=>true));?>
    <br /><!-- メールアドレス -->
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">（例）営業部</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）ヤマダタロウ</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）山田太郎</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">上記御社情報と異なる場合にご入力下さい。 ・・・１４文字以内の数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">もしあればご入力下さい。 ・・・１４文字以内の数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8','type'=>'password'));?>
    <p class="offset2">※ ８文字以上の半角英数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8','type'=>'password'));?>
    <p class="offset2">※ 同じパスワードをここにも入力してください。</p>

<br />
<?php echo $this->Form->button('登録・更新',array('class' => 'btn btn-primary offset2'));?>
<?php echo $this->Form->end();?>
<?php endif; // $enable_register ?>
<br />
<br />
<br />
