<?php
    $labels1 = array('id','authkey','会社名','郵便番号','住所','電話番号','FAX');
    $fields1 = array('id','authkey','company','zipcode','address','company_phone','fax');
    $labels2 = array('メールアドレス','所属部署','お名前（フリガナ）','お名前','ご担当者電話番号','内線番号');
    $fields2 = array('email','department','name_kana','name','phone','extension');
    $labels = array_merge( $labels1, $labels2 );
    $fields = array_merge( $fields1, $fields2 );
    if( $role == 'admin'){
        array_push( $labels, '属性','ユーザー・ステータス','サービス・ステータス');
        array_push( $fields, 'role','user_status','service_status');
    }
    $key = 0;
?>
<?php if( $print == null ){ ?>
<div class="hero-unit">
	<h2><?php echo $title_for_layout ?></h2>
	<p>ご登録いただいた御社情報をご確認／編集できます。</p>
</div>

<?php }else{ ?>
<div class="row-fluid">
	<div class="span1">
	</div>
	<div class="span5">
		<h2><?php echo $title_for_layout ?></h2>
	</div>
	<div class="span6">
		<?php if( isset( $user_info['profile_modified'])){ ?>
			<p>最終更新日： <?php echo( $user_info['profile_modified'] );?></p>
		<?php }else{ ?>
			<p>最終更新日： <?php echo( $user_info['created'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php echo $this->Session->flash(); ?>
<?php echo $this->Form->create('User', array('class'=>'edit form-horizontal span12','action'=>'edit'));?>

<div class="alert alert-info span10">
<h3>御社情報</h3>
</div>

<?php echo $this->Form->input( $fields[ $key++ ], array('type'=>'hidden'));?>
<?php echo $this->Form->input( $fields[ $key++ ], array('type'=>'hidden'));?>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）イオンモール株式会社</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）2618539 ・・・７文字の半角数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）千葉県千葉市美浜区中瀬一丁目5番地1</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）0312345678 ・・・１４文字以内の数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">（例）0312345678 ・・・１４文字以内の数字で入力してください。</p>
<br /><br />

<div class="alert alert-info span10">
<h3>ご担当者情報</h3>
</div>
<?php if( !empty($user_info['loginid'])): ?>
    <?php echo $this->Form->input('loginid',
        array('label'=>'ログインID','readonly'=>true,'class'=>'span8'));?>
    <br />
<?php endif; ?>

<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）yamada.tarou@xxxx.com</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">（例）営業部</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）ヤマダタロウ</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">※ （例）山田太郎</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">上記御社情報と異なる場合にご入力下さい。 ・・・１４文字以内の数字で入力してください。</p>
<?php echo $this->Form->input( $fields[ $key ], array('label'=>$labels[ $key++ ],'class'=>'span8'));?>
    <p class="offset2">もしあればご入力下さい。 ・・・１４文字以内の数字で入力してください。</p>

<?php /* 副メールアドレスの表示 */
if( true ){
    foreach ( $submails as $key => $submail ){
        if( preg_match('/^MailAddress\.\d+\./', $submail )):
            if( preg_match('/^MailAddress\.\d+\.id$/', $submail )){
                echo $this->Form->input( $submail, array('type'=>'hidden'));
            }else{
                echo
                '<table><tr><td>'
                . $this->Form->input( $submail,
                      array('label'=>'副メールアドレス','type'=>'text','readonly'=>'true','class'=>'span8'))
                . '</td><td>'
                . '　削除'
                . '</td><td>'
                . $this->Form->checkbox( $submail,
                      array('checked'=> false,'value'=>'delete','class'=>'pull-right'))
                . '</td>'
                . '</tr></table>'
                ;
            }
        endif;
    }
}
?>
<br /><br />
<?php /* ステータス表示 */
if( $role == 'admin'){ // 管理者のみ
		// 幕張、東員、和歌山のみ区画番号を追加
		switch( SUB_DOMAIN ){
		case 'makuharishintoshin':
		case 'toin':
		case 'wakayama':
			echo '<br />';
			echo '<div class="alert alert-info span10">';
			echo '  <h3>区画番号／テナントコード</h3>';
			echo '</div>';
			echo $this->Form->input('block', array('label'=>'区画番号','class'=>'span3'));
            if( SUB_DOMAIN == 'wakayama')
                echo '<p class="offset2">※ 区画番号に連動して、ログインIDも変りますので、ご注意下さい。</p>';
			echo '<br />';
			echo $this->Form->input('tenant_code', array('label'=>'テナントコード','class'=>'span3'));
			echo '<br /><br />';
			break;
		}
        echo '<div class="alert alert-info span10">';
        echo '<h3>ステータス表示（管理者用）</h3>';
        echo '</div>';

        echo '<h4 class="clear offset1">管理者権限</h4>';
        echo $this->Form->select('role', array(''=>'テナント', 'partner'=>'協力会社', 'admin'=>'管理者'),
                                  array('class'=>'offset2','label'=>'権限'));
        echo '<p class="offset2">※権限を 管理者／協力会社／テナント から選択してください。</p>';

        // =========================
        // 権限
        // =========================
		$permission_list = array();
		if( defined('LAST_SHOP_SIGN'))
			$permission_list['users_last_shop']='【店名最終確認】';
		$permission_list['users_edit']='【御社情報】';
		$permission_list['sections']='【担当者確認シート】';
		switch( SUB_DOMAIN ):
		case 'makuharishintoshin':
		case 'toin':
			$permission_list['hearings']='【ヒアリングシート】';
			$permission_list['option_hearings']='【ヒアリングシート２】';
			$permission_list['signboards']='【店名確認シート】';
		break;
		default:
		  // ヒアリングシートなし
		  // 店名確認シートなし
		endswitch;
		if( defined('SYOKUJYU_LINK'))
			$permission_list['syokujyu_items']='【植樹祭参加者登録】';
		if( defined('TERMINAL_COMPANY')){
			$permission_list['users_terminal']='【入力用端末申込書】';
			$permission_list['users_receipt']='【レシート用情報】';
		}
		if( defined('GOOGLEMAP_LINK'))
			$permission_list['users_google_map']='【Google Mapについて】';

		if( defined('FUKUBUKURO'))
			$permission_list['fukubukuros']='【福袋について】';
		if( defined('LAST_HANSOKU'))
			$permission_list['last_hansokus']='【販促企画最終確認】';
		if( defined('MEDIA_STATION'))
			$permission_list['media_stations']='【メディアステーション・イオンモールメンバーズ説明会】';
		if( defined('MOCK_OPERATIONS'))
			$permission_list['mock_operations']='【模擬営業、店頭準備についてのアンケート】';
		if( defined('CHRISTMAS_FEATURES'))
			$permission_list['christmas_features']='【クリスマスギフト特集・クリスマスグルメ特集】';
		if( defined( 'PERMIT_CARDS' ))
			$permission_list['permit_cards']=	PERMIT_CARDS;
		if( defined( 'PRE_CAR_PORTS' ))
			$permission_list['pre_car_ports']=	PRE_CAR_PORTS;
		if( defined( 'CAR_PORTS' ))
			$permission_list['car_ports']=	CAR_PORTS;
		if( defined( 'BICYCLES' ))
			$permission_list['bicycles']=	BICYCLES;
		if( defined( 'LOCKERS' ))
			$permission_list['lockers']=	LOCKERS;
		if( defined( 'CASHBOXES' ))
			$permission_list['cashboxes']=	 CASHBOXES;
		if( defined( 'IRE_DRILLS' ))
			$permission_list['fire_drills']=FIRE_DRILLS;
		if( defined( 'PERATION_BRIEFINGS' ))
			$permission_list['operation_briefing']=OPERATION_BRIEFINGS;

		if( defined('ADD_AUTHORIZED_USER')){
			echo '<h4 class="clear offset1">閲覧権限</h4>';
			echo '<p class="clear offset1">各ページの閲覧（Read）権限を設定できます。</p>';
			echo $this->Form->input('read_permission', array(
				'label'		=> false,
				'type'		=> 'select' ,
				'multiple'	=> 'checkbox',
				'options'	=> $permission_list
			));
			echo '<h4 class="clear offset1">編集権限</h4>';
			echo '<p class="clear offset1">各ページの編集（Read + Write）権限を設定できます。</p>';
			echo $this->Form->input('write_permission', array(
				'label'		=> false,
				'type'		=> 'select' ,
				'multiple'	=> 'checkbox',
				'options'	=> $permission_list
			));
		}

		echo '<h4 class="clear offset1">ユーザー状態</h4>';
		if( ! isset( $user_info['user_status'])){
			echo '<p class="offset2"><strong>メール認証開始</strong></p>';
		}else{
			switch( $user_info['user_status'] ){
			case 'authbacked': echo '<p class="offset2"><strong>メール認証中</strong></p>';
					break;
			case 'registered': echo '<p class="offset2"><strong>承認待ち</strong></p>';
					break;
			case 'authorized': echo '<p class="offset2"><strong>承認済</strong></p>';
					break;
			case 'rejected': echo '<p class="offset2"><strong>却下</strong></p>';
					break;
			}
		}
        echo '<p class="offset2">※ユーザー状態は変更できません。</p>';
}
?>
<br /><hr /><br />

<?php if( $locked ): ?>
<?php echo MESSAGE_AT_LOCKED;?>
<?php else: ?>
<?php echo $this->Form->button('登録',array('class' => 'btn btn-primary offset1'));?>
<?php endif; ?>

<?php echo $this->Form->end();?>

<div id="changepassword" class="span12">
<?php
    echo $this->Html->link('パスワード変更',
        '/Users/ChangePassword/'. $this->data['User']['id'],
        array('class'=>'btn btn-primary offset1')
    );
?>
</div>
<p class="clear"></p>
<br />
<br />
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
