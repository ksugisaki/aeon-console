<div class ="hero-unit">
	<h2>消防提出書類</h2>
	<i class="icon-file"></i>
	<large>
	<a href="<?php echo $this->Html->url('/files/makuharishintoshin/operation_briefing_1st_2013_1007.pdf#page=45'); ?>">第１回運営管理説明会資料(PDF)</a>
	p.45〜p.49参照
	</large>
</div>
<?php echo $this->Session->flash(); ?>

<p>・イオンモールが取りまとめ、美浜消防署へ提出する書類です。一式揃えて、<strong>原紙</strong>をご送付下さい。<br />
　※セットにしてご提出下さい。</p>
<p>・事業所消防計画については、現在、美浜消防署と内容を協議中ですので、協議が完了次第、<br />
　事業所消防計画のフォーマットと記入方法をアップロード致します。</p>
<p>・記入見本をしっかり確認し、漏れのないように記入、押印をお願い致します。</p>
<br />

<?php $paper_mark = '<span class="label label-success"> 原紙 </span>'; ?>
<table class="table table-bordered table-striped">
<tr><td>分類</td><td colspan="2">消防 OP提出⑩ <?php echo $paper_mark; ?></td>
</tr>

<tr><td rowspan="6">提出資料名</td>
<td colspan="2"><a target="_blank" href="<?php echo $this->Html->url('/files/makuharishintoshin/syoubou_set_2013_1001.pdf'); ?>"><i class="icon-file"></i>消防提出書類(PDF)</a>
</td></tr>
<tr><td>消防提出書類　送付チェックシート</td><td><span class="badge badge-success">1部</span></td></tr>
<tr><td>防火・防災管理者選任（解任）届出書</td><td><span class="badge badge-success">2部</span></td></tr>
<tr><td>講習機関発行の甲種防火管理者及び防災管理者資格証明書のコピー</td><td><span class="badge badge-success">2部</span></td></tr>
<tr><td>消防計画作成（変更）届出書</td><td><span class="badge badge-success">2部</span></td></tr>
<tr><td>イオンモール幕張新都心　事業所消防計画<br />
※事業所消防計画は区画の大きさと構成によりフォーマットが異なりますので、下記の表をご参照ください。
</td><td><span class="badge badge-success">2部</span></td></tr>

</table>
<br /><br />

<h3>事業所消防計画</h3>

<table class="table table-bordered table-striped">
<tr><th>店舗名</th><th>消防提出書類</th></tr>
<tr><td>
<h4>大型店舗２フロア以上</h4>
<ul>
  <li>ＰＥＣＯＳ</li>
  <li>ノジマ</li>
  <li>スポーツオーソリティー</li>
</ul>
</td><td style="vertical-align: bottom;">
<a target="_blank" href="<?php echo $this->Html->url(
'/files/makuharishintoshin/syoubou_keikaku_2floor.pdf'); ?>">
<i class="icon-file"></i>
大型店舗２フロア以上の店舗様、消防提出書類(PDF)
</a>
</td></tr>

<tr><td>
<h4>大型店舗１フロア</h4>
<ul>
  <li>（仮）Ｈ＆Ｍ</li>
  <li>イオンシネマ</li>
  <li>（仮）namco</li>
  <li>よしもと幕張イオンモール劇場</li>
  <li>トイザらス・ベビザらス</li>
  <li>未来屋書店</li>
  <li>ユニクロ</li>
  <li>（仮）キッズ共和国</li>
  <li>モーリーファンタジー</li>
  <li>東映ヒーローワールド</li>
  <li>カンドゥー</li>
</ul>
</td><td style="vertical-align: bottom;">
<a target="_blank" href="<?php echo $this->Html->url(
'/files/makuharishintoshin/syoubou_keikaku_1floor.pdf'); ?>">
<i class="icon-file"></i>
大型店舗１フロアの店舗様、消防提出書類(PDF)
</a>
</td></tr>

<tr><td>
<h4>その他</h4>
<ul>
  <li>上記以外の店舗</li>
</ul>
</td><td style="vertical-align: bottom;">
<a target="_blank" href="<?php echo $this->Html->url(
'/files/makuharishintoshin/syoubou_keikaku_other.pdf'); ?>">
<i class="icon-file"></i>
その他の店舗様、消防提出書類(PDF)
</a>
</td></tr>
</table>
