<?php

    foreach( $users as $s_key => $user ){
        // Signboardの重複しているデータを除く
        if( isset( $user['Signboard'])){
            $all_signboards = $user['Signboard'];
            $signboard_last = array();
            $prev_modified = 0;
            foreach( $all_signboards as $signboard){
                if( $prev_modified == 0 )
                    $signboard_last = $signboard;
                else if( $signboard['modified'] > $prev_modified ) // 新しいなら
                    $signboard_last = $signboard; // 上書き
                $prev_modified = $signboard['modified'];
            }
        }
        // idの桁には、店名を入れて表示する
        if( isset( $signboard_last['shop_sign']))
            $user['User']['id'] = $signboard_last['shop_sign'];
        else
            $user['User']['id'] = '';

        $td[] = $user['User'];

    }

    //ファイル名設定
    $this->Csv->setFilename( $filename );

    //ヘッダー行を追加
    $this->Csv->addRow( $th );

    //表の各行を追加していく
    foreach( $td as $t ){
        $this->Csv->addRow( $t );
    }

    //文字コードをUTF-8からSJISに変換して出力。
    echo mb_convert_encoding( $this->Csv->render(), 'SJIS', 'UTF-8');

