<?php echo $this->Html->script(array('mailing_list'), array('inline'=>false)); ?>
<div class ="hero-unit">
  <h2><?php echo $title_for_layout;?></h2>
  <!--<p>あなたの登録メールアドレスは <?php echo h( $userinfo['email']);?> です。</p>-->
</div>
<p>※管理者様は薄黄の背景色、協力会社様は薄緑の背景色で表示されます。</p>

<?php $all_check = $this->Form->input('check_all', array(
						'id'=>'check_select',
						'label'=> 'ALL',
						'type'=>'checkbox','div'=>false)); ?>

<?php echo $this->BootstrapForm->create('User', Array('url' => 'MailingList'));?>

<table class="mailing_list table table-striped table-bordered table-condensed">

<?php
	// テーブル・ヘッダー
	foreach( $fields as $key => $field ){
		$header_link = $this->Html->url('/users/MailingList/'.$field, true);
		$labels[ $key ] = $this->Html->link( $labels[ $key ], $header_link );
	}
	$labels[0] .= $all_check;
	echo $this->Html->tableHeaders( $labels );

	// テーブル・ボディー
	for( $i = 0; $i < count( $all ); $i++ ){
		$users[] = $all[$i]['User'];
		// チェックボックス
		$radio_box = $this->BootstrapForm->input( $all[$i]['User']['id'], Array(
			'class' => 'check_select',
			'label' => false,
			'type' => 'checkbox',
			'default' => 0,
			'options' => array('1'=>'1')
		));
		$all[$i]['User']['id'] = $radio_box;
		// 店名を追加
		if( isset( $all[$i]['Signboard'])){
			$all_signboards = $all[$i]['Signboard'];
			// user_id重複しているデータを除く
			$signboard_last = array();
			$prev_modified = 0;
			foreach( $all_signboards as $signboard){
				if( $prev_modified == 0 )
					$signboard_last = $signboard;
				else if( $signboard['modified'] > $prev_modified ) // 新しいなら
					$signboard_last = $signboard; // 上書き
				$prev_modified = $signboard['modified'];
			}
		}
		if( isset( $signboard_last['shop_sign'])){
			$all[$i]['User']['shop'] = $signboard_last['shop_sign'];
		}else{
			$all[$i]['User']['shop'] = '';
		}
		// プロフィール編集へのリンクを追加
		if( $userinfo['role'] == 'admin' ){
			// 属性の表示
			if( $all[$i]['User']['role']=='admin') $all[$i]['User']['role'] = '管理者';
		}
		// カラムの順番を変更
		$row = array(
			'id'=> $all[$i]['User']['id'],
			'loginid'=> $all[$i]['User']['loginid'],
			'company'=> $all[$i]['User']['company'],
			'shop'=> $all[$i]['User']['shop'],
			'phone'=> $all[$i]['User']['phone'],
			'name'=> $all[$i]['User']['name'],
			'email'=> $all[$i]['User']['email']
		);
		// 表を作成
		$tr = $this->Html->tableCells( $row );
		$role = $all[$i]['User']['role'];
		if( $role == 'partner' ) // 色つけ用のクラス名
			$tr = str_replace( '<tr>', '<tr class="partner">', $tr);
		else if( ! empty( $role))
			$tr = str_replace( '<tr>', '<tr class="admin">', $tr);
		echo( $tr );
	}
?>
</table>

<h3>送信元メールアドレス</h3>
	<?php echo $this->Form->input('mail_from',
		array('label'=>false,'type'=>'text','class'=>'span8','value'=>$userinfo['email']));?>
<h3>件名</h3>
<?php echo $this->Form->input('mail_subject',
	array('label'=>false,'type'=>'text','class'=>'span8'));?>
<h3>内容</h3>
<?php echo $this->Form->input('mail_body',
	array('label'=>false,'type'=>'text','class'=>'span8','rows'=>'8'));?>

<?php echo $this->Form->button('送信',array('class' => 'btn btn-primary'));?>
<?php echo $this->Form->end();?>
