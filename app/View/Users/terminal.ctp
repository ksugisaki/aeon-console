<?php if( $print == null ){ ?>
<div class ="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
  <p>この申込書は内装設計説明会においてご案内致しました入力用端末機の申込書となります。</p>
  <p>本書に申込み台数を記入して下さい。</p>
</div>

<?php }else{ ?>
<div class="row-fluid">
	<div class="span1">
	</div>
	<div class="span5">
		<h2><?php echo $title_for_layout ?></h2>
	</div>
	<div class="span6">
		<?php if( isset( $user_info['terminal_modified'])){ ?>
			<p>最終更新日： <?php echo( $user_info['terminal_modified'] );?></p>
		<?php }else{ ?>
			<p>最終更新日： <?php echo( $user_info['created'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php echo $this->Session->flash(); ?>
<?php
    echo $this->Form->create('User', array('class'=>'terminal form-horizontal span10','action'=>'terminal'));
	// テーブルの中身を準備( $cells )
    $cells = array();
    foreach ( $fields as $key => $field ){
        if( $field == 'id' ):
            echo $this->Form->input('id', array('type'=>'hidden'));
        else: // 例など注意書きを追記
            $input = $this->Form->input( $field, array('label'=>'','type'=>'text','class'=>'span8'));
            if( $field == 'regi_num' )
                $input .= '<p>※ （例） 1 ・・・半角数字だけで入力してください。</p>';
            else if( $field == 'terminal_num' )
                $input .= '<p>※ （例） 2 ・・・半角数字だけで入力してください。</p>';
            else if( $field == 'receipt_tenant_name' )
                $input .= '<p>※ （例） ◯◯ショップ△△店 ・・・２６文字以内で入力してください。</p>';
            else if( $field == 'receipt_phone' )
                $input .= '<p>※ （例） 031234567 ・・・１４文字以内の半角数字で入力してください。</p>';
			else if( $field == 'contact_name' )
				$input .= '※ （例）山田太郎';
			else if( $field == 'contact_name_kana' )
				$input .= '※ （例）ヤマダタロウ';
			else if( $field == 'contact_department' )
				$input .= '※ （例）営業部';
			else if( $field == 'contact_position' )
				$input .= '※ （例）営業マネージャー';
			else if( $field == 'contact_zipcode' )
				$input .= '※ （例）2618539 ・・・７文字の半角数字で入力してください。';
			else if( $field == 'contact_address' )
				$input .= '※ （例）千葉県千葉市美浜区中瀬一丁目5番地1';
			else if( $field == 'contact_phone' )
				$input .= '※ （例）0312345678 ・・・１４文字以内の数字で入力してください。';
			else if( $field == 'contact_extension' )
				$input .= '※ （例）0123 ・・・１４文字以内の数字で入力してください。';
			else if( $field == 'contact_fax' )
				$input .= '※ （例）0312345679 ・・・１４文字以内の数字で入力してください。';
			else if( $field == 'contact_cellphone' )
				$input .= '※ （例）09012345678 ・・・１４文字以内の数字で入力してください。';
			else if( $field == 'contact_email' )
				$input .= '※ （例）yamada@xxxx.com ・・・メールアドレスを入力してください。';
            else
                $input .= '<p></p>';

            $cells[] = array(
                 '<div class="span2">' . $labels[ $key ] . '</div>',
                 $input
            );
        endif;

		// テーブルとヘッダーを出力
        if( $field == 'regi_num' ):
            echo '<h4>■持込みレジ台数</h4>';
            echo '<table class="communication table table-bordered">';
            echo $this->Html->tableCells( $cells );
            echo '</table>';
            $cells = array();
            if( empty( $locked )){
                echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
            }else{
                echo MESSAGE_AT_LOCKED;
            }
			echo '<br /><br />';

        elseif( $field == 'terminal_num' ):
            echo '<br />';
            echo '<h4>■申し込み台数</h4>';
            echo '<table class="communication table table-bordered">';
            echo $this->Html->tableCells( $cells );
            echo '</table>';
            $cells = array();
            if( empty( $locked )){
                echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
            }else{
                echo MESSAGE_AT_LOCKED;
            }
			echo '<br /><br />';

        elseif( $field == 'receipt_phone' ):
            echo '<br />';
            echo '<h4>■レシート用情報</h4>';
            echo '<table class="communication table table-bordered">';
            echo $this->Html->tableCells( $cells );
            echo '</table>';
            $cells = array();
            if( empty( $locked )){
                echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
            }else{
                echo MESSAGE_AT_LOCKED;
            }
			echo '<br /><br />';

		elseif( $field == 'contact_position' ):
			echo '<br />';
			echo '<h4>■ご担当者情報</h4>';
			echo '<table class="communication table table-bordered">';
			echo $this->Html->tableCells( $cells );
			echo '</table>';
			$cells = array();
            if( empty( $locked )){
                echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
            }else{
                echo MESSAGE_AT_LOCKED;
            }
			echo '<br /><br />';

		elseif( $field == 'contact_email' ):
			echo '<br />';
			echo '<h5>※【御社情報】でご登録頂いたご連絡先と同じ場合は、下記の入力は不要です。<br />　異なる場合にご入力ください。</h5>';
			echo $this->Html->link('御社情報登録はこちら',
				 '/users/edit',array('class'=>'pull-right'));
			echo '<br />';
			echo '<br />';
			echo '<table class="communication table table-bordered">';
			echo $this->Html->tableCells( $cells );
			echo '</table>';
			$cells = array();

        endif;
    }
    echo '<br />';
    if( empty( $locked )){
        echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
    }else{
        echo MESSAGE_AT_LOCKED;
    }
    echo '<br /><br />';

    if( $role =='admin'|| $role =='partner'){
        echo '<br />';
        echo '<h4>■備考</h4>';
        echo '<p>管理者、及び協力会社のみ閲覧／編集できる備考欄です。</p>';
        $cells = array('<div class="span2">備考</div>',
          $this->Form->input('terminal_remark', array('label'=>'','type'=>'text','class'=>'span8','rows'=>'5'))
        );
        echo '<table class="communication table table-bordered">';
        echo $this->Html->tableCells( $cells );
        echo '</table>';
        if( empty( $locked )){
            echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
        }else{
            echo MESSAGE_AT_LOCKED;
        }
        echo '<br /><br />';
    }

    echo $this->Form->end();
?>
<br />
<br />
<br />
<!-- モーダル -->
<div class="modal hide" id="mymodal">
  <div class="modal-header">
    <div>登録結果</div>
    <br />
  </div>
  <div class="modal-body">
    <p id="modal_message"></p>
  </div>
  <div class="modal-footer">
    <button id="close_mymodal" data-dismiss="modal">OK</button>
  </div>
</div>
<p class="clear"></p>
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
