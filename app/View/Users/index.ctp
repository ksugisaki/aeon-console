<div class ="hero-unit">
  <h2><?php echo MALL_NAME;?><br />コミュニケーションサイトへようこそ</h2>
  <p>本サイトでは、出店情報の登録・編集を行えます。</p>
</div>
<?php
	switch( SUB_DOMAIN ):
	case 'testsite':
		$nyuuryoku_kigen = array('○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）','○月○日（ ）');
		break;

	case 'toin':
		$nyuuryoku_kigen = array('8月23日（金）','8月23日（金）','8月23日（金）','8月23日（金）','8月23日（金）','9月20日（金）','-','-','8月23日（金）');
		$nyuuryoku_kigen[] = '10月13日（日）'; // 福袋
		$nyuuryoku_kigen[] = '10月13日（日）'; // 販促企画最終確認
		$nyuuryoku_kigen[] = '10月20日（日）'; // メディアステーション
		break;

	case 'makuharishintoshin':
		$nyuuryoku_kigen = array('8月29日(木)','8月29日(木)','8月29日(木)','8月29日(木)','8月29日(木)');
        break;

	case 'wakayama':
		$nyuuryoku_kigen = array('11月7日（木）','11月7日（木）','11月7日（木）','11月7日（木）','','','','','','','');
        break;

	default:
		$nyuuryoku_kigen = array('未定','未定','未定','未定','未定','未定','未定','未定','未定','未定','未定','未定','未定','未定');
		break;
	endswitch;
?>
<?php echo $this->Session->flash(); ?>
<?php if(!$this->Session->check('Auth.User') ){ ?>
  <?php if( SUB_DOMAIN !='wakayama'): // 和歌山は、新規登録はなし ?>
  <?php echo $this->Html->link('新規登録','agreement',array('class'=>'btn btn-primary btn-large')); ?>
  <?php endif; ?>
  <?php echo $this->Html->link('ログイン','login',array('class'=>'btn btn-primary btn-large')); ?>
<?php }else{ ?><!-- 認証済 -->

<?php if( defined('LAST_SHOP_SIGN')): ?>
<h3>店名最終確認</h3>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
			<th nowrap width="20px">No.</th>
            <th nowrap width="160px">入力シート項目</th>
            <th nowrap>登録内容詳細</th>
            <th nowrap>問い合わせ先</th>
            <th nowrap>入力期限</th>
            <th nowrap>ステータス</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><span style="color:red">重要</span></td>
            <td><?php echo $this->Html->link('【店名最終確認】'
                    ,array('controller'=>'users','action'=>'LastShopSign')); ?></td>
            <td>11月中旬～下旬発表予定の出店店舗名の最終リリース及び館内フロアマップ、フロアガイド等に使用いたしますので、
                店名の最終確認をお願い致します。<br />
                <strong>これ以降の修正はできませんのでご注意ください。</strong>
            </td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
            <td><span style="color:red">10月25日（金）</span></td>
            <td> - </td>
        </tr>
    </tbody>
</table>
<?php endif; ?>

<h3>業務メニュー</h3>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
			<th nowrap width="20px">No.</th>
            <th nowrap width="160px">入力シート項目</th>
            <th nowrap>登録内容詳細</th>
            <th nowrap>問い合わせ先</th>
            <th nowrap>入力期限</th>
            <th nowrap>ステータス</th>
        </tr>
    </thead>
    <tbody>
        <tr>
			<?php $row_num = 1;?>
			<td><?php echo $row_num;?></td>
            <td><?php echo $this->Html->link('【御社情報】','edit'); ?></td>
            <td>
                ご登録いただいた御社情報をご確認／編集できます。
            </td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
            <td> - </td>
        </tr>
        <tr>
			<td><?php echo ++$row_num;?></td>
            <td>
            <strong>【担当者確認シート】</strong><br />
			<?php echo $this->Html->link('　【営業部門】',
				array('controller'=>'sections','action'=>'addSection',$user_id,0)); ?><br />
			<?php echo $this->Html->link('　【店舗開発部門】',
				array('controller'=>'sections','action'=>'addSection',$user_id,1)); ?><br />
			<?php echo $this->Html->link('　【経理部門】',
				array('controller'=>'sections','action'=>'addSection',$user_id,2)); ?><br />
			<?php echo $this->Html->link('　【広報部門】',
				array('controller'=>'sections','action'=>'addSection',$user_id,3)); ?><br />
			<?php echo $this->Html->link('　【搬入部門】',
				array('controller'=>'sections','action'=>'addSection',$user_id,4)); ?><br />
			<?php echo $this->Html->link('　【備考】',
				array('controller'=>'sections','action'=>'addSection',$user_id,5)); ?><br />
            </td>
            <td>ご担当者さまの連絡先について<br /><br />
    貴店における部門別の担当者様の連絡先についてお伺いさせて頂きます。<br />
    左の各担当部門毎のシートからご入力ください。<br />
    左の各担当部門と大きく異なる組織体の場合、備考のシートにご入力ください。<br />
    なお、本シートとは別に緊急連絡先届出書を頂戴いたします。<br />
            </td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
            <td> - </td>
        </tr>

<?php switch( SUB_DOMAIN ):?>
<?php case 'makuharishintoshin':?>
<?php case 'toin':?>
        <tr>
            <td><?php echo ++$row_num;?></td>
            <td><?php echo $this->Html->link('【ヒアリングシート】',
                    array('controller'=>'hearings','action'=>'index')); ?></td>
            <td>出店に関するヒアリングシートです。<br /><br />
            </td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
            <td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
            <td> - </td>
        </tr>
        <tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【ヒアリングシート２】',
					array('controller'=>'option_hearings','action'=>'check')); ?></td>
            <td>出店に関するヒアリングシートです。<br /><br />
            </td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
            <td> - </td>
        </tr>
		<tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【店名確認シート】'
					,array('controller'=>'signboards','action'=>'index')); ?></td>
            <td>本シートは、館内サイン・フロアガイド・HP及び販促告知媒体などのカテゴリー・店名の標記確認書です。<br /><br />
媒体等のカテゴリー・店名は、全て記載いたしますので、くれぐれもお間違いの無い様、御社本部確認の上、ご入力ください。</td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
            <td> - </td>
        </tr>

<?php break;?>
<?php default:?>
        <!-- ヒアリングシートなし -->
        <!-- 店名確認シートなし -->
<?php endswitch;?>

	<?php if( SUB_DOMAIN =='toin' && defined('SYOKUJYU_LINK')):?>
		<tr>
		  <td><?php echo ++$row_num;?></td>
		  <td><?php echo $this->Html->link('【植樹祭参加者登録】',
					array('controller'=>'syokujyu_items','action'=>'check')); ?></td>
			<td>植樹祭参加者の情報をご入力ください。<br /><br />
			</td>
			<td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('TERMINAL_COMPANY')):?>
        <tr>
			<td><?php echo ++$row_num;?></td>
            <td><?php echo $this->Html->link('【入力用端末申込書】','terminal'); ?></td>
            <td>
                この申込書は内装設計説明会においてご案内致しました入力用端末機の申込書となります。<br />
                本書に申込み台数をご入力ください。
            </td>
            <td><?php echo $this->Html->link( TERMINAL_COMPANY, '#terminal_co');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
            <td> - </td>
        </tr>
        <tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【レシート用情報】','receipt'); ?></td>
			<td>
				レシートに用印字される情報をご入力ください。
			</td>
			<td><?php echo $this->Html->link( TERMINAL_COMPANY, '#terminal_co');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('GOOGLEMAP_LINK')):?>
		<tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【Google Mapについて】','settingGoogleMap'); ?></td>
			<td>Google Map掲載への可否の確認です。</td>
			<td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('FUKUBUKURO')):?>
		<tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【福袋について】',
					array('controller'=>'fukubukuros','action'=>'edit')); ?></td>
			<td>福袋の販売の有無などの情報をご入力ください。</td>
			<td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('LAST_HANSOKU')):?>
		<tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【販促企画最終確認】',
					array('controller'=>'last_hansokus','action'=>'edit')); ?></td>
            <td>
                こちらのシートへの入力をもって、最終確認とさせて頂き、
                館内ポスターや、イオンモール東員ＨＰへ、ご入力頂いたとおりに掲載させて頂きますので、
                正確にご入力をお願い致します。<br /><br />
            </td>
			<td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('MEDIA_STATION')):?>
		<tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【メディアステーション・イオンモールメンバーズ説明会】',
					array('controller'=>'media_stations','action'=>'edit')); ?></td>
			<td>メディアステーション・イオンモールメンバーズ説明会、参加者の情報をご入力ください。</td>
			<td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red"><?php echo $nyuuryoku_kigen[ $row_num -1 ];?></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('MOCK_OPERATIONS')):?>
		<tr>
			<td><?php echo ++$row_num;?></td>
			<td><?php echo $this->Html->link('【飲食・食物販店舗のみ】模擬営業、店頭準備についてのアンケート',
					array('controller'=>'mock_operations','action'=>'edit')); ?></td>
			<td>飲食店（レストラン・フードコート・外部棟・食物販店舗）を対象に模擬営業（オペレーション確認）を行います。また、ウェイティングゾーンやイーゼルの設置を各店舗ごとに、各店舗の責任者さまとイオンモール担当者の立会いで実施致します。<br />模擬営業の参加の有無、店頭準備の希望日時などの情報をご入力ください。<br />
			</td>
			<td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
			<td><span style="color:red">10月13日（日）</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

    <?php if( defined('CHRISTMAS_FEATURES')):?>
        <tr>
            <td><?php echo ++$row_num;?></td>
            <td><?php echo $this->Html->link('【クリスマスギフト特集・クリスマスグルメ特集】',
                    array('controller'=>'christmas_features','action'=>'edit')); ?></td>
            <td>ホームページにて「クリスマスギフト特集・クリスマスグルメ特集」を掲載いたします。
                掲載を希望される商品についての情報をご記入ください（最大３件）。<br /><br />
            </td>
            <td><?php echo $this->Html->link( MALL_NAME, '#sougou');?></td>
            <td><span style="color:red">11月10日（日）</span></td>
            <td> - </td>
        </tr>
    <?php endif;?>

    </tbody>
</table>
<br />


<?php if( SUB_DOMAIN =='toin'): ?><!-- 東員、提出書類 -->
<h3>提出書類</h3>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th nowrap width="60px">分類</th>
            <th nowrap width="30px">No.</th>
            <th nowrap width="160px">提出資料名</th>
            <th nowrap width="260px">登録内容詳細</th>
            <th width="120px">イオンモール東員担当者</th>
            <th nowrap>提出期限</th>
            <th nowrap>ステータス</th>
        </tr>
    </thead>
    <tbody>
    <?php if( defined('PERMIT_CARDS')):?>
        <tr>
            <td rowspan="7">従業員・施設管理</td>
            <td>OP提出</td>
            <td><?php
                    echo $this->Html->link('【オープン時臨時入館証発行願】',
                        array('controller'=>'permit_cards','action'=>'edit'));
            ?>
            </td>
            <td>
              2013年11月5日～2013年12月1日に入退館する全ての方に必要となる入館証です。
              セキュリティ確保のため必要最小限の枚数をご入力下さい。<br />
            </td>
            <td class="tantou">西上</td>
            <td><span style="color:red">10月11日(金)</span></td>
            <td> - </td>
        </tr>
    <?php endif;?>
	<?php if( defined('CAR_PORTS')):?>
		<tr>
			<td>OP提出</td>
			<td><?php echo $this->Html->link('【従業員駐車場利用希望アンケート】',
					array('controller'=>'car_ports','action'=>'edit')); ?></td>
			<td>
車通勤をされる方には、駐車許可証を発行いたしますので、貴店の駐車台数を取りまとめ、ご入力下さい。<br />
			</td>
            <td class="tantou">西上</td>
            <td><span style="color:red">10月11日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>
    <?php if( defined('LOCKERS')):?>
        <tr>
            <td>OP提出</td>
            <td><?php echo $this->Html->link('【従業員ロッカー利用台数申込書】',
                    array('controller'=>'lockers','action'=>'edit')); ?></td>
            <td>
                従業員ロッカーの希望台数をご入力下さい。<br /><br />
            </td>
            <td class="tantou">西上</td>
            <td><span style="color:red">10月11日(金)</span></td>
            <td> - </td>
        </tr>
    <?php endif;?>
    <?php if( defined('CASHBOXES')):?>
        <tr>
            <td>OP提出</td>
            <td><?php echo $this->Html->link('【釣銭金庫利用申込書】',
                    array('controller'=>'cashboxes','action'=>'edit')); ?></td>
            <td>
                釣銭金を保管する金庫をお貸しします。希望台数をご入力下さい。<br /><br />
            </td>
            <td class="tantou">西上</td>
            <td><span style="color:red">10月11日(金)</span></td>
            <td> - </td>
        </tr>
    <?php endif;?>

        <?php $email_mark = '<span class="label label-warning"> Email </span>'; ?>
        <tr>
            <td>OP提出<?php echo $email_mark; ?></td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/pre_open_hannyuu_keikaku.xls'); ?>">
              <i class="icon-file"></i>オープン前商品搬入計画表(Excel)
            </a></td>
            <td>
              2013年11月5日～11月14日に搬入をする際に必要な申請です。専用メールアドレスに申請して下さい。<br />
              Email：hannyu-toin@aeonmall.com<br /><br />
            </td>
            <td class="tantou">加藤</td>
            <td><span style="color:red">10月15日(火)</span></td>
            <td> - </td>
        </tr>
        <tr>
            <td>OP情報<span class="label label-warning">原本</span></td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/bouka_bousai_kanrisya.doc'); ?>">
              <i class="icon-file"></i>防火・防災管理者の選任・指示事項の履行について(MS-Word)
            </a></td>
            <td>消防法上の管理をイオンモール東員ＧＭに選任する承諾書となります。
                消防届出書類となりますので、全テナント様は原本をＡＭ事務所にご提出ください。<br />
            </td>
            <td class="tantou">西上</td>
            <td><span style="color:red">10月28日(月)</span></td>
            <td> - </td>
        </tr>
        <tr>
            <td>OP情報</td>
            <td>館内一括について
            </td>
            <td>イオンモール東員では館内配送業者を指定しております。⇒<br />
            </td>
            <td><a href="#haisou">ヤマト運輸（株）<br />担当：星様</a>
            </td>
            <td> - </td>
            <td> - </td>
        </tr>
    </tbody>
</table>
<br />

<h3>営業細則書類</h3>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th nowrap width="90px">分類</th>
            <th nowrap width="40px">No.</th>
            <th nowrap width="340px">様式名</th>
            <th nowrap width="200px">問い合わせ先</th>
        </tr>
    </thead>
    <?php $fax_mark = '<span class="label label-warning"> FAX </span>'; ?>
    <?php $mail_mark = '<span class="label label-warning"> Mail </span>'; ?>
    <?php $paper_mark = '<span class="label label-success"> 原紙 </span>'; ?>
    <?php $new_mark = '<span class="label label-important"> New </span>'; ?>

    <tbody>
        <tr>
            <td rowspan="11">営業細則様式</td>
            <td>様式①</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou1_syumoku.pdf'); ?>">
              <i class="icon-file"></i>営業種目変更申請書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式②</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou2_irai.pdf'); ?>">
              <i class="icon-file"></i>依頼書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式③</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou3_simatu.pdf'); ?>">
              <i class="icon-file"></i>始末書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式④</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou4_kouji.pdf'); ?>">
              <i class="icon-file"></i>工事・業者作業届出書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑤</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou5_turisen.pdf'); ?>">
              <i class="icon-file"></i>釣銭金庫利用申込票(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑥</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou6_kinkyuu.pdf'); ?>">
              <i class="icon-file"></i>緊急連絡及び火元責任者届出書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑦</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou7_idou.pdf'); ?>">
              <i class="icon-file"></i>従業員異動届(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑧</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou8_tentyou_hikitugi.pdf'); ?>">
              <i class="icon-file"></i>店長の交代に伴う新店長への引継書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑨</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou9_funsitu.pdf'); ?>">
              <i class="icon-file"></i>従業員証・駐車許可証紛失届兼発行願(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑩</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou10_jikangai.pdf'); ?>">
              <i class="icon-file"></i>休業日・時間外作業届出書(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
        <tr>
            <td>様式⑪</td>
            <td><a target="_blank" href="<?php echo $this->Html->url(
              '/files/toin/eigyou11_motidasi.pdf'); ?>">
              <i class="icon-file"></i>商品持出票(PDF)
            </a></td>
            <td class="tantou">イオンモール東員</td>
        </tr>
    </tbody>
</table>
<br />

<?php elseif( SUB_DOMAIN =='makuharishintoshin'): ?><!-- 幕張新都心、提出書類 -->
<h3>提出書類</h3>
<hr />

<table class="makuhari table table-bordered table-striped">
    <thead>
        <tr>
			<th nowrap width="60px">分類</th>
			<th nowrap width="30px">No.</th>
            <th nowrap width="160px">提出資料名</th>
            <th nowrap width="260px">登録内容詳細</th>
            <th width="120px">イオンモール幕張新都心担当者</th>
            <th nowrap>提出期限</th>
            <th nowrap>ステータス</th>
        </tr>
    </thead>
    <?php $fax_mark = '<span class="label label-warning"> FAX </span>'; ?>
    <?php $mail_mark = '<span class="label label-warning"> Mail </span>'; ?>
    <?php $paper_mark = '<span class="label label-success"> 原紙 </span>'; ?>
    <?php $new_mark = '<span class="label label-important"> New </span>'; ?>
    <tbody>
        <tr>
          <td rowspan="5">営業</td>
          <td>営業</td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/eigyou1_sales_schedule.pdf'); ?>">
            <i class="icon-file"></i><?php echo $new_mark; ?>セールススケジュール店舗特典集約について(PDF)
          </a></td>
          <td>イオンモール幕張新都心　セールススケジュール及び店舗フェア内容等情報集約についてのご案内文書<br />【提出不要】<br /><br />
          </td>
          <td class="tantou">【営業】鈴木</td>
          <td><span style="color:red">提出不要</span></td>
          <td></td>
        </tr>
        <tr>
          <td>営業</td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/eigyou2_promotion.pdf'); ?>">
            <i class="icon-file"></i><?php echo $new_mark; ?>資料【A】プロモーションよりお願い事項(PDF)
          </a></td>
          <td>イオンモール幕張新都心　販促スケジュール 並びに 実施予定概要<br />【提出不要】<br /><br />
          </td>
          <td class="tantou">【営業】鈴木</td>
          <td><span style="color:red">提出不要</span></td>
          <td></td>
        </tr>
        <tr>
          <td>営業提出書類<?php echo $fax_mark; ?><br /><br /><?php echo $mail_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/eigyou3_media_info.pdf'); ?>">
            <i class="icon-file"></i><?php echo $new_mark; ?>資料【B】媒体用情報集約【提出シート】(PDF)
          </a></td>
          <td>オープニングフェア・バーゲン・クリアランス・福袋に関する提出書類<br />
              【FAX 又は メール添付送付にてご提出下さい】<br /><br />
          </td>
          <td class="tantou">【営業】鈴木</td>
          <td><span style="color:red">11月8日(金)</span></td>
          <td></td>
        </tr>
        <tr>
          <td>営業提出書類<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/eigyou4_koten_hansoku.pdf'); ?>">
            <i class="icon-file"></i><?php echo $new_mark; ?>資料【C】個店販促情報集約【提出シート】(PDF)
          </a></td>
          <td>ソフトオープン・グランドオープン・オープン後に関する各店販促情報に関する提出書類<br />
              【FAXにてご提出下さい】<br /><br />
          </td>
          <td class="tantou">【営業】鈴木</td>
          <td><span style="color:red">11月1日(金)</span></td>
          <td></td>
        </tr>
        <tr>
            <td>営業</td>
            <td><?php echo $this->Html->link('【イオン ふるさとの森づくり植樹祭】',
                    array('controller'=>'syokujyu_items','action'=>'check')); ?></td>
            <td>植樹祭参加者の情報をご入力ください。<br /><br />
            </td>
            <td class="tantou">【営業】東（ヒガシ）</td>
            <td><span style="color:red">9月30日(月)</span></td>
            <td> - </td>
        </tr>
        <tr>
          <td rowspan="3">飲食</td>
          <td>飲食提出①<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/syokuhin_eisei.pdf'); ?>">
            <i class="icon-file"></i>食品衛生責任者届出書(PDF)
          </a></td>
          <td>
            <span class="pdf_insyoku p17">『食品衛生管理対象店舗資料(PDF)』</span>p.17参照<br /><br />
            食品衛生責任者の情報を記入し、資格証のコピーとともにＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【営業】松倉, 稲垣</td>
          <td><span style="color:red">11月15日(金)</span></td>
          <td></td>
        </tr>
        <tr>
          <td>飲食提出②<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/recycle.pdf'); ?>">
            <i class="icon-file"></i>リサイクルカート・廃油容器　発注依頼書(PDF)
          </a></td>
          <td>
            <span class="pdf_insyoku p27">『食品衛生管理対象店舗資料(PDF)』</span>p.27参照<br /><br />
            購入希望を記入し、ＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【営業】松倉, 稲垣</td>
          <td><span style="color:red">10月25日(金)</span></td>
          <td></td>
        </tr>
        <tr>
          <td>配布なし<?php echo $fax_mark; ?></td>
          <td>所管の保健所からの営業許可証類　のコピー</td>
          <td><span class="pdf_insyoku p17">『食品衛生管理対象店舗資料(PDF)』</span>p.17参照<br /><br />
              営業許可証のコピーをＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【営業】松倉, 稲垣</td>
          <td><span style="color:red">11月15日(金)</span></td>
          <td></td>
        </tr>

        <tr>
          <td rowspan="8">従業員・施設管理</td>
          <td>OP提出①<?php echo $paper_mark; ?></td>
          <td>受領書</td>
          <td>説明会当日、原紙を置いてご退席ください。</td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td></td>
          <td></td>
        </tr>
	<?php if( defined('PERMIT_CARDS')):?>
      <tr>
			<td>OP提出②</td>
			<td><?php
					echo $this->Html->link('【オープン時仮入館証発行願】',
						array('controller'=>'permit_cards','action'=>'edit'));
            ?>
            </td>
			<td><span class="pdf_operation p12">『第１回運営管理説明会資料(PDF)』</span>p.12〜p.18参照<br /><br />
			  2013年12月2日～2014年1月13日（応援者の方は1月31日まで）に入退館する全ての方に必要となる入館証です。
			  セキュリティ確保のため必要最小限の枚数をご入力下さい。<br />
			</td>
			<td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
			<td><span style="color:red">10月25日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

        <tr>
          <td>OP提出③<?php echo $paper_mark; ?></td>
          <td>オープン後も従業員として勤務される方の情報を記入し、従業員さまの人数分、原紙をご送付下さい。
              <span class="badge badge-success">人数分</span><br /><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_sample.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（見本）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_grand1f.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（グランドモール１階）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_grand2f.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（グランドモール２階）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_grand3f.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（グランドモール３階）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_family1f.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（ファミリーモール１階）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_family2f.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（ファミリーモール２階）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_family3f.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（ファミリーモール３階）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_active.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（アクティブモール）</a><br />
              <a target="_blank" href="<?php echo $this->Html->url(
                                         '/files/makuharishintoshin/jyuugyouin_pet.pdf'); ?>">
              <i class="icon-file"></i>従業員届出書（ペットモール）</a><br />
          </td>
          <td><span class="pdf_operation p12">『第１回運営管理説明会資料(PDF)』</span>p.12〜p.18参照<br /><br />
              ・貴店の棟・階数にあった『従業員届出書』を選び、ご提出下さい。<br />
              ・全従業員の方に、『新人研修』を受講していただきます。必ず希望日をご記入下さい。
                棟・階数ごとに研修のお時間を分けておりますので、ご注意下さい。
          </td>
          <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
          <td><span style="color:red">11月15日(金)</span></td>
          <td></td>
        </tr>

	<?php if( defined('PRE_CAR_PORTS')):?>
		<tr>
			<td>OP提出④</td>
			<td><?php echo $this->Html->link('【オープン前：臨時駐車場利用台数アンケート】',
					array('controller'=>'pre_car_ports','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p19">『第１回運営管理説明会資料(PDF)』</span>p.19〜p.23参照<br /><br />
			  2013年12月2日～2013年12月15日に車通勤される方には『オープン前臨時駐車許可証』を発行いたしますので、貴店の駐車台数をとりまとめ、ご入力下さい。<br />
			</td>
            <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
			<td><span style="color:red">10月25日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('CAR_PORTS')):?>
		<tr>
			<td>OP提出⑤</td>
			<td><?php echo $this->Html->link('【オープン後：従業員駐車場台数アンケート】',
					array('controller'=>'car_ports','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p19">『第１回運営管理説明会資料(PDF)』</span>p.19〜p.23参照<br /><br />
従業員としてオープン後、車通勤をされる方には、『オープン後駐車許可証』を発行いたしますので、貴店の駐車台数を取りまとめ、ご入力下さい。<br />
			</td>
            <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
			<td><span style="color:red">11月15日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('BICYCLES')):?>
		<tr>
			<td>OP提出⑥</td>
			<td><?php echo $this->Html->link('【従業員駐輪場利用台数アンケート】',
					array('controller'=>'bicycles','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p19">『第１回運営管理説明会資料(PDF)』</span>p.19〜p.23参照<br /><br />
			  自転車・バイクの駐輪台数を取りまとめ、ご入力下さい。<br />
			</td>
            <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
			<td><span style="color:red">11月15日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('LOCKERS')):?>
		<tr>
			<td>OP提出⑦</td>
			<td><?php echo $this->Html->link('【従業員ロッカー利用台数アンケート】',
					array('controller'=>'lockers','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p24">『第１回運営管理説明会資料(PDF)』</span>p.24〜p.25参照<br /><br />
			    ＜グランドモール、ファミリーモールの専門店さま限定＞<br />従業員ロッカーの希望台数をご入力下さい。<br /><br />
			</td>
            <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
			<td><span style="color:red">11月15日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('CASHBOXES')):?>
		<tr>
			<td>OP提出⑧</td>
			<td><?php echo $this->Html->link('【釣銭金庫使用申込書】',
					array('controller'=>'cashboxes','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p24">『第１回運営管理説明会資料(PDF)』</span>p.24〜p.25参照<br /><br />
			    釣銭金を保管する金庫をお貸しします。希望台数をご入力下さい。<br /><br />
			</td>
            <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
			<td><span style="color:red">10月25日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

        <tr>
          <td rowspan="">売上管理</td>
          <td>OP提出⑨<?php echo $paper_mark; ?></td>

          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/kaikei1-5.pdf'); ?>">
            <i class="icon-file"></i>会計様式①～⑤(PDF)
          </a><br />
①精算レシート登録内容届<br />
②売上控除申請書<br />
③掛売上届出書<br />
④ご精算書送付先登録届<br />
⑤金融機関口座届出書
          </td>
          <td><span class="pdf_operation p26">『第１回運営管理説明会資料(PDF)』</span>p.26〜p.39参照<br /><br />
・原紙をご送付下さい。<br />
※セットにしてご提出下さい
          </td>
          <td class="tantou">【オペレーション】<br />小黒（オグロ）, 村田</td>
          <td><span style="color:red">10月25日(金)</span></td>
          <td></td>
        </tr>

        <tr>
          <td rowspan="7">消防</td>
          <td>当日配布<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/bousai_sikaku.pdf'); ?>">
            <i class="icon-file"></i>防火・防災管理者　資格取得状況確認書(PDF)
          </a></td>
          <td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
			  防火・防災管理者となる店長の、防火・防災管理者の資格取得状況を記入し、ＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td><span style="color:red">10月25日(金)</span></td>
          <td></td>
        </tr>
        <tr>
          <td>当日配布<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/bousai_kousyuu.pdf'); ?>">
            <i class="icon-file"></i>防火防災管理講習申込書　（受講者のみ提出）(PDF)
          </a></td>
          <td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
			  防火・防災管理者の資格を取得できる講習会を開催いたします。受講される方はＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td><span style="color:red">10月25日(金)</span></td>
          <td></td>
        </tr>

        <tr>
          <td>OP提出⑩<?php echo $paper_mark; ?></td>
          <td><a href="<?php echo $this->Html->url('/users/SyoubouKeikaku'); ?>">【消防提出書類】</a>
          </td>
          <td><a href="<?php echo $this->Html->url('/users/SyoubouKeikaku'); ?>">【消防提出書類】</a>
            ページをご覧下さい。
          </td>
          <!--
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/syoubou_set_2013_1001.pdf'); ?>">
            <i class="icon-file"></i>消防提出書類(PDF)
          </a><br />
・消防提出書類　送付チェックシート<span class="badge badge-success">1部</span><br />
・防火・防災管理者選任（解任）届出書<span class="badge badge-success">2部</span><br />
・講習機関発行の甲種防火管理者及び防災管理者資格証明書のコピー<span class="badge badge-success">2部</span><br />
・消防計画作成（変更）届出書<span class="badge badge-success">2部</span><br />
・イオンモール幕張新都心　事業所消防計画<span class="badge badge-success">2部</span>
          </td>
          <td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
イオンモールが取りまとめ、美浜消防署へ提出する書類です。一式揃えて、原紙をご送付下さい。<br /><br />
・事業所消防計画については、現在、美浜消防署と内容を協議中ですので、協議が完了次第、事業所消防計画のフォーマットと記入方法をアップロード致します。<br />
・記入見本をしっかり確認し、漏れのないように記入、押印をお願い致します。<br />
※セットにしてご提出下さい -->
          </td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td><span style="color:red">11月29日(金)</span></td>
          <td></td>
        </tr>

	<?php if( defined('FIRE_DRILLS')):?>
		<tr>
			<td>OP提出⑪</td>
			<td><?php echo $this->Html->link('【消防訓練説明会の参加申込】',
					array('controller'=>'fire_drills','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
			    消防訓練事前説明会の参加者（消防訓練に参加される方）の情報をご入力下さい。<br /><br />
			</td>
            <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
			<td><span style="color:red">11月15日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>
        <tr>
          <td>OP提出⑫<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/kinkyuu.pdf'); ?>">
            <i class="icon-file"></i>緊急連絡先及び火元責任者届出書(PDF)
          </a></td>
          <td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
			  災害・事故などの緊急時のご連絡先を記入し、ＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td><span style="color:red">11月15日(金)</span></td>
          <td></td>
        </tr>
        <tr>
          <td>OP提出⑬<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/netugen.pdf'); ?>">
            <i class="icon-file"></i>熱源機器使用申請書(PDF)
          </a></td>
          <td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
			  営業上の理由でやむを得ずアイロン等の熱源機器使用を希望する場合は、必要事項を記入し、ＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td><span style="color:red">11月15日(金)</span></td>
          <td></td>
        </tr>
        <tr>
          <td>OP提出⑭<?php echo $fax_mark; ?></td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/bousai_bihin.pdf'); ?>">
            <i class="icon-file"></i>防災備品発注依頼書(PDF)
          </a></td>
          <td><span class="pdf_operation p45">『第１回運営管理説明会資料(PDF)』</span>p.45〜p.49参照<br /><br />
			  ヘルメット、懐中電灯、非常持ち出し袋を1セット以上購入していただきます。必要事項を記入し、ＦＡＸにてご提出下さい。
          </td>
          <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
          <td><span style="color:red">11月15日(金)</span></td>
          <td></td>
        </tr>

	<?php if( defined('OPERATION_BRIEFINGS')):?>
		<tr>
            <td rowspan="">説明会</td>
			<td>OP提出⑮</td>
			<td><?php echo $this->Html->link('【第２回運営管理説明会申込】',
					array('controller'=>'operation_briefing','action'=>'edit')); ?></td>
			<td><span class="pdf_operation p61">『第１回運営管理説明会資料(PDF)』</span>p.61参照<br /><br />
			    申込者の情報をご入力ください。<br />店長様または営業責任者の方、２名様までのご出席をお願い致します。<br /><br />
			</td>
            <td class="tantou">【オペレーション】<br />渡會（ワタライ）, 水野</td>
			<td><span style="color:red">10月25日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

        <tr><td colspan="7">以下の書類の提出先はイオンモール幕張新都心ではありません。</td>
        </tr>

        <tr>
          <td rowspan="2">他部署・協力会社</td>
          <td> - </td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/hannyuu.pdf'); ?>">
            <i class="icon-file"></i>イオンモール幕張新都心　商品搬入計画表(PDF)
          </a></td>
          <td><span class="pdf_operation p40">『第１回運営管理説明会資料(PDF)』</span>p.40～44参照<br /><br />
              ダウンロードし、イオンディライト株式会社へメールにて送付して下さい。<br />
              E-mail： makuhari-mbox@aeondelight.jp
          </td>
          <td class="tantou"></td>
          <td><span style="color:red">10月14日(月)</span></td>
          <td></td>
        </tr>

        <tr>
          <td> - </td>
          <td><a target="_blank" href="<?php echo $this->Html->url(
            '/files/makuharishintoshin/hoken.pdf'); ?>">
            <i class="icon-file"></i>イオン保険サービス株式会社　保険届出書(PDF)
          </a></td>
          <td>イオン保険サービスへ、ＦＡＸにて送付して下さい。<br />
              FAX： 043－351-8670<br /><br />
              運営管理説明会にて、イオン保険サービスの保険についてのご案内をお渡ししておりますので、ご確認下さい。
          </td>
          <td class="tantou"></td>
          <td><span style="color:red">11月30日(土)</span></td>
          <td></td>
        </tr>

	<?php if( defined('BILLS')):?>
		<tr>
            <td></td>
			<td></td>
			<td><?php echo $this->Html->link('【請求書送付先登録届】',
					array('controller'=>'bills','action'=>'edit')); ?></td>
			<td>ご請求書の送付先住所をご入力ください。<br /><br />
			</td>
			<td></td>
			<td><span style="color:red"></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('MOCK_OPERATIONS')):?>
		<tr>
            <td></td>
			<td></td>
			<td><?php echo $this->Html->link('【ソフトオープン前 模擬営業（オペレーション確認）について】',
					array('controller'=>'mock_operations','action'=>'edit')); ?></td>
			<td>きたるオープンを万全の態勢で迎えるため、飲食・食品店舗を対象にオペレーション練習（模擬営業）を行います。
			  <br />参加の有無などの情報をご入力ください。<br />
			</td>
			<td></td>
			<td><span style="color:red">11月8日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('EMERGENCY_CONTACTS')):?>
		<tr>
            <td></td>
			<td></td>
			<td><?php echo $this->Html->link('【緊急連絡先及び火元責任者届出書】',
					array('controller'=>'emergency_contacts','action'=>'edit')); ?></td>
			<td>本書は緊急連絡時に使用するものです。<br />緊急連絡先及び火元責任者の情報をご入力ください。
			</td>
			<td></td>
			<td><span style="color:red"></span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

	<?php if( defined('CASHIERS')):?>
		<tr>
            <td></td>
			<td></td>
			<td><?php echo $this->Html->link('【両替機・入金機運用説明会・売上会計説明会参加申込書】',
					array('controller'=>'cashiers','action'=>'edit')); ?></td>
			<td>両替機・入金機運用説明会・売上会計説明会参加者の情報をご入力ください。<br /><br />
			</td>
			<td></td>
			<td><span style="color:red">11月15日(金)</span></td>
			<td> - </td>
		</tr>
	<?php endif;?>

    </tbody>
</table>
<br />
<?php endif;?><!-- /幕張新都心、提出書類 -->

<h3>参考資料</h3>
<hr />
<div>
  <ul>
    <?php if( SUB_DOMAIN == 'makuharishintoshin'):?>
		<li><a target="_blank" href="<?php echo $this->Html->url('/files/makuharishintoshin/operation_briefing_1st_2013_1007.pdf'); ?>">
		<i class="icon-file"></i><span class="label label-important"> New </span> 第１回運営管理説明会資料(PDF)</a>
		</li>
		<li><a target="_blank" href="<?php echo $this->Html->url('/files/makuharishintoshin/insyoku_syokuhin.pdf'); ?>">
		<i class="icon-file"></i><span class="label label-important"> New </span> 第１回運営管理説明会 - 食品衛生管理対象店舗資料(PDF)</a>
		</li>
		<li><a target="_blank" href="<?php echo $this->Html->url('/files/makuharishintoshin/inquiry.pdf'); ?>">
		<i class="icon-file"></i><span class="label label-important"> New </span> ご質問状(PDF)</a>
		</li>
    <?php endif;?>

    <?php if( defined('MEDIA_STATION_LINK')) :?>
        <li><a target="_blank" href="<?php echo $this->Html->url( MEDIA_STATION_LINK ) ?>">
        <i class="icon-file"></i>
        メディアステーション・イオンモールメンバーズ 説明会開催のご案内(PDF)</a>
        </li>
    <?php endif; ?>
	<?php if( defined('MOCK_OPERATIONS_LINK')):?>
		<li><a target="_blank" href="<?php echo $this->Html->url( MOCK_OPERATIONS_LINK ) ?>">
		<i class="icon-file"></i>
        模擬営業についてのご案内(PDF)</a>
		</li>
	<?php endif; ?>
	<?php if( defined('SYOKUJYU_LINK')) :?>
		<li><a target="_blank" href="<?php echo $this->Html->url( SYOKUJYU_LINK ) ?>">
		<i class="icon-file"></i>植樹祭のご案内(PDF)</a>
		</li>
	<?php endif; ?>
    <?php if( defined('BAITAI_LINK')) :?>
		<li><a target="_blank" href="<?php echo $this->Html->url(BAITAI_LINK) ?>">
		<i class="icon-file"></i>媒体製作事務局について(PDF)</a>
		</li>
	<?php endif; ?>
	<?php if( defined('THANKSDAY_LINK')) :?>
		<li><a target="_blank" href="<?php echo $this->Html->url(THANKSDAY_LINK) ?>">
		<i class="icon-file"></i>お客さま感謝デー・GG感謝デーについて(PDF)</a>
		</li>
	<?php endif; ?>

	<?php if( defined('HANSOKU_LINK')) :?>
		<li><a target="_blank" href="<?php echo $this->Html->url( HANSOKU_LINK ) ?>">
		<i class="icon-file"></i>販促企画について(PDF)</a>
		</li>
	<?php endif; ?>

	<?php if( defined('SCHEDULE_LINK')) :?>
		<li><a target="_blank" href="<?php echo $this->Html->url(SCHEDULE_LINK) ?>">
		<i class="icon-file"></i>SCスケジュール(PDF)</a>
		</li>
	<?php endif; ?>

	<?php if( defined('GOOGLEMAP_LINK')) :?>
		<li><a target="_blank" href="<?php echo $this->Html->url( GOOGLEMAP_LINK ) ?>">
		<i class="icon-file"></i>Google　Map導入の案内文書(PDF)</a>
		</li>
	<?php endif; ?>

  </ul>
</div>

<br />

<?php // お問い合わせの下に表示する
if( defined('TERMINAL_COMPANY')):
	$omuron_renraku = '
	<p class="clear"></p><br />
	<h4 id="terminal_co">入力用端末機お問い合わせ</h4>
	<p>入力用端末機の申込み、固有情報（レシート印字用テナント名称、電話番号）の申込みに関するお問い合わせ</p>
	<pre class="span9" style="font-size:small;">
	オムロンソフトウェア株式会社
	TEL：043-297-0691　（平日9:30～12:15、13:00～17:30）
	Email：am-support@oss-g.omron.co.jp
	</pre>
	';
else:
	$omuron_renraku = '';
endif;
?>

<p class="clear"></p><br />
<h4 id="sougou">お問い合わせ</h4>
<?php switch( SUB_DOMAIN ):?>
<?php case 'makuharishintoshin':?>
	<pre class="span9" style="font-size:small;">
	イオンモール株式会社幕張新都心推進事業部
	TEL：043-212-6349（平日8:30〜17:30）
	</pre>
	<?php echo $omuron_renraku;?>
	<p class="clear"></p><br />
	<h4>エラーが出た、ログインができないなど、技術的なお問い合わせ</h4>
	<pre class="span9" style="font-size:small;">
	株式会社ＲＣＴジャパン　担当：迫田（さこだ）
	TEL：03-5768-2061（平日9:30〜18:00）
	Email：order@mail.makuharishintoshin-aeonmall.com
	</pre>
<?php break;?>
<?php case 'toin':?>
	<pre class="span9" style="font-size:small;">
	イオンモール東員
	〒511－0255
	三重県員弁郡東員町大字長深字築田510-1
	TEL：0594-87-7161（平日9:30〜18:00）
	FAX：0594-87-7162
	</pre>

	<?php echo $omuron_renraku;?>
	<p class="clear"></p><br />

	<h4>エラーが出た、ログインができないなど、技術的なお問い合わせ</h4>
	<pre class="span9" style="font-size:small;">
	株式会社ＲＣＴジャパン　担当：迫田（さこだ）
	TEL：03-5768-2061（平日9:30〜18:00）
	Email：order@mail.toin-aeonmall.com
	</pre>
	<p class="clear"></p><br />

    <h4 id="haisou">館内配送業者</h4>
    <p>イオンモール東員では館内配送業者を指定しております。</p>
    <pre class="span9" style="font-size:small;">
	ヤマト運輸株式会社担当：星様
	TEL：052-725-3618
	FAX：052-725-3619
    </pre>

<?php break;?>
<?php case 'tendo':?>
	<pre class="span9" style="font-size:small;">
	イオンモール天童 開設準備室
	TEL：023-664-2510 担当：進藤、土居
	</pre>
	<?php echo $omuron_renraku;?>
	<p class="clear"></p><br /><br />
	<h4>エラーが出た、ログインができないなど、技術的なお問い合わせ</h4>
	<pre class="span9" style="font-size:small;">
	株式会社ＲＣＴジャパン　担当：迫田（さこだ）
	TEL：03-5768-2061（平日9:30〜18:00）
	Email：order@mail.tendo-aeonmall.com
	</pre>
<?php break;?>
<?php case 'wakayama':?>
	<pre class="span9" style="font-size:small; ">
	イオンモール和歌山　営業担当：千葉・伊東・佐伯（サイキ）
	TEL：073-480-3640（平日9:30～17:30）
	</pre>
	<?php echo $omuron_renraku;?>
	<p class="clear"></p><br /><br />
	<h4>エラーが出た、ログインができないなど、技術的なお問い合わせ</h4>
	<pre class="span9" style="font-size:small;">
	株式会社ＲＣＴジャパン　担当：迫田（さこだ）
	TEL：03-5768-2061（平日9:30〜18:00）
	Email：order@mail.wakayama-aeonmall.com
	</pre>
<?php break;?>
<?php default:?>
	<pre class="span9" style="font-size:small;">
	イオンモール◯◯営業担当
	TEL：xxx-xxx-xxxx（平日9:30〜18:00）
	</pre>
	<?php echo $omuron_renraku;?>
	<p class="clear"></p><br /><br />
	<h4>エラーが出た、ログインができないなど、技術的なお問い合わせ</h4>
	<pre class="span9" style="font-size:small;">
	株式会社ＲＣＴジャパン　担当：迫田（さこだ）
	TEL：03-5768-2061（平日9:30〜18:00）
	</pre>
<?php break;?>
<?php endswitch;?>

<?php } ?><!-- /認証済 -->

<script type="text/javascript">
$(document).ready(function(){
    // tantouクラスのセルを総合問い合わせへのアンカーにする
    $('td.tantou').wrapInner('<a href="#sougou">');

    // 第１回運営管理説明会資料(PDF)へのアンカーにする
    $('span.pdf_operation').wrapInner(
'<a target="_blank" href="<?php echo $this->Html->url('/files/makuharishintoshin/operation_briefing_1st_2013_1007.pdf'); ?>">');

    // 食品衛生管理対象店舗資料(PDF)へのアンカーにする
    $('span.pdf_insyoku').wrapInner(
'<a target="_blank" href="<?php echo $this->Html->url('/files/makuharishintoshin/insyoku_syokuhin.pdf'); ?>">');

    // 資料(PDF)へのアンカーにページ指定を追加
    $('span.pdf_insyoku a, span.pdf_operation a').each(function(i){
        var p_class = $(this).parent().attr('class'); // 親のクラス
        var res = p_class.match(/p([0-9].+)/); // pXX のクラスのマッチ
        // console.log( res[1] ); // 数字を取り出し
        if( res ){
            var url2 = $(this).attr('href') + '#page=' + res[1];
            $(this).attr('href',url2); //URL変更
        }
   });

})
</script>
