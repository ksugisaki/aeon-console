<div class ="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
</div>
<div>
<?php if( $permission_sections ): ?>
  <h3>【担当者確認シート】</h3>
    <p>担当者確認シートに入力いただいた各社の担当者の情報を確認できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【営業部門】','/sections/viewSection/eigyou'); ?></li>
      <li><?php echo $this->Html->link('【店舗開発部門】','/sections/viewSection/tenpo_kaihatu'); ?></li>
      <li><?php echo $this->Html->link('【経理部門】','/sections/viewSection/keiri'); ?></li>
      <li><?php echo $this->Html->link('【広報部門】','/sections/viewSection/kouhou'); ?></li>
      <li><?php echo $this->Html->link('【搬入部門】','/sections/viewSection/hannyuu'); ?></li>
      <li><?php echo $this->Html->link('【各部門の担当者をすべてエクスポート】','/sections/DownloadAllCsv'); ?></li>
      </ul>
    </div>
    <hr />
<?php endif; ?>

<?php switch( SUB_DOMAIN ): ?>
<?php case 'makuharishintoshin': ?>
<?php case 'toin': ?>
<?php case 'testsite': ?>
<?php case 'aeon-console': ?>
<?php if( $permission_hearings ): ?>
    <h3>【ヒアリングシート／売上予算】</h3>
    <p>ヒアリングシートに入力いただいた各テナントの情報、及び売上予算を確認できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【売上予算、年度別】','/hearings/AllYosanList'); ?></li>
	  <li><?php echo $this->Html->link('【売上予算をエクスポート】','/hearings/DownloadYosanCsv/0'); ?></li>
      <li><?php echo $this->Html->link('【売上予算以外のヒアリング情報をエクスポート】','/hearings/DownloadAllCsv'); ?></li>
      </ul>
    </div>
    <hr />
<?php endif; ?>
<?php if( $permission_option_hearings ): ?>
	<h3>【ヒアリングシート2】</h3>
	<p>ヒアリングシート2に入力いただいた各テナントの情報を確認できます。</p>
	<p>イメージ／ロゴ・リストからは、イメージ／ロゴのファイルをダウンロードできます。</p>
	<div>
	  <ul>
	  <li><?php echo $this->Html->link('【ヒアリングシート2をエクスポート】','/option_hearings/download'); ?></li>
	  <li><?php echo $this->Html->link('【イメージ／ロゴ・リスト】','ImageLogoList'); ?></li>
      <li>
		<ul class="nav pills">
		<li class="dropdown" id="menu1">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#menu1"
		>【イメージ／ロゴ・リスト】（印刷用フォーム）<b class="caret"></b></a>
		<ul class="dropdown-menu">
		  <li><?php echo $this->Html->link('ID順','ImageLogoList/id/print'); ?></li>
		  <li><?php echo $this->Html->link('区画番号順','ImageLogoList/block/print'); ?></li>
		  <li><?php echo $this->Html->link('会社名順','ImageLogoList/company/print'); ?></li>
		</ul>
		</li>
		</ul>
      </li>
	  </ul>
	</div>
	<hr />
<?php endif; ?>
<?php if( $permission_signboards ): ?>
    <h3>【店名確認シート】</h3>
    <p>店名確認シートに入力いただいた店名情報を確認できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【店名確認シートをエクスポート】','/signboards/DownloadAllCsv'); ?></li>
      </ul>
    </div>
    <hr />
<?php endif; ?>
<?php break; ?>
<?php endswitch; ?>

<?php if( $permission_sections ): ?>
    <h3>【店舗カルテ】</h3>
    <p>各店舗の基本情報と担当者情報を確認できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【店舗カルテ】','/print/sections'); ?></li>
      </ul>
    </div>
    <hr />
<?php endif; ?>

<?php switch( SUB_DOMAIN ): ?>
<?php case 'makuharishintoshin': ?>
<?php case 'toin': ?>
<?php if( $permission_signboards && $permission_hearings && $permission_option_hearings ): ?>
    <h3>【テナント情報】</h3>
    <p>広報向けテナント情報のまとめ資料をダウンロードできます。</p>
    <p>店名確認シート、ヒアリングシート、ヒアリングシート２からのデータを抽出まとめたデータです。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【テナント情報】','/users/DownloadTenantInfo'); ?></li>
      </ul>
    </div>
    <hr />
<?php endif; ?>
<?php break; ?>
<?php endswitch; ?>

<?php switch( SUB_DOMAIN ): ?>
<?php case 'makuharishintoshin': ?>
    <h3>【提出書類のエクスポート】</h3>
    <p>提出書類の各項目に入力いただいた各テナントの情報を確認できます。</p>
    <div>
    <ul>
    <?php if( $permission_permit_cards ): ?>
      <li><?php echo $this->Html->link('OP提出②【オープン時仮入館証発行願をエクスポート】','/permit_cards/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_pre_car_ports ): ?>
      <li><?php echo $this->Html->link('OP提出④【オープン前：臨時駐車場利用台数アンケートをエクスポート】','/pre_car_ports/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_car_ports ): ?>
      <li><?php echo $this->Html->link('OP提出⑤【オープン後：従業員駐車場台数アンケートをエクスポート】','/car_ports/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_bicycles ): ?>
      <li><?php echo $this->Html->link('OP提出⑥【従業員駐輪場利用台数アンケートをエクスポート】','/bicycles/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_lockers ): ?>
      <li><?php echo $this->Html->link('OP提出⑦【従業員ロッカー利用台数申込書をエクスポート】','/lockers/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_cashboxes ): ?>
      <li><?php echo $this->Html->link('OP提出⑧【釣銭金庫利用申込書をエクスポート】','/cashboxes/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_fire_drills ): ?>
      <li><?php echo $this->Html->link('OP提出⑪【消防訓練説明会の参加申込をエクスポート】','/fire_drills/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_operation_briefing ): ?>
      <li><?php echo $this->Html->link('OP提出⑮【第２回運営管理説明会申込をエクスポート】','/operation_briefing/download'); ?></li>
    <?php endif; ?>
    </ul>
    </div>
    <hr />
<?php break; ?>
<?php case 'toin': ?>
    <h3>【提出書類のエクスポート】</h3>
    <p>提出書類の各項目に入力いただいた各テナントの情報を確認できます。</p>
    <div>
    <ul>
    <?php if( $permission_permit_cards ): ?>
      <li><?php echo $this->Html->link('【オープン時臨時入館証発行願をエクスポート】','/permit_cards/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_car_ports ): ?>
      <li><?php echo $this->Html->link('【従業員駐車場利用希望アンケートをエクスポート】','/car_ports/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_lockers ): ?>
      <li><?php echo $this->Html->link('【従業員ロッカー利用台数申込書をエクスポート】','/lockers/download'); ?></li>
    <?php endif; ?>
    <?php if( $permission_cashboxes ): ?>
      <li><?php echo $this->Html->link('【釣銭金庫利用申込書をエクスポート】','/cashboxes/download'); ?></li>
    <?php endif; ?>
    </ul>
    </div>
    <hr />
<?php break; ?>
<?php endswitch; ?>

    <h3>【その他のエクスポート】</h3>
    <div>
    <ul>
      <?php if( defined('SYOKUJYU_LINK') && $permission_syokujyu_items ): ?>
        <li><?php echo $this->Html->link('【植樹祭参加者登録をエクスポート】','/SyokujyuItems/DownloadAllCsv'); ?></li>
      <?php endif; ?>
      <?php if( defined('TERMINAL_COMPANY') && $permission_users_terminal && $permission_users_receipt ): ?>
        <li><?php echo $this->Html->link('【入力用端末／レシート用情報をエクスポート】','/users/DownloadTerminalInfo'); ?></li>
      <?php endif; ?>
      <?php if( defined('FUKUBUKURO') && $permission_fukubukuros ): ?>
        <li><?php echo $this->Html->link('【福袋の情報をエクスポート】','/fukubukuros/download'); ?></li>
      <?php endif; ?>
      <?php if( defined('LAST_HANSOKU') && $permission_last_hansokus ): ?>
        <li><?php echo $this->Html->link('【販促企画最終確認情報をエクスポート】','/last_hansokus/download'); ?></li>
      <?php endif; ?>
      <?php if( defined('MEDIA_STATION') && $permission_media_stations ): ?>
        <li><?php echo $this->Html->link('【メディアステーション・イオンモールメンバーズ説明会、参加者情報をエクスポート】','/media_stations/download'); ?></li>
      <?php endif; ?>
      <?php if( defined('MOCK_OPERATIONS') && $permission_mock_operations ): ?>
        <li><?php echo $this->Html->link('【模擬営業、店頭準備についてのアンケート情報をエクスポート】','/mock_operations/download_toin'); ?></li>
      <?php endif; ?>
    </ul>
    </div>
    <hr />

<?php if( defined('LAST_SHOP_SIGN')): ?>
  <?php if( $permission_users_last_shop ): ?>
    <h3>【店名最終確認リスト】</h3>
    <p>店名最終確認の一覧を確認できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【店名最終確認リスト】','/users/ListLastShopSign'); ?></li>
      <li><?php echo $this->Html->link('【店名最終確認情報をエクスポート】','/users/DownloadLastShopSign'); ?></li>
      </ul>
    </div>
    <hr />
  <?php endif; ?>
<?php endif; ?>

    <h3>【メーリングリスト】</h3>
    <p>各テナントにメールを発信できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【メーリングリスト】','/users/MailingList'); ?></li>
      </ul>
    </div>
    <hr />

<?php if( $userinfo['role']=='admin'): // 管理者のみ ?>
    <h3>【シートロック管理】</h3>
    <p>各シートの編集の可否を設定できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【シートロック管理】','/users/AdminControl'); ?></li>
      </ul>
    </div>
    <hr />

  <?php if( defined('ADD_AUTHORIZED_USER')): ?>
    <h3>【ユーザー追加】</h3>
    <p>ユーザーを追加できます。</p>
    <div>
      <ul>
      <li><?php echo $this->Html->link('【ユーザー追加】','/users/AddAuthorizedUser'); ?></li>
      </ul>
    </div>
    <hr />
  <?php endif; ?>

<?php endif; ?>

</div>

<script type="text/javascript">
$(function() {
  $('.dropdown-toggle').dropdown()
});
</script>
