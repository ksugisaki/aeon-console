<?php echo $this->Html->script(array('check_sheet_lock'), array('inline'=>false)); ?>
<div class ="hero-unit">
  <h2>シートロック管理</h2>
  <!--<p>あなたの登録メールアドレスは <?php echo h( $userinfo['email']);?> です。</p>-->
</div>
<?php
    $lock_fields = array();
    $lock_labels = array();

    if( defined('LAST_SHOP_SIGN')){
        $lock_fields[] = 'lock_last_shop_sign';
        $lock_labels[] = '重要【店名最終確認】';
    }
    $lock_fields[] = 'lock_user_profile';
    $lock_fields[] = 'lock_section';
    $lock_fields[] = 'lock_hearing1';
    $lock_fields[] = 'lock_hearing2';
    $lock_fields[] = 'lock_signboard';
    $num = 1;
    $lock_labels[] = 'No. '.$num++.'【御社情報】';
    $lock_labels[] = 'No. '.$num++.'【担当者確認シート】';
    $lock_labels[] = 'No. '.$num++.'【ヒアリングシート】';
    $lock_labels[] = 'No. '.$num++.'【ヒアリングシート２】';
    $lock_labels[] = 'No. '.$num++.'【店名確認シート】';

	if( defined('SYOKUJYU_LINK')): // 植樹祭の項目を追加
		$lock_fields[] = 'lock_syokujyu';
		$lock_labels[] = 'No. '.$num++.'【植樹祭参加者登録】';
	endif;
    if( defined('TERMINAL_COMPANY')): // 端末の項目を追加
        $lock_fields[] = 'lock_user_terminal';
        $lock_labels[] = 'No. '.$num++.'【入力用端末申込書】';

        $lock_fields[] = 'lock_receipt';
        $lock_labels[] = 'No. '.$num++.'【レシート用情報】';
    endif;
    if( defined('GOOGLEMAP_LINK')):
        $lock_fields[] = 'lock_google_map';
        $lock_labels[] = 'No. '.$num++.'【Google Mapについて】';
    endif;
    if( defined('FUKUBUKURO')):
        $lock_fields[] = 'lock_fukubukuro';
        $lock_labels[] = 'No. '.$num++.FUKUBUKURO;
    endif;
    if( defined('LAST_HANSOKU')):
        $lock_fields[] = 'lock_last_hansoku';
        $lock_labels[] = 'No. '.$num++.LAST_HANSOKU;
    endif;
    if( defined('MEDIA_STATION')):
        $lock_fields[] = 'lock_media_station';
        $lock_labels[] = 'No. '.$num++.MEDIA_STATION;
    endif;
    if( defined('MOCK_OPERATIONS')):
        $lock_fields[] = 'lock_mock_operation';
        $lock_labels[] = 'No. '.$num++.MOCK_OPERATIONS;
    endif;
    if( defined('CHRISTMAS_FEATURES')):
        $lock_fields[] = 'lock_christmas_feature';
        $lock_labels[] = 'No. '.$num++.CHRISTMAS_FEATURES;
    endif;

	if( defined('PERMIT_CARDS')):
		$lock_fields[] = 'lock_permit_card';
		$lock_labels[] = PERMIT_CARDS;
	endif;
	if( defined('PRE_CAR_PORTS')):
		$lock_fields[] = 'lock_pre_car_port';
		$lock_labels[] = PRE_CAR_PORTS;
	endif;
	if( defined('CAR_PORTS')):
		$lock_fields[] = 'lock_car_port';
		$lock_labels[] = CAR_PORTS;
	endif;
	if( defined('BICYCLES')):
		$lock_fields[] = 'lock_bicycle';
		$lock_labels[] = BICYCLES;
	endif;
	if( defined('LOCKERS')):
		$lock_fields[] = 'lock_locker';
		$lock_labels[] = LOCKERS;
	endif;
	if( defined('CASHBOXES')):
		$lock_fields[] = 'lock_cashbox';
		$lock_labels[] = CASHBOXES;
	endif;
	if( defined('FIRE_DRILLS')):
		$lock_fields[] = 'lock_fire_drill';
		$lock_labels[] = FIRE_DRILLS;
	endif;
	if( defined('OPERATION_BRIEFINGS')):
		$lock_fields[] = 'lock_operation_briefing';
		$lock_labels[] = OPERATION_BRIEFINGS;
	endif;
	if( defined('BILLS')):
		$lock_fields[] = 'lock_bill';
		$lock_labels[] = '【請求書送付先登録届】';
	endif;
	if( defined('EMERGENCY_CONTACTS')):
		$lock_fields[] = 'lock_emergency_contact';
		$lock_labels[] = '【緊急連絡先及び火元責任者届出書】';
	endif;
	if( defined('CASHIERS')):
		$lock_fields[] = 'lock_cashier';
		$lock_labels[] = '【両替機・入金機運用説明会・売上会計説明会参加申込書】';
	endif;

//	if( defined('MOCK_OPERATIONS') && ( SUB_DOMAIN =='toin')):
//		$lock_fields[] = 'lock_mock_operation';
//		$lock_labels[] = '【模擬営業、店頭準備についてのアンケート】';
//	endif;

?>
<?php echo $this->Form->create('User', array('class'=>'','action'=>'AdminControl'));?>
<?php echo $this->Form->button('登録',array('class' => 'btn btn-primary'));?>
<p class="clear"></p>
<table class="admin_list table table-striped table-bordered table-condensed">
<?php
	$labels = array('ID','会社名','店名','担当者名','シートロック','最終更新日');
	$class_check_box = array('シートロック');
	foreach( $lock_fields as $key => $field ){
		$class_check_box[] = $this->Form->input( $field, array(
						'id'=>$field,
						'label'=> $lock_labels[$key],'type'=>'checkbox','div'=>false));
	}
    $labels[4] = implode('', $class_check_box );

	$fields = array('loginid','company','shop','name','lock','lastupdate');

	// テーブル・ヘッダー
	foreach( $fields as $key => $field ){
	if( $field =='loginid'|| $field =='company'|| $field =='name'){
		$header_link = $this->Html->url('/users/AdminControl/'.$field, true);
		$labels[ $key ] = $this->Html->link( $labels[ $key ], $header_link );
	}
	}

	echo '<tr>';
	foreach( $labels as $ix => $item ){
		echo '<th nowrap>';
		echo $item;
		echo '</th>';
	}
	echo '</tr>';

	// テーブル・ボディー
	for( $i = 0; $i < count( $all ); $i++ ){
		// プロフィール編集へのリンクを追加
		if( $userinfo['role'] == 'admin'||$userinfo['role'] == 'partner' ){
			// プロフィールへのリンク
			$edit_link = $this->Html->url('/Users/edit/'.$all[$i]['User']['id'].'?mode=admin', true);
			$all[$i]['User']['name'] = $this->Html->link( $all[$i]['User']['name'], $edit_link);
			// IDの表示
			if( ! $all[$i]['User']['loginid']) $all[$i]['User']['loginid'] = '未設定';
			// 属性の表示
			if( $all[$i]['User']['role']=='admin') $all[$i]['User']['role'] = '管理者';

			if( isset( $all[$i]['Signboard'])){
				$all_signboards = $all[$i]['Signboard'];
				// user_id重複しているデータを除く
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
			// 店名を表示
			if( isset( $signboard_last['shop_sign'])){
				$all[$i]['User']['shop'] = $signboard_last['shop_sign'];
			}else{
				$all[$i]['User']['shop'] = '';
			}

			$lock_input = array();

			foreach( $lock_fields as $key => $field ){
				$lock_input[] = $this->Form->input( $all[$i]['User']['id'] .'.'. $field, array(
								'class'=>$field,
								'label'=> $lock_labels[$key],'type'=>'checkbox','div'=>false,
								'checked'=>$all[$i]['User'][$field]));
			}

			$all[$i]['User']['lock'] = implode('', $lock_input );

            $all[$i]['User']['lastupdate'] = '';
            // 最終更新日（users/LastShopSign）
            if( defined('LAST_SHOP_SIGN')){
                if( isset( $all[$i]['User']['last_shop_sign_modified']))
                    $modified = date('m/d(H:i)', strtotime( $all[$i]['User']['last_shop_sign_modified']));
                else
                    $modified = '＜未登録＞';
                $all[$i]['User']['lastupdate'] .=  $modified . '<br />';
            }

			// 最終更新日（User）
			$user_created = $all[$i]['User']['created'];
            $user_modified = $user_created;
            if( isset( $all[$i]['User']['profile_modified']))
                $user_modified = $all[$i]['User']['profile_modified'];
			$user_update = $user_modified? date('m/d(H:i)', strtotime( $user_modified )) : '';
			$all[$i]['User']['lastupdate'] .= $user_update .'<br />';

			// 最終更新日（Section）
			$section_modified = isset( $all[$i]['Section']['created'])? $all[$i]['Section']['created'] : $user_created;
			$section_update ='';
			if( count( $all[$i]['Section']) >0 ){
 				foreach( $all[$i]['Section'] as $section ){
					if( $section['modified'] ){
						if( $section['modified'] > $section_modified ){
							$section_modified = $section['modified'];
							$section_update = $section_modified? date('m/d(H:i)', strtotime( $section_modified )) : '';
						}
					}
				}
				$section_update = $section_modified? date('m/d(H:i)', strtotime( $section_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $section_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}

			// 最終更新日（Hearing）
			if( isset( $all[$i]['Hearing'][0])){
				$hearing1_modified = $all[$i]['Hearing'][0]['updated'];
				$hearing1_update = $hearing1_modified? date('m/d(H:i)', strtotime( $hearing1_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $hearing1_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}
			// 最終更新日（OptionHearing）
			if( isset( $all[$i]['OptionHearing'][0])){
				$hearing2_modified = $all[$i]['OptionHearing'][0]['modified'];
				$hearing2_update = $hearing2_modified? date('m/d(H:i)', strtotime( $hearing2_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $hearing2_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}

			// 最終更新日（Signboard）
			$signboard_modified = $user_created;
			$signboard_update ='';
			if( count( $all[$i]['Signboard']) >0 ){
				foreach( $all[$i]['Signboard'] as $signboard ){
					if( $signboard['modified'] ){
						if( $signboard['modified'] > $signboard_modified ){
							$signboard_modified = $signboard['modified'];
							$signboard_update = $signboard_modified? date('m/d(H:i)', strtotime( $signboard_modified )) : '';
						}
					}
				}
				$signboard_update = $signboard_modified? date('m/d(H:i)', strtotime( $signboard_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $signboard_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}

			// 最終更新日（SyokujyuItem）
			if( defined('SYOKUJYU_LINK')):
				if( isset( $all[$i]['SyokujyuItem'][0])){
					$syokujyu_modified = $all[$i]['SyokujyuItem'][0]['modified'];
					$syokujyu_update = $syokujyu_modified? date('m/d(H:i)', strtotime( $syokujyu_modified )) : '';
					$all[$i]['User']['lastupdate'] .= $syokujyu_update .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

            // 最終更新日（terminal, receipt）
            if( defined('TERMINAL_COMPANY')):
                // 端末
                if(  !( isset( $all[$i]['User']['regi_num']))
                  && !( isset( $all[$i]['User']['terminal_num']))){
                    $all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
                }elseif( isset( $all[$i]['User']['terminal_modified'])){
                    $terminal_modified = $all[$i]['User']['terminal_modified'];
                    $terminal_modified = date('m/d(H:i)', strtotime( $terminal_modified ));
                    $all[$i]['User']['lastupdate'] .= $terminal_modified .'<br />';
                }else{
                    $all[$i]['User']['lastupdate'] .= date('m/d(H:i)', strtotime( $user_created )) .'<br />';
                }
                // レシート
                if( ! isset( $all[$i]['User']['receipt_tenant_name'])){
                    $all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
                }elseif( isset( $all[$i]['User']['receipt_modified'])){
                    $receipt_modified = $all[$i]['User']['receipt_modified'];
                    $receipt_modified = date('m/d(H:i)', strtotime( $receipt_modified ));
                    $all[$i]['User']['lastupdate'] .= $receipt_modified .'<br />';
                }else{
                    $all[$i]['User']['lastupdate'] .= date('m/d(H:i)', strtotime( $user_created )) .'<br />';
                }
            endif;

            // 最終更新日（ Google Map Link ）
            if( defined('GOOGLEMAP_LINK')):
                if( isset( $all[$i]['User']['google_map_modified'])){
                    $google_map_modified = $all[$i]['User']['google_map_modified'];
                    $google_map_modified = date('m/d(H:i)', strtotime( $google_map_modified ));
                    $all[$i]['User']['lastupdate'] .= $google_map_modified .'<br />';
                }else{
                    $all[$i]['User']['lastupdate'] .= date('m/d(H:i)', strtotime( $user_created )) .'<br />';
                }
            endif;

            if( defined('FUKUBUKURO')):
                    $all[$i]['User']['lastupdate'] .= '<br />';
            endif;
            if( defined('LAST_HANSOKU')):
                    $all[$i]['User']['lastupdate'] .= '<br />';
            endif;
            if( defined('MEDIA_STATION')):
                    $all[$i]['User']['lastupdate'] .= '<br />';
            endif;
            if( defined('MOCK_OPERATIONS')):
                    $all[$i]['User']['lastupdate'] .= '<br />';
            endif;
            if( defined('CHRISTMAS_FEATURES')):
                    $all[$i]['User']['lastupdate'] .= '<br />';
            endif;

			// 最終更新日（MockOperations）
			if( false && defined('MOCK_OPERATIONS')):
				if( isset( $all[$i]['MockOperation'][0])){
					$modified = $all[$i]['MockOperation'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（PermitCards）op4
			if( defined('PERMIT_CARDS')):
				if( isset( $all[$i]['PermitCard'][0])){
					$modified = $all[$i]['PermitCard'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（PreCarPorts）op4
			if( defined('PRE_CAR_PORTS')):
				if( isset( $all[$i]['PreCarPort'][0])){
					$modified = $all[$i]['PreCarPort'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（CarPorts）op5
			if( defined('CAR_PORTS')):
				if( isset( $all[$i]['CarPort'][0])){
					$modified = $all[$i]['CarPort'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Bicycles）op6
			if( defined('BICYCLES')):
				if( isset( $all[$i]['Bicycle'][0])){
					$modified = $all[$i]['Bicycle'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Lockers）op7
			if( defined('LOCKERS')):
				if( isset( $all[$i]['Locker'][0])){
					$modified = $all[$i]['Locker'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Cashboxes）op8
			if( defined('CASHBOXES')):
				if( isset( $all[$i]['Cashbox'][0])){
					$modified = $all[$i]['Cashbox'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（FireDrills）
			if( defined('FIRE_DRILLS')):
				if( isset( $all[$i]['FireDrill'][0])){
					$modified = $all[$i]['FireDrill'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（OperationBriefing）
			if( defined('OPERATION_BRIEFINGS')):
				if( isset( $all[$i]['OperationBriefing'][0])){
					$modified = $all[$i]['OperationBriefing'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Bills）
			if( defined('BILLS')):
				if( isset( $all[$i]['Bill'][0])){
					$modified = $all[$i]['Bill'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Cashiers）
			if( defined('CASHIERS')):
				if( isset( $all[$i]['Cashier'][0])){
					$modified = $all[$i]['Cashier'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（EmergencyContacts）
			if( defined('EMERGENCY_CONTACTS')):
				if( isset( $all[$i]['EmergencyContact'][0])){
					$modified = $all[$i]['EmergencyContact'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			$all[$i]['User']['lastupdate'] ='<div style="line-height:25px;">'
										   . $all[$i]['User']['lastupdate']
										   . '</div>';

			$role = $all[$i]['User']['role'];
			// カラムの順番を変更
			$row = array(
				'loginid'=> $all[$i]['User']['loginid'],
				'company'=> $all[$i]['User']['company'],
				'shop'=> $all[$i]['User']['shop'],
				'name'=> $all[$i]['User']['name'],
				'lock'=> $all[$i]['User']['lock'],
				'lastupdate'=> $all[$i]['User']['lastupdate'],
				'id'=> $all[$i]['User']['id']);
			$all[$i]['User'] = $row;
			//debug( $all[$i]['User']);

		}
		// 表を作成
		unset( $all[$i]['User']['id']); // idは除く
		unset( $all[$i]['User']['role']); // roleは除く
		$tr = $this->Html->tableCells( $all[$i]['User']);
		if( $role != '' ){
			$tr = str_replace( '<tr>', '<tr class="admin">', $tr);
		}
		// 表1行を出力
		echo( $tr );
	}
?>
</table>
<?php echo $this->Form->button('登録',array('class' => 'btn btn-primary'));?>
<?php echo $this->Form->end();?>

<!-- テーブルの5,6桁目（フォーム）のセルを折り返し無しにする -->
<script type="text/javascript">
$(function() {
    $("td:eq(4)").attr("nowrap", true);
    $("td:eq(5)").attr("nowrap", true);
});
</script>

<!-- ページを離れる時に警告 -->
<script type="text/javascript">
$(function(){
    $("input[type=checkbox]").change(function() {
        $(window).on('beforeunload', function() {
            return '「登録」ボタンを押さずにページを離れると入力された設定が反映されません。';
        });
    });
    $("button[type=submit]").click(function() {
        $(window).off('beforeunload');
    });
});
</script>
