<div class ="hero-unit">
  <div>
    <img src="<?php echo $this->Html->url('/img/step2.jpg') ?>" />
  </div>
  <br />
  <h2>2.メール認証</h2>
    <p>
    <?php if(isset($send_message)): ?>
入力いただいたメールアドレスに本登録用URLを記載したメールを送信しました。<br />届いたメールに記載されたURLにアクセスして本登録手続きをお願い致します。
    <?php else: ?>
    ログインに使用するメールアドレスを登録して下さい。<br />
    登録完了後、仮登録を継続する為のURLをご登録アドレス宛にお送り致します。<br />メール送信後24時間以内にご登録を完了して頂きますようよろしくお願い致します。
    <?php endif ?>
    </p>
</div>
<?php echo $this->Session->flash(); ?>
<?php
if( isset( $email )){
    // echo '<div class="span12 alert alert-success alert-block">';
    // echo 'ご登録メールアドレス : ';
    // echo '<strong>'. $email .'</strong>';
    // echo '</div>';
}else{ ?>
<div class="user form">
    <?php
        echo $this->Form->create('User', Array('class'=>'form-horizontal span10','url' =>'add'));
        $i = 0;
        foreach ( $fields as $field ){
            if( $field == 'id' ):
                echo $this->Form->input('id', array('type'=>'hidden'));
            elseif( $field == 'password' ):
                echo $this->Form->input( $field, array('label'=>$labels[ $i ],'type'=>'password','class'=>'span8'));
            elseif( $field == 'password_confirm' ):
                echo $this->Form->input('password_confirm', 
                           Array('type' => 'password','class'=>'span8', 'label'=>$labels[ $i ]));
            else:
                echo $this->Form->input( $field, array('label'=>$labels[ $i ],'type'=>'text','class'=>'span8'));
            endif;
            $i += 1;
        }
        echo '<br />';
        echo $this->Form->button('次へ',array('class' => 'btn btn-primary offset2'));
        echo $this->Form->end();
    ?>
</div>
<?php } ?>
