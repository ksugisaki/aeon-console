<?php if( $print == null ){ ?>
<div class ="hero-unit">
  <h2><?php echo $title_for_layout ?></h2>
  <p>本書にレシートに印字される情報を記入して下さい。</p>
</div>

<?php }else{ ?>
<div class="row-fluid">
	<div class="span1">
	</div>
	<div class="span5">
		<h2><?php echo $title_for_layout ?></h2>
	</div>
	<div class="span6">
		<?php if( isset( $user_info['receipt_modified'])){ ?>
			<p>最終更新日： <?php echo( $user_info['receipt_modified'] );?></p>
		<?php }else{ ?>
			<p>最終更新日： <?php echo( $user_info['created'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['company'])){ ?>
			<p>会社名： <?php echo( $user_info['company'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>テナント名： <?php echo( $user_info['receipt_tenant_name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['receipt_tenant_name'])){ ?>
			<p>部署： <?php echo( $user_info['department'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['name'])){ ?>
			<p>担当者名： <?php echo( $user_info['name'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>電話番号： <?php echo( $user_info['phone'] );?></p>
		<?php } ?>
		<?php if( isset( $user_info['phone'])){ ?>
			<p>Email： <?php echo( $user_info['email'] );?></p>
		<?php } ?>
	</div>
</div>
<?php } ?>

<?php echo $this->Session->flash(); ?>
<?php
    echo $this->Form->create('User', array('class'=>'terminal form-horizontal span10','action'=>'receipt'));
	// テーブルの中身を準備( $cells )
    $cells = array();
    foreach ( $fields as $key => $field ){
        if( $field == 'id' ):
            echo $this->Form->input('id', array('type'=>'hidden'));
        else: // 例など注意書きを追記
            $input = $this->Form->input( $field, array('label'=>'','type'=>'text','class'=>'span8'));
            if( $field == 'receipt_tenant_name' )
                $input .= '<p>※ （例） ◯◯書房 ・・・半角文字：16文字、全角文字：8文字以内で入力してください。</br>'
                           . '（混在の場合は、全角文字を２文字分と数えて16文字以内で入力してください。）</p>';
            else if( $field == 'receipt_phone' )
                $input .= '<p>※ （例） 03-123-4567 ・・・１４文字以内の半角数字とハイフン(-)で入力してください。</p>';
            else
                $input .= '<p></p>';

            $cells[] = array(
                 '<div class="span2">' . $labels[ $key ] . '</div>',
                 $input
            );
        endif;

		// テーブルとヘッダーを出力
        if( $field == 'receipt_phone' ):
            echo '<br />';
            echo '<h4>■レシート用情報</h4>';
            echo '<table class="communication table table-bordered">';
            echo $this->Html->tableCells( $cells );
            echo '</table>';
            $cells = array();
			echo '<br /><br />';

        endif;
    }
    echo '<br />';
    if( empty( $locked )){
        echo $this->Form->button('登録',array('class' => 'btn btn-primary'));
    }else{
        echo MESSAGE_AT_LOCKED;
    }
    echo $this->Form->end();

?>
<br />
<br />
<br />
<!-- モーダル -->
<div class="modal hide" id="mymodal">
  <div class="modal-header">
    <div>登録結果</div>
    <br />
  </div>
  <div class="modal-body">
    <p id="modal_message"></p>
  </div>
  <div class="modal-footer">
    <button id="close_mymodal" data-dismiss="modal">OK</button>
  </div>
</div>
<p class="clear"></p>
<a href="<?php echo $this->Html->url('/',true);?>"><i class="icon-home"></i>業務メニューに戻る</a>
