<div class ="hero-unit">
  <?php if( $edit_mode ): ?>
      <h2>ユーザー管理</h2>
      <?php $link_field_title = '編集'; ?>
  <?php else: ?>
      <h2>ユーザーリスト</h2>
      <?php $link_field_title = 'フォーム'; ?>
  <?php endif; ?>
</div>
<p>※管理者様は薄黄の背景色、協力会社様は薄緑の背景色で表示されます。</p>
<table class="admin_list table table-striped table-bordered table-condensed">
<?php
	// 幕張、東員のみ区画番号を追加
	switch( SUB_DOMAIN ){
	case 'makuharishintoshin':
	case 'toin':
		$labels = array('ID','区画番号','会社名','店名','担当者名',$link_field_title,'最終更新日','ステータス');
		$fields = array('loginid','block','company','shop','name','link','lastupdate','user_status');
		break;
	default:
		$labels = array('ID','会社名','店名','担当者名',$link_field_title,'最終更新日','ステータス');
		$fields = array('loginid','company','shop','name','link','lastupdate','user_status');
	}
	if( $userinfo['role'] == 'admin') // 管理者のみ削除付き
		$labels[] = '削除';

    // テーブル・ヘッダー
    foreach( $fields as $key => $field ){
        if( $field =='loginid'|| $field =='block'|| $field =='company'
         || $field =='name'|| $field =='user_status'){
            $header_link = $this->Html->url( $this_action .'/'. $field, true);
            $labels[ $key ] = $this->Html->link( $labels[ $key ], $header_link );
        }
    }

	echo '<tr>';
	foreach( $labels as $ix => $item ){
		echo '<th nowrap>';
		echo $item;
		echo '</th>';
	}
	echo '</tr>';

    // テーブル・ボディー
    for( $i = 0; $i < count( $all ); $i++ ){
        $users[] = $all[$i]['User'];
        $email = $all[$i]['User']['email'];
        $role = $all[$i]['User']['role'];
        // プロフィール編集へのリンクを追加
        if( $userinfo['role'] == 'admin' || $userinfo['role'] == 'partner' ){
            // プロフィールへのリンク
            $myrole = $this->Session->read('Auth.User.role');
            $edit_profile = $this->Html->url('/Users/edit/'.$all[$i]['User']['id'].'?mode='. $myrole, true);
            if( $userinfo['role'] == 'admin')
                $all[$i]['User']['name'] = $this->Html->link( $all[$i]['User']['name'], $edit_profile);
            // IDの表示
            if( ! $all[$i]['User']['loginid']) $all[$i]['User']['loginid'] = '未設定';
            // 属性の表示
            if( $all[$i]['User']['role']=='admin') $all[$i]['User']['role'] = '管理者';

			if( isset( $all[$i]['Signboard'])){
				$all_signboards = $all[$i]['Signboard'];
				// user_id重複しているデータを除く
				$signboard_last = array();
				$prev_modified = 0;
				foreach( $all_signboards as $signboard){
					if( $prev_modified == 0 )
						$signboard_last = $signboard;
					else if( $signboard['modified'] > $prev_modified ) // 新しいなら
						$signboard_last = $signboard; // 上書き
					$prev_modified = $signboard['modified'];
				}
			}
            // 店名を表示
			if( isset( $signboard_last['shop_sign'])){
				$all[$i]['User']['shop'] = $signboard_last['shop_sign'];
			}else{
				$all[$i]['User']['shop'] = '';
			}

/*
            // 履歴へのリンクを追加
            if( count( $histories[$i])){
                $history_link = $this->Html->url('/Histories/index/'.$all[$i]['User']['id'], true);
                $all[$i]['User']['history'] = $this->Html->link( '履歴', $history_link);
            }else{
                $all[$i]['User']['history'] = '';
            }
*/

            // 「フォーム」へのリンクを追加
            if( $edit_mode ){ // 編集へのリンク
                $last_shop_link = $this->Html->url('/users/LastShopSign/'.$all[$i]['User']['id'], true);
                $user_link  = $this->Html->url('/users/edit/'.$all[$i]['User']['id'], true);
                $section_link = $this->Html->url('/sections/addSection/'.$all[$i]['User']['id'], true);
                $hearing_link  = $this->Html->url('/hearings/index/'.$all[$i]['User']['id'], true);
                $hearing_link2 = $this->Html->url('/option_hearings/check/'.$all[$i]['User']['id'], true);
                $signboard_link = $this->Html->url('/signboards/index/'.$all[$i]['User']['id'], true);
                $syokujyu_link = $this->Html->url('/syokujyu_items/checkprint/'.$all[$i]['User']['id'], true);
                $pre_car_port_link = $this->Html->url('/pre_car_ports/edit/'.$all[$i]['User']['id'], true);
                $car_port_link = $this->Html->url('/car_ports/edit/'.$all[$i]['User']['id'], true);
                $bicycle_link = $this->Html->url('/bicycles/edit/'.$all[$i]['User']['id'], true);
                $permit_card_link = $this->Html->url('/permit_cards/edit/'.$all[$i]['User']['id'], true);
                $cashbox_link = $this->Html->url('/cashboxes/edit/'.$all[$i]['User']['id'], true);
                $bill_link = $this->Html->url('/bills/edit/'.$all[$i]['User']['id'], true);
                $locker_link = $this->Html->url('/lockers/edit/'.$all[$i]['User']['id'], true);
                $mock_link = $this->Html->url('/mock_operations/edit/'.$all[$i]['User']['id'], true);
                $emergency_link = $this->Html->url('/emergency_contacts/edit/'.$all[$i]['User']['id'], true);
                $cashier_link = $this->Html->url('/cashiers/edit/'.$all[$i]['User']['id'], true);
                $fire_drill_link = $this->Html->url('/fire_drills/edit/'.$all[$i]['User']['id'], true);
                $operation_briefing_link = $this->Html->url('/operation_briefing/edit/'.$all[$i]['User']['id'], true);

                $terminal_link = $this->Html->url('/users/terminal/'.$all[$i]['User']['id'], true);
                $receipt_link = $this->Html->url('/users/receipt/'.$all[$i]['User']['id'], true);
                $googlemap_link = $this->Html->url('/users/settingGoogleMap/'.$all[$i]['User']['id'], true);

                $link_option = array();

            }else{ // 印刷用フォームへのリンク
                $last_shop_link = $this->Html->url('/users/LastShopSign/'.$all[$i]['User']['id'].'/print', true);
                $user_link  = $this->Html->url('/users/edit/'.$all[$i]['User']['id'].'/print', true);
                $section_link = $this->Html->url('/print/section/'.$all[$i]['User']['id'], true);
                $hearing_link  = $this->Html->url('/hearings/index/'.$all[$i]['User']['id'].'/print', true);
                $hearing_link2 = $this->Html->url('/option_hearings/checkprint/'.$all[$i]['User']['id'], true);
                $signboard_link = $this->Html->url('/signboards/index/'.$all[$i]['User']['id'].'/print', true);
                $syokujyu_link = $this->Html->url('/syokujyu_items/checkprint/'.$all[$i]['User']['id'].'/print', true);
                $pre_car_port_link = $this->Html->url('/pre_car_ports/edit/'.$all[$i]['User']['id'].'/print', true);
                $car_port_link = $this->Html->url('/car_ports/edit/'.$all[$i]['User']['id'].'/print', true);
                $bicycle_link = $this->Html->url('/bicycles/edit/'.$all[$i]['User']['id'].'/print', true);
                $permit_card_link = $this->Html->url('/permit_cards/edit/'.$all[$i]['User']['id'].'/print', true);
                $cashbox_link = $this->Html->url('/cashboxes/edit/'.$all[$i]['User']['id'].'/print', true);
                $bill_link = $this->Html->url('/bills/edit/'.$all[$i]['User']['id'].'/print', true);
                $locker_link = $this->Html->url('/lockers/edit/'.$all[$i]['User']['id'].'/print', true);
                $mock_link = $this->Html->url('/mock_operations/edit/'.$all[$i]['User']['id'].'/print', true);
                $emergency_link = $this->Html->url('/emergency_contacts/edit/'.$all[$i]['User']['id'].'/print', true);
                $cashier_link = $this->Html->url('/cashiers/edit/'.$all[$i]['User']['id'].'/print', true);
                $fire_drill_link = $this->Html->url('/fire_drills/edit/'.$all[$i]['User']['id'].'/print', true);
                $operation_briefing_link = $this->Html->url('/operation_briefing/edit/'.$all[$i]['User']['id'].'/print', true);


                $terminal_link = $this->Html->url('/users/terminal/'.$all[$i]['User']['id'].'/print', true);
                $receipt_link = $this->Html->url('/users/receipt/'.$all[$i]['User']['id'].'/print', true);
                $googlemap_link = $this->Html->url('/users/settingGoogleMap/'.$all[$i]['User']['id'].'/print', true);

                $link_option = array('target'=>'_blank');
            }

            $all[$i]['User']['link'] = ''; // ここにリンクを入れる
            if( defined('LAST_SHOP_SIGN')){
                $all[$i]['User']['link']
                .= '<div class="users_last_shop">'
                . $this->Html->link('重要【店名最終確認】', $last_shop_link, $link_option)
                . '</div>';
            }
            $num = 1;
            $all[$i]['User']['link']
                .= '<div class="users_edit">'
                . $this->Html->link( 'No. '.$num++.'【御社情報】', $user_link, $link_option)
//                . $this->My->sheet_link( 'No. '.$num++.'【御社情報】', $user_link, $link_optin, $permission_users_edit)
                . '</div>'
                . '<div class="sections">'
                . $this->Html->link( 'No. '.$num++.'【担当者確認シート】', $section_link, $link_option)
                . '</div>';
            switch( SUB_DOMAIN ):
            case 'makuharishintoshin':
            case 'toin':
                $all[$i]['User']['link']
                    .= '<div class="hearings">'
                    . $this->Html->link( 'No. '.$num++.'【ヒアリングシート】', $hearing_link, $link_option)
                    . '</div>'
                    . '<div class="option_hearings">'
                    . $this->Html->link( 'No. '.$num++.'【ヒアリングシート２】', $hearing_link2, $link_option)
                    . '</div>'
                    . '<div class="signboards">'
                    . $this->Html->link( 'No. '.$num++.'【店名確認シート】', $signboard_link, $link_option)
                    . '</div>';
            break;
            endswitch;

            if( defined('SYOKUJYU_LINK')):
              if(SUB_DOMAIN =='toin'){
                $all[$i]['User']['link']
                    .= '<div class="syokujyu_items">'
                    . $this->Html->link( 'No. '.$num++.'【植樹祭参加者登録】', $syokujyu_link, $link_option)
                    . '</div>';
              }else{
                $all[$i]['User']['link']
                    .= '<div class="syokujyu_items">'
                    . $this->Html->link( '営業'.'【植樹祭参加者登録】', $syokujyu_link, $link_option)
                    . '</div>';
              }
            endif;

            if( defined('TERMINAL_COMPANY')):
                $all[$i]['User']['link']
                    .= '<div class="users_terminal">'
                    . $this->Html->link( 'No. '.$num++.'【入力用端末申込書】', $terminal_link, $link_option)
                    . '</div>';
                $all[$i]['User']['link']
                    .= '<div class="users_receipt">'
                    . $this->Html->link( 'No. '.$num++.'【レシート用情報】', $receipt_link, $link_option)
                    . '</div>';
            endif;

            if( defined('GOOGLEMAP_LINK')):
                $all[$i]['User']['link']
                    .= '<div class="users_google_map">'
                    . $this->Html->link( 'No. '.$num++.'【Google Mapについて】', $googlemap_link, $link_option)
                    . '</div>';
            endif;

            if( defined('PERMIT_CARDS')):
                $all[$i]['User']['link'] .= '<br />';
                
                $all[$i]['User']['link']
                    .= '<div class="permit_cards">'
                    . $this->Html->link( PERMIT_CARDS, $permit_card_link, $link_option)
                    . '</div>';
            endif;
            if( defined('PRE_CAR_PORTS')):
                $all[$i]['User']['link']
                    .= '<div class="pre_car_ports">'
                    . $this->Html->link( PRE_CAR_PORTS, $pre_car_port_link, $link_option)
                    . '</div>';
            endif;
            if( defined('CAR_PORTS')):
                $all[$i]['User']['link']
                    .= '<div class="car_ports">'
                    . $this->Html->link( CAR_PORTS, $car_port_link, $link_option)
                    . '</div>';
            endif;
            if( defined('BICYCLES')):
                $all[$i]['User']['link']
                    .= '<div class="bicycles">'
                    . $this->Html->link( BICYCLES, $bicycle_link, $link_option)
                    . '</div>';
            endif;
            if( defined('LOCKERS')):
                $all[$i]['User']['link']
                    .= '<div class="lockers">'
                    . $this->Html->link( LOCKERS, $locker_link, $link_option)
                    . '</div>';
            endif;
            if( defined('CASHBOXES')):
                $all[$i]['User']['link']
                    .= '<div class="cashboxes">'
                    . $this->Html->link( CASHBOXES, $cashbox_link, $link_option)
                    . '</div>';
            endif;
            if( defined('FIRE_DRILLS')):
                $all[$i]['User']['link']
                    .= '<div class="fire_drills">'
                    . $this->Html->link( FIRE_DRILLS, $fire_drill_link, $link_option)
                    . '</div>';
            endif;
            if( defined('OPERATION_BRIEFINGS')):
                $all[$i]['User']['link']
                    .= '<div class="operation_briefing">'
                    . $this->Html->link( OPERATION_BRIEFINGS, $operation_briefing_link, $link_option)
                    . '</div>';
            endif;
            if( defined('BILLS')):
                $all[$i]['User']['link']
                    .= '<div class="bills">'
                    . $this->Html->link( '【請求書送付先登録届】', $bill_link, $link_option)
                    . '</div>';
            endif;
            if( defined('MOCK_OPERATIONS') && ( SUB_DOMAIN =='makuharishintoshin')):
                $all[$i]['User']['link']
                    .= '<div class="mock_operations">'
                    . $this->Html->link( '【ソフトオープン前 模擬営業（オペレーション確認）について】', $mock_link, $link_option)
                    . '</div>';
            endif;
            if( defined('EMERGENCY_CONTACTS')):
                $all[$i]['User']['link']
                    .= '<div class="emergency_contacts">'
                    . $this->Html->link( '【緊急連絡先及び火元責任者届出書】', $emergency_link, $link_option)
                    . '</div>';
            endif;
            if( defined('CASHIERS')):
                $all[$i]['User']['link']
                    .= '<div class="cashiers">'
                    . $this->Html->link( '【両替機・入金機運用説明会・売上会計説明会参加申込書】', $cashier_link, $link_option)
                    . '</div>';
            endif;

            $all[$i]['User']['lastupdate'] = '';
            // 最終更新日（users/LastShopSign）
            if( defined('LAST_SHOP_SIGN')){
                if( isset( $all[$i]['User']['last_shop_sign_modified']))
                    $modified = date('m/d(H:i)', strtotime( $all[$i]['User']['last_shop_sign_modified']));
                else
                    $modified = '＜未登録＞';
                $all[$i]['User']['lastupdate'] .=  $modified . '<br />';
            }

			// 最終更新日（User）
			$user_created = $all[$i]['User']['created']; // 後で使う
            $user_modified = $user_created;
            if( isset( $all[$i]['User']['profile_modified']))
                $user_modified = $all[$i]['User']['profile_modified'];
			$user_update = $user_modified? date('m/d(H:i)', strtotime( $user_modified )) : '';
			$all[$i]['User']['lastupdate'] .= $user_update .'<br />';

			// 最終更新日（Section）
			$section_modified = isset( $all[$i]['Section']['created'])? $all[$i]['Section']['created'] : $user_created;
			$section_update ='';
			if( count( $all[$i]['Section']) >0 ){
 				foreach( $all[$i]['Section'] as $section ){
					if( $section['modified'] ){
						if( $section['modified'] > $section_modified ){
							$section_modified = $section['modified'];
							$section_update = $section_modified? date('m/d(H:i)', strtotime( $section_modified )) : '';
						}
					}
				}
				$section_update = $section_modified? date('m/d(H:i)', strtotime( $section_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $section_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}

			// 最終更新日（Hearing）
			if( isset( $all[$i]['Hearing'][0])){
				$hearing1_modified = $all[$i]['Hearing'][0]['updated'];
				$hearing1_update = $hearing1_modified? date('m/d(H:i)', strtotime( $hearing1_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $hearing1_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}
			// 最終更新日（OptionHearing）
			if( isset( $all[$i]['OptionHearing'][0])){
				$hearing2_modified = $all[$i]['OptionHearing'][0]['modified'];
				$hearing2_update = $hearing2_modified? date('m/d(H:i)', strtotime( $hearing2_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $hearing2_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}

			// 最終更新日（Signboard）
			$signboard_modified = $user_created;
			$signboard_update ='';
			if( count( $all[$i]['Signboard']) >0 ){
				foreach( $all[$i]['Signboard'] as $signboard ){
					if( $signboard['modified'] ){
						if( $signboard['modified'] > $signboard_modified ){
							$signboard_modified = $signboard['modified'];
							$signboard_update = $signboard_modified? date('m/d(H:i)', strtotime( $signboard_modified )) : '';
						}
					}
				}
				$signboard_update = $signboard_modified? date('m/d(H:i)', strtotime( $signboard_modified )) : '';
				$all[$i]['User']['lastupdate'] .= $signboard_update .'<br />';
			}else{
				$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
			}

			// 最終更新日（SyokujyuItem）
			if( defined('SYOKUJYU_LINK')):
				if( isset( $all[$i]['SyokujyuItem'][0])){
					$syokujyu_modified = $all[$i]['SyokujyuItem'][0]['modified'];
					$syokujyu_update = $syokujyu_modified? date('m/d(H:i)', strtotime( $syokujyu_modified )) : '';
					$all[$i]['User']['lastupdate'] .= $syokujyu_update .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

            // 最終更新日（terminal, receipt）
            if( defined('TERMINAL_COMPANY')):
                // 端末
                if(  !( isset( $all[$i]['User']['regi_num']))
                  || !( isset( $all[$i]['User']['terminal_num']))){
                    $all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
                }elseif( isset( $all[$i]['User']['terminal_modified'])){
                    $terminal_modified = $all[$i]['User']['terminal_modified'];
                    $terminal_modified = date('m/d(H:i)', strtotime( $terminal_modified ));
                    $all[$i]['User']['lastupdate'] .= $terminal_modified .'<br />';
                }else{
                    $all[$i]['User']['lastupdate'] .= date('m/d(H:i)', strtotime( $user_created )) .'<br />';
                }
                // レシート
                if( ! isset( $all[$i]['User']['receipt_tenant_name'])){
                    $all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
                }elseif( isset( $all[$i]['User']['receipt_modified'])){
                    $receipt_modified = $all[$i]['User']['receipt_modified'];
                    $receipt_modified = date('m/d(H:i)', strtotime( $receipt_modified ));
                    $all[$i]['User']['lastupdate'] .= $receipt_modified .'<br />';
                }else{
                    $all[$i]['User']['lastupdate'] .= date('m/d(H:i)', strtotime( $user_created )) .'<br />';
                }
            endif;

            // 最終更新日（ Google Map Link ）
            if( defined('GOOGLEMAP_LINK')):
                if( isset( $all[$i]['User']['google_map_modified'])){
                    $google_map_modified = $all[$i]['User']['google_map_modified'];
                    $google_map_modified = date('m/d(H:i)', strtotime( $google_map_modified ));
                    $all[$i]['User']['lastupdate'] .= $google_map_modified .'<br />';
                }else{
                    $all[$i]['User']['lastupdate'] .= date('m/d(H:i)', strtotime( $user_created )) .'<br />';
                }
            endif;

			// 最終更新日（MockOperations）
			if( false && defined('MOCK_OPERATIONS')):
				if( isset( $all[$i]['MockOperation'][0])){
					$modified = $all[$i]['MockOperation'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（PermitCards）op4
			if( defined('PERMIT_CARDS')):
				if( isset( $all[$i]['PermitCard'][0])){
					$modified = $all[$i]['PermitCard'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= '<br />'. $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '<br />'. '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（PreCarPorts）op4
			if( defined('PRE_CAR_PORTS')):
				if( isset( $all[$i]['PreCarPort'][0])){
					$modified = $all[$i]['PreCarPort'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（CarPorts）op5
			if( defined('CAR_PORTS')):
				if( isset( $all[$i]['CarPort'][0])){
					$modified = $all[$i]['CarPort'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Bicycles）op6
			if( defined('BICYCLES')):
				if( isset( $all[$i]['Bicycle'][0])){
					$modified = $all[$i]['Bicycle'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Lockers）op7
			if( defined('LOCKERS')):
				if( isset( $all[$i]['Locker'][0])){
					$modified = $all[$i]['Locker'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Cashboxes）op8
			if( defined('CASHBOXES')):
				if( isset( $all[$i]['Cashbox'][0])){
					$modified = $all[$i]['Cashbox'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（FireDrills）
			if( defined('FIRE_DRILLS')):
				if( isset( $all[$i]['FireDrill'][0])){
					$modified = $all[$i]['FireDrill'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（OperationBriefing）
			if( defined('OPERATION_BRIEFINGS')):
				if( isset( $all[$i]['OperationBriefing'][0])){
					$modified = $all[$i]['OperationBriefing'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Bills）
			if( defined('BILLS')):
				if( isset( $all[$i]['Bill'][0])){
					$modified = $all[$i]['Bill'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（Cashiers）
			if( defined('CASHIERS')):
				if( isset( $all[$i]['Cashier'][0])){
					$modified = $all[$i]['Cashier'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

			// 最終更新日（EmergencyContacts）
			if( defined('EMERGENCY_CONTACTS')):
				if( isset( $all[$i]['EmergencyContact'][0])){
					$modified = $all[$i]['EmergencyContact'][0]['modified'];
					$modified = $modified? date('m/d(H:i)', strtotime( $modified )) : '';
					$all[$i]['User']['lastupdate'] .= $modified .'<br />';
				}else{
					$all[$i]['User']['lastupdate'] .= '＜未登録＞<br />';
				}
			endif;

            // 削除ボタンへのリンクを追加
            $delete_link = "#myModal_delete_". $all[$i]['User']['id'];
            $all[$i]['User']['delete'] = $this->Html->link("X", $delete_link,
                array('class'=>'btn btn-warning','data-toggle'=>'modal')
            );

			// カラムの順番を変更
			$row = array(
				'loginid'=> $all[$i]['User']['loginid'],
				'block'=> $all[$i]['User']['block'],
				'company'=> $all[$i]['User']['company'],
				'shop'=> $all[$i]['User']['shop'],
				'name'=> $all[$i]['User']['name'],
				'link'=> $all[$i]['User']['link'],
				'lastupdate'=> $all[$i]['User']['lastupdate'],
				'user_status'=> $all[$i]['User']['user_status'],
				'delete'=> $all[$i]['User']['delete'],
				'id'=> $all[$i]['User']['id']);
			// 幕張、東員のみ区画番号を追加
			switch( SUB_DOMAIN ){
			case 'makuharishintoshin':
			case 'toin':
				break;
			default:
				unset( $row['block'] );
			}

			$all[$i]['User'] = $row;
			//debug( $all[$i]['User']);

        }
        // 認証判定ボタンを作成
        $auth_link = "#myModal_auth_". $all[$i]['User']['id'];

        if( $all[$i]['User']['user_status'] == "registered"){
            if( $userinfo['role'] == 'admin')
                $all[$i]['User']['user_status'] = '<strong>(3/3) 認証待ち</strong><br />'
                    . $this->Html->link("認証", $auth_link, array(
                        'class'=>'btn btn-success','data-toggle'=>'modal'));
            else
                $all[$i]['User']['user_status'] = '<strong>(3/3) 認証待ち</strong><br />';
        }else if( $all[$i]['User']['user_status'] == ""){
            $all[$i]['User']['user_status'] = "<strong>(1/3) メール発行済</strong>" .'<br />'. $email;
        }else if( $all[$i]['User']['user_status'] == "authbacked"){
            $all[$i]['User']['user_status'] = "<strong>(2/3) メール認証中</strong>" .'<br />'. $email;
        }else if( $all[$i]['User']['user_status'] == "authorized"){
            $all[$i]['User']['user_status'] = "承認済";
        }else if( $all[$i]['User']['user_status'] == "rejected"){
            $all[$i]['User']['user_status'] = "却下";
        }else{
            $all[$i]['User']['user_status'] = $email;
        }
        // 表を作成
        unset( $all[$i]['User']['id']); // idは除く
        unset( $all[$i]['User']['role']); // roleは除く
        if( $userinfo['role'] != 'admin') // 削除は管理者のみ
            unset( $all[$i]['User']['delete']);

        $tr = $this->Html->tableCells( $all[$i]['User']);
            if( ! empty( $role))
                $tr = '<tr class="user '. $role .'">';
            else
                $tr = '<tr class="user">';
            foreach( $all[$i]['User'] as $cn => $cell ){
            if( $cn =='link' || $cn =='lastupdate'){
                $tr .= '<td nowrap>';
            }else{
                $tr .= '<td>';
            }
            $tr .= $cell;
            $tr .= '</td>';
        }
        $tr .= '</tr>';
        // 表1行を出力
        echo( $tr );
    }
?>
</table>

<div id="paginator_bar">
<?php // ページネーション・バー表示
print $this->Paginator->numbers(array(
    'before'=>' ***** ',
    'after'=>' ***** ',
    'separator'=>' | ',
    'currentClass'=>'current',
    'class'=>'paginator',
    'modulus'=>100,
    'first'=>3,
    'last'=>3,
    'tag'=>'span',
    'ellipsis' => '...'
));
?>
</div>
<div style="display: none"><!-- 隠してる -->
  <p id="html_path"><?php echo $this->Html->url(); ?></p>
</div>
<p id="progress"></p>

<?php foreach( $users as $user ){ ?>
  <?php $id = h($user['id']);?>
  <?php $name = h($user['name']);?>
  <?php $company = h($user['company']);?>
  <div class="modal hide" id="<?php echo 'myModal_auth_' . $id;?>">
    <div class="modal-header">
      <div>ユーザー登録申請の承認／却下</div>
      <button class="close" data-dismiss="modal">×</button>
      <br />
    </div>
  
    <div class="modal-body">
      <dl><dt>会社名</dt><dd><?php echo $company;?></dd><dl>
      <dl><dt>お名前</dt><dd><?php echo $name;?></dd><dl>
      <br />
      <form class="form" action="<?php echo $authAction . "/" . $id;?>" METHOD="POST">
        <input id="user_id" type="hidden" name="id" value="<?php echo $id;?>"/>
        <!-- type="submit" name='reject' class='btn btn-warning' value="却下" -->
        <input type="submit" name='accept' class='btn btn-success' value="承認" />
      </form>
    </div>
  
    <div class="modal-footer">
      <a href="#" class="btn" data-dismiss="modal">キャンセル</a>
    </div>
  </div>
<?php } ?>

<?php foreach( $users as $user ){ ?>
  <?php $id = h($user['id']);?>
  <?php $name = h($user['name']);?>
  <?php $company = h($user['company']);?>
  <div class="modal hide" id="<?php echo 'myModal_delete_' . $id;?>">
    <div class="modal-header">
      <div>ユーザーの削除</div>
      <button class="close" data-dismiss="modal">×</button>
      <br />
    </div>
  
    <div class="modal-body">
      <dl><dt>会社名</dt><dd><?php echo $company;?></dd><dl>
      <dl><dt>お名前</dt><dd><?php echo $name;?></dd><dl>
      <br />
      <form class="form" action="<?php echo $deleteAction . "/" . $id;?>" METHOD="POST">
        <input id="user_id" type="hidden" name="id" value="<?php echo $id;?>"/>
        <input type="submit" name='delete' class='btn btn-warning' value="削除" />
      </form>
    </div>
  
    <div class="modal-footer">
      <a href="#" class="btn" data-dismiss="modal">キャンセル</a>
    </div>
  </div>
<?php } ?>

<div class="user-control">
<?php
	echo $this->Html->link('【御社情報】のエクスポート',
		 '/Users/DownloadAllCsv',
		 array('class'=>'btn btn-primary')
	);
	echo '<br />';
//	echo $this->Html->link('【店名確認シート】のエクスポート',
//		 '/Users/DownloadSignboardsCsv',
//		 array('class'=>'btn btn-primary')
//	);
?>

<script type="text/javascript">
$("#paginator_bar").css("display","none"); // ページネーション・バーを隱す

function turn_link_off(){
    // テーブルの6,7,8桁目（フォーム）のセルを折り返し無しにする
    //$("td:eq(4)").attr("nowrap", true);
    //$("td:eq(5)").attr("nowrap", true);
    //$("td:eq(6)").attr("nowrap", true);

    // PHP変数を取得
    var edit_mode = "<?php echo ($edit_mode)? 1 : 0; ?>";
    var role      = "<?php echo $userinfo['role']; ?>";
    var read_permission  = "<?php echo @implode(',',$userinfo['read_permission']); ?>";
    var write_permission = "<?php echo @implode(',',$userinfo['write_permission']); ?>";
    var writeArray = write_permission.split(','); // 書きこみ許可配列
    var readArray = read_permission.split(','); // 読み出し許可配列
    // ユーザーリスト画面の各シートへの閲覧リンクを制限（管理者以外）
    if( edit_mode =='0' && role !='admin'){
        $("tr td div").each(function(){
            var this_class = $(this).attr('class');
            if( jQuery.inArray( this_class, readArray ) < 0  // 閲覧できない
             && jQuery.inArray( this_class, writeArray ) < 0 ){ // 編集できない
                var title = $("a",this).text();
                $("a",this).parent().html( title ); // リンクを外す
            }
        });
    };

    // ユーザー管理画面の各シートへの編集リンクを制限（管理者以外）
    if( edit_mode == '1' && role !='admin'){
        $("tr td div").each(function(){
            var this_class = $(this).attr('class');
            if( jQuery.inArray( this_class, writeArray ) < 0 ){ // 編集できない
                var title = $("a",this).text();
                $("a",this).parent().html( title ); // リンクを外す
            }
        });
    };

    // 機能制限（管理者、暫定）
    if( edit_mode == '1'){
        $("tr td div").each(function(){
            var this_class = $(this).attr('class');
            if(   this_class != 'users_last_shop'
               && this_class != 'users_edit'
               && this_class != 'users_receipt'
               && this_class != 'users_terminal'
               && this_class != 'hearings'
               && this_class != 'sections'
               && this_class != 'signboards' ){ // これら以外は、
                var title = $("a",this).text();
                $("a",this).parent().html( title ); // リンクを外す
            }
        });
    };
}

//$(function() {
$(window).load(function () {
   turn_link_off();
});
</script>

<script type="text/javascript">
// AJAX 実行順番を保証するキュー
function doOrderGuaranteedAjax(ajaxOptionArray, allCompleteHandler){
    var defaults = {
        complete : function() {
            ajaxOptionArray.shift();
            if (ajaxOptionArray.length == 0 ) {
                if (allCompleteHandler) {
                    allCompleteHandler();
                }
            } else {
                option = ajaxOptionArray[0];
                opts = $.extend({}, defaults, option);
                $.ajax(opts);
            };
        }
    };

    var option = ajaxOptionArray[0];
    var opts = $.extend({}, defaults, option);
    $.ajax(opts);
};

$( function() {
	var ajax_queue = []; // キュー
	var page_num = 1;
	var page_total = $("span.paginator").size(); // 全ページ数
	var progress = 0;
	$("span.paginator").each(function(){
		if( $(this).hasClass('current')){
				console.log( $(this).html());
		}else{
				var page = $("a",this).text();
				var html_path = $("#html_path").text() + '/page:' + page;
				ajax_queue.push({ // キューに入れる
					type: 'GET',
					url: html_path,
					dataType: 'html',
					success: function(data) { // 成功！
						var table_tr = $("table tr.user", data);
						$('table.admin_list').append( table_tr );
						turn_link_off();
						progress = ++page_num / page_total * 100;
						if( progress == 100 ){
							$('#progress').text( progress + '%');
							$('#progress').css("display","none");
						}else{
							$('#progress').text('読込中 ' + progress + '% ...');
						}
					},
					//error:function() {
					//	alert('問題がありました。:' + html_path);
					//}
				});
//				$.ajax({
//					type: 'GET',
//					url: html_path,
//					dataType: 'html',
//					success: function(data) { // 成功！
//						var table_tr = $("table tr.user", data);
//						$('table.admin_list').append( table_tr );
//					},
//					error:function() {
//						alert('問題がありました。:' + html_path);
//					}
//				});
		}
	});
	doOrderGuaranteedAjax( ajax_queue ); // キューにはいった ajax を実行
});
</script>
