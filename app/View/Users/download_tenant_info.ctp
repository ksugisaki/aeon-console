<?php
// 項目設定
    $th[] = '店名（日本語）'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_sign_jpn';
    $th[] = '店名（英語）'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_sign_eng';
    $th[] = 'メインの標記方法'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_sign_main_type';
    $th[] = '分類1'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_category1';
    $th[] = '分類2'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_category2';
    $th[] = '分類3'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_category3';
    $th[] = '分類4'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_category4';
    $th[] = '分類5'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_category5';
    $th[] = '詳細業種'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_detail';
    $th[] = '備考'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_remarks';
    $th[] = 'PRポイント'; /* ヒアリングシート2 */
    $models[] = 'OptionHearing';
    $fields[] = 'haikei';
    $th[] = 'キャッチコピー'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_copy';
    $th[] = 'SHOPコンセプト'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_concept';
    $th[] = '出店状況'; /* ヒアリングシート ※項目（日本初・新業態・中部初出店・三重県初出店）がわかるように表示してください。 */
    $models[] = 'Hearing';
    $fields[] = 'syutten_jyoukyou';
    $th[] = '媒体用画像'; /* ヒアリングシート2 ※登録しているかどうかわかるようにしてください。 */
    $models[] = 'OptionHearing';
    $fields[] = 'image1_file_name';
    $th[] = '媒体用画像'; /* ヒアリングシート2 ※登録しているかどうかわかるようにしてください。 */
    $models[] = 'OptionHearing';
    $fields[] = 'image2_file_name';
    $th[] = '媒体用画像'; /* ヒアリングシート2 ※登録しているかどうかわかるようにしてください。 */
    $models[] = 'OptionHearing';
    $fields[] = 'image3_file_name';
    $th[] = '店舗ロゴ'; /* ヒアリングシート2 ※登録しているかどうかわかるようにしてください。 */
    $models[] = 'OptionHearing';
    $fields[] = 'logo_file_name';
    $th[] = '店舗ロゴ'; /* ヒアリングシート2 ※登録しているかどうかわかるようにしてください。 */
    $models[] = 'OptionHearing';
    $fields[] = 'logo2_file_name';
    $th[] = 'テナント電話番号'; /* 店名確認シート */
    $models[] = 'Signboard';
    $fields[] = 'shop_phone';
    $th[] = '座席数'; /* ヒアリングシート2 */
    $models[] = 'OptionHearing';
    $fields[] = 'food_comment2';
    $th[] = '喫煙・禁煙'; /* ヒアリングシート2 ※項目（喫煙・全面禁煙　等）がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_comment3';
    $th[] = '予約受付について'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_comment5';
    $th[] = '予約方法'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_yoyaku_tel';
    $th[] = '予約方法'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_yoyaku_fax';
    $th[] = '予約方法'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_yoyaku_hp';
    $th[] = '予約方法'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_yoyaku_etc';
    $th[] = 'ラストオーダー時間'; /* ヒアリングシート2 */
    $models[] = 'OptionHearing';
    $fields[] = 'food_comment1';
    $th[] = 'テイクアウトの可否'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_takeout';
    $th[] = 'アルコール扱い'; /* ヒアリングシート2　※ 目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_alcohol_ok';
    $th[] = 'キッズ用椅子の有無'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_childseat';
    $th[] = 'キッズメニューの有無'; /* ヒアリングシート2 ※項目がわかるように表示してください */
    $models[] = 'OptionHearing';
    $fields[] = 'food_kidsmenu';

    // 定数設定
    if( SUB_DOMAIN == 'testsite' || SUB_DOMAIN == 'toin' ){
        $syutten_jyoukyou_label = array(
            '日本初出店',
            '新業態',
            '中部（愛知、三重、岐阜）初出店',
            '三重県初出店'
        );
    }else{
        $syutten_jyoukyou_label = array(
            '日本初出店',
            '新業態',
            '関東（千葉県・埼玉県・神奈川県・東京都）初出店',
            '千葉県初出店'
        );
    }
    // 定数設定
    $category_options = array();
    $category_options[0] = array(
                               'レディス',
                               'メンズ',
                               'キッズ',
                               'メンズ・レディス',
                               'レディス・キッズ',
                               'ファミリー',
                               '靴・鞄',
                               'アクセサリー'
    );
    $category_options[1] = array(
        'カフェ',
        'フードコート',
        '食料品'
    );
    $category_options[2] = array(
        '書籍',
        '音楽・楽器',
        'アミューズメント'
    );
    $category_options[3] = array(
        'インテリア',
        'キッチン',
        'スポーツ',
        'おもちゃ',
        '文具',
        '眼鏡・コンタクト',
        'ヘルス＆ビューティー'
    );
    $category_options[4] = array(
        'サービス',
        '銀行',
        '保険',
        '趣味',
        '病院',
        'ヘルス＆ビューティー',
        'スクール'
    );

    foreach( $users as $s_key => $user ){
        // Signboardの重複しているデータを除く
        if( isset( $user['Signboard'])){
            $all_signboards = $user['Signboard'];
            $signboard_last = array();
            $prev_modified = 0;
            foreach( $all_signboards as $signboard){
                if( $prev_modified == 0 )
                    $signboard_last = $signboard;
                else if( $signboard['modified'] > $prev_modified ) // 新しいなら
                    $signboard_last = $signboard; // 上書き
                $prev_modified = $signboard['modified'];
            }
        }
        // idの桁には、店名を入れて表示する
        if( isset( $signboard_last['shop_sign']))
            $user['User']['id'] = $signboard_last['shop_sign'];
        else
            $user['User']['id'] = '';

        // カテゴリー１〜５のチェックボックスを文字列にする
        if( isset( $signboard_last['shop_category1'])){
          for( $cnum = 0; $cnum < 5; $cnum++ ){
            $array = @unserialize( $signboard_last['shop_category' . ($cnum +1)]);
            $label = $category_options[ $cnum ];
            $text = '';
            foreach( $array as $key => $value ){
              $text .= $label[ $value ] ."\n";
            }
            $signboard_last['shop_category' . ($cnum +1)] = $text;
          }
        }
        // 出店状況を文字列にする
        $array = @unserialize( $user['Hearing'][0]['syutten_jyoukyou']);
        $text = '';
        foreach( $array as $key => $value ){
            $text .= $syutten_jyoukyou_label[ $value ] . "\n";
        }
        $user['Hearing'][0]['syutten_jyoukyou']= $text;

		// メインの標記方法を文字にする
		$main = $signboard_last['shop_sign_main_type'];
		if( $main == '1' ){
			$signboard_last['shop_sign_main_type'] = '英語標記';
		}else{
			$signboard_last['shop_sign_main_type'] = '日本語標記';
		}

		// 予約方法を文字にする
		$food_yoyaku_tel = ($user['OptionHearing'][0]['food_yoyaku_tel']=='1')? '電話' : '';
		$food_yoyaku_fax = ($user['OptionHearing'][0]['food_yoyaku_fax']=='1')? 'FAX' : '';
		$food_yoyaku_hp  = ($user['OptionHearing'][0]['food_yoyaku_hp']=='1')? 'HP' : '';
		$food_yoyaku_etc = ($user['OptionHearing'][0]['food_yoyaku_etc']=='1')? 'その他' : '';
        $user['OptionHearing'][0]['food_yoyaku_tel'] = $food_yoyaku_tel;
        $user['OptionHearing'][0]['food_yoyaku_fax'] = $food_yoyaku_fax;
        $user['OptionHearing'][0]['food_yoyaku_hp']  = $food_yoyaku_hp;
        $user['OptionHearing'][0]['food_yoyaku_etc'] = $food_yoyaku_etc;

        // $user['User'] エクスポート項目をまとめる
        foreach( $models as $key => $model ){
            $field = $fields[$key];
            if( $model=='Signboard' ){
                if( isset( $signboard_last[$field])){
                    $user['User'][$field] = $signboard_last[$field];
                }else{
                    $user['User'][$field] = '';
                }
            }else{
                if( isset( $user[$model][0][$field])){
                    $user['User'][$field] = $user[$model][0][$field];
                }else{
                    $user['User'][$field] = '';
                }
            }
        }
        $td[] = $user['User'];

//if($s_key == 10){
//debug($td);
//debug($user);
//exit();
//}
    }


    //ファイル名設定
    $this->Csv->setFilename( $filename ); 

    //ヘッダー行を追加
    $this->Csv->addRow( $th );

    //表の各行を追加していく
    foreach( $td as $t ){
        $this->Csv->addRow( $t );
    }

    //文字コードをUTF-8からSJISに変換して出力。
    echo mb_convert_encoding( $this->Csv->render(), 'SJIS', 'UTF-8');

