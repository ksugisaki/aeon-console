<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>
        <?php echo __(SITE_NAME); ?> | 
        <?php echo $title_for_layout; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="<?php echo $this->Html->url('/') ?>js/jquery.min.js"></script>
    <!-- TwitterBootstrap -->
    <?php echo $this->Html->css('bootstrap.min'); ?>
	<?php echo $this->Html->css('bootstrap_for_aeon'); ?>
    <?php // echo $this->Html->css('bootstrap-responsive.min'); // レスポンシブ ?>
    <?php echo $this->Html->script('bootstrap.min'); ?>
	<?php echo $this->Html->script('jquery.cookie'); ?>

    <script src="<?php echo $this->Html->url('/') ?>js/tiny_mce/jquery.tinymce.js"></script>
    <script src="<?php echo $this->Html->url('/') ?>js/tiny_mce/tiny_mce.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo $this->Html->url('/js') ?>html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <!--
    <link rel="shortcut icon" href="/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">
    -->
    <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
    ?>

    <?php echo $this->Html->css('style.css');?>
	<?php echo $this->fetch('script');?>

</head>
<body>


<div class="container">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
</div> <!-- /container -->

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- 
<?php // echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->fetch('script'); ?>
<?php echo $this->element('sql_dump'); ?>
-->

<div><?php
        if( false ){
            if( empty( $this->Session )){
                echo '<p>********  セッション変数なし ********</p>';
            }else{
                echo '<p>******** セッション変数を表示 ********</p>';
                echo '<table>';
                foreach( $this->Session->read() as $key=>$val ){
                 echo '<tr><td>'. $key .'</td><td>'. $val .'</td></tr>';
                }
                echo '<table><br />';
            }
        }
?></div>

<div><?php
        if( false ){
            if( ! $this->Session->check('Auth.User') ){
                echo '<p>********  セッション変数 Auth.User なし ********</p>';
            }else{
                echo '<p>******** セッション変数 Auth.User を表示 ********</p>';
                echo '<table>';
                foreach( $this->Session->read('Auth.User') as $key=>$val ){
                 echo '<tr><td>'. $key .'</td><td>'. $val .'</td></tr>';
                }
                echo '<table><br />';
            }
        }
?></div>
</body>

<script type="text/javascript">
  $("textarea").css("height", "200px");

  // プリントモードでは、ボタンなどは隱す
  $("button").hide();
  $(".btn").hide();
  $(".modal").hide();
  
  $(function() {
    // プリントモードでは、エンターによる登録を出来なくする。
    $('form').keypress(function(event) {
        if (event.keyCode == 13) {
            return false;
        }
    });

    $(".close").hide();
  });
</script>
</html>
