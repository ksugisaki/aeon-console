<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>
        <?php echo __(SITE_NAME); ?> | 
        <?php echo $title_for_layout; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="<?php echo $this->Html->url('/') ?>js/jquery.min.js"></script>
    <!-- TwitterBootstrap -->
    <?php echo $this->Html->css('bootstrap.min'); ?>
	<?php echo $this->Html->css('bootstrap_for_aeon'); ?>
    <?php // echo $this->Html->css('bootstrap-responsive.min'); // レスポンシブ ?>
    <?php echo $this->Html->script('bootstrap.min'); ?>
	<?php echo $this->Html->script('jquery.cookie'); ?>

    <script src="<?php echo $this->Html->url('/') ?>js/tiny_mce/jquery.tinymce.js"></script>
    <script src="<?php echo $this->Html->url('/') ?>js/tiny_mce/tiny_mce.js"></script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="<?php echo $this->Html->url('/js') ?>html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <!--
    <link rel="shortcut icon" href="/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/ico/apple-touch-icon-57-precomposed.png">
    -->
    <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
    ?>

    <?php echo $this->Html->css('style.css?ver=0914');?>
	<?php echo $this->fetch('script');?>

</head>
<body>

<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="<?php echo h($this->Html->url('/', true)) ?>">
        <?php echo __(SITE_NAME); ?>
      </a>
      <ul class="nav">
      <?php if( false ): ?>
        <li><a href="<?php echo h($this->Html->url('/payment/menu',true)) ?>">Service Menu</a></li>
      <?php endif; ?>
      <?php if( false && $this->Session->check('Auth.User') && $this->Session->read('Auth.User.role')==''){ ?>
        <li><a href="<?php echo h($this->Html->url('/users/terminal',  true)) ?>">入力用端末申込書</a></li>
        <li><a href="<?php echo h($this->Html->url('/users/edit',  true)) ?>">御社情報</a></li>
        <li><a href="<?php echo h($this->Html->url('/hearings/index',  true)) ?>">ヒアリングシート</a></li>
      <?php } ?>
      <?php if(( $this->Session->read('Auth.User.role') == 'admin' )
            ||(  $this->Session->read('Auth.User.role') == 'partner' )){
          echo '<li>';
          echo $this->Html->link('ユーザーリスト', $this->Html->url('/users/AdminList', true));
          echo '</li>';

        if( defined('ADMIN_EDIT_PAGE')):
          echo '<li>';
          echo $this->Html->link('ユーザー管理', $this->Html->url('/users/AdminEdit', true));
          echo '</li>';
        endif;

          echo '<li>';
          echo $this->Html->link('データ管理', $this->Html->url('/users/AdminIndex', true));
          echo '</li>';
      } ?>
      <?php if( $this->Session->read('Auth.User.role') == 'admin' ){
          echo '<li>';
          echo $this->Html->link('管理設定', $this->Html->url('/Administrations', true));
          echo '</li>';
      } ?>
      </ul>
      <?php if( $this->Session->check('Auth.User') ){ ?>
      <ul class="nav pull-right">
        <li><a href="<?php echo h($this->Html->url('/users/logout', true)) ?>">ログアウト</a></li>
      </ul>
      <?php }else{ ?>
      <ul class="nav pull-right">
        <?php if( SUB_DOMAIN !='wakayama'): // 和歌山は、新規登録はなし ?>
        <li><a href="<?php echo h($this->Html->url('/users/agreement', true)) ?>">新規登録</a></li>
        <?php endif; ?>
        <li><a href="<?php echo h($this->Html->url('/users/login', true)) ?>">ログイン</a></li>
      </ul>
      <?php } ?>
    </div>
  </div>
</div>

<div class="container">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
</div> <!-- /container -->

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- 
<?php // echo $this->Html->script('bootstrap.min'); ?>
<?php echo $this->fetch('script'); ?>
<?php echo $this->element('sql_dump'); ?>
-->

<div><?php
        if( false ){
            if( empty( $this->Session )){
                echo '<p>********  セッション変数なし ********</p>';
            }else{
                echo '<p>******** セッション変数を表示 ********</p>';
                echo '<table>';
                foreach( $this->Session->read() as $key=>$val ){
                 echo '<tr><td>'. $key .'</td><td>'. $val .'</td></tr>';
                }
                echo '<table><br />';
            }
        }
?></div>

<div><?php
        if( false ){
            if( ! $this->Session->check('Auth.User') ){
                echo '<p>********  セッション変数 Auth.User なし ********</p>';
            }else{
                echo '<p>******** セッション変数 Auth.User を表示 ********</p>';
                echo '<table>';
                foreach( $this->Session->read('Auth.User') as $key=>$val ){
                 echo '<tr><td>'. $key .'</td><td>'. $val .'</td></tr>';
                }
                echo '<table><br />';
            }
        }
?></div>
<hr />
<div class="container">
    <div class="span12">
    <form action="https://www.login.secomtrust.net/customer/customer/pfw/CertificationPage.do" name="CertificationPageForm" method="post" target="_blank">
    <input type="image" border="0" name="Sticker" src="<?php echo $this->Html->url('/img/B0452513.gif') ?>" alt="クリックして証明書の内容をご確認ください。" oncontextmenu="return false;">
    <input type="hidden" name="Req_ID" value="9231724640">
    </form>
    </div>
</div>
</body>

<!-- alertの右側に×印を見せなくする
<script type="text/javascript">
$(function() {
  $(".close").hide();
});
</script>
 -->
</html>
