<?php
mb_internal_encoding('UTF-8');
require_once('Mail' . DS . 'mimeDecode.php');
class ReceiverShell extends Shell {

	var $uses = array('User', 'Content');
	function testing() {
		$stdin=$this->Dispatch->stdin;
		while( !feof($stdin) ){
			$mail .= fgets($stdin,4096);
		}
		$this->log($mail, LOG_DEBUG);
	}
	
	function receiveMail() {
		$this->log('HOGEHOGE', LOG_DEBUG);
		$this->log(PERMIT_FILETYPES, LOG_DEBUG);
		$mail = "";
		$image_filename = "";
		$from = array();
		$headers = array();

		// 標準入力から受け取ったメールを取得
		$stdin=$this->Dispatch->stdin;
		while( !feof($stdin) ){
			$mail .= fgets($stdin,4096);
		}
	    
		// デコード方法の指定
		$params['include_bodies'] = true;
		$params['decode_bodies']  = true;
		$params['decode_headers'] = true;
		// デコード処理
		$decoder = new Mail_mimeDecode($mail);
		$decoded = $decoder->decode($params);

		$temp_file = false;
		// 添付ファイルの有無で処理を分岐
		if ( !empty($decoded->parts) ) {
			// 添付ファイルしか存在しないケースがある
			// bodyを取得
			// $body = $decoded->parts[0]->body;
			//　添付ファイルの数だけループ
			$this->log('TEMP START', LOG_DEBUG);
			for($idx = 0; $idx < count($decoded->parts); $idx++) {
				if ($decoded->parts[$idx]->ctype_primary == 'text'){
					$body = $decoded->parts[$idx]->body;
				} else {
					$file = $decoded->parts[$idx]->body;
					$filetype = $decoded->parts[$idx]->ctype_secondary;
					$filename = $decoded->parts[$idx]->ctype_parameters["name"];
					preg_match("/.+\.([\w\d]{2,4})$/", $filename, $matches);
					$filetype = $matches[1];
					
					$this->log($filename, LOG_DEBUG);
					$this->log($filetype, LOG_DEBUG);

					/**********************************************************
					 * ファイルタイプのチェックしたり、指定したディレクトリに *
					 * 添付ファイルを保存したりの処理をここでする             *
					 **********************************************************/
					if (preg_match(PERMIT_FILETYPES, $filetype)){
						// 保存する
						$temp_file = $file;
						if ($filetype == 'jpeg') $filetype = 'jpg';
						$temp_filetype = $filetype;
						$this->log('OK: ' . $filetype, LOG_DEBUG);
					} else {
						$this->log('NG: ' . $filetype, LOG_DEBUG);
					}
				}
			}
		} else {
			// 添付ファイルが無い場合はbodyを取得するだけ
			$body = $decoded->body;
		}
		// ヘッダ部分を取得して漢字コードを変換する
		$headers = $decoded->headers;
		$to = $headers["to"];
		$this->log('before TO: ' . $to, LOG_DEBUG);
		if (preg_match('/\s*\<(.+)\>$/', $to, $to_matches)){
			$to = $to_matches[1];
		} 
		$this->log('after TO: ' . $to, LOG_DEBUG);

		// TOに特定の文字列が含まれていなかった場合は処理を終了する
		if (! preg_match("/^(al|us)/", $to)){
			return false;
			exit;
		}
		// $from_text = // mb_convert_encoding($headers['from'], mb_internal_encoding(),'ISO-2022-JP');
		$from = $headers["from"];
		$this->log('before FROM: ' . $from, LOG_DEBUG);
		// ereg("[0-9a-zA-Z_\.\-]+@[0-9a-zA-Z_\.\-]+",$from_text,$from);
		if (preg_match('/\s*\<(.+)\>$/', $from, $matches)){
			$from = $matches[1];
		} 

		$this->log('after FROM: ' . $from, LOG_DEBUG);
		// FromがNGリストの場合はエラー
		$ngemail = $this->NgEmail->find('first', array('conditions' => array(
			'email' => $from
		)));
		if ($ngemail){
			$this->log('NG - FROM MAIL', LOG_DEBUG);
			return false;
			exit;
		}
		$this->log('OK - FROM MAIL', LOG_DEBUG);


		$subject = mb_convert_encoding($headers['subject'], mb_internal_encoding(),'ISO-2022-JP');
		$body = mb_convert_encoding($body, mb_internal_encoding(),'ISO-2022-JP');
		/**********************************************************
		 * DBに格納したり、メールを送り返したりの処理をここでする *
		 **********************************************************/

		$to = preg_split('/[-@]/', $to);

		// メールアドレス変更の場合
		// us : USer
		if ($to[0] === 'us'){
			$key_url = $to[1];
			$user = $this->User->findByKeyUrl($key_url);
			if ($user){
				// 変更はまだ行わない
				// TmpEmailUserに情報を書き込むのみ
				$user["User"]["email"] = $from;
				$this->TmpEmailUser->save(array('TmpEmailUser' => array(
					'user_id' => $user["User"]["id"],
					'old_email' => $user["User"]["email"],
					'new_email' => $from,
					'key_url' => uniqid('te')
				)));
				$id = $this->TmpEmailUser->getLastInsertId();
				$tmp = $this->TmpEmailUser->read(null, $id);
				$this->Mail->sendEmailChangeMail($from, $tmp);
			}
		} else {

			// temp_fileがマッチしない場合は終了する
			if ($temp_file === false) exit;
			// temp_fileをtempへ保存
			$tmp_filename = $this->params["root"] . '/app/tmp/mail/' . uniqid() . '.' . $temp_filetype;
			$fp = fopen($tmp_filename, 'w+');
			$size = fwrite($fp, $temp_file);
			fclose($fp);
			chmod($tmp_filename, 0777);

			// アルバム処理
			$post_id = null;
			if ($to[0] === 'al'){
				if ($to[1] === 'new'){
					// 新規アルバム
					$data = array(
						'User' => array(
							'email' => $from
						),
						'Post' => array(
							'filename' => array(
								'name'     => $tmp_filename,
								'tmp_name' => $tmp_filename,
							),
							'title'    => $subject,
							'comment'  => $body
						),
					);
				} elseif ($to[1] === 'add'){
					// 新規アルバム
					$data = array(
						'User' => array(
							'email' => $from
						),
						'Post' => array(
							'filename' => array(
								'name'     => $tmp_filename,
								'tmp_name' => $tmp_filename,
							),
							'title'    => $subject,
							'comment'  => $body
						),
						'Album_New' => true
					);
				} else {
					// 既存アルバム
					$data = array(
						'User' => array(
							'email' => $from
						),
						'Post' => array(
							'filename' => array(
								'name'     => $tmp_filename,
								'tmp_name' => $tmp_filename,
							),
							'title'   => $subject,
							'comment' => $body
						),
						'Album' => array(
							'key_url' => $to[1]
						)
					);
				}
				$post_id = $this->User->initialSave($data);
			}
			if (is_numeric($post_id)) {
				$this->Post->recursive = 0;
				$post = $this->Post->read(null, $post_id);
				$this->Mail->completeUploadMail($from, $post);
			} else {
				// アップロードに失敗 ($post_id には エラーメッセージ)
				$this->Mail->sendEmail("photo@4919.jp", $post_id, print_r($data, true), "system@up-life.com");
			}
		}
	}
	
}
?>

