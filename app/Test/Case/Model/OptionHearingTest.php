<?php
App::uses('OptionHearing', 'Model');

/**
 * OptionHearing Test Case
 *
 */
class OptionHearingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.option_hearing',
		'app.user',
		'app.mail_address',
		'app.history',
		'app.section',
		'app.hearing',
		'app.signboard'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OptionHearing = ClassRegistry::init('OptionHearing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OptionHearing);

		parent::tearDown();
	}

}
