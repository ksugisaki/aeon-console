<?php
App::uses('OptionHearingsController', 'Controller');

/**
 * OptionHearingsController Test Case
 *
 */
class OptionHearingsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.option_hearing',
		'app.user',
		'app.mail_address',
		'app.history',
		'app.section',
		'app.hearing',
		'app.signboard'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
